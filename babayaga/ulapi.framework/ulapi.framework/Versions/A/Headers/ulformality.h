/**
 * @file ulformality.h
 * @brief The interface for the ULFormality class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULFORMALITY_H
#define ULFORMALITY_H

#include "ulenum.h"

/**
 * @class ULFormality
 * @brief An enhanced enumerated type used to represent formality levels (e.g. Sie vs. du).
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULFormality : public ULEnum
{
public:
    static const ULFormality& None;
    static const ULFormality& Any;
    static const ULFormality& Formal;
    static const ULFormality& Informal;


    ULFormality();
    ULFormality(const ULFormality& other);
    ULFormality(int id);
    ULFormality(const char *otherStringID);
    virtual ~ULFormality();

    virtual void clear();
    ULFormality& operator=(const ULFormality& other);
    ULFormality& operator=(int otherID);
    ULFormality& operator=(const char *otherStringID);

    bool operator==(const ULFormality& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULFormality& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULFormality& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULFormality& getInstance(int id);



private:
    static int nEnumeratedValues;
    static ULVector<const ULFormality *> enumeratedValueVector;
    ULFormality(const char *stringID, const char *displayString);
};


#endif

