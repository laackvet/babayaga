/**
 * @file ulutilities.h
 * @brief The interface for the ULUtilities class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULUTILITIES_H
#define ULUTILITIES_H

#include "ulstring.h"
#include "ulcontainers.h"

/**
 * @class ULUtilities
 * @brief A collection of static functions that perform various reading and
 * writing operations with big-endian integers, byte strings, and "base 255"
 * integers.  These functions are used by low-level ULAPI kernel classes
 * (e.g. ULSegmentedFile), but may have applications elsewhere.
 *
 * If you need to store an arbitrary integer in a form that involves no
 * null bytes, you can use a representation we call "base 255".  The idea
 * is that each byte represents a "digit" between 0 (represented in binary
 * as 00000001) and 254 (represented as 11111111).  Then you combine
 * four such "digits" into a single base-255 number, none of whose
 * bytes is represented as 00000000.  This can be handy if you have
 * a buffer or file with null-terminated records containing integers.
 * With base-255 integers, there's no ambiguity about what a 0-byte means.
 */
class ULUtilities
{
public:
    // Reading and writing big-endian integers from buffers.
    static uluint16 readInt16FromBuffer(const uluint8 *buffer);
    static uluint32 readInt24FromBuffer(const uluint8 *buffer);
    static uluint32 readInt32FromBuffer(const uluint8 *buffer);
    static uluint8 *writeInt16ToBuffer(uluint8 *buffer, uluint32 n);
    static uluint8 *writeInt24ToBuffer(uluint8 *buffer, uluint32 n);
    static uluint8 *writeInt32ToBuffer(uluint8 *buffer, uluint32 n);

    // Converting between normal (typically two's complement) and
    // base-255 representations of integers.
    static uluint32 intToBase255(uluint32 n);
    static uluint32 base255ToInt(uluint32 n);

#if defined(UL_ANSI)
    // Reading and writing big-endian integers from and to iostreams.
    static void writeInt16(ostream& out, uluint32 n);
    static void writeInt24(ostream& out, uluint32 n);
    static void writeInt32(ostream& out, uluint32 n);

    static void readInt16(istream& in, uluint16& n);
    static void readInt24(istream& in, uluint32& n);
    static void readInt32(istream& in, uluint32& n);
#endif
};

#endif
