/**
 * @file ulforesttype.h
 * @brief The interface for the ULForestType class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULFORESTTYPE_H
#define ULFORESTTYPE_H

#include "ulenum.h"

/**
 * @class ULForestType
 * @brief An enhanced enumerated type used to represent the types of dictionary
 * data forests that can occur in dictionary data sources.
 * 
 * For example, Ultralingua-owned data is organized into a forest of trees,
 * each of whose root nodes represents one "term" (typically a word or phrase plus
 * a part of speech), with child nodes for definitions/translations, example phrases,
 * semantic or sociolinguistic annotations, etc. Then there's Collins data that
 * originates in "CXML" form, or Vox "tagged" data, etc.
 * 
 * ULForestType objects are used to request the right child of ULForester when you
 * want to manipulate the data in a dictionary data source.
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULForestType : public ULEnum
{
public:
    static const ULForestType& None;
    static const ULForestType& Any;
    static const ULForestType& Standard;
    static const ULForestType& CollinsXML;
    static const ULForestType& CollinsTagged;
    static const ULForestType& CollinsRobert;
    static const ULForestType& VoxXML;
    static const ULForestType& VoxTagged;


    ULForestType();
    ULForestType(const ULForestType& other);
    ULForestType(int id);
    ULForestType(const char *otherStringID);
    virtual ~ULForestType();

    virtual void clear();
    ULForestType& operator=(const ULForestType& other);
    ULForestType& operator=(int otherID);
    ULForestType& operator=(const char *otherStringID);

    bool operator==(const ULForestType& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULForestType& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULForestType& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULForestType& getInstance(int id);
    bool isCollins() const;



private:
    static int nEnumeratedValues;
    static ULVector<const ULForestType *> enumeratedValueVector;
    ULForestType(const char *stringID, const char *displayString);
};


#endif

