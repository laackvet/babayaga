/**
 * @file ullanguagedatasource.h
 * @brief The interface for the ULLanguageDataSource class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULLANGUAGEDATASOURCE_H
#define ULLANGUAGEDATASOURCE_H

#include "uldatasource.h"
#include "ulverbmodel.h"
#include "ulinflectionrule.h"
#include "ultaggingrule.h"
#include "ulfrequency.h"

/**
 * @class ULLanguageDataSource
 * @brief ULLanguageDataSource is the abstract parent for classes that
 * interface with single-language data stored somewhere like a .ulc file
 * or a database.
 *
 * <em>Warning</em>: If you find yourself thinking about directly using one
 * of the subclasses of this class, you should reconsider. It is much easier
 * to use ULAPI's data sources correctly by working with a ULFactory and the
 * associated higher-level tools such as ULConjugator or ULStemmer, which take
 * care of the initialization and manipulation of the data sources for you.
 */
class ULLanguageDataSource : public ULDataSource
{
public:
    virtual ~ULLanguageDataSource() {}

    // Required ULDataSource interfaces.
    virtual ULError attach(const ULString& dataSourceIdentifier) = 0;
    virtual ULError detach() = 0;
    virtual ULError load() = 0;
    virtual ULError close() = 0;
    virtual ULString getDataSourceIdentifier() = 0;
    virtual ULDataSourceVersion getVersion() = 0;

    // Extracting data from the language data source.

    /**
     * @return the language for which this data source provides data.
     */
    virtual const ULLanguage& getLanguage() = 0;

    /**
     * Finds all the words in this data source whose roots (infinitive for verbs, singular
     * for nouns and adjectives, etc.) match the specified root (in an accent- and
     * case-insensitive way) 
     *
     * @return ULError::NoError, ULError::NoMatch, or any of the error codes
     * related to failure to open or attach to a data source.
     * @param[in] root The root form of the desired words.
     * @param[out] wordList The matching words.
     */
    virtual ULError getWords(const ULString& root, ULList<ULDerivation>& wordList, bool filterResults=false) = 0;

    /**
     * Finds all the verbs in this data source whose infinitives match (in an accent-
     * and case-insensitive way) the specified infinitive.
     *
     * @return ULError::NoError, ULError::NoMatch, or any of the error codes
     * related to failure to open or attach to a data source.
     * @param[in] infinitive the infinitive of the desired verbs.
     * @param[out] verbList the matching verbs.
     */
    virtual ULError getVerbs(const ULString& infinitive, ULList<ULDerivation>& verbList, bool filterResults=false) = 0;

    /**
     * Finds all the nouns in this data source whose root (typically singular) forms match
     * (in an accent- and case-insensitive way) the specified text.
     *
     * @return ULError::NoError, ULError::NoMatch, or any of the error codes
     * related to failure to open or attach to a data source.
     * @param[in] text the search string.
     * @param[out] nounList the matching nouns.
     */
    virtual ULError getNouns(const ULString& text, ULList<ULDerivation>& nounList) = 0;

    /**
     * Gets the verb model associated with the specified ID.
     *
     * @return ULError::NoError if the operation succeeds, ULError::InvalidID
     * if the specified verb class ID is invalid, or an error associated with the
     * failure to open or attach to a data source.
     * @param[in] verbClassID The ID of the desired verb model.
     * @param[out] model The verb model.
     */
    virtual ULError getVerbModel(uluint32 verbClassID, ULVerbModel& model) = 0;

    /**
     * Gets the list of root words contained in this data source that
     * match (accent- and case-insensitively) the specified prefix. For example,
     * the prefix "spri" might (depending on the specific English data source)
     * yield a root list of "spring", "sprinkle", and "sprint" among verbs, and
     * "springy" etc. among adjectives. Typically, only verbs are returned by this
     * way for languages with simple noun and adjective inflection structures. But
     * for languages like Russian, German, and Latin, this method will typically return
     * verbs, nouns, and adjectives.
     *
     * @return ULError::NoError, ULError::NoMatch, or an error associated with the
     * failure to open or attach to a data source.
     * @param[in] prefix The prefix to match.
     * @param[in] maxMatches if non-zero, this is the maximum number of matches to return; if zero, the method
     * returns all matches (which can be a very long list if, for example, prefix is one letter)
     * @param[out] rootList The list of matching root words.
     */
    virtual ULError getMatchingRoots(const ULString& prefix, uluint32 maxMatches, ULList<ULString>& rootList) = 0;

    /**
     * Gets the list of nouns contained in this data source that match (accent- and
     * case-insensitively) the specified prefix.
     *
     * @return ULError::NoError, ULError::NoMatch, or an error associated with the
     * failure to open or attach to a data source.
     * @param[in] prefix The prefix to match.
     * @param[in] maxMatches if non-zero, this is the maximum number of matches to return; if zero, the method
     * returns all matches (which can be a very long list if, for example, prefix is one letter)
     * @param[out] nounList The list of matching nouns.
     */
    virtual ULError getMatchingNouns(const ULString& prefix, uluint32 maxMatches, ULList<ULString>& nounList) = 0;

    /**
     * Gets the list of verb infinitives contained in this data source that
     * match (accent- and case-insensitively) the specified prefix. For example,
     * the prefix "spri" might (depending on the specific English data source)
     * yield an infinitive list of "spring", "sprinkle", and "sprint".
     *
     * @return ULError::NoError, ULError::NoMatch, or an error associated with the
     * failure to open or attach to a data source.
     * @param[in] prefix The prefix to match.
     * @param[in] maxMatches if non-zero, this is the maximum number of matches to return; if zero, the method
     * returns all matches (which can be a very long list if, for example, prefix is one letter)
     * @param[out] infinitiveList The list of matching infinitives.
     */
    virtual ULError getMatchingInfinitives(const ULString& prefix, uluint32 maxMatches, ULList<ULString>& infinitiveList) = 0;

    virtual ULError getMatchingInfinitives(const ULString& prefix, uluint32 maxMatches, ULList<ULDerivation>& infinitiveList) = 0;

    /**
     * Finds the combinations of tense, number, person, and any other relevant features that are used
     * to specify a particular conjugated form for the specified verb. Though each type is stored in
     * a ULPartOfSpeech object, that object should be read as representing something like "future perfect
     * tense first person plural".
     *
     * For most verbs in a given language, the list of allowed verb form types is the same. But some verbs
     * are irregular or "defective" and have fewer or different types of conjugated forms. For example, depending
     * on which grammar you consult, the verb "snow" may not have first person forms ("I snow" doesn't really
     * make sense).
     *
     * The list of form types is sorted by the language's canonical tense ordering, then by number, then by person.
     *
     * This method should be used instead of the deprecated getTenses and getPersons methods.
     *
     * @return ULError::NoError, ULError::NoMatch, or an error associated with the
     * failure to open or attach to a data source.
     * @param[in] verb the verb whose form types are desired.
     * @param[out] verbFormTypes the admissible form types for the given verb.
     */
    virtual ULError getVerbFormTypes(const ULDerivation& verb, ULList<ULPartOfSpeech>& verbFormTypes) = 0;
    
    /**
     * Finds all the tenses available for the specified verb. For most verbs
     * in a given language, the list of tenses is the same. Occasionally there are
     * irregular or defective verbs that have a different collection of tenses.
     *
     * @return ULError::NoError, ULError::NoMatch, or an error associated with the
     * failure to open or attach to a data source.
     * @param[in] v The verb whose tenses are desired.
     * @param[out] tenseList The tenses.
     * @param[in] includeParticiples True if the tense list should include
     * participles (e.g. infinitive, past participle, present participle, gerund...).
     */
    virtual ULError getTenses(const ULDerivation& v, ULList<ULTense>& tenseList, bool includeParticiples=false) = 0;

    /**
     * Finds all the tenses available for the specified verb class. For most verb
     * classes in a given language, the list of tenses is the same. Occasionally
     * there are irregular or defective verb classes that have a different collection
     * of tenses.
     *
     * @return ULError::NoError, ULError::NoMatch, or an error associated with the
     * failure to open or attach to a data source.
     * @param[in] classID The class ID whose tenses are desired.
     * @param[out] tenseList The tenses.
     * @param[in] includeParticiples True if the tense list should include
     * participles (e.g. infinitive, past participle, present participle, gerund...).
     */
    virtual ULError getTensesForClass(uluint32 classID, ULList<ULTense>& tenseList, bool includeParticiples=false) = 0;

    /**
     * Finds all the tenses available for the language associated with this data source.
     *
     * @return ULError::NoError, ULError::NoMatch, or an error associated with the
     * failure to open or attach to a data source.
     * @param[in] includeParticiples True if the tense list should include
     * participles (e.g. infinitive, past participle, present participle, gerund...).
     */
    virtual ULError getAllTenses(ULList<ULTense>& tenseList, bool includeParticiples=false) = 0;

    /**
     * Finds all the persons available for the specified verb in the specified tense.
     * For most verb + tense combinations, the list of tenses is the same. Occasionally
     * there are irregular or defective verbs that have a different collection of persons.
     * For example, the French verb "apparoir" ("to be evident") only takes the third
     * person singular in the present tense.
     *
     * @return ULError::NoError, ULError::NoMatch, or an error associated with the
     * failure to open or attach to a data source.
     * @param[in] v The verb whose persons are desired.
     * @param[in] tense The tense for which the persons are desired.
     * @param[out] personList The list of persons.
     */
    virtual ULError getPersons(const ULDerivation& v, ULTense tense, ULList<ULPerson>& personList) = 0;

    /**
     * Finds all the persons available for the specified verb class in the specified tense,
     * assuming temporarily that the verb falls in the specified verb model class.
     * For most verb + tense combinations, the list of tenses is the same. Occasionally
     * there are irregular or defective verbs that have a different collection of persons.
     * For example, the French verb "apparoir" ("to be evident") only takes the third
     * person singular in the present tense.
     *
     * In most cases, the classID parameter is redundant, because it is equal
     * to v.getClassID(). But during the development of new conjugators,
     * Ultralingua's data editors need to be able to try out different verb classes
     * for each new verb to help them classify the verb correctly. In general,
     * if you find yourself using this method, you should switch to getPersons,
     * which does not have the classID parameter.
     *
     * @return ULError::NoError, ULError::NoMatch, or an error associated with the
     * failure to open or attach to a data source.
     * @param[in] v The verb whose persons are desired.
     * @param[in] classID The verb model class ID in which this verb is to be interpreted.
     * @param[in] tense The tense for which the persons are desired.
     * @param[out] personList The list of persons.
     */
    virtual ULError getPersonsForClass(const ULDerivation& v, uluint32 classID, ULTense tense, ULList<ULPerson>& personList) = 0;

    /**
     * Retrieves all the part-of-speech tagging rules used in this data source's language.
     *
     * @return ULError::NoError, ULError::NoMatch, or an error associated with the
     * failure to open or attach to a data source.
     * @param[out] ruleList The list of rules.
     */
    virtual ULError getAllTaggingRules(ULList<ULTaggingRule>& ruleList) = 0;

    /**
     * Retrieves the list of all feature names stored in this data source. These feature names will typically
     * include some that refer to global features represented by subclasses of ULEnum (e.g. "pastparticiple"),
     * and others that refer to features used only internally in the language data source.
     *
     * @return ULError::NoMatch if there are no feature names in this data source, or ULError::NoError otherwise.
     * @param[out] featureNameList the desired feature names, or the empty list if an error occurs.
     */
    virtual ULError getFeatureNameList(ULList<ULString>& featureNameList) = 0;

    /**
     * Retrieves the inflection rules in this data source that might contribute to a successful inflection
     * from the specified derivation to the specified target part of speech. This method is an essential part
     * of the inflection process coordinated by ULInflector.
     *
     * @return ULError::DataSourceOpenFailed; ULError::NoMatch if there are no appropriate inflection rules in this
     * data source; or ULError::NoError.
     * @param[in] derivation the derivation so far, on which we wish to build. This derivation may simply consist
     * of a root word and part of speech, or it may already have some inflection rules to which we're hoping to add.
     * @param[in] targetPartOfSpeech the part of speech towards which the current inflection is being directed.
     * @param[out] ruleList the desired inflection rules (if any), or the empty list if an error occurs.
     */
    virtual ULError getInflectionRules(const ULDerivation& derivation,
                                       const ULPartOfSpeech& targetPartOfSpeech,
                                       ULList<ULInflectionRule>& ruleList) = 0;

    /**
     * Retrieves the inflection rules in this data source that might contribute to a successful dissection
     * by being inserted at the front of the specified derivation. This method is an essential part
     * of the dissection/stemming process coordinated by ULDissector.
     *
     * @return ULError::DataSourceOpenFailed; ULError::NoMatch if there are no appropriate inflection rules in this
     * data source; or ULError::NoError.
     * @param[in] derivation the derivation so far, on which we wish to build. This derivation may simply consist
     * of a root word and part of speech, or it may already have some inflection rules in front of which we're hoping
     * to add a rule.
     * @param[out] ruleList the desired inflection rules (if any), or the empty list if an error occurs.
     */
    virtual ULError getInflectionRulesForDissection(const ULDerivation& derivation, ULList<ULInflectionRule>& ruleList) = 0;
    
    /**
     * Retrieves the list of successor rules for the specified inflection rule.
     *
     * @return ULError::DataSourceOpenFailed; ULError::NoMatch if there are no appropriate inflection rules in this
     * data source; or ULError::NoError.
     * @param[in] rule the inflection rule whose successors are desired.
     * @param[out] ruleList the desired inflection rules (if any), or the empty list if an error occurs.
     */
    virtual ULError getSuccessors(const ULInflectionRule& rule, ULList<ULInflectionRule>& successorList) = 0;
    
    /**
     * Retrieves the list of predecessor rules for the specified inflection rule.
     *
     * @return ULError::DataSourceOpenFailed; ULError::NoMatch if there are no appropriate inflection rules in this
     * data source; or ULError::NoError.
     * @param[in] rule the inflection rule whose predecessors are desired.
     * @param[out] ruleList the desired inflection rules (if any), or the empty list if an error occurs.
     */
    virtual ULError getPredecessors(const ULInflectionRule& rule, ULList<ULInflectionRule>& predecessorList) = 0;
    
    /**
     * ULLanguageDataSource objects may contain a list of "stop words"--words that are very common, and should be
     * ignored in some search contexts. These words tend to be from closed linguistic classes like articles, pronouns,
     * prepositions, etc.
     *
     * This method retrieves all the stop words in this data source.
     *
     * @return ULError::NoError or ULError::NoMatch, depending on whether there are any matching words or not.
     */
    virtual ULError getAllStopWords(ULList<ULString>& wordList) = 0;
    
    /**
     * ULLanguageDataSource objects may contain a list of "stop words"--words that are very common, and should be
     * ignored in some search contexts. These words tend to be from closed linguistic classes like articles, pronouns,
     * prepositions, etc.
     *
     * @return true if this language data source's stop word list includes the specified word.
     * @param[in] word the word we're testing.
     */
    virtual bool hasStopWord(const ULString& word) = 0;
    
    /**
     * ULLanguageDataSource objects may contain a list of closed class words with corresponding parts of speech.
     * Typically, a data source will include articles, conjunctions, pronouns, and prepositions.
     *
     * This method retrieves ULDerivation objects for every closed class word in this data source.
     *
     * Each ULDerivation, as usual, will include part-of-speech information. Furthermore, its root and derived
     * form will be identical, as will the root part of speech and the derived form part of speech.
     *
     * @return ULError::NoError or ULError::NoMatch, depending on whether there are any matching words or not.
     * @param[in] searchText the word you're searching for. Matches are case- and accent-insensitive.
     */
    virtual ULError getAllClosedClassWords(ULList<ULDerivation>& wordList) = 0;
    
    /**
     * ULLanguageDataSource objects may contain a list of closed class words with corresponding parts of speech.
     * Typically, a data source will include articles, conjunctions, pronouns, and prepositions.
     *
     * This method retrieves ULDerivation objects for every closed class word that matches the specified string.
     * For example, getClosedClassWord("la", wordList) for a French language data source will return one
     * ULDerivation object corresponding to the feminine definite article, and another ULDerivation object
     * corresponding to the third person singular feminine direct object pronoun.
     *
     * Each ULDerivation, as usual, will include part-of-speech information. Furthermore, its root and derived
     * form will be identical, as will the root part of speech and the derived form part of speech.
     *
     * @return ULError::NoError or ULError::NoMatch, depending on whether there are any matching words or not.
     * @param[in] searchText the word you're searching for. Matches are case- and accent-insensitive.
     */
    virtual ULError getClosedClassWord(const ULString& searchText, ULList<ULDerivation>& wordList) = 0;
    
    /**
     * ULLanguageDataSource objects may contain a list of closed class words with corresponding parts of speech.
     * Typically, a data source will include articles, conjunctions, pronouns, and prepositions.
     *
     * This method retrieves ULDerivation objects for every closed class word whose part of speech satisfies the
     * category and features in the partOfSpeech parameter. If you want all the closed class words, just set partOfSpeech's
     * category to ULPartOfSpeechCategory::Any, without any features.
     *
     * @return ULError::NoError or ULError::NoMatch, depending on whether there are any matching words or not.
     * @param[in] partOfSpeech the part of speech for which we want 
     */
    virtual ULError getClosedClassWordForPartOfSpeech(const ULPartOfSpeech& partOfSpeech, ULList<ULDerivation>& wordList) = 0;
    
    /**
     * ULLanguageDataSource objects may contain frequency data of the form (word, root, part-of-speech, count).
     * These data come from manually tagged corpora similar to the American National Corpus or the Penn Treebank.
     *
     * This method returns a list of frequency objects corresponding to the specified word. (For example, the
     * word "chairs" might yield ("chairs", "chair", verb, 21), ("chairs", "chair", noun, 623), and
     * ("chairs", "chair", unknown, 2).
     *
     * The method performs its search in a case-insensitive and accent-insensitive way.
     *
     * @return ULError::NoMatch if there are no frequency records corresponding to the specified word,
     * ULError::DataSourceOpenFailed if there was a problem with the data source, or ULError::NoError otherwise.
     * @param[in] word the word whose frequencies are sought
     * @param[out] frequencyList the corresponding frequencies, sorted in decreasing order of frequency
     * 
     */
    virtual ULError getFrequencies(const ULString& word, ULList<ULFrequency>& frequencyList) = 0;
};

#endif
