/**
 * @file ulgender.h
 * @brief The interface for the ULGender class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULGENDER_H
#define ULGENDER_H

#include "ulenum.h"

/**
 * @class ULGender
 * @brief An enhanced enumerated type used to represent genders.
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULGender : public ULEnum
{
public:
    static const ULGender& None;
    static const ULGender& Any;
    static const ULGender& Masculine;
    static const ULGender& Feminine;
    static const ULGender& Neuter;
    static const ULGender& Common;


    ULGender();
    ULGender(const ULGender& other);
    ULGender(int id);
    ULGender(const char *otherStringID);
    virtual ~ULGender();

    virtual void clear();
    ULGender& operator=(const ULGender& other);
    ULGender& operator=(int otherID);
    ULGender& operator=(const char *otherStringID);

    bool operator==(const ULGender& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULGender& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULGender& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULGender& getInstance(int id);



private:
    static int nEnumeratedValues;
    static ULVector<const ULGender *> enumeratedValueVector;
    ULGender(const char *stringID, const char *displayString);
};


#endif

