/**
 * @file uldatasourceversion.h
 * @brief The interface for the ULDataSourceVersion class.
 * @author Copyright (C) 2012 Ultralingua, Inc.
 */

#ifndef ULDATASOURCEVERSION_H
#define ULDATASOURCEVERSION_H

#include "ulstring.h"

// Version numbers compatible with current ULAPI
#define ALLOWED_ULD_VERSION_NUMBER 0x08000001
#define ALLOWED_ULC_VERSION_NUMBER 0x08000001

// Version number compatible with ULAPI at the time we first introduced
// version number checking (September 2012).
#define FIRST_ALLOWED_ULD_VERSION_NUMBER 0x08000001
#define FIRST_ALLOWED_ULC_VERSION_NUMBER 0x08000001

// Version number that existed at the time of the switch to the new
// versioning system, and which was also compatible with ULAPI at that
// time. Note that for the .ulc file for many months, the major version
// number (8) was stored in the lowest byte of the version number. Oops.
#define GRANDFATHERED_ULD_VERSION_NUMBER 0x08000000
#define GRANDFATHERED_ULC_VERSION_NUMBER 0x00000008


/**
 * @class ULDataSourceVersion
 * @brief A class representing version information for a datasource.
 */
class ULDataSourceVersion
{
public:
    ULDataSourceVersion();
    ULDataSourceVersion(const ULDataSourceVersion& other);
    virtual ~ULDataSourceVersion();

    virtual void clear();
    ULDataSourceVersion& operator=(const ULDataSourceVersion& other);

    bool operator==(const ULDataSourceVersion& other) const;
    bool operator!=(const ULDataSourceVersion& other) const;
    bool operator<(const ULDataSourceVersion& other) const;

    void setVersionNumber(uluint32 n);
    uluint32 getVersionNumber() const;
    void setDate(const ULString& s);
    ULString getDate() const;
    void setVersionControlID(const ULString& s);
    ULString getVersionControlID() const;
    void setBodyHash(const ULString& s);
    ULString getBodyHash() const;

private:
    uluint32 versionNumber;
    ULString date;
    ULString versionControlID;
    ULString bodyHash;
};

#endif

