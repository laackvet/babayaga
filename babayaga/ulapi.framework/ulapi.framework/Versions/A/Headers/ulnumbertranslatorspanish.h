/**
 * @file ulnumbertranslatorspanish.h
 * @brief The interface for the ULNumberTranslatorSpanish class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORSPANISH_H
#define ULNUMBERTRANSLATORSPANISH_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorSpanish
 * @brief ULNumberTranslatorSpanish does number translation in Spanish.
 */
class ULNumberTranslatorSpanish : public ULNumberTranslator
{
public:
    ULNumberTranslatorSpanish();
    ULNumberTranslatorSpanish(const ULNumberTranslatorSpanish& other);
    virtual ~ULNumberTranslatorSpanish();

    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

