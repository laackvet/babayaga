/**
 * @file ulfile.h
 * @brief The interface for the ULFile class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULFILE_H
#define ULFILE_H

#include "ulstring.h"

/**
 * @class ULFile
 * @brief ULFile provides a simple, platform-independent interface to
 * files and other data storage mechanisms for use in ULAPI.
 * 
 * ULFile has limited write capabilities, to allow only for what
 * ULAPI's data sources need.  On opening for writing,
 * the file is either created if it doesn't already exist, or
 * truncated to size 0 if it does.  Seeks beyond the current file
 * size are allowed, however.
 */
// TODO This really should have an inheritance tree rather than #ifdefs.
class ULFile
{
public:
    enum{ ReadOnly=1, WriteOnly = 2, Append = 3 }; // Open modes.
    enum{ Beginning=1, CurrentPosition=2, End=3 }; // Seek starting points.

    ULFile();
    ~ULFile();

    uluint32 getOpenMode() const { return this->openMode; }
    bool open(const ULString& fileName,
              uluint32 openMode=ULFile::ReadOnly,
              const char *pathEncoding=0);
    void close();
    void clear();
    bool isOpen();
    uluint32 read(uluint8 *buffer, uluint32 nBytes);
    bool write(const uluint8 *buffer, uluint32 nBytes);
    uluint32 seek(ulint32 offset, int origin=ULFile::Beginning);
    uluint32 tellp();
    uluint32 tellg();

private:
    uluint32 openMode;
#if defined(UL_ANSI)
    ULString filePath;
    fstream file;
#endif
};

#endif

