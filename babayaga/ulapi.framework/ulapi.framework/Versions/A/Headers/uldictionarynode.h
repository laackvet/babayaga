/**
 * @file uldictionarynode.h
 * @brief The interface for the ULDictionaryNode class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULDICTIONARYNODE_H
#define ULDICTIONARYNODE_H

#include "ulstring.h"
#include "ulfeature.h"

class ULDictionaryDataSource;

/**
 * @class ULDictionaryNode
 * @brief A ULDictionaryNode represents a unit of data from a
 * ULDictionary.
 *
 * ULDictionaryNodes can form a tree (since each node contains
 * a list of child nodes).  Each node also contains a list of
 * (name, value) pairs called "features."
 *
 * As a concrete example, consider the entry "stop" in an
 * English-Spanish dictionary.  The entry as a whole would be a
 * single node with the feature ("headword", "stop"). This node
 * would have two child nodes, one with feature ("partOfSpeech", "n.")
 * and the other with feature ("partOfSpeech", "v.").  In turn,
 * the noun subnode would have child nodes of their own representing
 * the Spanish translations of "stop" the noun and "stop" the verb.
 *
 * Roughly, the "stop" node might look like this:
 *
 * <pre><tt>Node
 *    headword="stop"
 *    pronunciation="sta:p"
 *
 *    Node
 *        text="stop"
 *        partOfSpeech="n."
 *        Node
 *            text="alto"
 *            partOfSpeech="s.m."
 *        Node
 *            text="pausa"
 *            partOfSpeech="s.f."
 *        (...more child Nodes...)
 *
 *    Node
 *        text="stop"
 *        partOfSpeech="v."
 *        Node
 *            text="detener"
 *            partOfSpeech="v."
 *        Node
 *            text="sobreseer"
 *            partOfSpeech="v."
 *            context="(~ a petition, etc.)
 *        (...more child Nodes...)
 * </tt></pre>
 */

class ULDictionaryNode
{
public:
    ULDictionaryNode();
    ULDictionaryNode(const ULDictionaryNode& other);
    virtual ~ULDictionaryNode();

    ULDictionaryNode& operator=(const ULDictionaryNode& other);
    void clear();

    uluint32 getType() const;
    void setType(uluint32 newType);
    ULDictionaryDataSource *getDataSource();
    void setDataSource(ULDictionaryDataSource *newDataSource);

    bool hasFeature(const ULFeatureType& featureType) const;
    bool hasFeature(const char *featureTypeStringID) const;
    bool getFeature(const ULFeatureType& featureType, ULFeature& feature) const;
    bool getFeatures(const ULFeatureType& featureType, ULList< ULFeature > &featureList) const;
    bool getFeature(const char *featureTypeStringID, ULFeature& feature) const;
    void addFeature(const ULFeature& feature);
    const ULList<ULFeature>& getFeatureList() const;

    ULString toString(int level=0) const;

private:
    uluint32 type;
    ULDictionaryDataSource *dataSource;
    ULList<ULFeature> featureList;
};

#endif

