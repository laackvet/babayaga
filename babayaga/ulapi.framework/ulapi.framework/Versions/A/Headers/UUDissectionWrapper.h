//
//  UUDissectionWrapper.h
//  ulapi
//
//  Created by SHAUN REYNOLDS on 5/21/12.
//  Copyright (c) 2012 Ultralingua, Inc. All rights reserved.
//
//  READ THIS NOTE!!
//
//  This class was restored only to allow new Ultralingua 2 (2.2 and later)
//  to read old Ultralingua 2 plists of history, favorites, and back-forward lists.
//  This class should *not* be used for other purposes.
//

#import <Foundation/Foundation.h>
#import "UUDictionaryIteratorWrapper.h"
#import "ulderivation.h"

@interface UUDissectionWrapper : NSObject<NSCoding>

// Using dot notation with C++ types seems to lead to memory errors.
// So we'll just not delcare this as a property and do it ourselves.
//@property (nonatomic, assign) ULPartOfSpeech partOfSpeech;
- (const ULDerivation&)derivation;

@property (nonatomic, readonly) UUDictionaryIteratorWrapper *iteratorWrapper;

@end
