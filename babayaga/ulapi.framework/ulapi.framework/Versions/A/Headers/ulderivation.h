/**
 * @file ulderivation.h
 * @brief The interface for the ULDerivation class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULDERIVATION_H
#define ULDERIVATION_H

#include "ulstring.h"
#include "ulinflectionrule.h"
#include "ulforester.h"
#include "ulpartofspeech.h"
#include "uldictionaryiterator.h"

class ULInflector;
class ULDissector;

/**
 * @class ULDerivation
 * @brief A ULDerivation object stores a complete morphological derivation of
 * a word. For example, the derivation of the word "bakers" would consist of the
 * word "bake [verb]" plus two inflection rules, one that transforms a verb
 * into a noun by adding an "-er" suffix, and the other that transforms a singular
 * noun into a plural noun.
 */
class ULDerivation
{
public:
    ULDerivation();
    ULDerivation(const ULDerivation& other);
    ~ULDerivation();

    ULDerivation& operator=(const ULDerivation& other);
    bool operator==(const ULDerivation& other) const;
    bool operator<(const ULDerivation& other) const;
    bool operator!=(const ULDerivation& other) const;
    void clear();

    bool initializeFromWordAndRules(const ULLanguage& language,
                                    const ULDerivation& word,
                                    const ULList<ULInflectionRule>& ruleList);

    ULLanguage getLanguage() const;
    void setLanguage(const ULLanguage& newLanguage);
    int getValidity() const;
    void setValidity(int newValidity);

    // Root
    uluint32 getClassID() const;
    void setClassID(uluint32 classID);
    const ULString& getRoot() const;
    void setRoot(const ULString& newRoot);
    const ULPartOfSpeech& getRootPartOfSpeech() const;
    void setRootPartOfSpeech(const ULPartOfSpeech& newPartOfSpeech);
    const ULDictionaryIterator& getRootDictionaryIterator() const;
    void setRootDictionaryIterator(const ULDictionaryIterator& dictionaryIterator);
    void addFeature(const ULFeatureType& featureType, int value);
    void addFeature(const ULFeature& feature);
    void addStringFeature(const ULFeatureType& featureType, const ULString& stringValue);
    void setFeature(const ULFeatureType& featureType, int value);
    bool hasFeature(const ULFeatureType& featureType) const;
    bool hasFeature(const ULFeature& feature) const;
    bool getFeature(const ULFeatureType& featureType, ULFeature& feature) const;
    const ULList<ULFeature>& getRootFeatureList() const;
    void clearRootFeatureList();

    // Derived form
    const ULString& getDerivedForm() const;
    void setDerivedForm(const ULString& newDerivedForm);
    const ULPartOfSpeech& getDerivedFormPartOfSpeech() const;
    void setDerivedFormPartOfSpeech(const ULPartOfSpeech& newPartOfSpeech);
    void appendInflectionRule(const ULInflectionRule& rule);
    void removeFrontRule();
    void removeBackRule();
    void prependInflectionRule(const ULInflectionRule& rule);
    const ULList<ULInflectionRule>& getInflectionRuleList() const;

    // General
    void collapseToRoot();
    bool finish(const ULString& originalSearchString, ULDissector *dissector, ULInflector *inflector);
    bool isReverseExtendable() const;
    ULString toString(bool verbose=false, bool showOnlyEndpoints=false) const;
    
    enum { Speculative, Verified, Invalid };

private:
    ULLanguage language;
    int validity;
    uluint32 rootClassID;
    ULString root;
    ULPartOfSpeech rootPartOfSpeech;
    ULDictionaryIterator rootDictionaryIterator;
    ULList<ULFeature> rootFeatureList;
    ULString derivedForm;
    ULPartOfSpeech derivedFormPartOfSpeech;
    ULList<ULInflectionRule> inflectionRules;

    bool findFeature(const ULFeatureType& featureType, int& featureValue) const;
};

#endif
