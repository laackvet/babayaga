/**
 * @file ulnumbertranslatoresperanto.h
 * @brief The interface for the ULNumberTranslatorEsperanto class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORESPERANTO_H
#define ULNUMBERTRANSLATORESPERANTO_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorEsperanto
 * @brief ULNumberTranslatorEsperanto does number translation in Esperanto.
 */
class ULNumberTranslatorEsperanto : public ULNumberTranslator
{
public:
    ULNumberTranslatorEsperanto();
    ULNumberTranslatorEsperanto(const ULNumberTranslatorEsperanto& other);
    virtual ~ULNumberTranslatorEsperanto();
    
    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);
    
    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif
