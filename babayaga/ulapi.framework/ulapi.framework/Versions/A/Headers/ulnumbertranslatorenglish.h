/**
 * @file ulnumbertranslatorenglish.h
 * @brief The interface for the ULNumberTranslatorEnglish class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORENGLISH_H
#define ULNUMBERTRANSLATORENGLISH_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorEnglish
 * @brief ULNumberTranslatorEnglish does number translation in English.
 */
class ULNumberTranslatorEnglish : public ULNumberTranslator
{
public:
    ULNumberTranslatorEnglish();
    ULNumberTranslatorEnglish(const ULNumberTranslatorEnglish& other);
    virtual ~ULNumberTranslatorEnglish();

    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

