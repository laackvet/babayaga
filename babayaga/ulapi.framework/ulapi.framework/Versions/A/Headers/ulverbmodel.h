/**
 * @file ulverbmodel.h
 * @brief The interface for the ULVerbModel class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULVERBMODEL_H
#define ULVERBMODEL_H

#include "ulerror.h"
#include "ulcontainers.h"
#include "ulstring.h"
#include "ultense.h"
#include "ulperson.h"
#include "ulfeature.h"

/**
 * @class ULTenseModel
 * @brief Helper class for ULVerbModel.
 */
struct ULTenseModel
{
    ULTense tense;
    ULList< ULPair<ULPerson,ULString> > personModelList;
};

/**
 * @class ULVerbModel
 * @brief A ULVerbModel object stores information used to conjugate
 * verbs of a particular class.  In general, ULVerbModel will be used
 * only by low-level ULAPI code, and will not appear in interfaces
 * intended for use by ULAPI licensors.
 */
class ULVerbModel
{
public:
    ULVerbModel();
    ULVerbModel(const ULVerbModel& model);
    ~ULVerbModel();

    ULVerbModel& operator=(const ULVerbModel& model);
    void clear();

    uluint32 getClassID() const;
    void setClassID(uluint32 id);
    ULList<ULTenseModel>& getTenseModelList();

    void addFeature(const ULFeatureType& featureType, int value);
    void addFeature(const ULFeature& feature);
    void addStringFeature(const ULFeatureType& featureType, const ULString& stringValue);
    bool hasFeature(const ULFeatureType& featureType) const;
    bool getFeature(const ULFeatureType& featureType, ULFeature& feature) const;
    const ULList<ULFeature>& getFeatureList() const;

    // Verb model constants.
    enum { ULVerbClassIrregular=254, ULVerbClassUnclassified=255 };

private:
    uluint32 classID;
    ULList<ULFeature> featureList;
    ULList<ULTenseModel> tenseModelList;
};

#endif

