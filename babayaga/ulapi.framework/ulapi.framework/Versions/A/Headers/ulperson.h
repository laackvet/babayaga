/**
 * @file ulperson.h
 * @brief The interface for the ULPerson class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULPERSON_H
#define ULPERSON_H

#include "ulenum.h"
#include "ulnumber.h"
#include "ulformality.h"

/**
 * @class ULPerson
 * @brief An enhanced enumerated type used to represent persons (first, second, third, as
 * well as first-singular, third-plural, second-singular-formal, etc.).
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULPerson : public ULEnum
{
public:
    static const ULPerson& None;
    static const ULPerson& Any;
    static const ULPerson& First;
    static const ULPerson& Second;
    static const ULPerson& Third;
    static const ULPerson& FirstSingular;
    static const ULPerson& SecondSingular;
    static const ULPerson& SecondSingularFormal;
    static const ULPerson& ThirdSingular;
    static const ULPerson& FirstPlural;
    static const ULPerson& SecondPlural;
    static const ULPerson& SecondPluralFormal;
    static const ULPerson& ThirdPlural;


    ULPerson();
    ULPerson(const ULPerson& other);
    ULPerson(int id);
    ULPerson(const char *otherStringID);
    virtual ~ULPerson();

    virtual void clear();
    ULPerson& operator=(const ULPerson& other);
    ULPerson& operator=(int otherID);
    ULPerson& operator=(const char *otherStringID);

    bool operator==(const ULPerson& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULPerson& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULPerson& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULPerson& getInstance(int id);
    void expandPerson(ULPerson& person, ULNumber& number, ULFormality& formality) const;
    static void getAllPersons(ULList<ULPerson>& persons);



private:
    static int nEnumeratedValues;
    static ULVector<const ULPerson *> enumeratedValueVector;
    ULPerson(const char *stringID, const char *displayString);
    const char *languageISOCode;
    ULPerson(const char *stringID, const char *languageISOCode, const char *displayString);
};


#endif

