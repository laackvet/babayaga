//
//  UUTenseWrapper.h
//  ulapi
//
//  Created by Jennifer Van Osdel on 3/25/13.
//
//

#import <Foundation/Foundation.h>

#include "ultense.h"

@interface UUTenseWrapper : NSObject<NSCoding>

- (void)setTense:(const ULTense &)tense;
- (const ULTense &)tense;

- (id)initWithTense:(const ULTense &)tense;

@end
