/**
 * @file uldefiner.h
 * @brief The interface for the ULDefiner class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULDEFINER_H
#define ULDEFINER_H

#include "uldictionary.h"
#include "uldictionarydatasource.h"
#include "ulstemmer.h"

/**
 * @class ULDefiner
 * @brief A worker that looks up words in bilingual or monolingual dictionary
 * data sources and supplies definitions for those words.
 */
class ULDefiner : public ULDictionary
{
    UL_TEST_FRIEND;

public:
    ULDefiner();
    ULDefiner(const ULDefiner& other);
    virtual ~ULDefiner();

    ULDefiner& operator=(const ULDefiner& other);
    virtual void clear();

    // ULWorker operations.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& serviceList);

    // Accessors.
    void setDictionaryDataSource(ULDictionaryDataSource *dataSource);
    ULDictionaryDataSource *getDictionaryDataSource();
    
    void setFirstStemmer(ULStemmer *stemmer);
    ULStemmer *getFirstStemmer();
    
    void setSecondStemmer(ULStemmer *stemmer);
    ULStemmer *getSecondStemmer();
    
    // ULDictionary operations.
    virtual ULError begin(const ULLanguage& searchLanguage,
                          uluint32 indexID,
                          ULDictionaryIterator& iterator);

    virtual ULError end(const ULLanguage& searchLanguage,
                        uluint32 indexID,
                        ULDictionaryIterator& iterator);

    virtual ULError find(const ULString& s,
                         const ULLanguage& searchLanguage,
                         uluint32 indexID,
                         ULDictionaryIterator& iterator);

    // Definition support beyond the ULDictionary operations.
    ULError simpleFind(const ULString& s,
                       const ULLanguage& searchLanguage,
                       ULDictionaryIterator& iterator);

    ULError find(const ULString& searchString,
                 const ULLanguage& searchLanguage,
                 ULDictionaryIterator& dictionaryIterator);

    ULError getHeadwordSearchResults(const ULString& originalSearchString,
                                     const ULLanguage& searchLanguage,
                                     uluint32 maxBrowsingResults,
                                     uluint32 approximateRequiredPrefixLength,
                                     /* Out: */
                                     ULList<ULDerivation>& stemmingMatches,
                                     ULList<ULDerivation>& substringMatches,
                                     ULList<ULDerivation>& approximateMatches);

    ULError getApproximateMatches(const ULString& searchString,
                                  const ULLanguage& searchLanguage,
                                  ULList<ULString>& approximateMatches);

    ULError lookUp(const ULString& searchString,
                   const ULLanguage& searchLanguage,
                   ULList<ULDictionaryIterator>& results);

private:
    ULDictionaryDataSource *dictionaryDataSource;
    ULStemmer *firstStemmer;
    ULStemmer *secondStemmer;
    
    enum { MaxHeadwordResults=50 };

    // Utility functions for lookUp.
    void tokenize(const ULString& s, const ULLanguage& language, ULList<ULString>& tokenList);
    uluint32 getNextRawToken(ULStringIterator& it, ULString& token);
    bool hasInsensitiveSubstring(const ULString& haystack, const ULString& needle, const ULLanguage& language);
    ULLanguageDataSource *getLanguageDataSource(const ULLanguage& language);
    ULStemmer *getStemmerForLanguage(const ULLanguage& language); // Needed to get correct stemmer w/o bad accesses
};

#endif

