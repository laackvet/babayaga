/**
 * @file ulnumbertranslatordutch.h
 * @brief The interface for the ULNumberTranslatorDutch class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORDUTCH_H
#define ULNUMBERTRANSLATORDUTCH_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorDutch
 * @brief ULNumberTranslatorDutch does number translation in Dutch.
 */
class ULNumberTranslatorDutch : public ULNumberTranslator
{
public:
    ULNumberTranslatorDutch();
    ULNumberTranslatorDutch(const ULNumberTranslatorDutch& other);
    virtual ~ULNumberTranslatorDutch();

    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

