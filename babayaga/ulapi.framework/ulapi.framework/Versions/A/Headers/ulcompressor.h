/**
 * @file ulcompressor.h
 * @brief The interface for the ULCompressor class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULCOMPRESSOR_H
#define ULCOMPRESSOR_H

#include "zlib.h"
#include "ultypes.h"

/**
 * @class ULCompressor
 * @brief A ULCompressor object provides a simple interface for
 * compressing and decompressing byte strings using zlib.
 */
class ULCompressor
{
public:
    static ULCompressor *create();

    ULCompressor();
    ~ULCompressor();

    uluint32 compress(uluint8 *source,
                      uluint32 sourceLength,
                      uluint8 *destination,
                      uluint32 destinationLength);

    uluint32 expand(uluint8 *source,
                    uluint32 sourceLength,
                    uluint8 *destination,
                    uluint32 destinationLength);

private:
    bool initialized;
    int error;
    z_stream compressionStream;
    z_stream expansionStream;

    bool init();
    void cleanup();
};

#endif
