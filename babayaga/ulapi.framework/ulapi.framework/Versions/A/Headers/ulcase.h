/**
 * @file ulcase.h
 * @brief The interface for the ULCase class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULCASE_H
#define ULCASE_H

#include "ulenum.h"

/**
 * @class ULCase
 * @brief An enhanced enumerated type used to represent noun/pronoun/adjective cases.
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULCase : public ULEnum
{
public:
    static const ULCase& None;
    static const ULCase& Any;
    static const ULCase& Nominative;
    static const ULCase& Genitive;
    static const ULCase& Accusative;
    static const ULCase& Dative;
    static const ULCase& Prepositional;
    static const ULCase& Instrumental;
    static const ULCase& Vocative;
    static const ULCase& Ablative;
    static const ULCase& Locative;
    static const ULCase& Partitive;
    static const ULCase& Inessive;
    static const ULCase& Elative;
    static const ULCase& Illative;
    static const ULCase& Adessive;
    static const ULCase& Allative;
    static const ULCase& Essive;
    static const ULCase& Translative;
    static const ULCase& Instructive;
    static const ULCase& Abessive;
    static const ULCase& Prolative;
    static const ULCase& Sublative;
    static const ULCase& Superessive;
    static const ULCase& Delative;
    static const ULCase& Terminative;
    static const ULCase& Temporal;
    static const ULCase& CausalFinal;


    ULCase();
    ULCase(const ULCase& other);
    ULCase(int id);
    ULCase(const char *otherStringID);
    virtual ~ULCase();

    virtual void clear();
    ULCase& operator=(const ULCase& other);
    ULCase& operator=(int otherID);
    ULCase& operator=(const char *otherStringID);

    bool operator==(const ULCase& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULCase& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULCase& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULCase& getInstance(int id);



private:
    static int nEnumeratedValues;
    static ULVector<const ULCase *> enumeratedValueVector;
    ULCase(const char *stringID, const char *displayString);
};


#endif

