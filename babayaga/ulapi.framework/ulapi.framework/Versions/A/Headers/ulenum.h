/**
 * @file ulenum.h
 * @brief The interface for the ULEnum class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULENUM_H
#define ULENUM_H

#include "ultypes.h"
#include "ulstring.h"
#include "ulinteger.h"

class ULFeature;
class ULFeatureType;

/**
 * @class ULEnum
 * @brief The base class for all of ULAPI's enhanced enumerated types.
 *
 * ULAPI's enhanced enumerated types are intended to provide an
 * enumerated type where:
 *
 * - Each enumerated value is an object of the type of the class.
 * - The class can contain methods (which C++ enums cannot).
 * - Each enumerated value may be associated with a string for
 *   display,
 * - Each enumerated value may be associated with other data as
 *   appropriate.  See, for example, the ISO language codes in
 *   each instance of the ULLanguage class.
 * - Looking at an enumerated value in a debugger will show the
 *   value's name or meaning in some form, rather than just an integer.
 * - The type is readily maintained.  When you want to change the list of
 *   enumerated values, you want to avoid having to change things in
 *   a lot of different places.  The approach shown here requires
 *   changes in one location in the .h file and one location in the
 *   .cpp file. No methods need to be modified.
 *
 * There is one disadvantage to keep in mind. ULAPI enhanced enums
 * cannot be used as cases in switch statements.
 */
class ULEnum
{
public:
    ULEnum();
    ULEnum(const ULEnum& other);
    virtual ~ULEnum();

    virtual void clear();
    virtual ULEnum& operator=(const ULEnum& other);

    operator int () const;
    uluint32 hash(uluint32 tableSize) const;

    int getID() const;
    const char *getStringID() const;
    virtual const char *getDisplayString() const;

    static bool isEnumeratedFeature(const ULString& featureName);
    static uluint32 getRunTimeFeatureID(const ULString& featureName);
    static const ULEnum *getEnumForFeature(const ULFeature& feature);

protected:
    int id;
    const char *stringID;
    const char *displayString;

    static void loadMaps();
    static ULHashTable<ULString, uluint32> featureNameToRunTimeID;
    static ULHashTable<ULFeature, const ULEnum *> featureToEnum;
};

#endif

