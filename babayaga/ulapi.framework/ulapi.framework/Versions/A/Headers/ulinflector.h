/**
 * @file ulinflector.h
 * @brief The interface for the ULInflector class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULINFLECTOR_H
#define ULINFLECTOR_H

#include "ulworker.h"
#include "ultense.h"
#include "ulperson.h"
#include "ullanguagedatasource.h"
#include "uldictionarydatasource.h"
#include "ulderivation.h"

/**
 * @class ULInflector
 * @brief A ULInflector is a low-level worker that takes morphological information
 * and constructs a word (or words) from it.  For example, (bus, noun, plural)
 * gets inflected into the pair of alternative spellings [buses, busses].
 */
class ULInflector : public ULWorker
{
    friend class ULDerivation;

public:
    ULInflector();
    ULInflector(const ULInflector& other);
    virtual ~ULInflector();

    ULInflector& operator=(const ULInflector& other);
    void clear();

    // Factory
    static ULInflector *createInflector(const ULLanguage& language);

    // Accessors
    const ULLanguage& getLanguage() const;
    void setLanguageDataSource(ULLanguageDataSource *dataSource);
    ULLanguageDataSource *getLanguageDataSource();
    void setDictionaryDataSource(ULDictionaryDataSource *dictionaryDataSource);
    ULDictionaryDataSource *getDictionaryDataSource();

    // ULWorker interfaces
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The inflector's core services
    virtual ULError inflect(const ULString& word,
                            const ULPartOfSpeech& sourcePartOfSpeech,
                            const ULPartOfSpeech& targetPartOfSpeech,
                            uluint32 maxDerivationLength,
                            ULList<ULDerivation>& derivationList,
                            bool applyTargetPOSPStrictly=false);

    virtual ULError inflect(const ULString& word,
                            const ULPartOfSpeech& sourcePartOfSpeech,
                            const ULPartOfSpeech& targetPartOfSpeech,
                            const ULList<ULFeature>& targetFeaturesToExclude,
                            uluint32 maxDerivationLength,
                            ULList<ULDerivation>& derivationList,
                            bool applyTargetPOSPStrictly=false);

    virtual ULError inflect(const ULDerivation& verb,
                            const ULPartOfSpeech& targetPartOfSpeech,
                            uluint32 maxDerivationLength,
                            ULList<ULDerivation>& derivationList,
                            bool applyTargetPOSPStrictly=false);

    // The inflector's new core services, in an attempt to simplify the interfaces for most uses.
    virtual ULError getAllWordsMatchingRoot(const ULString& root, ULList<ULDerivation>& wordList);
    virtual ULError inflect(const ULString& root, ULList<ULDerivation>& inflectionList);
    virtual ULError inflect(const ULDerivation& word, ULList<ULDerivation>& inflectionList);

protected:
    ULLanguageDataSource *languageDataSource;
    ULDictionaryDataSource *dictionaryDataSource;

    ULError inflectWithWord(const ULString& originalSearchString,
                            const ULDerivation& word,
                            const ULPartOfSpeech& targetPartOfSpeech,
                            uluint32 maxDerivationLength,
                            ULList<ULDerivation>& derivationList);
    
    void getInflections(const ULString& originalSearchString,
                        ULDerivation& derivation,
                        const ULPartOfSpeech& targetPartOfSpeech,
                        uluint32 maxDerivationLength,
                        ULList<ULDerivation>& derivationList);

    void prepareTargetPartOfSpeech(const ULPartOfSpeech& originalTargetPOSP, ULPartOfSpeech& newTargetPOSP);
    bool isWordWithPartOfSpeech(const ULString& s, const ULPartOfSpeech& targetPartOfSpeech);
    void getMatchingTerms(const ULString& s, ULList<ULPartOfSpeech>& pospList, ULList<ULDictionaryIterator>& iteratorList);

    static void filterRulesForInflection(const ULPartOfSpeech& targetPartOfSpeech, ULList<ULInflectionRule>& ruleList);
};

#endif

