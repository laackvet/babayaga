/**
 * @file ullock.h
 * @brief The interface for the ULLock class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULLOCK_H
#define ULLOCK_H

/**
 * @class ULLock
 * @brief ULLock is an abstract class describing a simple mutual
 * exclusion primitive.  Each operating system or environment will
 * require a different implementation.
 */
class ULLock
{
public:
    virtual ~ULLock() {}

    /**
     * If this lock is currently unclaimed, the caller of claim()
     * is granted ownership of the lock.  Otherwise, the calling
     * thread is put to sleep until the lock is released.  When
     * claim() returns, the caller has ownership of the lock.
     * As usual with locking primitives, it's important to nest
     * calls to claim() and release() properly so as to avoid deadlock.
     */
    virtual void claim() = 0;

    /**
     * When called by the thread that currently has ownership of this
     * lock, release() causes the thread to relinquish ownership.
     * Otherwise, release() does nothing (though a good implementation
     * of ULLock should fire an assertion in such a case).
     */
    virtual void release() = 0;
};

#endif

