/**
 * @file ulnumbertranslatorgerman.h
 * @brief The interface for the ULNumberTranslatorGerman class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORGERMAN_H
#define ULNUMBERTRANSLATORGERMAN_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorGerman
 * @brief ULNumberTranslatorGerman does number translation in German.
 */
class ULNumberTranslatorGerman : public ULNumberTranslator
{
public:
    ULNumberTranslatorGerman();
    ULNumberTranslatorGerman(const ULNumberTranslatorGerman& other);
    virtual ~ULNumberTranslatorGerman();

    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

