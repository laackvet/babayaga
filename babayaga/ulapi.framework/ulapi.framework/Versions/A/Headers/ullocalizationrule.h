/**
 * @file ullocalizationrule.h
 * @brief The interface for the ULInflectionRule class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULLOCALIZATIONRULE_H
#define ULLOCALIZATIONRULE_H

#include "ulstring.h"
#include "ullanguage.h"

/**
 * @class ULLocalizationRule
 * @brief A ULLocalizationRule object stores one of the rules stored in a
 * ULStandardForester and used to localize ULPartOfSpeech objects and
 * various other enumerated feature types.
 */
class ULLocalizationRule
{
public:
    ULLocalizationRule();
    ULLocalizationRule(const ULLocalizationRule& other);
    ~ULLocalizationRule();

    ULLocalizationRule& operator=(const ULLocalizationRule& other);
    bool operator==(const ULLocalizationRule& other) const;
    bool operator!=(const ULLocalizationRule& other) const;
    void clear();

    ULLanguage getTargetLanguage() const;
    void setTargetLanguage(ULLanguage language);
    void setTargetLanguage(ULString languageISOCode);
    ULString getFeatureValue() const;
    void setFeatureValue(ULString newFeatureValue);
    ULString getLocalization() const;
    void setLocalization(ULString newLocalization);
    ULString getAbbreviation() const;
    void setAbbreviation(ULString newAbbreviation);

    ULString toString() const;

private:
    ULLanguage targetLanguage;
    ULString featureValue;
    ULString localization;
    ULString abbreviation;
};

#endif

