/**
 * @file ulnumbertranslatornorwegian.h
 * @brief The interface for the ULNumberTranslatorNorwegian class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORNORWEGIAN_H
#define ULNUMBERTRANSLATORNORWEGIAN_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorNorwegian
 * @brief ULNumberTranslatorNorwegian does number translation in Norwegian.
 */
class ULNumberTranslatorNorwegian : public ULNumberTranslator
{
public:
    ULNumberTranslatorNorwegian();
    ULNumberTranslatorNorwegian(const ULNumberTranslatorNorwegian& other);
    virtual ~ULNumberTranslatorNorwegian();

    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

