/**
 * @file ulconfiguration.h
 * @brief The interface for the ULConfiguration class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULCONFIGURATION_H
#define ULCONFIGURATION_H

/**
 * @enum ULConfigLockType
 * An enumerated type describing the various types of lock that
 * might be used by ULAPI. Used by ULConfiguration.
 */
enum ULConfigLockType
{
    ULConfigLockTypeNone,
    ULConfigLockTypePThreads,
    ULConfigLockTypeCocoa,
    ULConfigLockTypeWindows
};

/**
 * @enum ULConfigDictionaryDataSourceType
 * An enumerated type describing the various types of dictionary
 * data sources. Used by ULConfiguration.
 */
enum ULConfigDictionaryDataSourceType
{
    ULConfigDictionaryDataSourceTypeNone,
    ULConfigDictionaryDataSourceTypeAutodetect,
    ULConfigDictionaryDataSourceTypeULDFile
};

/**
 * @enum ULConfigLanguageDataSourceType
 * An enumerated type describing the various types of language
 * data sources. Used by ULConfiguration.
 */
enum ULConfigLanguageDataSourceType
{
    ULConfigLanguageDataSourceTypeNone,
    ULConfigLanguageDataSourceTypeAutodetect,
    ULConfigLanguageDataSourceTypeULCFile
};

/**
 * @class ULConfiguration
 * @brief Each object of type ULFactory (or one of its descendants) uses
 * a ULConfiguration object to maintain configuration state needed to make
 * decisions about instantiation of workers, locks, data sources, etc.
 * The ULConfiguration object may draw its values from defaults, from a
 * config file, or from the factory's own initialization routines.
 */
class ULConfiguration
{
public:
    ULConfiguration();
    ULConfiguration(const ULConfiguration& other);
    virtual ~ULConfiguration();

    void clear();
    ULConfiguration& operator=(const ULConfiguration& other);

    ULConfigLockType getLockType() const;
    void setLockType(ULConfigLockType type);
    ULConfigDictionaryDataSourceType getDictionaryDataSourceType() const;
    void setDictionaryDataSourceType(ULConfigDictionaryDataSourceType type);
    ULConfigLanguageDataSourceType getLanguageDataSourceType() const;
    void setLanguageDataSourceType(ULConfigLanguageDataSourceType type);

protected:
    ULConfigLockType lockType;
    ULConfigDictionaryDataSourceType dictionaryDataSourceType;
    ULConfigLanguageDataSourceType languageDataSourceType;
};

#endif

