/**
 * @file ulconjugator.h
 * @brief The interface for the ULConjugator class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULCONJUGATOR_H
#define ULCONJUGATOR_H

#include "ulworker.h"
#include "ulinflector.h"
#include "uldissector.h"

/**
 * @class ULConjugator
 * @brief A ULConjugator is a worker that conjugates verbs. Most ULConjugator
 * services are also available via ULInflector, but ULConjugator is designed
 * to have simple interfaces to support the most common conjugation requests.
 */
class ULConjugator : public ULWorker
{
public:
    ULConjugator();
    ULConjugator(const ULConjugator& other);
    virtual ~ULConjugator();

    ULConjugator& operator=(const ULConjugator& other);
    void clear();

    // Accessors
    ULInflector *getInflector();
    void setInflector(ULInflector *inflector);
    ULDissector *getDissector();
    void setDissector(ULDissector *dissector);
    const ULLanguage& getLanguage() const;

    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The core services provided by the conjugator.
    ULError getVerbFromDictionaryIterator(const ULDictionaryIterator& dictionaryIterator, ULDerivation& verb);
    
    ULError getVerbsMatchingVerbForm(const ULString& verbForm, ULList<ULDerivation>& verbList);

    ULError getVerbsMatchingInfinitive(const ULString& infinitive, ULList<ULDerivation>& verbList);
    
    ULError getInfinitivesMatchingPrefix(const ULString& prefix, uluint32 maxMatches, ULList<ULString>& infinitiveList);

    ULError getInfinitivesMatchingPrefix(const ULString& prefix, uluint32 maxMatches, ULList<ULDerivation>& infinitiveList);

    ULError getTensesForVerb(const ULDerivation& verb, ULList<ULTense>& tenseList);

    ULError getPersonsForVerbAndTense(const ULDerivation& verb,
                                      const ULTense& tense,
                                      ULList<ULPerson>& personList);

    ULError getConjugation(const ULDerivation& verb,
                           const ULTense& tense,
                           const ULPerson& person,
                           ULDerivation& conjugation);

    ULError getConjugations(const ULDerivation& verb,
                            ULList<ULDerivation>& conjugations,
                            const ULTense& tense,
                            const ULNumber& number,
                            const ULPerson& person,
                            const ULFormality& formality,
                            bool includeCompoundForms=true,
                            bool includeNegativeConjugations=false,
                            bool reportOnlyOneConjugationPerDerivedForm=false);

    ULError getConjugationsForTense(const ULDerivation& verb,
                                    const ULTense& tense,
                                    ULList<ULDerivation>& conjugationList);

    ULError getAllConjugations(const ULDerivation& verb, ULList<ULDerivation>& conjugationList);

    ULError getConjugationWithObjects(const ULDerivation& verb,
                                      const ULTense& tense,
                                      const ULPerson& person,
                                      const ULNumber& number,
                                      const ULFormality& formality,
                                      const ULPartOfSpeech& directObjectPartOfSpeech,
                                      const ULPartOfSpeech& indirectObjectPartOfSpeech,
                                      ULString& conjugatedForm);

    void getPronouns(const ULPerson& person,
                     ULList<ULString>& pronounList);

    void getPronouns(const ULPerson& person,
                     const ULNumber& number,
                     const ULFormality& formality,
                     ULList<ULString>& pronounList);
    
    void getConjugationDisplayComponents(const ULDerivation& conjugation,
                                         ULString& pronounText,
                                         ULString& separatorText,
                                         ULString& conjugationText);

    ULString getConjugationDisplayText(const ULDerivation& conjugation);
    
    static ULError convertRussianForDisplay(ULString& text);
    static void removeNegativeConjugations(ULList<ULDerivation>& derivationList);

private:
    ULInflector *inflector;
    ULDissector *dissector;

    ULError getConjugatedForms(const ULDerivation& verb,
                               const ULTense& tense,
                               const ULPerson& person,
                               ULList<ULString>& conjugatedForms);

    ULError getConjugatedForms(const ULDerivation& verb,
                               const ULTense& tense,
                               const ULPerson& person,
                               const ULNumber& number,
                               const ULFormality& formality,
                               ULList<ULString>& conjugatedForms);

    void removeDerivationsUsedForStemmingOnly(ULList<ULDerivation>& derivationList);
    
    static int derivationCmp(ULDerivation& a, ULDerivation& b, void *context);
    static bool pospSatisfiesPOSP(const ULPartOfSpeech& a, const ULPartOfSpeech& b);
};

#endif

