/**
 * @file ulerror.h
 * @brief The interface for the ULError class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULERROR_H
#define ULERROR_H

#include "ulenum.h"

/**
 * @class ULError
 * @brief An enhanced enumerated type used as return values throughout
 * ULAPI to represent various error conditions.
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULError : public ULEnum
{
public:
    static const ULError& NoError;
    static const ULError& NotImplemented;
    static const ULError& ObjectUninitialized;
    static const ULError& MemoryAllocationFailed;
    static const ULError& InvalidID;
    static const ULError& InvalidName;
    static const ULError& ICUError;
    static const ULError& DataSourceAttachFailed;
    static const ULError& DataSourceOpenFailed;
    static const ULError& InvalidDataSourceVersion;
    static const ULError& InvalidLanguageFile;
    static const ULError& InvalidIterator;
    static const ULError& NoMatch;
    static const ULError& TooManyMatches;
    static const ULError& NoSuchObject;
    static const ULError& OperationCancelled;


    ULError();
    ULError(const ULError& other);
    ULError(int id);
    ULError(const char *otherStringID);
    virtual ~ULError();

    virtual void clear();
    ULError& operator=(const ULError& other);
    ULError& operator=(int otherID);
    ULError& operator=(const char *otherStringID);

    bool operator==(const ULError& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULError& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULError& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULError& getInstance(int id);



private:
    static int nEnumeratedValues;
    static ULVector<const ULError *> enumeratedValueVector;
    ULError(const char *stringID, const char *displayString);
};


#endif

