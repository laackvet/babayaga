/**
 * @file uldatasource.h
 * @brief The interface for the ULDataSource class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULDATASOURCE_H
#define ULDATASOURCE_H

#include "ulerror.h"
#include "ulstring.h"
#include "ullockable.h"
#include "uldatasourceversion.h"

/**
 * @class ULDataSource
 * @brief ULDataSource is the abstract parent for classes that
 * interface with data stored somewhere like a .uld or .ulc/.ull file
 * or a database.
 */
class ULDataSource : public ULLockable
{
public:
    virtual ~ULDataSource() {}

    /**
     * Causes this ULDataSource object to be associated with the specified
     * data source, and reads enough information from that data source to
     * determine its language(s), etc.
     *
     * The exact behavior of attach will be dependent on the nature of the
     * data source. If the data source is a file, then attach will read
     * header information from the file and then close the file to save
     * memory until the data source is actually needed. On the other hand,
     * if the data source is a remote database, then attach might open
     * a connection, collect header information, and then close the
     * connection.
     *
     * @return ULError::NoError if the attachment is successful,
     * or some other ULError value if not.
     * @param[in] dataSourceIdentifier A string describing the data
     * source (e.g. a file name, a database connection string, a URL,
     * etc.).
     */
    virtual ULError attach(const ULString& dataSourceIdentifier) = 0;

    /**
     * Releases the connection between this ULDataSource object and the
     * data source specified in the previous open() or attach() call,
     * closing any relevant files or network connections and freeing
     * memory in the process.
     *
     * @return ULError::NoError if the attachment is successful,
     * or some other ULError value if not.
     */
    virtual ULError detach() = 0;

    /**
     * Perform one-time opening and loading operations. Normally, such operations
     * are performed lazily, when the data source is first queried. If you would
     * prefer to control the time at which loading is performed, call this method.
     * @return ULError::NoError if the loading was successful.
     */
    virtual ULError load() = 0;

    /**
     * Frees dynamically allocated memory associated with this data source 
     * while keeping it attached to the file, db, etc. to which it was
     * previously attached. Also closes any relevant files, db connections, etc.
     * @return ULError::NoError if the memory freeing was successful.
     */
    virtual ULError close() = 0;

    /**
     * @return the data source identifier for the data source
     * attached to this ULDataSource object, or the empty string if
     * no data source is attached.
     */
    virtual ULString getDataSourceIdentifier() = 0;

    /**
     * @return the ULDataSourceVersion associated with this data source.
     */
    virtual ULDataSourceVersion getVersion() = 0;
};

#endif

