/**
 * @file ulnumbertranslatorhaitian.h
 * @brief The interface for the ULNumberTranslatorHaitian class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORHAITIAN_H
#define ULNUMBERTRANSLATORHAITIAN_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorHaitian
 * @brief ULNumberTranslatorHaitian does number translation in Haitian.
 */
class ULNumberTranslatorHaitian : public ULNumberTranslator
{
public:
    ULNumberTranslatorHaitian();
    ULNumberTranslatorHaitian(const ULNumberTranslatorHaitian& other);
    virtual ~ULNumberTranslatorHaitian();
    
    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);
    
    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

