/**
 * @file ulfrequency.h
 * @brief The interface for the ULFrequency class.
 * @author Copyright (C) 2013 Ultralingua, Inc.
 */

#ifndef ULFREQUENCY_H
#define ULFREQUENCY_H

#include "ulpartofspeech.h"

/**
 * @class ULFrequency
 * @brief A ULFrequency object contains frequency information
 * for a specific word and part of speech category.
 */
class ULFrequency
{
    friend class ULCFileLanguageDataSource;

public:
    ULFrequency();
    ULFrequency(const ULFrequency& other);
    ~ULFrequency();
    
    ULFrequency& operator=(const ULFrequency& other);
    void clear();
    
    const ULString& getSurfaceForm() const;
    void setSurfaceForm(const ULString& surfaceForm);
    const ULString& getRoot() const;
    void setRoot(const ULString& root);
    const ULPartOfSpeech& getPartOfSpeech() const;
    void setPartOfSpeech(const ULPartOfSpeech& posp);
    uluint32 getCount() const;
    void setCount(uluint32 root);
    
    ULString toString() const;
    
private:
    ULString surfaceForm;
    ULString root;
    ULPartOfSpeech partOfSpeech;
    uluint32 count;
};

#endif
