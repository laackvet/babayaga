/**
 * @file ulforester.h
 * @brief The interface for the ULForester class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULFORESTER_H
#define ULFORESTER_H

#include "ulerror.h"
#include "ulstring.h"
#include "ullanguage.h"
#include "ulforesttype.h"
#include "uldictionaryiterator.h"
#include "ulpartofspeech.h"
#include "ullocalizationrule.h"
#include "ulexample.h"
#include "ulderivation.h"

/**
 * @class ULForester
 * @brief ULForester is the abstract parent for classes that encapsulate the idiosyncrasies
 * of the organization of dictionary data sources. All ULDictionaryDataSources represent
 * data as one or more sequences of trees containing dictionary data in whatever form is
 * appropriate for the original data.
 */
class ULForester
{
public:
    ULForester();
    ULForester(const ULForester& other);
    virtual ~ULForester();
    virtual ULForester& operator=(const ULForester& other);
    virtual void clear();

    // Static factory method.
    static ULForester *createForester(const ULForestType& forestType);
    
    // Accessors
    virtual const ULForestType& getForestType() const;
    virtual void setForestType(const ULForestType& forestType);
    virtual ULDictionaryDataSource *getDictionaryDataSource();
    virtual void setDictionaryDataSource(ULDictionaryDataSource *dataSource);

    // Services common to all ULForester children.

    /**
     * Searches for all the matching terms in this dictionary. A "term" in this
     * context refers to a ULDictionaryNode containing a word or
     * phrase and a corresponding part of speech. For example, the nodes corresponding
     * to "rake [noun]" and "rake [verb]" will represent two distinct terms. The
     * terms in turn will normally have sub-node structures that contain
     * definitions/translations plus additional information about the term in
     * question.
     *
     * Most of the time, we want to search for a word that is filed (in the dictionary's
     * headword/sortkey index) under the word itself. For example, the sort key associated
     * with the word "goat" is "goat". But other times, we want to search for a multi-word
     * term (e.g. "break up") under a different sort key ("break"). Another common example
     * is the search for reflexive verbs, such as "s'appeler" or "lavarse" which are filed
     * under non-reflexive sort keys "appeler" and "lavar".
     *
     * For the most common case, looking for a word that is equal to its sortKey, you
     * can call this method with identical textToFind and sortKeyToFind, or you can call
     * the convenience method getMatchingTerms(searchString, searchLanguage,...) instead.
     *
     * @return ULError::NoError, ULError::MemoryAllocationFailed,
     * or ULError::DataSourceOpenFailed.
     * @param[in] textToFind the string you're looking for.
     * @param[in] sortKeyToFind the sort key under which you wish to find the text.
     * @param[in] searchLanguage For bilingual dictionaries, this specifies
     * the direction in which to search--i.e. the language in which
     * searchString is to be interpreted.
     * @param[in] ignoreCase True if comparisons should be case-insensitive.
     * @param[in] ignoreAccents True if comparisons should be accent-insensitive.
     * @param[out] iteratorList A list of iterators pointing to the matches
     * found in the specified index.
     */
    virtual ULError getMatchingTerms(const ULString& textToFind,
                                     const ULString& sortKeyToFind,
                                     const ULLanguage& searchLanguage,
                                     bool ignoreCase,
                                     bool ignoreAccents,
                                     ULList<ULDictionaryIterator>& iteratorList) = 0;

    /**
     * Convenience version of getMatchingTerms when the text you wish to find is the same
     * as the sort key under which you wish to search.
     * See getMatchingTerms(textToFind, sortKeyToFind, searchLanguage,...) for more details.
     *
     * @return ULError::NoError, ULError::MemoryAllocationFailed,
     * or ULError::DataSourceOpenFailed.
     * @param[in] searchString the string you're looking for.
     * @param[in] searchLanguage for bilingual dictionaries, this specifies
     * the direction in which to search--i.e. the language in which
     * searchString is to be interpreted.
     * @param[in] ignoreCase true if comparisons should be case-insensitive.
     * @param[in] ignoreAccents true if comparisons should be accent-insensitive.
     * @param[out] iteratorList a list of iterators pointing to the matches
     * found in the specified index.
     */
    virtual ULError getMatchingTerms(const ULString& searchString,
                                     const ULLanguage& searchLanguage,
                                     bool ignoreCase,
                                     bool ignoreAccents,
                                     ULList<ULDictionaryIterator>& iteratorList) = 0;

    virtual ULError getApproximateMatchingTerms(const ULString& searchString,
                                                const ULLanguage& searchLanguage,
                                                uluint32 requiredPrefixLength,
                                                ULList<ULDictionaryIterator>& approximateMatches) = 0;

    virtual ULError getApproximateMatchingTerms(const ULString& searchString,
                                                const ULLanguage& searchLanguage,
                                                ULList<ULString>& approximateMatches) = 0;
    
    /**
     * Moves the specified iterator to point to the root of the renderable subtree containing
     * the iterator's current target node. A renderable subtree is a tree of nodes that constitute
     * a minimum renderable unit in the forest of trees constituting this forester's data source.
     * The standard example of a minimum renderable unit is "what can go in a cell in the
     * Ultralingua 2 scrolling table view?" This will vary depending on dataset and application.
     *
     * @return ULError::NoError if the operation is successful. Otherwise, returns ULError::NoSuchObject,
     * in which case dictionaryIterator is unchanged.
     * @param[in, out] dictionaryIterator the iterator
     */
    virtual ULError toCurrentRenderingRoot(ULDictionaryDataSourceIterator& dictionaryIterator) = 0;

    /**
     * Moves the specified iterator to point to the root of the renderable subtree immediately following
     * the one containing the iterator's current target node.
     *
     * @return ULError::NoError if the operation is successful. Otherwise, returns ULError::NoSuchObject,
     * in which case dictionaryIterator is unchanged.
     * @param[in, out] dictionaryIterator the iterator
     */
    virtual ULError toNextRenderingRoot(ULDictionaryDataSourceIterator& dictionaryIterator) = 0;
    
    /**
     * Moves the specified iterator to point to the root of the renderable subtree immediately preceding
     * the one containing the iterator's current target node. If the iterator starts out pointing to the very last
     * rendering root in the forest, this method moves the iterator to the end of its range and returns ULError::NoError.
     *
     * @return ULError::NoError if the operation is successful. Otherwise, returns ULError::NoSuchObject,
     * in which case dictionaryIterator is unchanged.
     * @param[in, out] dictionaryIterator the iterator
     */
    virtual ULError toPreviousRenderingRoot(ULDictionaryDataSourceIterator& dictionaryIterator) = 0;
    
    /**
     * @return true if the iterator points to the root of the very first renderable subtree in the forest.
     * @param[in, out] dictionaryIterator the iterator
     */
    virtual bool isAtFirstRenderingRoot(const ULDictionaryDataSourceIterator& dictionaryIterator) = 0;
    
    /**
     * @return true if the iterator points to the root of the very last renderable subtree in the forest.
     * @param[in, out] dictionaryIterator the iterator
     */
    virtual bool isAtLastRenderingRoot(const ULDictionaryDataSourceIterator& dictionaryIterator) = 0;

    /**
     * Moves the specified iterator to point to the root of the term subtree containing
     * the iterator's current target node. A term is a subtree corresponding roughly to an ordered
     * triple (text, part of speech category, homonym number). For example, (ground, noun, 0),
     * (ground [as in grounding an electrical current], verb, 1), (ground [as in past tense of grind], verb, 2)
     * would be three separate terms.
     *
     * There may be occasions when nodes above the term are considered to be "in the term" for purposes
     * of this method. For example, if terms are grouped into "entries", then the root node of an entry
     * tree might be considered to be part of the first term in the entry. This is the sort of niggling
     * detail ULForester and its children are intended to hide from client code.
     *
     * @return ULError::NoError if the operation is successful. Otherwise, returns ULError::NoSuchObject,
     * in which case dictionaryIterator is unchanged.
     * @param[in, out] dictionaryIterator the iterator
     */
    virtual ULError toCurrentTerm(ULDictionaryDataSourceIterator& dictionaryIterator) = 0;
    
    /**
     * Moves the specified iterator to point to the root of the term subtree immediately following the one
     * containing the iterator's current target node. If the iterator starts out pointing to the very last
     * term in the forest, this method moves the iterator to the end of its range and returns ULError::NoError.
     *
     * @return ULError::NoError if the operation is successful. Otherwise, returns ULError::NoSuchObject,
     * in which case dictionaryIterator is unchanged.
     * @param[in, out] dictionaryIterator the iterator
     */
    virtual ULError toNextTerm(ULDictionaryDataSourceIterator& dictionaryIterator) = 0;
    
    /**
     * Moves the specified iterator to point to the root of the term subtree immediately preceding the one
     * containing the iterator's current target node.
     *
     * @return ULError::NoError if the operation is successful. Otherwise, returns ULError::NoSuchObject,
     * in which case dictionaryIterator is unchanged.
     * @param[in, out] dictionaryIterator the iterator
     */
    virtual ULError toPreviousTerm(ULDictionaryDataSourceIterator& dictionaryIterator) = 0;

    /**
     * @return true if the iterator points to the root of a term subtree.
     * @param[in, out] dictionaryIterator the iterator
     */
    virtual bool isAtTerm(const ULDictionaryDataSourceIterator& dictionaryIterator) = 0;
    
    /**
     * @return true if the iterator points to the root of the very last term subtree in the forest.
     * @param[in, out] dictionaryIterator the iterator
     */
    virtual bool isAtFirstTerm(const ULDictionaryDataSourceIterator& dictionaryIterator) = 0;
    
    /**
     * @return true if the iterator points to the root of the very last renderable subtree in the forest.
     * @param[in, out] dictionaryIterator the iterator
     */
    virtual bool isAtLastTerm(const ULDictionaryDataSourceIterator& dictionaryIterator) = 0;

    /**
     * Moves the specified iterator to point to the root of the definition subtree containing
     * the iterator's current target node. A definition is a subtree intended to provide a translation
     * of the term containing the definition.
     *
     * @return ULError::NoError if the operation is successful. Otherwise, returns ULError::NoSuchObject,
     * in which case dictionaryIterator is unchanged.
     * @param[in, out] dictionaryIterator the iterator
     */
    virtual ULError toCurrentDefinition(ULDictionaryDataSourceIterator& dictionaryIterator) = 0;

    /**
     * Moves the specified iterator to point to the root of the nearest definition subtree coming after the
     * the iterator's target node, but still in the same term as the iterator's target. (That is,
     * toNextDefinition will not move the iterator past the term containing the iterator's target node.)
     *
     * @return ULError::NoError if the operation is successful. Otherwise, returns ULError::NoSuchObject,
     * in which case dictionaryIterator is unchanged.
     * @param[in, out] dictionaryIterator the iterator
     */
    virtual ULError toNextDefinition(ULDictionaryDataSourceIterator& dictionaryIterator) = 0;
    
    /**
     * Moves the specified iterator to point to the root of the definition subtree immediately preceding the one
     * containing the iterator's current target node.
     *
     * @return ULError::NoError if the operation is successful. Otherwise, returns ULError::NoSuchObject,
     * in which case dictionaryIterator is unchanged.
     * @param[in, out] dictionaryIterator the iterator
     */
    virtual ULError toPreviousDefinition(ULDictionaryDataSourceIterator& dictionaryIterator) = 0;

    virtual ULString getText(ULDictionaryDataSourceIterator& dictionaryIterator) = 0;
    virtual ULError getPartOfSpeech(ULDictionaryDataSourceIterator& dictionaryIterator, ULPartOfSpeech& partOfSpeech) = 0;

    /**
     * TODO needs clear documentation. This method does not set the root iterator.
     */
    virtual void getWord(ULDictionaryDataSourceIterator& dictionaryIterator, ULDerivation& word) = 0;

    virtual bool hasPartOfSpeechCategory(ULDictionaryDataSourceIterator& dictionaryIterator, const ULPartOfSpeechCategory& category) = 0;
    virtual bool getWordWithPartOfSpeechCategory(ULDictionaryDataSourceIterator& dictionaryIterator, const ULPartOfSpeechCategory& partOfSpeech, ULDerivation& word) = 0;
    virtual void getTranslationsForPartOfSpeechCategory(ULDictionaryDataSourceIterator& dictionaryIterator,
                                                        const ULPartOfSpeechCategory& category,
                                                        ULList<ULString>& translations) = 0;

    virtual uluint32 getRunTimeFeatureID(uluint32 dataSourceFeatureID);
    
    // Part of Speech localization methods
    virtual ULError getPartOfSpeechLocalization(const ULPartOfSpeech& posp, const ULLanguage& language, ULString& localization) = 0;
    virtual ULError getPartOfSpeechLocalizedAbbreviation(const ULPartOfSpeech& posp, const ULLanguage& language, ULString& abbreviation) = 0;
    virtual ULError getLocalizationForFeatureValue(const ULString& featureValue, const ULLanguage& language, ULString& localization) = 0;
    virtual ULError getLocalizedAbbreviationForFeatureValue(const ULString& featureValue, const ULLanguage& language, ULString& abbreviation) = 0;
    
    virtual void freeInessentialMemory() = 0;
    
protected:
    ULForestType type;
    ULDictionaryDataSource *dictionaryDataSource;

    ULHashTable<ULString, uluint32> featureNameToIDMap;
    ULVector<ULString> featureIDToNameMap;
    ULVector<uluint32> dataSourceIDToRunTimeIDMap;
    virtual void initializeFeatureMaps();
};

#endif

