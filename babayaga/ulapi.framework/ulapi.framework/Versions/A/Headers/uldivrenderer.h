/**
 * @file uldivrenderer.h
 * @brief The interface for the ULDivRenderer class.
 * @author Copyright (C) 2010 Ultralingua, Inc.
 */

#ifndef ULDIVRENDERER_H
#define ULDIVRENDERER_H

#include "ulrenderer.h"

/**
 * @class ULDivRenderer
 * @brief The ULRenderer child that simply wraps everything in a div in an overly simplistic way.
 */
class ULDivRenderer : public ULRenderer
{
public:
    ULDivRenderer();
    ULDivRenderer(const ULDivRenderer& renderer);
    virtual ~ULDivRenderer();

    virtual ULDivRenderer& operator=(const ULDivRenderer& renderer);
    virtual void clear();

    // Required ULRenderer interfaces.
    virtual ULError render(const ULDictionaryIterator& iterator, ULString& result) const;
	
private:
    virtual ULError renderNode(ULDictionaryIterator& iterator, ULString& result, uluint32 depth) const;	
};

#endif

