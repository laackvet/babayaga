/**
 * @file ultaggingrule.h
 * @brief The interface for the ULTaggingRule class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULTAGGINGRULE_H
#define ULTAGGINGRULE_H

#include "ullanguage.h"
#include "ulpartofspeechcategory.h"

/**
 * @class ULTaggingRule
 * @brief A ULTaggingRule object contains a rule for resolving
 * part-of-speech ambiguities.
 */
class ULTaggingRule
{
public:
    ULTaggingRule();
    ULTaggingRule(const ULTaggingRule& other);
    ~ULTaggingRule();

    ULTaggingRule& operator=(const ULTaggingRule& other);
    void clear();

    void addCategory(const ULPartOfSpeechCategory& category);
    const ULList<ULPartOfSpeechCategory>& getCategoryList() const;

    ULString toString() const;

private:
    ULLanguage language;
    ULList<ULPartOfSpeechCategory> categoryList;
};

#endif

