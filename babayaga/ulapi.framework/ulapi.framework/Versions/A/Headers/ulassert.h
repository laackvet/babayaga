/**
 * @file ulassert.h
 * @brief Assertions for ULAPI.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * The UL_ASSERT macro used in ULAPI can have one of several behaviors,
 * depending on compiler switches used in building ULAPI.
 *
 * - If UL_NO_ASSERT is #define'd, then the UL_ASSERT is empty.
 *
 * - if UL_EXTERNAL_ASSERT is #define'd, you need to implement a
 * global function with interface:
 *
 *   void ULExternalAssert(char *condition, char *fileName, int lineNumber)
 *
 * When an assertion fires, ULExternalAssert will get called with
 * appropriate parameters.
 *
 * - If UL_EXTERNAL_ASSERT is not #define'd, but UL_ANSI is, then
 * when an assertion fires, a suitable message is printed to cerr.
 *
 * - If neither UL_NO_ASSERT, UL_EXTERNAL_ASSERT, nor UL_ANSI is
 * #define'd, then the UL_ASSERT macro is empty.
 */

#ifndef ULASSERT_H
#define ULASSERT_H

#ifdef UL_NO_ASSERT
#define UL_ASSERT(a)

#elif defined(UL_EXTERNAL_ASSERT)
#include "assert.h"
//void ULExternalAssert(const char *condition, const char *file, int line) __attribute__((analyzer_noreturn));
//#define UL_ASSERT(a) if (!(a)) ULExternalAssert(#a, __FILE__, __LINE__)
#define UL_ASSERT(a) assert(a)

#elif defined(__ANDROID__)
#define UL_ASSERT(a) do { if(!(a)) { LOGDEBUG("ULDEBUG (%s:%d) assertion failed: %s", __FILE__, __LINE__, #a); } } while(0)

#elif defined(UL_ANSI)
#define UL_ASSERT(a) do { if(!(a)) { cerr << __FILE__ << ":" << __LINE__ << " assertion failed '" << #a << "'" << endl; } } while(0)

// Use this one when you want to run Xcode's static analyzer.
//#define UL_ASSERT(a) do { if(!(a)) { exit(1); } } while(0)

#else
#define UL_ASSERT(a)
#endif

#endif // ULASSERT_H

