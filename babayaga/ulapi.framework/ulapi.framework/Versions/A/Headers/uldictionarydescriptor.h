/**
 * @file uldictionarydescriptor.h
 * @brief The interface for the ULDictionaryDescriptor class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULDICTIONARYDESCRIPTOR_H
#define ULDICTIONARYDESCRIPTOR_H

#include "ullanguage.h"

/**
 * @class ULDictionaryDescriptor
 * @brief A ULDictionaryDescriptor describes a particular type of
 * dictionary data source without uniquely specifying the file or
 * database from which it comes. For example, if there are two
 * English-French Ultralingua dictionary data source modules, either
 * or both of them could be identified by specifying the same
 * ULDictionaryDescriptor.
 *
 * TODO: Decide what this is other than a language pair.
 */

class ULDictionaryDescriptor
{
public:
    ULDictionaryDescriptor();
    ULDictionaryDescriptor(const ULLanguage& first, const ULLanguage& second);
    ULDictionaryDescriptor(const ULDictionaryDescriptor& other);
    virtual ~ULDictionaryDescriptor();

    void clear();
    ULDictionaryDescriptor& operator=(const ULDictionaryDescriptor& other);
    bool operator==(const ULDictionaryDescriptor& other) const;
    bool operator<(const ULDictionaryDescriptor& other) const;

    void setLanguages(const ULLanguage& first, const ULLanguage& second);
    void getLanguages(ULLanguage& first, ULLanguage& second) const;
    const ULLanguage& getFirstLanguage() const;
    const ULLanguage& getSecondLanguage() const;

private:
    ULLanguage firstLanguage;
    ULLanguage secondLanguage;
    // Product? ImporterID?
};

#endif

