/**
 * @file ulmood.h
 * @brief The interface for the ULMood class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULMOOD_H
#define ULMOOD_H

#include "ulenum.h"

/**
 * @class ULMood
 * @brief An enhanced enumerated type used to represent the mood of verbs.
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULMood : public ULEnum
{
public:
    static const ULMood& None;
    static const ULMood& Any;
    static const ULMood& Indicative;
    static const ULMood& Subjunctive;
    static const ULMood& Conditional;
    static const ULMood& Imperative;
    static const ULMood& Jussive;


    ULMood();
    ULMood(const ULMood& other);
    ULMood(int id);
    ULMood(const char *otherStringID);
    virtual ~ULMood();

    virtual void clear();
    ULMood& operator=(const ULMood& other);
    ULMood& operator=(int otherID);
    ULMood& operator=(const char *otherStringID);

    bool operator==(const ULMood& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULMood& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULMood& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULMood& getInstance(int id);



private:
    static int nEnumeratedValues;
    static ULVector<const ULMood *> enumeratedValueVector;
    ULMood(const char *stringID, const char *displayString);
};


#endif

