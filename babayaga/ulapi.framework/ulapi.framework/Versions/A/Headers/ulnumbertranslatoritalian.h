/**
 * @file ulnumbertranslatoritalian.h
 * @brief The interface for the ULNumberTranslatorItalian class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORITALIAN_H
#define ULNUMBERTRANSLATORITALIAN_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorItalian
 * @brief ULNumberTranslatorItalian does number translation in Italian.
 */
class ULNumberTranslatorItalian : public ULNumberTranslator
{
public:
    ULNumberTranslatorItalian();
    ULNumberTranslatorItalian(const ULNumberTranslatorItalian& other);
    virtual ~ULNumberTranslatorItalian();

    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

