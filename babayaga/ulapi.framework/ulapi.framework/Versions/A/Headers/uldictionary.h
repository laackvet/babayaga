/**
 * @file uldictionary.h
 * @brief The interface for the ULDictionary class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULDICTIONARY_H
#define ULDICTIONARY_H

#include "ulworker.h"
#include "ullanguage.h"
#include "uldictionaryiterator.h"

/**
 * @class ULDictionary
 * @brief ULDictionary is the abstract class describing
 * a protocol for accessing mono- or bi-lingual dictionary data.
 */
class ULDictionary : public ULWorker
{
public:
    virtual ~ULDictionary() {}

    /**
     * Creates an iterator pointing to the first ULDictionaryNode in
     * the set of nodes associated with the specified index. An "index"
     * in this context refers to a sorted list of strings in the data
     * source associated with this ULDictionary object. The most common
     * index is the "headword" index, which is an alphabetized list of
     * labels for dictionary entries. Other indexes may support searching
     * for dictionary entries whose definitions contain the search string,
     * or whose "domain" labels (e.g. "Cooking", "Botany", "Computers",
     * "Sports") match the search string, etc.
     *
     * @return ULError::NoError, ULError::MemoryAllocationFailed,
     * or ULError::DataSourceOpenFailed.
     * @param[in] searchLanguage For bilingual dictionaries, this indicates
     * the language of the index in question. (For example, an English-Spanish
     * data source would have both an English headword index and a Spanish
     * headword index, and searchLanguage would tell begin() which of these
     * indexes to use.)
     * @param[in] indexID An enumerated value specifying which index to search.
     * @param[out] iterator An iterator pointing to the first ULDictionaryNode
     * associated with the specified index.
     */
    virtual ULError begin(const ULLanguage& searchLanguage,
                          uluint32 indexID,
                          ULDictionaryIterator& iterator) = 0;

    /**
     * Creates an iterator pointing to the end of the list of
     * ULDictionaryNodes associated with the specified index. An "index"
     * in this context refers to a sorted list of strings in the data
     * source associated with this ULDictionary object. The most common
     * index is the "headword" index, which is an alphabetized list of
     * labels for dictionary entries. Other indexes may support searching
     * for dictionary entries whose definitions contain the search string,
     * or whose "domain" labels (e.g. "Cooking", "Botany", "Computers",
     * "Sports") match the search string, etc.
     *
     * @return ULError::NoError, ULError::MemoryAllocationFailed,
     * or ULError::DataSourceOpenFailed.
     * @param[in] searchLanguage For bilingual dictionaries, this indicates
     * the language of the index in question. (For example, an English-Spanish
     * data source would have both an English headword index and a Spanish
     * headword index, and searchLanguage would tell begin() which of these
     * indexes to use.)
     * @param[in] indexID An enumerated value specifying which index to search.
     * @param[out] iterator An iterator pointing to the end of the ULDictionaryNode
     * list associated with the specified index.
     */
    virtual ULError end(const ULLanguage& searchLanguage,
                        uluint32 indexID,
                        ULDictionaryIterator& iterator) = 0;

    /**
     * Searches the data source(s) associated with this ULDictionary
     * for the closest match to the specified search string, using the
     * specified language and index. An "index" in this context refers
     * to a sorted list of strings in the data source associated with
     * this ULDictionary object. The most common index is the "headword"
     * index, which is an alphabetized list of labels for dictionary
     * entries. Other indexes may support searching for dictionary
     * entries whose definitions contain the search string,
     * or whose "domain" labels (e.g. "Cooking", "Botany", "Computers",
     * "Sports") match the search string, etc.
     *
     * The "closest match" found by this method is closest in the sense
     * of a binary search on a sorted list. More specifically, the closest
     * match is the lexicographically smallest item in the specified index
     * that is greater than or equal to the search string. Thus, for example,
     * if you search for "sp" in English, you'll get an iterator pointing
     * to the first English word starting with "sp" or "Sp" (e.g. "spa").
     *
     * @return ULError::NoError, ULError::MemoryAllocationFailed,
     * or ULError::DataSourceOpenFailed.
     * @param[in] searchString The string you're looking for.
     * @param[in] searchLanguage For bilingual dictionaries, this specifies
     * the direction in which to search--i.e. the language in which
     * searchString is to be interpreted.
     * @param[in] indexID An enumerated value specifying which index to search.
     * @param[out] iterator An iterator pointing to the closest match in
     * the specified index. This iterator will point to a top-level
     * ULDictionaryNode (i.e. one with no parent node).
     */
    virtual ULError find(const ULString& searchString,
                         const ULLanguage& searchLanguage,
                         uluint32 indexID,
                         ULDictionaryIterator& iterator) = 0;

};

#endif

