
/**
 * @file ulcontainers.h
 * @brief Interfaces for a collection of templated container classes.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * These templates are used instead of their STL analogues to make
 * sure we can control their behavior precisely.  Also, ULAPI does
 * not use C++ exceptions to avoid bad interactions with Objective-C,
 * C#, and various mobile platforms.  Since STL uses exceptions
 * heavily, we needed a replacement.
 */

#ifndef ULCONTAINERS_H
#define ULCONTAINERS_H

#include "ultypes.h"
#include "ulassert.h"

// Forward declarations.
template <class T>
class ULList;
template <class K, class V>
class ULHashTable;


/**
 * @class ULPair
 * @brief A ULPair&lt;T, U&gt; object is an ordered pair
 * of objects of the specified types.
 *
 * The generic types T and U must have
 * operators == and < defined.
 */

template <class T, class U>
class ULPair
{
    UL_TEST_FRIEND;

public:
    ULPair();
    ULPair(const T& firstItem, const U& secondItem);
    ULPair(const ULPair<T,U>& p);

    ULPair<T,U>& operator=(const ULPair<T,U>& p);

    bool operator==(const ULPair<T,U>& p) const;
    bool operator<(const ULPair<T,U>& p) const;
    bool operator!=(const ULPair<T,U>& p) const;
    
    const T& getFirst() const;
    const U& getSecond() const;
    void setFirst(const T& f);
    void setSecond(const U& s);

    T first;
    U second;
};

/**
 * Default constructor.
 */
template <class T, class U>
ULPair<T,U>::ULPair()
{
}

/**
 * Constructor that assigns the parameters to the ULPair's components.
 */
template <class T, class U>
ULPair<T,U>::ULPair(const T& firstItem, const U& secondItem)
{
    this->first = firstItem;
    this->second = secondItem;
}

/**
 * Copy constructor.
 */
template <class T, class U>
ULPair<T,U>::ULPair(const ULPair<T,U>& p)
{
    *this = p;
}

/**
 * Assignment operator.
 */
template <class T, class U>
ULPair<T,U>& ULPair<T,U>::operator=(const ULPair<T,U>& p)
{
    if (this != &p) {
        this->first = p.first;
        this->second = p.second;
    }

    return *this;
}

/**
 * Equality operator.
 * @return true if both the first and second components of
 * this pair and p are equal.  Depends on operator== for both T and U.
 */
template <class T, class U>
bool ULPair<T,U>::operator==(const ULPair<T,U>& p) const
{
    return (this->first == p.first && this->second == p.second);
}

/**
 * Inequality operator.
 * @return false if both the first and second components of
 * this pair and p are equal, and true otherwise.  Depends on operator==
 * for both T and U.
 */
template <class T, class U>
bool ULPair<T,U>::operator!=(const ULPair<T,U>& p) const
{
    return !(*this == p);
}


/**
 * Comparison operator.
 * Tests for lexigraphical ordering of the ULPairs.
 * The U element is only used to determine order if the T elements are of equal value.
 * 
 * @param p The second pair in the evaluation
 * @return  True if this pair is lexicographically strictly less than p.
 */
template <class T, class U>
bool ULPair<T,U>::operator<(const ULPair<T,U>& p) const
{
    if (!(this->first == p.first)) {
        return this->first < p.first;
    }

    return this->second < p.second;
}

/**
 * @return the first component of this pair.
 */
template <class T, class U>
const T& ULPair<T,U>::getFirst() const
{
    return this->first;
}

/**
 * @return the second component of this pair.
 */
template <class T, class U>
const U& ULPair<T,U>::getSecond() const
{
    return this->second;
}

/**
 * Assigns a new value to this pair's first component.
 * @param[in] f The new value.
 */
template <class T, class U>
void ULPair<T,U>::setFirst(const T& f)
{
    this->first = f;
}

/**
 * Assigns a new value to this pair's second component.
 * @param[in] s The new value.
 */
template <class T, class U>
void ULPair<T,U>::setSecond(const U& s)
{
    this->second = s;
}


/**
 * @class ULListNode
 * @brief A struct used internally by ULList.
 */

template <class T>
struct ULListNode
{
    ULListNode();
    ULListNode(const T& data);
    ULListNode(const ULListNode<T>& node);

    ULListNode<T>& operator=(const ULListNode<T>& node);

    T data;
    ULListNode *next;
    ULListNode *previous;
};

/**
 * Default constructor.
 */
template <class T>
ULListNode<T>::ULListNode()
{
    this->next = 0;
    this->previous = 0;
}

/**
 * Constructor.
 * @param[in] data The data object with which to initialize
 * this node's data field.
 */
template <class T>
ULListNode<T>::ULListNode(const T& data)
{
    this->next = 0;
    this->previous = 0;
    this->data = data;
}

/**
 * Copy constructor.
 */
template <class T>
ULListNode<T>::ULListNode(const ULListNode<T>& node)
{
    *this = node;
}

/**
 * Assignment operator.
 */
template <class T>
ULListNode<T>& ULListNode<T>::operator=(const ULListNode<T>& node)
{
    if (this != &node) {
        this->data = node.data;
        this->next = node.next;
        this->previous = node.previous;
    }

    return *this;
}


/**
 * @class ULListIterator
 * @brief A class for iterating over ULList objects.
 */
template <class T>
class ULListIterator
{
    friend class ULList<T>;
    UL_TEST_FRIEND;

public:
    ULListIterator();
    ULListIterator(const ULListIterator<T>& it);

    void clear();
    ULListIterator<T>& operator=(const ULListIterator<T>& it);

    operator bool() const;
    bool operator==(const ULListIterator<T>& it) const;
    bool operator!=(const ULListIterator<T>& it) const;

    ULListIterator<T>& operator++();
    ULListIterator<T> operator++(int);
    ULListIterator<T>& operator--();
    ULListIterator<T> operator--(int);
    void increment(); // non-operator synonym for operator++(), to help SWIG
    void decrement(); // non-operator synonym for operator--(), to help SWIG

    T& operator*();
    T& getData(); // non-operator synonym for operator*, to help SWIG

    bool isAtEnd() const;
    bool isAtBeginning() const;
    int getIndex() const;

    ULList<T> *getList();

private:
    ULList<T> *list;
    ULListNode<T> *current;
};

/**
 * Default construcor.
 */
template <class T>
ULListIterator<T>::ULListIterator()
{
    this->current = 0;
    this->list = 0;
}

/**
 * Copy construcor.
 */
template <class T>
ULListIterator<T>::ULListIterator(const ULListIterator<T>& it)
{
    *this = it;
}

/**
 * Sets all data to default values (specifically, pointing to
 * no node in no list).
 */
template <class T>
void ULListIterator<T>::clear()
{
    this->current = 0;
    this->list = 0;
}

/**
 * Assignment operator.
 */
template <class T>
ULListIterator<T>& ULListIterator<T>::operator=(const ULListIterator<T>& it)
{
    if (this != &it) {
        this->current = it.current;
        this->list = it.list;
    }

    return *this;
}

/**
 * @return true if this iterator points to the end of the
 * list with which it is associated.  The "end" of a list is not a
 * node--it's a spot just beyond the final node in the list.  Note
 * that an iterator for which isAtEnd returns true must not be
 * dereferenced using operator*.
 */
template <class T>
bool ULListIterator<T>::isAtEnd() const
{
    return (this->current == 0);
}

/**
 * @return true if this iterator points to the first node
 * of the list with which it is associated.  Note that if the list is
 * empty, isAtBeginning returns true even though there is no "first node."
 */
template <class T>
bool ULListIterator<T>::isAtBeginning() const
{
    return (this->list == 0 || this->current == this->list->frontNode);
}

/**
 * @return the 0-based index of the item within the list to which
 * this iterator points. If this iterator is pointing to the end of the list
 * or to no list at all, getIndex returns -1. Note that getIndex takes
 * time proportional to the index returned.
 */
template <class T>
int ULListIterator<T>::getIndex() const
{
    if (this->list == 0 || this->current == 0) {
        return -1;
    }

    int count = -1;
    const ULListNode<T> *nodePtr;
    for (nodePtr = this->current; nodePtr != 0; nodePtr = nodePtr->previous) {
        count++;
    }

    return count;
}

/**
 * @return a pointer to the list into which this iterator points.
 */
template <class T>
ULList<T> *ULListIterator<T>::getList()
{
    return this->list;
}

/**
 * @return true if this iterator does not point to the end
 * of the list with which it is associated.  A typical use of the boolean
 * operator is to control a loop iterating over the whole list, like this:
 * "for (iterator = list.begin(); iterator; ++iterator) {...}".
 */
template <class T>
ULListIterator<T>::operator bool() const
{
    return !(this->isAtEnd());
}

/**
 * Equality operator.
 */
template <class T>
bool ULListIterator<T>::operator==(const ULListIterator<T>& it) const
{
    return this->current == it.current && this->list == it.list;
}

/**
 * Inequality operator.
 */
template <class T>
bool ULListIterator<T>::operator!=(const ULListIterator<T>& it) const
{
    return !(*this == it);
}

/**
 * Pre-increment operator.  Advances this iterator to the next
 * node in the list, or to the end of the list if no such node
 * exists.
 * @return a reference to this iterator.
 */
template <class T>
ULListIterator<T>& ULListIterator<T>::operator++()
{
    if (this->current != 0) {
        this->current = this->current->next;
    }

    return *this;
}

/**
 * Post-increment operator.  Advances this iterator to the next
 * node in the list, or to the end of the list if no such node
 * exists.
 * @return a copy of this iterator as it was before
 * incrementation occurred.
 */
template <class T>
ULListIterator<T> ULListIterator<T>::operator++(int)
{
    ULListIterator<T> tmp = *this;
    ++(*this);
    return tmp;
}

/**
 * Pre-decrement operator.  Advances this iterator to the previous
 * node in the list, or leaves the iterator unchanged if no such
 * node exists.
 * @return a reference to this iterator.
 */
template <class T>
ULListIterator<T>& ULListIterator<T>::operator--()
{
    UL_ASSERT(this->list != 0);

    if (this->current == this->list->frontNode) {
    } else if (this->current != 0) {
        this->current = this->current->previous;
    } else if (this->list != 0) {
        this->current = this->list->backNode;
    }

    return *this;
}

/**
 * Post-decrement operator.  Advances this iterator to the previous
 * node in the list, or leaves the iterator unchanged if no such
 * node exists.
 * @return a copy of this iterator as it was before
 * decrementation occurred.
 */
template <class T>
ULListIterator<T> ULListIterator<T>::operator--(int)
{
    ULListIterator<T> tmp = *this;
    --(*this);
    return tmp;
}

/**
 * Advances this iterator. This method's main purpose is to give SWIG-based
 * wrappers a non-operator way to increment an iterator.
 */
template <class T>
void ULListIterator<T>::increment()
{
    ++(*this);
}

/**
 * Decrements this iterator. This method's main purpose is to give SWIG-based
 * wrappers a non-operator way to decrement an iterator.
 */
template <class T>
void ULListIterator<T>::decrement()
{
    --(*this);
}

/**
 * @return a reference to the data stored in the node
 * pointed to by this iterator.
 * @pre This iterator must point to a node in a list.  This condition
 * is satisfied if isAtEnd() returns false or operator bool() returns
 * true.
 */
template <class T>
T& ULListIterator<T>::operator*()
{
    UL_ASSERT(this->current != 0);
    return this->current->data;
}

/**
 * Synonym for operator*. This method's main purpose is to provide a
 * non-operator dereferencing method for use by SWIG-based wrappers.
 */
template <class T>
T& ULListIterator<T>::getData()
{
    return *(*this);
}

/**
 * @class ULListConstIterator
 * @brief A class for iterating over lists of type <code>const ULList&</code>.
 */
template <class T>
class ULListConstIterator
{
    friend class ULList<T>;
    UL_TEST_FRIEND;

public:
    ULListConstIterator();
    ULListConstIterator(const ULListConstIterator<T>& it);

    void clear();
    ULListConstIterator<T>& operator=(const ULListConstIterator<T>& it);

    operator bool() const;
    bool operator==(const ULListConstIterator<T>& it) const;
    bool operator!=(const ULListConstIterator<T>& it) const;

    ULListConstIterator<T>& operator++();
    ULListConstIterator<T> operator++(int);
    ULListConstIterator<T>& operator--();
    ULListConstIterator<T> operator--(int);
    void increment(); // non-operator synonym for operator++(), to help SWIG
    void decrement(); // non-operator synonym for operator--(), to help SWIG

    const T& operator*() const;
    const T& getData(); // non-operator synonym for operator*, to help SWIG

    bool isAtEnd() const;
    bool isAtBeginning() const;
    int getIndex() const;

    const ULList<T> *getList() const;

private:
    const ULList<T> *list;
    const ULListNode<T> *current;
};

/**
 * Default constructor.
 */
template <class T>
ULListConstIterator<T>::ULListConstIterator()
{
    this->current = 0;
    this->list = 0;
}

/**
 * Copy constructor.
 */
template <class T>
ULListConstIterator<T>::ULListConstIterator(const ULListConstIterator<T>& it)
{
    *this = it;
}

/**
 * Sets all data to default values (specifically, pointing to
 * no node in no list).
 */
template <class T>
void ULListConstIterator<T>::clear()
{
    this->current = 0;
    this->list = 0;
}

/**
 * Assignment operator.
 */
template <class T>
ULListConstIterator<T>& ULListConstIterator<T>::operator=(const ULListConstIterator<T>& it)
{
    if (this != &it) {
        this->current = it.current;
        this->list = it.list;
    }

    return *this;
}

/**
 * @return true if this iterator points to the end of the
 * list with which it is associated.  The "end" of a list is not a
 * node--it's a spot just beyond the final node in the list.  Note
 * that an iterator for which isAtEnd returns true must not be
 * dereferenced using operator*.
 */
template <class T>
bool ULListConstIterator<T>::isAtEnd() const
{
    return (this->current == 0);
}

/**
 * @return true if this iterator points to the first node
 * of the list with which it is associated.  Note that if the list is
 * empty, isAtBeginning returns true even though there is no "first node."
 */
template <class T>
bool ULListConstIterator<T>::isAtBeginning() const
{
    return (this->list == 0 || this->current == this->list->frontNode);
}

/**
 * @return the 0-based index of the item within the list to which
 * this iterator points. If this iterator is pointing to the end of the list
 * or to no list at all, getIndex returns -1. Note that getIndex takes
 * time proportional to the index returned.
 */
template <class T>
int ULListConstIterator<T>::getIndex() const
{
    if (this->list == 0 || this->current == 0) {
        return -1;
    }

    int count = -1;
    const ULListNode<T> *nodePtr;
    for (nodePtr = this->current; nodePtr != 0; nodePtr = nodePtr->previous) {
        count++;
    }

    return count;
}

/**
 * @return a pointer to the list into which this iterator points.
 */
template <class T>
const ULList<T> *ULListConstIterator<T>::getList() const
{
    return this->list;
}

/**
 * @return true if this iterator does not point to the end
 * of the list with which it is associated.  A typical use of the boolean
 * operator is to control a loop iterating over the whole list, like this:
 * "for (iterator = list.cbegin(); iterator; ++iterator) {...}".
 */
template <class T>
ULListConstIterator<T>::operator bool() const
{
    return !(this->isAtEnd());
}

/**
 * Equality operator.
 */
template <class T>
bool ULListConstIterator<T>::operator==(const ULListConstIterator<T>& it) const
{
    return this->current == it.current && this->list == it.list;
}

/**
 * Inequality operator.
 */
template <class T>
bool ULListConstIterator<T>::operator!=(const ULListConstIterator<T>& it) const
{
    return !(*this == it);
}

/**
 * Pre-increment operator.  Advances this iterator to the next
 * node in the list, or to the end of the list if no such node
 * exists.
 * @return a reference to this iterator.
 */
template <class T>
ULListConstIterator<T>& ULListConstIterator<T>::operator++()
{
    if (this->current != 0) {
        this->current = this->current->next;
    }

    return *this;
}

/**
 * Post-increment operator.  Advances this iterator to the next
 * node in the list, or to the end of the list if no such node
 * exists.
 * @return a copy of this iterator as it was before
 * incrementation occurred.
 */
template <class T>
ULListConstIterator<T> ULListConstIterator<T>::operator++(int)
{
    ULListConstIterator<T> tmp = *this;
    ++(*this);
    return tmp;
}

/**
 * Pre-decrement operator.  Advances this iterator to the previous
 * node in the list, or leaves the iterator unchanged if no such
 * node exists.
 * @return a reference to this iterator.
 */
template <class T>
ULListConstIterator<T>& ULListConstIterator<T>::operator--()
{
    UL_ASSERT(this->list != 0);

    if (this->current == this->list->frontNode) {
    } else if (this->current != 0) {
        this->current = this->current->previous;
    } else if (this->list != 0) {
        this->current = this->list->backNode;
    }

    return *this;
}

/**
 * Post-decrement operator.  Advances this iterator to the previous
 * node in the list, or leaves the iterator unchanged if no such
 * node exists.
 * @return a copy of this iterator as it was before
 * decrementation occurred.
 */
template <class T>
ULListConstIterator<T> ULListConstIterator<T>::operator--(int)
{
    ULListConstIterator<T> tmp = *this;
    --(*this);
    return tmp;
}

/**
 * Advances this iterator. This method's main purpose is to give SWIG-based
 * wrappers a non-operator way to increment an iterator.
 */
template <class T>
void ULListConstIterator<T>::increment()
{
    ++(*this);
}

/**
 * Decrements this iterator. This method's main purpose is to give SWIG-based
 * wrappers a non-operator way to decrement an iterator.
 */
template <class T>
void ULListConstIterator<T>::decrement()
{
    --(*this);
}

/**
 * Dereference operator.
 *
 * @return a const reference to the data stored in the node
 * pointed to by this iterator.
 * @pre This iterator must point to a node in a list.  This condition
 * is satisfied if isAtEnd() returns false or operator bool() returns
 * true.
 */
template <class T>
const T& ULListConstIterator<T>::operator*() const
{
    UL_ASSERT(this->current != 0);
    return this->current->data;
}

/**
 * Synonym for operator*. This method's main purpose is to provide a
 * non-operator dereferencing method for use by SWIG-based wrappers.
 */
template <class T>
const T& ULListConstIterator<T>::getData()
{
    return *(*this);
}

/**
 * @class ULList
 * @brief A doubly-linked list.
 *
 * Any type used as the template parameter T must have operators == and &lt;.
 *
 * Keep in mind the efficiency implications of the fact that ULList is a
 * linked list.  Notably, operator[] takes time proporation to the index
 * you give it.  (The length of the list is stored as an instance variable,
 * however, so length() is a constant-time operation.)
 */
template <class T>
class ULList
{
    friend class ULListIterator<T>;
    friend class ULListConstIterator<T>;
    UL_TEST_FRIEND;

public:
    ULList();
    ULList(const ULList<T>& list);
    ~ULList();

    void clear();
    ULList<T>& operator=(const ULList<T>& list);
    bool operator==(const ULList<T>& list) const;

    T& operator[](uluint32 n) const;
    T& getAtIndex(uluint32 n) const; // non-operator synonym for operator[], to help SWIG
    
    ULListConstIterator<T> find(const T& data) const;

    void erase(uluint32 n);
    void erase(ULListIterator<T>& it);
    void erase(ULListIterator<T>& start, ULListIterator<T>& stop);

    bool empty() const;
    uluint32 size() const;
    uluint32 length() const; // synonym for size

    void push_front(const T& data);
    void push_back(const T& data);
    void append(const T& data); // synonym for push_back
    void prepend(const T& data); // synonym for push_front
    void add(const T& data);
    void insert(ULListIterator<T>& it, const T& data);
    void insert(const T& data);

    void pop();
    T& top();

    ULListIterator<T> begin();
    ULListConstIterator<T> cbegin() const;
    ULListIterator<T> end();
    ULListConstIterator<T> cend() const;
    ULListIterator<T> back();
    ULListConstIterator<T> cback() const;

    // Node-based operations to allow rearrangement of the list
    // without lots of copying when T is large.
    ULListNode<T> *removeNode(ULListIterator<T>& it);
    ULListNode<T> *removeFrontNode();
    void insertNode(ULListIterator<T>& it, ULListNode<T> *node);

    void splice(ULList<T>& tail);
    void spliceToFront(ULList<T>& tail);
    void sort(int (*func)(T&, T&, void *), void *context);
    void mergeSort(int (*func)(T&, T&, void *), void *context);

private:
    uluint32 listLength;
    ULListNode<T> *frontNode;
    ULListNode<T> *backNode;

    void destroy();
};

/**
 * Default constructor.
 */
template <class T>
ULList<T>::ULList()
{
    this->listLength = 0;
    this->frontNode = 0;
    this->backNode = 0;
}

/**
 * Copy constructor.
 */
template <class T>
ULList<T>::ULList(const ULList<T>& list)
{
    this->listLength = 0;
    this->frontNode = 0;
    this->backNode = 0;
    *this = list;
}

/**
 * Destructor.
 */
template <class T>
ULList<T>::~ULList()
{
    this->destroy();
}

/**
 * Private method that frees all the nodes in the list.
 */
template <class T>
void ULList<T>::destroy()
{
    ULListNode<T> *tmp;
    ULListNode<T> *current = this->frontNode;

    while (current != 0) {
        tmp = current;
        current = current->next;
        delete tmp;
    }

    this->listLength = 0;
}

/**
 * Assignment operator.  This operator deletes this list's existing nodes,
 * builds a complete new copy of the specified list, and sets up this list
 * to contain the new copy.
 */
template <class T>
ULList<T>& ULList<T>::operator=(const ULList<T>& list)
{
    if (this == &list) {
        return *this;
    }

    ULListNode<T> *current, *tmp;
    this->destroy();
    this->frontNode = this->backNode = 0;
    this->listLength = 0;
    for (current=list.frontNode; current != 0; current = current->next) {
        tmp = new ULListNode<T>(current->data); // sets next = previous = 0
        if (tmp == 0) {
            return *this;
        }

        tmp->previous = this->backNode;
        if (this->backNode == 0) {
            this->frontNode = tmp;
        } else {
            this->backNode->next = tmp;
        }
        this->backNode = tmp;
        (this->listLength)++;
    }

    return *this;
}

/**
 * @return true if this list and the other list contain identical data.
 * Note that this is one of the methods that requires the type T to have an
 * == operator.
 */
template <class T>
bool ULList<T>::operator==(const ULList<T>& other) const
{
    if (this == &other) {
        return true;
    }
    
    if (this->length() != other.length()) {
        return false;
    }
    
    ULListNode<T> *current = this->frontNode;
    ULListNode<T> *otherCurrent = other.frontNode;
    while (current != 0 && otherCurrent != 0) {
        if (!(current->data == otherCurrent->data)) {
            return false;
        }
        current = current->next;
        otherCurrent = otherCurrent->next;
    }

    return current == 0 && otherCurrent == 0;
}

/**
 * @return a reference to the data stored in the node
 * at the specified 0-based index.
 * @param[in] n The desired index.
 * @pre n &gt;= 0 and n &lt; the length of this list.
 */
template <class T>
T& ULList<T>::operator[](uluint32 n) const
{
    UL_ASSERT(n < this->listLength);
    ULListNode<T> *current = this->frontNode;
    for (uluint32 k=0; current != 0 && k < n; k++) {
        current = current->next;
    }
    
    UL_ASSERT(current != 0);
    return current->data;
}

/**
 * @return a reference to the data stored in the node
 * at the specified 0-based index.
 * @param[in] n The desired index.
 * @pre n &gt;= 0 and n &lt; the length of this list.
 */
template <class T>
T& ULList<T>::getAtIndex(uluint32 n) const
{
    UL_ASSERT(n < this->listLength);
    ULListNode<T> *current = this->frontNode;
    for (uluint32 k=0; current != 0 && k < n; k++) {
        current = current->next;
    }
    
    UL_ASSERT(current != 0);
    return current->data;
}

/**
 * @return the number of nodes in this list.  (Synonym: length().)
 */
template <class T>
uluint32 ULList<T>::size() const
{
    return this->listLength;
}

/**
 * @return the number of nodes in this list.  (Synonym: size().)
 */
template <class T>
uluint32 ULList<T>::length() const
{
    return this->listLength;
}

/**
 * Makes this list an empty list, freeing any nodes that were in the
 * list before clear() was called.
 */
template <class T>
void ULList<T>::clear()
{
    this->destroy();
    this->frontNode = this->backNode = 0;
    this->listLength = 0;
}

/**
 * Searches for the specified data in this list.
 * @return an iterator pointing to the first node
 * in the list that contains the specified data.
 */
template <class T>
ULListConstIterator<T> ULList<T>::find(const T& data) const
{
    ULListConstIterator<T> it = cbegin();
    while (!it.isAtEnd()) {
        if (data == *it) {
            break;
        }

        ++it;
    }

    return it;
}

/**
 * @return true if this list has no nodes.
 */
template <class T>
bool ULList<T>::empty() const
{
    return this->frontNode == 0;
}

/**
 * Erases the node at the specified index.  Note that this
 * operation takes time proportional to n.
 * @param[n] n The 0-based index of the node to be erased.
 * @pre n &gt;= 0 and n < the length of this list.
 */
template <class T>
void ULList<T>::erase(uluint32 n)
{
    ULListNode<T> *current = this->frontNode;
    for (uluint32 k=0; current != 0 && k < n; k++) {
        current = current->next;
    }

    UL_ASSERT(current != 0);

    ULListIterator<T> it;
    it.list = this;
    it.current = current;
    erase(it);
}

/**
 * Erases the node pointed to by the specified iterator.
 * @param[in,out] it An iterator pointing to the node to be
 * erased.  After the erasure, the iterator points to the
 * node immediately following the erased node, or to the end
 * of the list if the erased node was the last one in the list.
 */
template <class T>
void ULList<T>::erase(ULListIterator<T>& it)
{
    if (!it.isAtEnd()) {
        ULListNode<T> *node = removeNode(it);
        delete node;
    }
}

/**
 * Removes all the nodes starting at start, and ending just before
 * stop.  That is, the node pointed to by stop will remain in the list.
 *
 * @param[in] start The first node to be erased.  Note that the iterators
 * start and stop will be rendered invalid by the erasure.
 * @param[in] stop The node following the last node to be erased. If
 * stop is pointing to the end of the list, then all nodes from stop
 * through the end will be erased.
 */
template <class T>
void ULList<T>::erase(ULListIterator<T>& start, ULListIterator<T>& stop)
{
    if (start.isAtBeginning()) {
        ULListIterator<T> it;
        while (!empty()) {
            it = begin();
            if (it.current == stop.current) {
                break;
            }
            erase(it);
        }
    } else {
        ULListIterator<T> previous = start;
        --previous;

        ULListIterator<T> it = start;
        while (!it.isAtEnd() && it.current != stop.current) {
            ULListNode<T> *node = removeNode(it);
            delete node;
            it = previous;
            ++it;
        }
    }
}

/**
 * Inserts a node containing the specified data at the front
 * of this list.  (Synonym: prepend().)
 * @param[in] data The data to be inserted.
 */
template <class T>
void ULList<T>::push_front(const T& data)
{
    ULListNode<T> *newNode = new ULListNode<T>;
    if (newNode == 0) {
        return;
    }

    newNode->data = data;
    newNode->next = this->frontNode;
    newNode->previous = 0;
    if (this->frontNode == 0) {
        this->backNode = newNode;
    } else {
        this->frontNode->previous = newNode;
    }
    this->frontNode = newNode;
    (this->listLength)++;
}

/**
 * Adds a node containing the specified data to the end of this list.
 * (Synonym: append().)
 * @param[in] data The data to be added to the list.
 */
template <class T>
void ULList<T>::push_back(const T& data)
{
    ULListNode<T> *newNode = new ULListNode<T>;
    if (newNode == 0) {
        return;
    }

    newNode->data = data;
    newNode->next = 0;
    newNode->previous = this->backNode;

    if (this->backNode == 0) {
        this->frontNode = newNode;
    } else {
        this->backNode->next = newNode;
    }

    this->backNode = newNode;
    (this->listLength)++;
}

/**
 * Adds a node containing the specified data to the end of this list.
 * (Synonym: push_back().)
 * @param[in] data The data to be added to the list.
 */
template <class T>
void ULList<T>::append(const T& data)
{
    this->push_back(data);
}

/**
 * Inserts a node containing the specified data at the front
 * of this list.  (Synonym: push_front().)
 * @param[in] data The data to be inserted.
 */
template <class T>
void ULList<T>::prepend(const T& data)
{
    this->push_front(data);
}

/**
 * Appends a node containing the specified data to the end of
 * this list only if the data is not yet in the list.
 * @param[in] data The data to add.
 */
template <class T>
void ULList<T>::add(const T& data)
{
    ULListIterator<T> it = begin();
    while (!it.isAtEnd()) {
        if (data == *it) {
            break;
        }
        ++it;
    }

    if (it.isAtEnd()) {
        push_back(data);
    }
}

/**
 * Inserts a node containing the specified data into this list
 * immediately before the node pointed to by the specified iterator.
 *
 * @param[in] it An iterator pointing to the place to insert
 * the new node.
 * @param[in] data The data to be inserted.
 */
template <class T>
void ULList<T>::insert(ULListIterator<T>& it, const T& data)
{
    ULListNode<T> *node = new ULListNode<T>(data);
    if (node != 0) {
        this->insertNode(it, node);
    }
}

/**
 * Inserts the specified data in its proper location in this
 * sorted list.  If this list already has a node with this same
 * data, no new node is added.
 *
 * @param[in] data The data to be inserted.
 * @pre This list is sorted in increasing order.
 */
template <class T>
void ULList<T>::insert(const T& data)
{
    ULListIterator<T> it = begin();
    while (!it.isAtEnd() && *it < data) {
        ++it;
    }

    if (it.isAtEnd() || !(*it == data)) {
        insert(it, data);
    }
}

/**
 * Removes the last node, if any, from this list.
 */
template <class T>
void ULList<T>::pop()
{
    if (!empty()) {
        ULListIterator<T> it = end();
        --it;
        erase(it);
    }
}

/**
 * @return a reference to the data contained in the last node
 * in this list.
 */
template <class T>
T& ULList<T>::top()
{
    UL_ASSERT(!empty());
    return this->backNode->data;
}

/**
 * @return an iterator pointing to the beginning of this list.
 */
template <class T>
ULListIterator<T> ULList<T>::begin()
{
    ULListIterator<T> it;
    it.list = this;
    it.current = this->frontNode;
    return it;
}

/**
 * @return a const iterator pointing to the beginning of this list.
 */
template <class T>
ULListConstIterator<T> ULList<T>::cbegin() const
{
    ULListConstIterator<T> it;
    it.list = this;
    it.current = this->frontNode;
    return it;
}

/**
 * @return an iterator pointing to the end of this list.  As
 * with ULListIterator::isAtEnd(), the "end" refers not to the final
 * node in this list, but to the position just beyond the final node.
 * (Compare to ULList::back().)
 */
template <class T>
ULListIterator<T> ULList<T>::end()
{
    ULListIterator<T> it;
    it.list = this;
    it.current = 0;
    return it;
}

/**
 * @return a const iterator pointing to the end of this list.  As
 * with ULListIterator::isAtEnd(), the "end" refers not to the final
 * node in this list, but to the position just beyond the final node.
 * (Compare to ULList::cback().)
 */
template <class T>
ULListConstIterator<T> ULList<T>::cend() const
{
    ULListConstIterator<T> it;
    it.list = this;
    it.current = 0;
    return it;
}

/**
 * @return an iterator pointing to the last node in this list.
 * (Compare to ULList::end().)
 */
template <class T>
ULListIterator<T> ULList<T>::back()
{
    ULListIterator<T> it;
    it.list = this;
    it.current = this->backNode;
    return it;
}

/**
 * @return a const iterator pointing to the last node in this list.
 * (Compare to ULList::end().)
 */
template <class T>
ULListConstIterator<T> ULList<T>::cback() const
{
    ULListConstIterator<T> it;
    it.list = this;
    it.current = this->backNode;
    return it;
}

/**
 * Removes from this list the node pointed to by the specified iterator.
 *
 * @return a pointer to the node that has been removed.  <strong>The
 * caller of removeNode becomes responsible for deleting the removed
 * node.</strong>
 * @param[in,out] it An iterator pointing to the node to be removed.  After
 * the node is removed, the iterator points to the node that used to follow
 * the removed node (or the end of the list, if the removed node was at
 * the end of the list).
 */
template <class T>
ULListNode<T> *ULList<T>::removeNode(ULListIterator<T>& it)
{
    ULListNode<T> *node = it.current;

    UL_ASSERT(it.list == this);
    if (it.current != 0 && it.list == this) {
        if (it.current == this->frontNode) {
            this->frontNode = this->frontNode->next;
        }

        if (it.current == this->backNode) {
            this->backNode = this->backNode->previous;
        }

        if (it.current->next != 0) {
            it.current->next->previous = it.current->previous;
        }

        if (it.current->previous != 0) {
            it.current->previous->next = it.current->next;
        }

        // This enables you to iterate through a list and safely remove nodes
        // based on some criterion without corrupting the iterator in the process.
        it.current = it.current->next;
    }

    (this->listLength)--;
    return node;
}

/**
 * Removes the front node from this list.
 * @return a pointer to the removed node.  (Note that the
 * caller becomes responsible for the later deletion of this node.)
 */
template <class T>
ULListNode<T> *ULList<T>::removeFrontNode()
{
    ULListIterator<T> it = this->begin();
    return removeNode(it);
}

/**
 * Inserts a node into this list at the location of the specified iterator.
 *
 * @param[in,out] it An iterator pointing to the node in front of which the
 * new node is to be inserted.  After the insertion, this iterator points
 * to the new node.
 * @param[in] node A pointer to the node to be inserted.  (Note that this
 * list becomes responsible for the later deletion of this node.)
 */
template <class T>
void ULList<T>::insertNode(ULListIterator<T>& it, ULListNode<T> *node)
{
    UL_ASSERT(node != 0);
    UL_ASSERT(it.list == this);
    if (node == 0 || it.list != this) {
        return;
    }

    node->next = it.current;
    if (it.current == 0) {
        node->previous = this->backNode;
        if (this->backNode != 0) {
            this->backNode->next = node;
        }
        this->backNode = node;
    } else {
        node->previous = it.current->previous;
        if (it.current->previous != 0) {
            it.current->previous->next = node;
        }

        it.current->previous = node;
    }

    if (it.current == this->frontNode) {
        this->frontNode = node;
    }

    this->listLength++;
    it.current = node;
}

/**
 * Remove the nodes from the tail list and append them to the end
 * of this list.  This operation does not involve copying nodes,
 * and thus runs in constant time.
 *
 * @param[inout] tail The list to be spliced onto this list.
 */
template <class T>
void ULList<T>::splice(ULList<T>& tail)
{
    // Adjust this list.
    if (this->listLength == 0) {
        this->frontNode = tail.frontNode;
    } else {
        this->backNode->next = tail.frontNode;
    }

    if (tail.frontNode != 0) {
        tail.frontNode->previous = this->backNode;
    }

    this->listLength += tail.listLength;
    if (tail.listLength != 0) {
        this->backNode = tail.backNode;
    }

    // Remove spliced nodes from tail list so they don't get deleted when tail gets deleted.
    tail.listLength = 0;
    tail.frontNode = 0;
    tail.backNode = 0;
}

/**
 * Remove the nodes from the head list and prepend them to the beginning
 * of this list.  This operation does not involve copying nodes,
 * and thus runs in constant time.
 *
 * @param[inout] headList The list to be spliced onto the front of this list.
 */
template <class T>
void ULList<T>::spliceToFront(ULList<T>& headList)
{
    // Nothing to do if headList is empty.
    if (headList.listLength == 0) {
        return;
    }

    // Adjust this list.
    if (this->listLength == 0) {
        this->frontNode = headList.frontNode;
        this->backNode = headList.backNode;
        this->listLength = headList.listLength;
    } else {
        UL_ASSERT(this->frontNode != 0);
        UL_ASSERT(headList.backNode != 0);
        headList.backNode->next = this->frontNode;
        this->frontNode->previous = headList.backNode;
        this->frontNode = headList.frontNode;
        this->listLength += headList.listLength;
    }

    // Remove spliced nodes from tail list so they don't get deleted when headList gets deleted.
    headList.listLength = 0;
    headList.frontNode = 0;
    headList.backNode = 0;
}

/**
 * Sorts this list using the specified comparison function
 * and quicksort.
 *
 * @param[in] func This funciton pointer takes two objects of
 * type T and the context pointer and returns -1, 0, or 1 to
 * indicate whether the first object is less than, equal to, or
 * greater than the second object.
 */
template <class T>
void ULList<T>::sort(int (*func)(T&, T&, void *), void *context)
{
    // this function implements a qsort algorithm with func() as the
    // comparison function
    
    // it relies on the internal structure of ULList, and tries to perform
    // an efficient sort. No objects are copied, just nodes rearranged and some
    // temporary ULList instances created on the stack
    
    if (this->frontNode == this->backNode) { // this is a fast way to see if size() is 0 or 1
        return; // a list of 0 or 1 elements is always sorted
    }
    
    // take the first node in the list as the pivot
    ULListNode<T> *pivot = this->removeFrontNode();
    
    ULList<T> lteqList, gtList; // our two sublists
    
    // iterate the list, removing elements and adding them to the sublists
    while (!empty()) {
        ULListNode<T> *cur = this->removeFrontNode();
        ULList<T>& list =
            func(cur->data, pivot->data, context) <= 0
            ? lteqList
            : gtList;

        ULListIterator<T> it = list.begin();
        list.insertNode(it, cur);
    }
    
    // the list is divided, now recurse!
    lteqList.sort(func, context);
    gtList.sort(func, context);
    
    // now we rebuild our own list from the pivot and the two others
    
    // add the <= list
    while (!lteqList.empty()) {
        ULListNode<T> *node = lteqList.removeFrontNode();
        ULListIterator<T> it = this->end();
        this->insertNode(it, node);
    }
    
    // add the pivot
    ULListIterator<T> it = this->end();
    this->insertNode(it, pivot);
    
    // add the > list
    while (!gtList.empty()) {
        ULListNode<T> *node = gtList.removeFrontNode();
        ULListIterator<T> it = this->end();
        this->insertNode(it, node);
    }
    
    // the list is now sorted, and there's no cleanup
}

/**
 * Sorts this list using the specified comparison function
 * and mergesort (which runs in O(n log n) time).
 *
 * @param[in] func This funciton pointer takes two objects of
 * type T and the context pointer and returns -1, 0, or 1 to
 * indicate whether the first object is less than, equal to, or
 * greater than the second object.
 */
template <class T>
void ULList<T>::mergeSort(int (*func)(T&, T&, void*), void *context)
{
    // this function implements an in-place mergesort using
    // func() as the comparison function

    // it relies on the internal structure of ULList and
    // avoids copying any objects

    if (this->frontNode == this->backNode) {
        return; // a list of 0 or 1 elements is always sorted
    }

    // first, split ourselves into two sublists
    // we go even/odd so we don't have to iterate twice
    ULList<T> evenList, oddList;
    for (bool odd = false; !this->empty(); odd = !odd) {
        ULListNode<T> *front = this->removeFrontNode();
        ULList<T>& list = odd ? oddList : evenList;
        ULListIterator<T> it = list.begin();
        list.insertNode(it, front);
    }

    // recurse on the sublists
    evenList.mergeSort(func, context);
    oddList.mergeSort(func, context);

    // now merge
    while (!evenList.empty() && !oddList.empty()) {
        ULList<T>& list =
            func(evenList.frontNode->data, oddList.frontNode->data, context) <= 0
              ? evenList
              : oddList;
        ULListIterator<T> it = this->end();
        this->insertNode(it, list.removeFrontNode());
    }

    // scoop up stragglers from whichever list has stuff left
    while (!evenList.empty()) {
        ULListIterator<T> it = this->end();
        this->insertNode(it, evenList.removeFrontNode());
    }
    while (!oddList.empty()) {
        ULListIterator<T> it = this->end();
        this->insertNode(it, oddList.removeFrontNode());
    }

    // all done!
}


/**
 * @class ULHashTableIterator
 * @brief A class used to iterate over the keys in a ULHashTable.
 * Iteration is not in a readily predictable or sorted order, but
 * it does visit each key exactly once.
 */
template <class K, class V>
class ULHashTableIterator
{
    friend class ULHashTable<K,V>;
    UL_TEST_FRIEND;

public:
    ULHashTableIterator();
    ULHashTableIterator(const ULHashTableIterator& it);
    ~ULHashTableIterator();

    const ULHashTableIterator& operator=(const ULHashTableIterator& it);
    const ULPair<K,V>& operator*() const;
    void operator++();
    void operator++(int);
    operator bool() const;

    bool isAtEnd() const;
    uluint32 getHashIndex() const;

private:
    const ULHashTable<K,V> *hashTable;
    uluint32 hashIndex;
    ULListConstIterator< ULPair<K,V> > listIterator;
};

/**
 * Default constructor.
 */
template <class K, class V>
ULHashTableIterator<K,V>::ULHashTableIterator()
    : hashTable(0), hashIndex(0)
{
}

/**
 * Copy constructor.
 */
template <class K, class V>
ULHashTableIterator<K,V>::ULHashTableIterator(const ULHashTableIterator& it)
    : hashTable(0), hashIndex(0)
{
    *this = it;
}

/**
 * Destructor.
 */
template <class K, class V>
ULHashTableIterator<K,V>::~ULHashTableIterator()
{
}

/**
 * Assignment operator.
 */
template <class K, class V>
const ULHashTableIterator<K,V>& ULHashTableIterator<K,V>::operator=(const ULHashTableIterator& it)
{
    if (&it != this) {
        this->hashTable = it.hashTable;
        this->hashIndex = it.hashIndex;
        this->listIterator = it.listIterator;
    }

    return *this;
}

/**
 * Dereference operator.
 *
 * @return a const reference to the (key, value) pair
 * pointed to by this iterator.
 */
template <class K, class V>
const ULPair<K,V>& ULHashTableIterator<K,V>::operator*() const
{
    UL_ASSERT(!isAtEnd());
    return *(this->listIterator);
}

/**
 * Pre-increment operator.  Advances this iterator to the next
 * (key, value) pair in the hash table it points to.  Note that
 * neither this nor the post-increment operator returns a value.
 */
template <class K, class V>
void ULHashTableIterator<K,V>::operator++()
{
    if (this->hashTable == 0) {
        return;
    }

    ++(this->listIterator);
    if (this->listIterator.isAtEnd()) {
        this->hashIndex++;
        this->listIterator = this->hashTable->table[this->hashIndex].cbegin();
        while (this->hashIndex < this->hashTable->tableSize && this->listIterator.isAtEnd()) {
            this->hashIndex++;
            this->listIterator = this->hashTable->table[this->hashIndex].cbegin();
        }
    }
}

/**
 * Post-increment operator.  Advances this iterator to the next
 * (key, value) pair in the hash table it points to.  Note that
 * neither this nor the pre-increment operator returns a value.
 */
template <class K, class V>
void ULHashTableIterator<K,V>::operator++(int)
{
    ++(*this);
}

/**
 * @return true if this iterator does not point to the end
 * of the sequence of (key, value) pairs in the hash table. A typical use
 * of the boolean operator is to control a loop iterating over the whole
 * hash table, like this:
 * "for (iterator = table.begin(); iterator; ++iterator) {...}".
 */
template <class K, class V>
ULHashTableIterator<K,V>::operator bool() const
{
    return !(this->isAtEnd());
}

/**
 * @return true if this iterator points to the end of the
 * sequence of (key, value) pairs in the the hash table.  The "end"
 * of this sequence is not a (key, value) pair--it's a spot just
 * beyond the final pair in the sequence.  Note that an iterator for
 * which isAtEnd returns true must not be dereferenced using operator*.
 */
template <class K, class V>
bool ULHashTableIterator<K,V>::isAtEnd() const
{
    return this->hashTable == 0 || this->hashIndex >= this->hashTable->tableSize;
}

/**
 * @return the hash value to which this iterator currently points.
 */
template <class K, class V>
uluint32 ULHashTableIterator<K,V>::getHashIndex() const
{
    return this->hashIndex;
}

/**
 * @class ULHashTable
 * @brief A simple implementation of a hash table.
 *
 * Types used as the template parameter K must have a method
 * with signature uluint32 hash(uluint32 tableSize) that returns
 * a hash value between 0 and tableSize - 1.
 *
 * Each of the slots in this hash table consists of a ULList
 * of (key, value) pairs.  That is, the table uses chaining
 * to resolve collisions.
 */
template <class K, class V>
class ULHashTable
{
    friend class ULHashTableIterator<K,V>;
    UL_TEST_FRIEND;

public:
    ULHashTable();
    ULHashTable(uluint32 tableSize);
    ~ULHashTable();

    void clear();
    ULHashTable<K,V>& operator=(const ULHashTable<K,V>& table);

    uluint32 getTableSize() const;
    uluint32 getKeyCount() const;
    bool resize(uluint32 tableSize);

    void add(const K& key, const V& value);
    void add(const ULPair<K,V>& keyValuePair);

    bool find(const K& key, V& value) const;
    bool find(const K& key) const;

    ULHashTableIterator<K,V> end() const;
    ULHashTableIterator<K,V> begin() const;

    void getStats(double& avgListLength, uluint32& maxListLength);

private:
    ULList< ULPair<K,V> > *table;
    uluint32 tableSize;
    uluint32 keyCount;
};

/**
 * Default constructor.  Sets the table size to zero.  Do not
 * try to use a ULHashTable whose table size is 0.  Make sure to
 * call resize with a non-zero parameter first.
 */
template <class K, class V>
ULHashTable<K,V>::ULHashTable()
    : table(0), tableSize(0), keyCount(0)
{
}

/**
 * Constructor.
 * @param[in] initialTableSize The desired table size.
 */
template <class K, class V>
ULHashTable<K,V>::ULHashTable(uluint32 initialTableSize)
{
    this->table = 0;
    this->tableSize = 0;
    this->keyCount = 0;
    if (initialTableSize > 0) {
        this->table = new ULList< ULPair<K,V> >[initialTableSize];
        if (this->table != 0) {
            this->tableSize = initialTableSize;
        }
    }
}

/**
 * Destructor.
 */
template <class K, class V>
ULHashTable<K,V>::~ULHashTable()
{
    delete [] this->table;
}

/**
 * Removes all items from the hash table, leaving the table the same size
 * as before, but empty.
 */
template <class K, class V>
void ULHashTable<K,V>::clear()
{
    if (this->table != 0) {
        for (uluint32 k=0; k < this->tableSize; k++) {
            this->table[k].clear();
        }
        this->keyCount = 0;
    }
}

/**
 * Assignment operator.  Note that this makes a complete new copy of
 * the hash table, so it's not normally a good idea to invoke this
 * function.
 */
template <class K, class V>
ULHashTable<K,V>& ULHashTable<K,V>::operator=(const ULHashTable<K,V>& otherTable)
{
    delete [] this->table;
    this->table = 0;
    this->tableSize = 0;
    this->keyCount = 0;
    if (otherTable.tableSize > 0) {
        this->table = new ULList< ULPair<K,V> >[otherTable.tableSize];
    }

    if (this->table != 0) {
        this->tableSize = otherTable.tableSize;
        for (uluint32 k=0; k < this->tableSize; k++) {
            this->table[k] = otherTable.table[k];
        }
        this->keyCount = otherTable.keyCount;
    }

    return *this;
}

/**
 * @return the number of slots in this hash table (<em>not</em>
 * the number of (key, value) pairs).
 */
template <class K, class V>
uluint32 ULHashTable<K,V>::getTableSize() const
{
    return this->tableSize;
}

/**
 * @return the number of (key, value) pairs in this hash table.
 */
template <class K, class V>
uluint32 ULHashTable<K,V>::getKeyCount() const
{
    return this->keyCount;
}

/**
 * Changes the size of this hash table.  After the new table is
 * allocated, the (key, value) pairs in the old table are rehashed
 * for insertion into the new table.
 *
 * @return true if the resizing is successful.
 * @param[in] tableSize The desired new table size.
 */
template <class K, class V>
bool ULHashTable<K,V>::resize(uluint32 tableSize)
{
    if (tableSize == 0 || tableSize == this->tableSize) {
        return true;
    }

    ULList< ULPair<K,V> > *newTable = new ULList< ULPair<K,V> >[tableSize];
    if (newTable == 0) {
        return false;
    }

    if (this->tableSize != 0) {
        uluint32 hashValue;
        ULListConstIterator< ULPair<K,V> > it;
        this->keyCount = 0;
        for (uluint32 k=0; k < this->tableSize; k++) {
            for (it = this->table[k].cbegin(); it; ++it) {
                hashValue = (*it).getFirst().hash(tableSize);
                UL_ASSERT(hashValue < tableSize);
                newTable[hashValue].push_back(*it);
                this->keyCount++;
            }
        }
    }

    delete [] this->table;
    this->table = newTable;
    this->tableSize = tableSize;

    return true;
}

/**
 * Adds the specified (key, value) pair to this hash table.
 * @param[in] key The key.
 * @param[in] value The value to associate with the key.
 */
template <class K, class V>
void ULHashTable<K,V>::add(const K& key, const V& value)
{
    ULPair<K,V> keyValuePair;
    keyValuePair.setFirst(key);
    keyValuePair.setSecond(value);
    this->add(keyValuePair);
}

/**
 * Adds a (key, value) pair to this hash table if the specified
 * key is not already present. If the key is already in the table,
 * this method changes its associated value.
 * @param[in] key The key.
 * @param[in] value The value to associate with the key.
 */
template <class K, class V>
void ULHashTable<K,V>::add(const ULPair<K,V>& keyValuePair)
{
    uluint32 hashValue = keyValuePair.getFirst().hash(this->tableSize);
    UL_ASSERT(hashValue < this->tableSize);
    if (hashValue < this->tableSize) {
        ULListIterator< ULPair<K,V> > it;
        for (it = this->table[hashValue].begin(); it; ++it) {
            if (keyValuePair.getFirst() == (*it).getFirst()) {
                (*it).setSecond(keyValuePair.getSecond());
                return;
            }
        }

        this->table[hashValue].push_back(keyValuePair);
        this->keyCount++;
    }
}

/**
 * Searches this table for the value associated with the specified key.
 * @return true if the specified key is in this table.
 * @param[in] key The key.
 * @param[out] value The value corresponding to the key (if the key
 * is in this table). value is unchanged if the key is not found.
 */
template <class K, class V>
bool ULHashTable<K,V>::find(const K& key, V& value) const
{
    uluint32 hashValue = key.hash(this->tableSize);
    UL_ASSERT(hashValue < this->tableSize);
    if (hashValue < this->tableSize) {
        ULListConstIterator< ULPair<K,V> > it;
        for (it = this->table[hashValue].cbegin(); it; ++it) {
            if (key == (*it).getFirst()) {
                value = (*it).getSecond();
                return true;
            }
        }
    }

    return false;
}

/**
 * @return true if the specified key is in this hash table.
 */
template <class K, class V>
bool ULHashTable<K,V>::find(const K& key) const
{
    uluint32 hashValue = key.hash(this->tableSize);
    UL_ASSERT(hashValue < this->tableSize);
    ULListConstIterator< ULPair<K,V> > it;
    for (it = this->table[hashValue].cbegin(); it; ++it) {
        if (key == (*it).getFirst()) {
            return true;
        }
    }

    return false;
}

/**
 * @return an iterator pointing to the end of the sequence
 * of (key, value) pairs stored in this hash table. The "end"
 * of this sequence is not a (key, value) pair--it's a spot just
 * beyond the final pair in the sequence.  Note that an iterator for
 * which isAtEnd returns true must not be dereferenced using operator*.
 */
template <class K, class V>
ULHashTableIterator<K,V> ULHashTable<K,V>::end() const
{
    ULHashTableIterator<K,V> it;
    it.hashTable = this;
    it.hashIndex = this->tableSize;
    return it;
}

/**
 * @return an iterator pointing to the first (key, value)
 * pair stored in this hash table.
 */
template <class K, class V>
ULHashTableIterator<K,V> ULHashTable<K,V>::begin() const
{
    ULHashTableIterator<K,V> it;
    it.hashTable = this;
    it.hashIndex = 0;
    it.listIterator = this->table[it.hashIndex].cbegin();
    while (it.hashIndex < this->tableSize && it.listIterator.isAtEnd()) {
        it.hashIndex++;
        it.listIterator = this->table[it.hashIndex].cbegin();
    }

    return it;
}

/**
 * Used for program tuning, this function reports statistics about
 * the distribution of keys in this hash table.
 *
 * @param[out] avgChainLength This gets the mean length of the
 * non-empty chains in this hash table.
 * @param[out] maxChainLength This gets the longest chain length
 * in this table.
 */
template <class K, class V>
void ULHashTable<K,V>::getStats(double& avgChainLength, uluint32& maxChainLength)
{
    maxChainLength = 0;
    uluint32 chainLengthTotal = 0;
    uluint32 nNonemptyChains = 0;
    for (uluint32 k=0; k < this->tableSize; k++) {
        uluint32 chainLength = this->table[k].size();
        chainLengthTotal += chainLength;
        if (chainLength > maxChainLength) {
            maxChainLength = chainLength;
        }
        if (chainLength > 0) {
            nNonemptyChains++;
        }
    }

    if (nNonemptyChains > 0) {
        avgChainLength = double(chainLengthTotal) / double(nNonemptyChains);
    } else {
        avgChainLength = 0;
    }
}


/**
 * @class ULVector
 * @brief A resizable array, with constant-time access by index.
 * Slots in the vector are pointers of type T* rather than
 * objects of type T, so unused slots take up only the memory
 * required to store a pointer.
 *
 * TODO: this container needs extension if it is to be used more
 * generally.  It's a bare-bones vector at the moment.
 */
template <class T>
class ULVector
{
    UL_TEST_FRIEND;

public:
    ULVector();
    ULVector(uluint32 newSize);
    ULVector(const ULVector<T>& v);
    ~ULVector();

    ULVector<T>& operator=(const ULVector<T>& v);
    void clear();
    T& operator[](uluint32 n);
    uluint32 size() const;
    void resize(uluint32 newSize);
    void append(const T& item);

private:
    T **vector;
    uluint32 vectorSize;
};

/**
 * Default constructor.  This sets the vector size to zero.
 */
template <class T>
ULVector<T>::ULVector()
{
    this->vector = 0;
    this->vectorSize = 0;
}

/**
 * Constructor.
 * @param[in] newSize The desired vector length.
 */
template <class T>
ULVector<T>::ULVector(uluint32 newSize)
{
    this->vector = 0;
    this->vectorSize = 0;
    resize(newSize);
}

/**
 * Copy constructor.
 */
template <class T>
ULVector<T>::ULVector(const ULVector<T>& v)
{
    this->vector = 0;
    this->vectorSize = 0;
    *this = v;
}

/**
 * Destructor.
 */
template <class T>
ULVector<T>::~ULVector()
{
    this->clear();
}

/**
 * Frees all memory associated with this vector, and sets the
 * vector size to zero.
 */
template <class T>
void ULVector<T>::clear()
{
    if (this->vector != 0) {
        for (uluint32 k=0; k < this->vectorSize; k++) {
            delete this->vector[k];
        }
        delete [] this->vector;
    }

    this->vector = 0;
    this->vectorSize = 0;
}

/**
 * Assignment operator.  This operator deletes this vector's existing nodes,
 * builds a complete new copy of the specified vector, and sets up this vector
 * to contain the new copy.
 */
template <class T>
ULVector<T>& ULVector<T>::operator=(const ULVector<T>& v)
{
    if (this == &v) {
        return *this;
    }

    this->clear();
    this->resize(v.size());
    if(this->vector != 0) {
        for (uluint32 k=0; k < this->vectorSize; k++) {
            (*this)[k] = v[k];
        }
    }

    return *this;
}

/**
 * @return a reference to the vector element at the specified index.
 * @param[in] n The index of the desired data.
 */
template <class T>
T& ULVector<T>::operator[](uluint32 n)
{
    UL_ASSERT(n < (1 << 30));
    if (n >= this->vectorSize) {
        uluint32 newSize = (this->vectorSize == 0 ? 16 : 2 * this->vectorSize);
        while (newSize <= n) {
            newSize *= 2;
        }
        UL_ASSERT(newSize <= (1 << 30));

        this->resize(newSize);
    }

    UL_ASSERT(n < this->vectorSize);
    if (this->vector[n] == 0) {
        this->vector[n] = new T;
    }

    return *(this->vector[n]);
}

/**
 * @return the size (i.e. the number of slots) in this vector.
 */
template <class T>
uluint32 ULVector<T>::size() const
{
    return this->vectorSize;
}

/**
 * Changes the size of this vector to the specified size, and moves
 * the data in the old vector to the new one.  If the new size is
 * smaller than the old, the excess items in the old vector are
 * deleted.
 *
 * @param[in] newSize The desired vector size.
 */
template <class T>
void ULVector<T>::resize(uluint32 newSize)
{
    if (newSize != this->vectorSize) {
        if (newSize == 0) {
            clear();
        } else {
            typedef T *TPtr;
            T **newVector = new TPtr[newSize];
            if (newVector != 0) {
                uluint32 k;
                for (k=0; k < newSize && k < this->vectorSize; k++) {
                    newVector[k] = this->vector[k];
                    this->vector[k] = 0;
                }

                for ( ; k < newSize; k++) {
                    newVector[k] = 0;
                }

                this->clear();
                this->vector = newVector;
                this->vectorSize = newSize;
            }
        }
    }
}

template <class T>
void ULVector<T>::append(const T& item)
{
}

#endif
