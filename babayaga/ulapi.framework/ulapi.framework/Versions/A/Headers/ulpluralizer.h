/**
 * @file ulpluralizer.h
 * @brief The interface for the ULPluralizer class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULPLURALIZER_H
#define ULPLURALIZER_H

#include "ulworker.h"
#include "ulinflector.h"
#include "uldissector.h"

/**
 * @class ULPluralizer
 * @brief A simplified interface to the noun pluralization services provided by ULInflector.
 */
class ULPluralizer : public ULWorker
{
public:
    ULPluralizer();
    ULPluralizer(const ULPluralizer& other);
    virtual ~ULPluralizer();

    ULPluralizer& operator=(const ULPluralizer& other);
    void clear();

    // Accessors
    ULInflector *getInflector();
    void setInflector(ULInflector *newInflector);
    ULDissector *getDissector();
    void setDissector(ULDissector *newDissector);
    const ULLanguage& getLanguage() const;

    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The core services provided by the pluralizer.
    ULError getPlural(const ULString& word, ULString& plural); 
    ULError getAllPlurals(const ULString& word, ULList<ULString>& pluralList);
    ULError getPluralFromSingular(const ULString& singular, ULString& plural); 
    ULError getAllPluralsFromSingular(const ULString& singular, ULList<ULString>& pluralList);

private:
    ULInflector *inflector;
    ULDissector *dissector;

    void getPluralNouns(const ULDerivation& singularNoun, ULList<ULString>& pluralList);
};

#endif
