/**
 * @file ulnumbertranslatorportuguese.h
 * @brief The interface for the ULNumberTranslatorPortuguese class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORPORTUGUESE_H
#define ULNUMBERTRANSLATORPORTUGUESE_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorPortuguese
 * @brief ULNumberTranslatorPortuguese does number translation in Portuguese.
 */
class ULNumberTranslatorPortuguese : public ULNumberTranslator
{
public:
    ULNumberTranslatorPortuguese();
    ULNumberTranslatorPortuguese(const ULNumberTranslatorPortuguese& other);
    virtual ~ULNumberTranslatorPortuguese();

    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

