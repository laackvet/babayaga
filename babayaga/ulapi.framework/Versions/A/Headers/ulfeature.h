/**
 * @file ulfeature.h
 * @brief The interface for the ULFeature  class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULFEATURE_H
#define ULFEATURE_H

#include "ulstring.h"
#include "ulfeaturetype.h"

/**
 * @class ULFeature 
 * @brief The class for storing the ULFeatureType and value (integer, enumerated type,
 * or string) associated with the type in some context. ULAPI objects like ULPartOfSpeech and
 * ULDictionaryNode contain lists of ULFeature objects (among other things).
 */
class ULFeature 
{
public:
    ULFeature ();
    ULFeature (const ULFeatureType& type, int value);
    ULFeature (const ULFeature & other);
    ~ULFeature ();

    void clear();
    ULFeature & operator=(const ULFeature & other);

    bool operator==(const ULFeature & other) const;
    bool operator!=(const ULFeature & other) const;
    bool operator<(const ULFeature & other) const;

    uluint32 hash(uluint32 tableSize) const;

    const ULFeatureType& getFeatureType() const;
    void setFeatureType(const ULFeatureType& newFeatureType);
    int getIntValue() const;
    void setIntValue(int newIntValue);
    bool hasStringValue() const;
    void getStringValue(ULString& val) const;
    const ULString& getStringValue() const;
    void setStringValue(const ULString& newStringValue);

    ULString getDisplayValue() const;
    ULString toString() const;

private:
    ULFeatureType featureType;
    int intValue;
    ULString *stringValue;
};

#endif

