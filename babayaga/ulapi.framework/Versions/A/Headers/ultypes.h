/**
 * @file ultypes.h
 * @brief Some definitions that are used throughout ULAPI,
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULTYPES_H
#define ULTYPES_H

#include "ultest.h"

#ifdef __APPLE__
#ifndef UL_ANSI
#define UL_ANSI
#endif
#endif

#ifdef __ANDROID__
#include <android/log.h>
#define  ANDROID_ULAPI_TAG "ULAPI"
#define  LOGDEBUG(...)  __android_log_print(ANDROID_LOG_DEBUG, ANDROID_ULAPI_TAG, __VA_ARGS__)
#define  LOGERROR(...)  __android_log_print(ANDROID_LOG_ERROR, ANDROID_ULAPI_TAG, __VA_ARGS__)
#endif

#if defined(UL_ANSI)
#include <iostream>
#include <fstream>
#include <string.h>

using namespace std;

typedef unsigned char uluint8;
typedef unsigned short uluint16;
typedef short ulint16;
typedef unsigned int uluint32;
typedef int ulint32;
#else

#include <string.h>

typedef unsigned char uluint8;
typedef unsigned short uluint16;
typedef short ulint16;
typedef unsigned int uluint32;
typedef int ulint32;
#endif

#endif // ULTYPES_H

