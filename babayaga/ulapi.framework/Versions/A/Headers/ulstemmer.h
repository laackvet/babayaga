/**
 * @file ulstemmer.h
 * @brief The interface for the ULStemmer class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULSTEMMER_H
#define ULSTEMMER_H

#include "ulworker.h"
#include "uldissector.h"
#include "ulfrequency.h"

/**
 * @class ULStemmer
 * @brief A worker that computes the stems of words. For example, the
 * French word "couvent" is both a singular noun and the third person plural
 * form of the verb "couver". Thus, a French ULStemmer object would identify
 * both "couvent n.m." and "couver v." as stems for "couvent".
 */
class ULStemmer : public ULWorker
{
public:
    ULStemmer();
    ULStemmer(const ULStemmer& other);
    virtual ~ULStemmer();

    ULStemmer& operator=(const ULStemmer& other);
    void clear();

    // Accessors
    ULDissector *getDissector();
    void setDissector(ULDissector *newDissector);
    const ULLanguage& getLanguage() const;

    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);
    virtual void setCancelOperation(bool shouldCancel);

    // The stemmer's core services.
    ULError getAllStems(const ULString& surfaceForm, ULList<ULDerivation>& stemList);

    ULError getStems(const ULString& surfaceForm, ULList<ULDerivation>& stemList);

    ULError getStems(const ULString& surfaceForm,
                     const ULPartOfSpeechCategory& category,
                     ULList<ULDerivation>& stemList);
    
    ULError getFrequencies(const ULString& surfaceForm, ULList<ULFrequency>& frequencyList);

private:
    ULDissector *dissector;
    
    void removeDuplicateRootWords(ULList<ULDerivation>& stemList);
};

#endif

