/**
 * @file ulnumbertranslator.h
 * @brief The interface for the ULNumberTranslator class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATOR_H
#define ULNUMBERTRANSLATOR_H

#include "ulworker.h"
#include "ullanguage.h"
#include "ulstring.h"

/**
 * @class ULPrimitiveNumber
 * @brief A utility struct used by the number translation routines.
 */
struct ULPrimitiveNumber
{
    int number;
    const char *text;
};

/**
 * @class ULNumberTranslator
 * @brief The abstract parent of the classes that translate numbers in
 * ULAPI's supported languages.
 */
class ULNumberTranslator : public ULWorker
{
public:
    ULNumberTranslator();
    ULNumberTranslator(const ULNumberTranslator& other);
    virtual ~ULNumberTranslator();

    // Factory method.
    static ULNumberTranslator *createNumberTranslator(const ULLanguage& language);

    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service) = 0;
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services) = 0;

    virtual ULLanguage getLanguage() const = 0;

    // The number translator's core services.

    /**
     * Computes the textual translation of a number. For example, if the
     * given digit string is "123" and the language supported by this 
     * ULNumberTranslator object is English, then the result will be
     * "one hundred (and) twenty-three".
     *
     * @return an error code.
     * @param[in] digits The string of digits to be translated.
     * @param[out] result The translation.
     */
    virtual ULError translateNumber(const ULString& digits, ULString& result) = 0;

protected:
    static bool getPrimitiveNumberText(int n, const ULPrimitiveNumber *primitives, ULString& text);
};

// ADDLANGUAGE (This marks places where changes are needed when adding a new language.)
#include "ulnumbertranslatorenglish.h"
#include "ulnumbertranslatorfrench.h"
#include "ulnumbertranslatorspanish.h"
#include "ulnumbertranslatorgerman.h"
#include "ulnumbertranslatoritalian.h"
#include "ulnumbertranslatorportuguese.h"
#include "ulnumbertranslatorrussian.h"
#include "ulnumbertranslatordutch.h"
#include "ulnumbertranslatornorwegian.h"
#include "ulnumbertranslatorswedish.h"
#include "ulnumbertranslatorpolish.h"
#include "ulnumbertranslatorlatin.h"
#include "ulnumbertranslatoresperanto.h"
#include "ulnumbertranslatorklingon.h"
#include "ulnumbertranslatormandarin.h"
#include "ulnumbertranslatorhaitian.h"

#endif

