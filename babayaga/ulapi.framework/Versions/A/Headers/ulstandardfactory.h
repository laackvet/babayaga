/**
 * @file uldefaultfactory.h
 * @brief The interface for the ULStandardFactory class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULSTANDARDFACTORY_H
#define ULSTANDARDFACTORY_H

#include "ulfactory.h"

/**
 * @class ULStandardFactory
 * @brief ULStandardFactory is the default factory type instantiated and
 * initialized by ULFactory::createFactory. It will serve the needs of most
 * applications of ULAPI. See ULFactory and the getting-started documentation
 * for ULAPI for more details on the use of ULFactory.
 */
class ULStandardFactory : public ULFactory
{
public:
    ULStandardFactory();
    virtual ~ULStandardFactory();

    virtual ULError addLanguageDataSource(const ULString& fileIdentifier, bool loadImmediately=false, ULLanguageDataSource **dataSource=NULL);
    virtual ULError addDictionaryDataSource(const ULString& fileIdentifier, bool loadImmediately=false, ULDictionaryDataSource **dataSource=NULL);

    virtual ULError getAvailableServices(ULList<ULServiceDescriptor>& serviceList);

    virtual ULLock *createLock();

    virtual ULCollator *getCollator(const ULLanguage& language);
    virtual ULStemmer *getStemmer(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource);
    virtual ULStemmer *getStemmer(const ULLanguage& stemmingLanguage, const ULLanguage& translationLanguage);
    virtual ULPartOfSpeechTagger *getPartOfSpeechTagger(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource);
    virtual ULPartOfSpeechTagger *getPartOfSpeechTagger(const ULLanguage& taggingLanguage, const ULLanguage& translationLanguage);
    virtual ULPluralizer *getPluralizer(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource);
    virtual ULPluralizer *getPluralizer(const ULLanguage& pluralizerLanguage, const ULLanguage& translationLanguage);
    virtual ULSingularizer *getSingularizer(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource);
    virtual ULSingularizer *getSingularizer(const ULLanguage& singularizerLanguage, const ULLanguage& translationLanguage);
    virtual ULConjugator *getConjugator(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource);
    virtual ULConjugator *getConjugator(const ULLanguage& conjugationLanguage, const ULLanguage& translationLanguage);
    virtual ULDefiner *getDefiner(ULDictionaryDataSource *dictionaryDataSource, ULLanguageDataSource *firstLanguageDataSource, ULLanguageDataSource *secondLanguageDataSource);
    virtual ULDefiner *getDefiner(const ULLanguage& definitionLanguage, const ULLanguage& translationLanguage);
    virtual ULInflector *getInflector(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource);
    virtual ULInflector *getInflector(const ULLanguage& inflectionLanguage, const ULLanguage& translationLanguage);
    virtual ULDissector *getDissector(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource);
    virtual ULDissector *getDissector(const ULLanguage& dissectionLanguage, const ULLanguage& translationLanguage);
    virtual ULNumberTranslator *getNumberTranslator(const ULLanguage& language);

private:
    ULLock *doCreateLock();
    
    bool areDataSourcesCompatible(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource);
};

#endif

