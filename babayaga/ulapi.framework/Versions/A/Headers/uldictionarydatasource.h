/**
 * @file uldictionarydatasource.h
 * @brief The interface for the ULDictionaryDataSource class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULDICTIONARYDATASOURCE_H
#define ULDICTIONARYDATASOURCE_H

#include "uldictionary.h"
#include "uldictionarydescriptor.h"
#include "uldatasource.h"
#include "ulforesttype.h"
#include "ullocalizationrule.h"

class ULForester;

/**
 * @class ULDictionaryDataSource
 * @brief ULDictionaryDataSource is the abstract parent for classes that
 * interface with dictionary data stored somewhere like a .uld file
 * or a database.
 * 
 * Dictionary data sources also support a mapping between
 * feature names and feature IDs that are native to the data source in
 * question. For example, the terms and translations in an Ultralingua
 * dataset might have features called "usage", "dialect", "tone", etc.
 * corresponding to integer values stored in a .uld file or database,
 * while Collins datasets might have some of the same features as
 * Ultralingua plus some idiosyncratic ones like "biographicalinfo".
 * getFeatureNameList enables the dictionary data source's associated
 * ULForester object to map names to IDs as needed.
 *
 * <em>Warning</em>: If you find yourself thinking about directly using one
 * of the subclasses of this class, you should reconsider. It is much easier
 * to use ULAPI's data sources correctly by working with a ULFactory and the
 * associated higher-level tools such as ULConjugator or ULStemmer, which take
 * care of the initialization and manipulation of the data sources for you.
 */
class ULDictionaryDataSource : public ULDictionary, public ULDataSource
{
public:
    // Required ULDataSource operations.
    virtual ULError attach(const ULString& dataSourceIdentifier) = 0;
    virtual ULError detach() = 0;
    virtual ULError load() = 0;
    virtual ULError close() = 0;
    virtual ULString getDataSourceIdentifier() = 0;
    virtual ULDataSourceVersion getVersion() = 0;

    // ULDictionary interfaces.
    virtual ULError begin(const ULLanguage& searchLanguage,
                          uluint32 indexID,
                          ULDictionaryIterator& iterator) = 0;

    virtual ULError end(const ULLanguage& searchLanguage,
                        uluint32 indexID,
                        ULDictionaryIterator& iterator) = 0;

    virtual ULError find(const ULString& s,
                         const ULLanguage& searchLanguage,
                         uluint32 indexID,
                         ULDictionaryIterator& iterator) = 0;

    // Other ULDictionaryDataSource methods.
    
    virtual bool operator==(const ULDictionaryDataSource& dataSource) const = 0;

    /**
     * @return a ULDictionaryDescriptor object describing the
     * data source attached to this ULDictionaryDataSource.
     */
    virtual ULDictionaryDescriptor getDictionaryDescriptor() = 0;
 
    /**
     * @return this data source's first language.
     */
    virtual const ULLanguage& getFirstLanguage() = 0;
 
    /**
     * @return this data source's second language.
     */
    virtual const ULLanguage& getSecondLanguage() = 0;
    
    /**
     * @return this data source's language that is not the parameter.
     */
    virtual const ULLanguage& getOtherLanguage(const ULLanguage& language) = 0;

    /**
     * @return the forest type for this data source, which can be used (among other
     * things) to decide which subclass of ULForester to assign to this data source.
     */
    virtual ULForestType getForestType() = 0;

    /**
     * Assigns the forester to be used by this ULDictionaryDataSource. The 
     * data source takes responsibility for deleting the forester.
     * @param[in] newForester A pointer to the desired forester.
     */
    virtual void setForester(ULForester *newForester) = 0;

    /**
     * @return a pointer to the forester used by this data source.
     */
    virtual ULForester *getForester() = 0;

    /**
     * Obtains the list of feature names stored in this data source. The feature
     * names in this list are associated, in the data source itself,  with integer
     * IDs starting at 0 for the first string in featureNameList, and going up
     * to featureNameList.length() - 1.
     *
     * @return ULError::NoError if the list can be retrieved.
     * @param[out] featureNameList the list of feature names stored in this
     * data source.
     */
    virtual ULError getFeatureNameList(ULList<ULString>& featureNameList) = 0;

    /**
     * Obtains the list of localization rules stored in this data source
     * for the given language.
     *
     * @return ULError::NoError if the list can be retrieved.
     * @param[in] language The language for which to retrieve the rules.
     * @param[out] localizationRuleList The list of localization rule stored in this
     * data source for the given language
     */
    virtual ULError getLocalizationRuleList(const ULLanguage& language, ULList<ULLocalizationRule>& localizationRulesList) = 0;
    
    /**
     * Retrieves the ULD attribute value associated with the given key
     * if it exists for this ULD.
     *
     * @return true if the uld has this attribute and false otherwise.
     * @param[in] key The string that is the key for the attribute you want to retrieve.
     * @param[out] value The value of the attribute if it exists.
     */
    virtual bool getULDAttribute(const ULString& key, ULString& value) = 0;

    // Common index IDs.
    enum { HeadwordIndex, OntologyIndex, NCommonIndexIDs };
};

#endif

