/**
 * @file ulinteger.h
 * @brief The ULInteger class.
 * @author Copyright (C) 2012 Ultralingua, Inc.
 */

#ifndef ULINTEGER_H
#define ULINTEGER_H

/**
 * @class ULInteger
 * @brief ULInteger is a wrapper class intended to enable integers
 * to act as keys ULHashTables.
 */
class ULInteger
{
public:
    ULInteger() { this->value = 0; }
    ULInteger(uluint32 n) { this->value = n; }
    ~ULInteger() {}

    bool operator==(const ULInteger& other) const { return this->value == other.value; }
    uluint32 hash(uluint32 tableSize) const { return value % tableSize; } 

    uluint32 value;
};

#endif

