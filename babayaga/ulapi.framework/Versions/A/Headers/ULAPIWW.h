//
//  ULAPI.h
//  ulapi
//
//  Created by Jeff Ondich on 11/4/12.
//
//

#import <Foundation/Foundation.h>

@interface ULAPIWW : NSObject

+ (ULAPIWW *)sharedULAPI;

- (id)init;
- (BOOL)addDictionaryDataSource:(NSString *)dataSourceFileName;
- (BOOL)addLanguageDataSource:(NSString *)dataSourceFileName;
- (NSString *)translateNumber:(NSString *)digits language:(NSString *)language;
- (NSArray *)pluralsForWord:(NSString *)word language:(NSString *)language translationLanguage:(NSString *)translationLanguage;
- (NSArray *)singularsForWord:(NSString *)word language:(NSString *)language translationLanguage:(NSString *)translationLanguage;
- (NSArray *)alternateSpellingsForWord:(NSString *)word language:(NSString *)language translationLanguage:(NSString *)translationLanguage;

@end
