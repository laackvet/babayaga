/**
 * @file ulpartofspeech.h
 * @brief The interface for the ULPartOfSpeech class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULPARTOFSPEECH_H
#define ULPARTOFSPEECH_H

#ifndef __STDC_LIMIT_MACROS
#define __STDC_LIMIT_MACROS
#endif
#include <stdint.h>

#include "ulerror.h"
#include "ulstring.h"
#include "ullanguage.h"
#include "ulpartofspeechcategory.h"
#include "ulpartofspeechsubcategory.h"
#include "ulfeature.h"
#include "ulnumber.h"
#include "ulgender.h"
#include "ulformality.h"
#include "ulcase.h"
#include "ultense.h"
#include "ulperson.h"
#include "ulmood.h"
#include "ulaspect.h"
#include "ultransitivity.h"

/**
 * @class ULPartOfSpeech
 * @brief An object of type ULPartOfSpeech represents a single part of speech,
 * including the base category (e.g. noun) and additional features (e.g. gender,
 * case, number, etc.).
 */
class ULPartOfSpeech
{
public:
    ULPartOfSpeech();
    ULPartOfSpeech(const ULPartOfSpeech& other);
    virtual ~ULPartOfSpeech();

    void clear();
    ULPartOfSpeech& operator=(const ULPartOfSpeech& other);
    bool operator==(const ULPartOfSpeech& other) const;
    bool operator!=(const ULPartOfSpeech& other) const;
    bool operator<(const ULPartOfSpeech& other) const;
    uluint32 hash(uluint32 tableSize) const;

    // Accessors
    ULNumber getNumber() const;
    ULNumber getObjectNumber() const;
    ULGender getGender() const;
    ULGender getObjectGender() const;
    ULPartOfSpeechSubcategory getPartOfSpeechSubcategory() const;
    ULCase getCase() const;
    ULTense getTense() const;
    ULPerson getPerson() const;
    ULFormality getFormality() const;
    ULMood getMood() const;
    ULAspect getAspect() const;
    ULTransitivity getTransitivity() const;

    bool isRootPartOfSpeech() const;
    bool isReflexiveVerb() const;

    const ULPartOfSpeechCategory& getPartOfSpeechCategory() const;
    void setPartOfSpeechCategory(const ULPartOfSpeechCategory& category);

    void addFeature(const ULFeatureType& featureType, int value);
    void addFeature(const ULFeature& feature);
    void addStringFeature(const ULFeatureType& featureType, const ULString& stringValue);
    void setFeature(const ULFeatureType& featureType, int value);
    bool hasFeature(const ULFeature& feature) const;
    bool hasFeature(const ULFeatureType& featureType) const;
    bool getFeature(const ULFeatureType& featureType, ULFeature& feature) const;

    ULString toString() const;

    const ULList<ULFeature>& getFeatureList() const;

protected:
    ULPartOfSpeechCategory partOfSpeechCategory;
    // The feature list is kept sorted for efficient comparison.
    ULList<ULFeature> featureList;
    bool findFeature(const ULFeatureType& featureType, int& featureValue) const;
};

#endif

