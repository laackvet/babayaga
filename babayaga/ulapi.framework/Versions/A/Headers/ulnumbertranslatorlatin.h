/**
 * @file ulnumbertranslatorlatin.h
 * @brief The interface for the ULNumberTranslatorLatin class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORLATIN_H
#define ULNUMBERTRANSLATORLATIN_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorLatin
 * @brief ULNumberTranslatorLatin does number translation in Latin.
 */
class ULNumberTranslatorLatin : public ULNumberTranslator
{
public:
    ULNumberTranslatorLatin();
    ULNumberTranslatorLatin(const ULNumberTranslatorLatin& other);
    virtual ~ULNumberTranslatorLatin();

    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

