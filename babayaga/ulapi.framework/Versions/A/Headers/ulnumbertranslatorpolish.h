/**
 * @file ulnumbertranslatorpolish.h
 * @brief The interface for the ULNumberTranslatorPolish class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORPOLISH_H
#define ULNUMBERTRANSLATORPOLISH_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorPolish
 * @brief ULNumberTranslatorPolish does number translation in Polish.
 */
class ULNumberTranslatorPolish : public ULNumberTranslator
{
public:
    ULNumberTranslatorPolish();
    ULNumberTranslatorPolish(const ULNumberTranslatorPolish& other);
    virtual ~ULNumberTranslatorPolish();

    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

