//
//  UUPartOfSpeechWrapper.h
//  ulapi
//
//  Created by Ben Cochran on 1/29/12.
//  Copyright (c) 2013 Ultralingua, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "ulpartofspeech.h"

@interface UUPartOfSpeechWrapper : NSObject<NSCoding>

// Using dot notation with C++ types seems to lead to memory errors.
// So we'll just not delcare this as a property and do it ourselves.
//@property (nonatomic, assign) ULPartOfSpeech partOfSpeech;
- (void)setPartOfSpeech:(const ULPartOfSpeech&)partOfSpeech;
- (const ULPartOfSpeech&)partOfSpeech;

- (id)initWithPartOfSpeech:(const ULPartOfSpeech &)partOfSpeech;

@end
