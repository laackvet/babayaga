/**
 * @file ulinflectionruletransform.h
 * @brief The interface for the ULInflectionRuleTransform class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULINFLECTIONRULETRANSFORM_H
#define ULINFLECTIONRULETRANSFORM_H

#include "ulenum.h"

/**
 * @class ULInflectionRuleTransform
 * @brief An enhanced enumerated type used to represent inflection rule transforms (e.g. "doublefinalconsonant"
 * for rules like the ones that transform "pin" into "pinning" or "pinned"). See
 * ULInflectionRule::applyTransformForInflection and ULInflectionRule::applyTransformForDissection
 * to see what these transforms actually do.
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULInflectionRuleTransform : public ULEnum
{
public:
    static const ULInflectionRuleTransform& None;
    static const ULInflectionRuleTransform& Any;
    static const ULInflectionRuleTransform& ENGDoubleConsonant;
    static const ULInflectionRuleTransform& FRAHyphenatedPlural;
    static const ULInflectionRuleTransform& FRAAcuteToGrave;
    static const ULInflectionRuleTransform& FRADoubleConsonant;
    static const ULInflectionRuleTransform& SPAAddAccent;
    static const ULInflectionRuleTransform& SPAEToIE;
    static const ULInflectionRuleTransform& SPAOToUE;
    static const ULInflectionRuleTransform& SPAOtoUUmlautE;
    static const ULInflectionRuleTransform& SPAAddAcuteAccent;
    static const ULInflectionRuleTransform& SPAEToI;
    static const ULInflectionRuleTransform& SPAEToI2;
    static const ULInflectionRuleTransform& SPAAddAcuteAccent2;
    static const ULInflectionRuleTransform& DEU2;
    static const ULInflectionRuleTransform& DEU5;
    static const ULInflectionRuleTransform& DEU6;
    static const ULInflectionRuleTransform& DEU7;
    static const ULInflectionRuleTransform& POR2;
    static const ULInflectionRuleTransform& POR5;
    static const ULInflectionRuleTransform& POR6;
    static const ULInflectionRuleTransform& RUS1;
    static const ULInflectionRuleTransform& RUS2;
    static const ULInflectionRuleTransform& RUS3;
    static const ULInflectionRuleTransform& RUS4;
    static const ULInflectionRuleTransform& RUS5;
    static const ULInflectionRuleTransform& RUS6;
    static const ULInflectionRuleTransform& RUS7;
    static const ULInflectionRuleTransform& NLDMaintainVowelLength;
    static const ULInflectionRuleTransform& NLDSToZ;
    static const ULInflectionRuleTransform& NLDFToV;


    ULInflectionRuleTransform();
    ULInflectionRuleTransform(const ULInflectionRuleTransform& other);
    ULInflectionRuleTransform(int id);
    ULInflectionRuleTransform(const char *otherStringID);
    virtual ~ULInflectionRuleTransform();

    virtual void clear();
    ULInflectionRuleTransform& operator=(const ULInflectionRuleTransform& other);
    ULInflectionRuleTransform& operator=(int otherID);
    ULInflectionRuleTransform& operator=(const char *otherStringID);

    bool operator==(const ULInflectionRuleTransform& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULInflectionRuleTransform& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULInflectionRuleTransform& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULInflectionRuleTransform& getInstance(int id);



private:
    static int nEnumeratedValues;
    static ULVector<const ULInflectionRuleTransform *> enumeratedValueVector;
    ULInflectionRuleTransform(const char *stringID, const char *displayString);
};


#endif

