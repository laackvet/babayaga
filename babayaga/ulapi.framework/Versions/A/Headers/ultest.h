/**
 * @file ultest.h
 * @brief This file supports ULAPI's unit test suite by providing
 * friend statements for the tested classes.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULTEST_H
#define ULTEST_H

#ifdef UL_TEST

class ULPairTestSuite;
class ULListIteratorTestSuite;
class ULListTestSuite;
class ULHashTableTestSuite;
class ULLanguageTestSuite;
class ULStringTestSuite;
class ULStemmerTestSuite;
class ULDFileDictionaryDataSourceTestSuite;

#define UL_TEST_FRIEND \
friend class ULPairTestSuite; \
friend class ULListIteratorTestSuite; \
friend class ULListTestSuite; \
friend class ULHashTableTestSuite; \
friend class ULLanguageTestSuite; \
friend class ULStringOperatorTestSuite; \
friend class ULStringMutateTestSuite; \
friend class ULStringSearchTestSuite; \
friend class ULStemmerTestSuite; \
friend class ULDFileDictionaryDataSourceTestSuite; \

#else
#define UL_TEST_FRIEND
#endif // UL_TEST

#endif
