/**
 * @file ulnumber.h
 * @brief The interface for the ULNumber class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULNUMBER_H
#define ULNUMBER_H

#include "ulenum.h"

/**
 * @class ULNumber
 * @brief An enhanced enumerated type used to represent the numbers of noun/pronoun/adjective.
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULNumber : public ULEnum
{
public:
    static const ULNumber& None;
    static const ULNumber& Any;
    static const ULNumber& Singular;
    static const ULNumber& Plural;
    static const ULNumber& Dual;
    static const ULNumber& Invariable;


    ULNumber();
    ULNumber(const ULNumber& other);
    ULNumber(int id);
    ULNumber(const char *otherStringID);
    virtual ~ULNumber();

    virtual void clear();
    ULNumber& operator=(const ULNumber& other);
    ULNumber& operator=(int otherID);
    ULNumber& operator=(const char *otherStringID);

    bool operator==(const ULNumber& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULNumber& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULNumber& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULNumber& getInstance(int id);



private:
    static int nEnumeratedValues;
    static ULVector<const ULNumber *> enumeratedValueVector;
    ULNumber(const char *stringID, const char *displayString);
};


#endif

