//
//  NSString+ULAPI.h
//  ulapi
//
//  Created by Ben Cochran on 9/22/10.
//  Copyright 2010 Ultralingua. All rights reserved.
//
//  Note: This file must be included from an Objective-C++ class.
//

#import <Foundation/Foundation.h>
#import "ulstring.h"

@interface NSString (ULAPI)

+ (id)stringWithULString:(ULString)str;
- (ULString)ULString;

@end
