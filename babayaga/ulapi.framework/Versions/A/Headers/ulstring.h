/**
 * @file ulstring.h
 * @brief The interface for the ULString class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULSTRING_H
#define ULSTRING_H

#include "ultypes.h"
#include "ulchar.h"
#include "ulcontainers.h"

#ifdef UL_USING_ICU
#include "unicode/unistr.h"
#endif

// Utility functions
int ulstrcmp(const char *a, const char *b);
int ulstrcmpi(const char *a, const char *b);

// Forward declarations
class ULStringIterator;
class ULStringReverseIterator;
class ULLanguage;
class ULCollator;
class ULFile;


/**
 * @class ULString
 * @brief A string wrapper class for use in the Ultralingua API.
 *
 * This wrapper will enable us to swap string libraries in and
 * out of ULAPI depending on the constraints of a particular
 * application or platform.
 *
 * If UL_USING_ICU is #defined, then an implementation based on ICU
 * (http://site.icu-project.org/) will be used. Otherwise, a "legacy"
 * implementation based on ULAPI version 7 will be used.
 */
class ULString
{
    friend class ULStringIterator;
    friend class ULStringReverseIterator;
    friend class ULCollator;
#ifdef UL_HAS_IOSTREAMS
    friend ostream& operator<<(ostream& out, const ULString& s);
    friend istream& operator>>(istream& in, ULString& s);
#endif
    friend bool operator==(const ULString& a, const ULString& b);
    friend bool operator<(const ULString& a, const ULString& b);
    UL_TEST_FRIEND;

public:
    ULString();
    ULString(const ULString& s);
    ULString(const char *s);
    ULString(ulchar ch);
    virtual ~ULString();

    void clear();
    ULString& operator=(const ULString& s);
    ULString& operator=(const char *s);
    ULString& operator+=(const ULString& s);
    ULString& operator+=(const char *s);
    ULString& operator+=(ulchar ch);
    ulchar operator[](int n) const;
    operator const char *();
    uluint32 length() const;
    void setAt(uluint32 index, ulchar value);

    int getEncoding() const;
    void setEncoding(const char *encodingName);
    void setEncoding(int newEncoding);
    enum { UTF8, UTF16BE, UTF16LE };
    const char *getBuffer();

    void fromUTF8(const char *utf8Buffer);

    ULStringIterator begin() const;
    ULStringReverseIterator rbegin() const;
    ULStringIterator end() const;
    ULStringReverseIterator rend() const;

    bool contains(ulchar ch) const;
    bool contains(const char *s) const;
    bool contains(const ULString& s) const;
    ULStringIterator find(ulchar ch) const;
    ULStringIterator find(const char *s) const;
    ULStringIterator find(const ULString& s) const;
    ULStringIterator find(const ULString& s, const ULStringIterator& startingAt) const;
    ULStringIterator rfind(ulchar ch) const;
    ULStringIterator rfind(const char *s) const;
    ULStringIterator rfind(const ULString& s) const;
    bool startsWith(const ULString& head) const;
    bool startsWith(const ULString& head, const ULStringIterator& startingAt) const;
    bool startsWith(const ULString& head, ULCollator *collator, bool ignoreCase, bool ignoreAccents) const;
    bool endsWith(const ULString& tail) const;
    bool endsWith(const ULString& tail, ULLanguage language, bool ignoreCase, bool ignoreAccents) const;

    void replace(const ULString& textToReplace, const ULString& replacementText);
    void replace(ulchar charToReplace, const ULString& replacementText);
    void replace(const ULStringIterator& iterator, uluint32 nCharsToReplace, const ULString& replacementText);
    void replace(uluint32 index, uluint32 nCharsToReplace, const ULString& replacementText);
    void erase(ULStringIterator& iterator, uluint32 nCharsToErase);
    void erase(uluint32 start, uluint32 nCharsToErase);
    void erase(ulchar charToErase);
    void insert(const ULStringIterator& iterator, const ULString& s);
    void insert(ulint32 insertionIndex, const ULString& s);
    ULString substr(ulint32 start, ulint32 nChars) const;
    ULString substr(const ULStringIterator& start, const ULStringIterator& end) const;

    ulint32 compare(const ULString& s, const ULLanguage& language) const;
    ulint32 compare(const ULString& s, const ULLanguage& language, bool ignoreCase, bool ignoreAccents) const;

    bool equals(const ULString& s, bool ignoreCase, bool ignoreAccents) const;

    uluint32 hash(uluint32 tableSize) const;

    ULString& reverse();
    ULString& toLower();
    ULString& toLower(const ULLanguage& language);
    ULString& toUpper();
    ULString& toUpper(const ULLanguage& language);
    ULString& toBase();
    ULString& toBase(const ULLanguage& language);
    int getIntegerValue() const;
    void appendInteger(int n, uluint32 base=10);
    void split(const ULString& s, ULList<ULString>& stringList, bool stripParts=false) const;
    void strip(const char *charactersToRemove = 0);
    void eraseExcessWhitespace();
    ULString join(const ULList<ULString>& stringList) const;

    static int compareForSorting(ULString& a, ULString& b, void *context);

private:
    int encoding;

    char *conversionBuffer;
    int conversionBufferCapacity;
    bool conversionBufferValid;
    void invalidateConversionBuffer();
    void convert();
    bool checkIntegrity() const; // for debugging
    enum { MinimumCapacity = 32 };
    
#ifdef UL_USING_ICU
    UnicodeString icuString;
    void initConversionBuffer();
#elif defined(UL_USING_COCOA_STRINGS)
    NSMutableString *nsMutableString;
#elif defined(UL_USING_LEGACY_STRINGS)
    // Legacy strings are the default
    enum { staticBufferSize = 32 };
    ulchar staticBuffer[staticBufferSize];
    ulchar *buffer;
    int capacity;
    int charLength; // doesn't include the null
    bool init();
    bool reallocate(int nCharactersNeeded, bool copyData);
#endif
};

ULString operator+(const ULString& a, const ULString& b);
ULString operator+(const ULString& a, const char *s);
ULString operator+(const ULString& a, const char ch);
ULString operator+(const ULString& a, uluint32 k);

bool operator<(const ULString& a, const char *s);
bool operator<=(const ULString& a, const char *s);
bool operator>(const ULString& a, const char *s);
bool operator>=(const ULString& a, const char *s);
bool operator==(const ULString& a, const char *s);
bool operator!=(const ULString& a, const char *s);

bool operator<(const ULString& a, const ULString& b);
bool operator<=(const ULString& a, const ULString& b);
bool operator>(const ULString& a, const ULString& b);
bool operator>=(const ULString& a, const ULString& b);
bool operator==(const ULString& a, const ULString& b);
bool operator!=(const ULString& a, const ULString& b);

//istream& getline(istream& in, ULString& s, ulchar delimiter='\n');
//istream& getstring(istream& in, ULString& s);
bool getUTF8Char(ULFile& inputFile, ulchar& ch);
bool getlineUTF8(ULFile& in, ULString& s, ulchar delimiter='\n');

#ifdef UL_HAS_IOSTREAMS
ostream& operator<<(ostream& out, const ULString& s);
istream& operator>>(istream& in, ULString& s);
#endif

#endif // ULSTRING_H

