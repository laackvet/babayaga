//
//  UURendererWrapper.h
//  ulapi
//
//  Created by Jeff Ondich on 1/21/13.
//  Copyright (c) 2013 Ultralingua, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ulrenderer.h"

@interface UURendererWrapper : NSObject

// Using dot notation with C++ types seems to lead to memory errors.
// So we'll just not delcare this as a property and do it ourselves.
//@property (nonatomic, assign) ULPartOfSpeech partOfSpeech;
- (void)setRenderer:(const ULRenderer *)aRenderer;
- (const ULRenderer *)renderer;

- (id)initWithRenderer:(const ULRenderer *)aRenderer;

@end
