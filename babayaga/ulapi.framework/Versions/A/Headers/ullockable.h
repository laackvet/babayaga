/**
 * @file ullockable.h
 * @brief The interface for the ULLockable class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULLOCKABLE_H
#define ULLOCKABLE_H

#include "ullock.h"
#include "ulscopelocker.h"

/**
 * @class ULLockable
 * @brief ULLockable can be used as a parent class for any class
 * whose operations need to be made atomic in a threaded context.
 *
 * Each ULLockable object has a pointer to a ULLock, which may be
 * null (in which case no locking operations will take place) or point
 * to some instance of a subclass of ULLock suitable for the platform
 * on which the code is running.
 *
 * ULLockable depends on ULScopeLocker, which implements a variant of
 * the "Scoped Locking" pattern
 * (http://www.cs.wustl.edu/~schmidt/PDF/ScopedLocking.pdf).
 *
 * A typical use of a ULLockable object would involve steps like this:
 *
 * <ul>
 *   <li>Derive <code>MyClass</code> from <code>ULLockable</code></li>
 *   <li>After you instantiate a <code>MyClass</code> object, call
 *   <code>setLock</code> with a pointer to a suitable instance of some
 *   child of ULLock.</li>
 *   <li>Add "<code>UL_LOCKSCOPE;</code>" to the top of any member
 *   function of <code>MyClass</code> that you want locked.</li>
 *   <li><strong>Note</strong>: Your <code>MyClass</code> object will
 *   never delete the ULLock object, so the lock needs to be deleted
 *   elsewhere.  (See ULFactory for a safe way of handling locks and
 *   memory in ULAPI.)</li>
 * </ul>
 */
class ULLockable
{
public:
    ULLockable();
    ULLockable(const ULLockable& lockable);
    virtual ~ULLockable();

    const ULLockable& operator=(const ULLockable& lockable);
    void clear();

    ULLock *getLock();
    void setLock(ULLock *newLock);

protected:
    ULLock *lock;
};

#endif

