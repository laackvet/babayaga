/**
 * @file ulodrenderer.h
 * @brief The interface for the ULODRenderer class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULODRENDERER_H
#define ULODRENDERER_H

#include "ulrenderer.h"

class ULStandardForester;
class ULExample;

/**
 * @class ULODRenderer
 * @brief The ULRenderer child that renders HTML specifically  for ULOD.
 */
class ULODRenderer : public ULRenderer
{
public:
    ULODRenderer();
    ULODRenderer(const ULODRenderer& renderer);
    virtual ~ULODRenderer();
    
    virtual ULODRenderer& operator=(const ULODRenderer& renderer);
    virtual void clear();
    
    // Required ULRenderer interfaces.
    virtual ULError render(const ULDictionaryIterator& iterator, ULString& result) const;
    
private:
    void renderTerm(ULDictionaryIterator& iterator, ULStandardForester *forester, ULString& result) const;
    void renderDefinitionContents(ULDictionaryIterator& iterator, ULStandardForester *forester, const ULLanguage& defLanguage, const ULLanguage& searchLanguage, ULString& result) const;
    void renderExampleContents(ULExample& example, bool isSource, ULStandardForester *forester, ULString& result) const;
    ULString renderPosp(const ULPartOfSpeech& posp, const ULLanguage &targetLanguage, ULStandardForester *forester) const;    

    // Private methods to put anchor tags in HTML for ULOD.
    ULString tag(const ULString& s, bool isSource) const;
    uluint32 getNextRawToken(ULStringIterator& it, ULString& token) const;
};

#endif

