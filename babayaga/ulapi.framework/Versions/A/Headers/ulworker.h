/**
 * @file ulworker.h
 * @brief The interface for the ULWorker class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULWORKER_H
#define ULWORKER_H

#include "ulerror.h"
#include "ulcontainers.h"
#include "ulservicedescriptor.h"

/**
 * @class ULWorker
 * @brief ULWorker is the abstract parent for the classes that provide core
 * ULAPI services like number translation, conjugation, word look-up, etc.
 *
 * ULWorker objects also have a "cancelOperation" boolean attribute that
 * enables clients of the worker to cancel time-consuming operations
 * (e.g. stemming or full conjugations). A typical usage pattern would look
 * like this:
 *
 * @code{.cpp}
 *   ULWorker *worker = new SomeULWorkerSubclass();
 *   worker->setCancelOperation(false);
 *   // spawn computation thread here
 *
 *   // in UI thread
 *   // when cancellation condition occurs, e.g. user hits a cancel button
 *   worker->setCancelOperation(true);
 *
 *   // in computation thread
 *   ULError error = worker->performBigOperation();
 *   worker->setCancelOperation(false);
 *   if (error == ULError::OperationCancelled) {
 *       // ...
 *   }
 *
 *   // in performBigOperation()
 *   while (!bigOperationIsComplete) {
 *       // do stuff
 *       if (this->shouldCancelOperation()) {
 *           // clean up
 *           return ULError::OperationCancelled;
 *       }
 *   }
 * @endcode
 */

class ULWorker
{
public:
    ULWorker();
    virtual ~ULWorker() {}

    /**
     * @return true if the specified service can be performed by
     * this ULWorker, and false otherwise.
     * @param service The desired service.
     */
    virtual bool isServiceAvailable(const ULServiceDescriptor& service) = 0;

    /**
     * @param[out] serviceList Used to return a list of all the services this
     * ULWorker can provide.
     */
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& serviceList) = 0;

    /**
     * Setter for the long-operation cancellation boolean attribute.
     * @param[in] set to true if 
     */
    virtual void setCancelOperation(bool shouldCancel);

    /**
     * @return true if the cancel-operation flag is set, false otherwise.
     */
    virtual bool shouldCancelOperation() const;

private:
    bool cancelOperation;
};

#endif

