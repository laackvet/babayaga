/**
 * @file uldissector.h
 * @brief The interface for the ULDissector class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULDISSECTOR_H
#define ULDISSECTOR_H

#include "ulworker.h"
#include "ullanguagedatasource.h"
#include "uldictionarydatasource.h"
#include "ulderivation.h"
#include "ulinflector.h"

/**
 * @class ULDissector
 * @brief A ULDissector is a worker that performs morphological analysis of
 * words in a particular language. Each ULDissector contains a pointer to
 * a language data source and a dictionary data source. The language data
 * source provides information about the general morphological structure
 * of the language in question, while the dictionary data source provides
 * a list of words in the language along with their parts of speech.
 */
class ULDissector : public ULWorker
{
public:
    ULDissector();
    ULDissector(const ULDissector& other);
    virtual ~ULDissector();

    ULDissector& operator=(const ULDissector& other);
    void clear();

    // Accessors.
    const ULLanguage& getLanguage() const;
    void setLanguageDataSource(ULLanguageDataSource *dataSource);
    ULLanguageDataSource *getLanguageDataSource();
    void setDictionaryDataSource(ULDictionaryDataSource *dictionaryDataSource);
    ULDictionaryDataSource *getDictionaryDataSource();
    void setInflector(ULInflector *newInflector);
    ULInflector *getInflector();
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The dissector's core services.
    ULError dissect(const ULString& surfaceForm,
                    ULList<ULDerivation>& dissectionList);

    ULError dissect(const ULString& surfaceForm,
                    const ULPartOfSpeechCategory& category,
                    ULList<ULDerivation>& dissectionList);

    ULError dissect(const ULString& surfaceForm,
                    uluint32 maxDerivationLength,
                    ULList<ULDerivation>& derivationList);

    ULError dissect(const ULString& surfaceForm,
                    const ULPartOfSpeechCategory& category,
                    uluint32 maxDerivationLength,
                    ULList<ULDerivation>& derivationList);

private:
    ULLanguageDataSource *languageDataSource;
    ULDictionaryDataSource *dictionaryDataSource;
    ULInflector *inflector;
    
    // Support methods.
    ULError getDerivations(const ULString& originalSearchString,
                           ULDerivation& derivation,
                           uluint32 maxDerivationLength,
                           ULList<ULDerivation>& derivationList);
    
    ULHashTable<ULString, ULList<ULDictionaryIterator> > matchingTermsCache;

    ULError getMatchingTerms(ULForester *forester,
                             const ULString& searchString,
                             ULList<ULDictionaryIterator>& iteratorList);
    
    ULError getMatchingTerms(ULForester *forester,
                             const ULString& searchString,
                             const ULString& sortkeyString,
                             ULList<ULDictionaryIterator>& iteratorList);
};

#endif

