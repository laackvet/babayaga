/**
 * @file ulchar.h
 * @brief The interface for the ulchar type.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULCHAR_H
#define ULCHAR_H

#include "ultypes.h"

#ifdef UL_USING_ICU
#include "unicode/uchar.h"
typedef UChar ulchar;

#elif defined(UL_USING_COCOA_STRINGS)
# ifdef __OBJC__
typedef unichar ulchar;
# else
typedef uluint16 ulchar;
# endif

# elif defined(UL_USING_LEGACY_STRINGS)
# ifdef UL_USING_32BIT_ULCHAR
typedef uluint32 ulchar;
# else
typedef uluint16 ulchar;
# endif

#endif

class ULLanguage;

// Some ulchar functions

/**
 * @return the lowercase version of ch if ch is alphabetic, or unchanged ch otherwise.
 * @param[in] ch the original character.
 */
ulchar ulcharToLower(ulchar ch);

/**
 * @return the uppercase version of ch if ch is alphabetic, or unchanged ch otherwise.
 * @param[in] ch the original character.
 */
ulchar ulcharToUpper(ulchar ch);

/**
 * @return the version of ch with diacritical marks removed if ch is alphabetic, or
 * unchanged ch otherwise. This function should depend on language, but for our existing
 * datasets, it's OK.
 * @param[in] ch the original character.
 */
ulchar ulcharToBase(ulchar ch);

/**
 * @return the version of ch converted to lower case (if toLower is true) and/or with
 * diacritical marks removed (if toBase is true).
 * @param[in] ch the original character.
 * @param[in] toLower true if the character should be converted to lower case.
 * @param[in] toBase true if the character should have diacritical marks removed.
 */
ulchar ulcharNormalize(ulchar ch, bool toLower, bool toBase);

/**
 * @return the version of ch with an acute accent added if ch is alphabetic, or unchanged
 * ch otherwise.
 * @param[in] ch the original character.
 */
ulchar ulcharAddAcuteAccent(ulchar ch);

/**
 * @return the version of ch with an umlaut added if ch is alphabetic, or unchanged
 * ch otherwise.
 * @param[in] ch the original character.
 */
ulchar ulcharAddUmlaut(ulchar ch);

/**
 * @return true if ch is considered a "word" character for ULAPI tokenization purposes.
 * Word characters include letters, digits, apostrophes (curly and straight), and hyphens.
 * @param[in] ch the character.
 */
bool ulcharIsWordChar(ulchar ch);

/**
 * @return true if ch is a letter in some alphabet.
 * @param[in] ch the character.
 */
bool ulcharIsAlpha(ulchar ch);

/**
 * @return true if ch is a base-ten digit.
 * @param[in] ch the character.
 */
bool ulcharIsDigit(ulchar ch);

/**
 * @return true if ch is a hexadecimal digit character (0...9, A...F, or a...f).
 * @param[in] ch the character.
 */
bool ulcharIsHexDigit(ulchar ch);

/**
 * @return true if ch is a whitespace character.
 * @param[in] ch the character.
 */
bool ulcharIsSpace(ulchar ch);

/**
 * @return true if ch is an uppercase letter in some alphabet.
 * @param[in] ch the character.
 */
bool ulcharIsUppercase(ulchar ch);

/**
 * @return true if ch is a lowercase letter in some alphabet.
 * @param[in] ch the character.
 */
bool ulcharIsLowercase(ulchar ch);

/**
 * @return true if ch is a Mandarin character.
 * @param[in] ch the character.
 */
bool ulcharIsMandarinChar(ulchar ch);

/**
 * @return true if ch is a vowel in some alphabet.
 * @param[in] ch the character.
 * @param[in] language the language
 */
bool ulcharIsVowel(ulchar ch, const ULLanguage& language);

/**
 * @return the first letter, upper-case, in the specified language.
 * @param[in] language the language
 */
ulchar ulcharFirstLetterForLanguage(const ULLanguage& language);

/**
 * Computes the UTF-8 encoding of a ulchar, storing the resulting bytes in
 * the specified buffer.
 * @param[in] ch the character to be encoded.
 * @param[out] buffer a pointer to a buffer of length at least 6 (i.e.
 * large enough to hold the largest UTF-8 character encoding).
 * @param[out] nBytes the number of bytes in the encoded version of the character.
 */
void ulcharToUTF8(ulchar ch, char *buffer, int& nBytes);

/**
 * Computes the UTF-16 big endian encoding of a ulchar, storing the resulting
 * bytes in the specified buffer.
 * @param[in] ch the character to be encoded.
 * @param[out] buffer a pointer to a buffer of length at least 2 (i.e.
 * large enough to hold the largest UTF-16 character encoding).
 * @param[out] nBytes the number of bytes in the encoded version of the character.
 */
void ulcharToUTF16BigEndian(ulchar ch, char *buffer, int& nBytes);

/**
 * Computes the UTF-16 big endian encoding of a ulchar, storing the resulting
 * bytes in the specified buffer.
 * @param[in] ch the character to be encoded.
 * @param[out] buffer a pointer to a buffer of length at least 2 (i.e.
 * large enough to hold the largest UTF-16 character encoding).
 * @param[out] nBytes the number of bytes in the encoded version of the character.
 */
void ulcharToUTF16LittleEndian(ulchar ch, char *buffer, int& nBytes);

#ifdef UL_USING_LEGACY_STRINGS
/**
 * @return a pointer to the byte immediately following the UTF-8 character pointed to by p.
 * @param[in] p a pointer to a buffer containing at least one character encoded in UTF-8.
 * @param[out] ch the extracted character.
 */
const char *utf8ToULChar(const char *p, ulchar& ch);
bool getUTF8ULChar(const char *p, ulchar& ch);

#endif

#endif // ULCHAR_H

