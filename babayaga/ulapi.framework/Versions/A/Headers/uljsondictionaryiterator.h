/**
 * @file uljsondictionaryiterator.h
 * @brief The interface for the ULJSONDictionaryIterator class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULJSONDICTIONARYITERATOR_H
#define ULJSONDICTIONARYITERATOR_H

#ifdef UL_USING_JSON

#include "uldictionarydatasourceiterator.h"
#include "ullanguage.h"
#include "json.h"

/**
 * @class ULJSONDictionaryIterator
 * @brief ULJSONDictionaryIterator is the dictionary data source iterator for
 * use with ULJSONDictionaryDataSource (i.e. dictionary data stored as JSON strings).
 */
class ULJSONDictionaryIterator : public ULDictionaryDataSourceIterator
{
    UL_TEST_FRIEND;
    friend class ULJSONDictionaryDataSource;

public:
    ULJSONDictionaryIterator();
    ULJSONDictionaryIterator(const ULJSONDictionaryIterator& other);
    virtual ~ULJSONDictionaryIterator();

    virtual ULJSONDictionaryIterator& operator=(const ULJSONDictionaryIterator& other);
    virtual void clear();

    virtual ULDictionaryDataSourceIterator *clone() const;

    virtual const ULDictionaryNode& operator*();
    virtual void operator++();
    virtual void operator--();

    virtual uluint32 hash(uluint32 tableSize) const;
    virtual bool operator==(const ULDictionaryDataSourceIterator& iterator) const;
    virtual bool operator==(const ULJSONDictionaryIterator& iterator) const;

    virtual void toNextTopLevelNode();
    virtual void toCurrentTopLevelNode();
    virtual void toPreviousTopLevelNode();
    virtual bool hasParent();
    virtual void toParent();
    virtual bool hasChildren();
    virtual void toFirstChild();
    virtual bool hasNextSibling();
    virtual void toNextSibling();
    virtual bool hasPreviousSibling();
    virtual void toPreviousSibling();
    virtual bool hasNextCousin();
    virtual void toNextCousin();
    virtual bool hasPreviousCousin();
    virtual void toPreviousCousin();
    virtual int getLevel();
    virtual uluint32 getType();
    virtual bool isAtEnd() const;
    virtual bool isAtBeginning() const;
    virtual bool isInFirstTree() const;
    virtual bool isInLastTree() const;
    virtual ULString getIndexKey();
    virtual uluint32 getIndexRank();
    virtual const ULLanguage& getLanguage() const;
    virtual void setLanguage(const ULLanguage& language);
    virtual const ULLanguage& getSearchLanguage() const;
    virtual const ULLanguage& getTranslationLanguage() const;

protected:
    struct JSONNodeInfo
    {
        uluint32 childIndex;
        uluint32 nChildrenOfSameParent;
        Json::Value jsonNode;

        JSONNodeInfo();
        JSONNodeInfo(uluint32 index, uluint32 nChildren, const Json::Value& node);
    };

    ULList<JSONNodeInfo> jsonNodePath;
    ULLanguage language;
    ULDictionaryNode currentNode;
    bool currentNodeLoaded;

    void lockDataSource() const;
    void unlockDataSource() const;
    void loadCurrentNode();
};

#endif // UL_USING_JSON

#endif

