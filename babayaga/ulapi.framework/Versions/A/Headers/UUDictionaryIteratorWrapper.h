//
//  UUDictionaryIteratorWrapper.h
//  ulapi
//
//  Created by Ben Cochran on 1/30/12.
//  Copyright (c) 2013 Ultralingua, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "uldictionaryiterator.h"
#import "ulpartofspeech.h"
#import "ulforesttype.h"

@class UUDictionaryProduct;

@interface UUDictionaryIteratorWrapper : NSObject<NSCoding>

// Using dot notation with C++ types seems to lead to memory errors.
// So we'll just not delcare this as a property and do it ourselves.
//@property (nonatomic, readonly) ULDictionaryIterator& iterator;
- (const ULDictionaryIterator&)iterator;

@property (nonatomic, readonly) BOOL iteratorIsLoaded;
@property (nonatomic, readonly) BOOL dataSourceIsThesaurus;
@property (nonatomic, readonly) NSString *termText;
@property (nonatomic, readonly) NSString *sortKey;
@property (nonatomic, assign) NSUInteger sortRank;
@property (nonatomic, assign) ULLanguage searchLanguage;
@property (nonatomic, assign) ULLanguage translationLanguage;
@property (nonatomic, assign) ULPartOfSpeech partOfSpeech;
@property (nonatomic, assign) ULForestType forestType;

- (id)initWithIterator:(const ULDictionaryIterator &)iterator;
- (id)initWithIteratorWrapper:(UUDictionaryIteratorWrapper *)wrapper;

- (void)loadFromDataSource:(ULDictionaryDataSource *)dataSource;
- (void)unloadIterator;

- (NSComparisonResult)compare:(UUDictionaryIteratorWrapper*)other;

@end
