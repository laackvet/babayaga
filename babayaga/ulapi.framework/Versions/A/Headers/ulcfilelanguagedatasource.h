/**
 * @file ulcfilelanguagedatasource.h
 * @brief The interface for the ULCFileLanguageDataSource class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULCFILELANGUAGEDATASOURCE_H
#define ULCFILELANGUAGEDATASOURCE_H

#include "ullanguagedatasource.h"
#include "ulsegmentedfile.h"
#include "ulinflectionrule.h"
#include "ulderivation.h"
#include "ulinteger.h"

#define ULCFILE_HEADER_SEGMENT_NAME "header"
#define ULCFILE_FEATURE_MAP_SEGMENT_NAME "featuremap"
#define ULCFILE_VERB_MODEL_SEGMENT_NAME "verbmodels"
#define ULCFILE_VERB_SEGMENT_NAME "verbs"
#define ULCFILE_NOUN_SEGMENT_NAME "nouns"
#define ULCFILE_INFLECTION_RULE_SEGMENT_NAME "inflectionrules"
#define ULCFILE_TAGGING_RULE_SEGMENT_NAME "taggingrules"
#define ULCFILE_FREQUENCY_SEGMENT_NAME "frequencies"
#define ULCFILE_STOP_WORDS_SEGMENT_NAME "stopwords"
#define ULCFILE_CLOSED_CLASS_WORDS_SEGMENT_NAME "closedclasswords"
#define ULCFILE_FULL_WORD_SOURCE_INDEX_SEGMENT_NAME "index/fullwordsource"
#define ULCFILE_FULL_WORD_TARGET_INDEX_SEGMENT_NAME "index/fullwordtarget"
#define ULCFILE_ORDINARY_RULE_INDEX_SEGMENT_NAME "index/ordinaryrules"

/**
 * @class ULCFileLanguageDataSource
 * @brief A ULCFileLanguageDataSource object provides the interface to the single-language
 * data stored in an ULAPI8 .ulc file. Please read the warning in the description
 * of the ULLanguageDataSource class.
 */
class ULCFileLanguageDataSource : public ULLanguageDataSource
{
public:
    ULCFileLanguageDataSource();
    ULCFileLanguageDataSource(const ULCFileLanguageDataSource& other);
    ~ULCFileLanguageDataSource();

    ULCFileLanguageDataSource& operator=(const ULCFileLanguageDataSource& other);

    // Required ULDataSource interfaces.
    virtual ULError attach(const ULString& dataSourceIdentifier);
    virtual ULError detach();
    virtual ULError load();
    virtual ULError close();
    virtual ULString getDataSourceIdentifier();
    virtual ULDataSourceVersion getVersion();

    // Extracting data from the language data source.
    virtual const ULLanguage& getLanguage();
    virtual ULError getWords(const ULString& root, ULList<ULDerivation>& wordList, bool filterResults=false);
    virtual ULError getVerbs(const ULString& infinitive, ULList<ULDerivation>& verbList, bool filterResults=false);
    virtual ULError getNouns(const ULString& text, ULList<ULDerivation>& nounList);
    virtual ULError getVerbModel(uluint32 verbClassID, ULVerbModel& model);
    virtual ULError getMatchingRoots(const ULString& prefix, uluint32 maxMatches, ULList<ULString>& rootList);
    virtual ULError getMatchingNouns(const ULString& prefix, uluint32 maxMatches, ULList<ULString>& nounList);
    virtual ULError getMatchingInfinitives(const ULString& prefix, uluint32 maxMatches, ULList<ULString>& infinitiveList);
    virtual ULError getMatchingInfinitives(const ULString& prefix, uluint32 maxMatches, ULList<ULDerivation>& infinitiveList);
    virtual ULError getVerbFormTypes(const ULDerivation& verb, ULList<ULPartOfSpeech>& verbFormTypes);
    virtual ULError getTenses(const ULDerivation& v, ULList<ULTense>& tenseList, bool includeParticiples=false);
    virtual ULError getTensesForClass(uluint32 classID, ULList<ULTense>& tenseList, bool includeParticiples=false);
    virtual ULError getAllTenses(ULList<ULTense>& tenseList, bool includeParticiples=false);
    virtual ULError getPersons(const ULDerivation& v, ULTense tense, ULList<ULPerson>& personList);
    virtual ULError getPersonsForClass(const ULDerivation& v, uluint32 classID, ULTense tense, ULList<ULPerson>& personList);    
    virtual ULError getAllTaggingRules(ULList<ULTaggingRule>& ruleList);

    virtual ULError getFeatureNameList(ULList<ULString>& featureNameList);

    virtual ULError getInflectionRules(const ULDerivation& derivation,
                                       const ULPartOfSpeech& targetPartOfSpeech,
                                       ULList<ULInflectionRule>& ruleList);

    virtual ULError getInflectionRulesForDissection(const ULDerivation& derivation, ULList<ULInflectionRule>& ruleList);

    virtual ULError getSuccessors(const ULInflectionRule& rule, ULList<ULInflectionRule>& successorList);
    virtual ULError getPredecessors(const ULInflectionRule& rule, ULList<ULInflectionRule>& predecessorList);

    virtual ULError getAllStopWords(ULList<ULString>& wordList);
    virtual bool hasStopWord(const ULString& word);
    virtual ULError getAllClosedClassWords(ULList<ULDerivation>& wordList);
    virtual ULError getClosedClassWord(const ULString& searchText, ULList<ULDerivation>& wordList);
    virtual ULError getClosedClassWordForPartOfSpeech(const ULPartOfSpeech& partOfSpeech, ULList<ULDerivation>& wordList);
    
    virtual ULError getFrequencies(const ULString& word, ULList<ULFrequency>& frequencyList);

private:
    enum { MagicNumber = 0xDADAD00D, RuleIndexEntrySize = 15 };

    ULString fileName;
    uluint32 magicNumber;
    ULDataSourceVersion version;
    ULLanguage language;
    uluint32 nVerbs;
    ULHashTable<ULString, ULString> ulcAttributes; // (name, value) pairs from the .ulc file header

    ULSegmentedFile segmentedFile;

    uluint32 nounSegmentIndex;
    uluint32 verbSegmentIndex;
    uluint32 verbModelSegmentIndex;
    uluint32 inflectionRuleSegmentIndex;
    uluint32 taggingRuleSegmentIndex;
    uluint32 frequencySegmentIndex;
    uluint32 fullWordSourceIndexSegmentIndex;
    uluint32 fullWordTargetIndexSegmentIndex;
    uluint32 ordinaryRuleIndexSegmentIndex;
 
    uluint32 nVerbModels;
    ULVerbModel *verbModels;
    uluint32 nOrdinaryInflectionRules;
    ULInflectionRule *ordinaryInflectionRules;
    bool *isInflectionRuleAtStartOfRange;
    ULHashTable<ULInteger, uluint32> offsetToIndexMap;
    ULHashTable<ULString, uluint32> stopWords;

    // File management.
    void clear();
    ULError open();
    ULError doClose();
    ULError doDetach();

    // Non-synchronized worker methods corresponding to synchronized public methods.
    ULError doGetVerbModel(uluint32 verbClassID, ULVerbModel& model);
    ULError doGetAllTenses(ULList<ULTense>& tenseList, bool includeParticiples);

    // Feature map.
    ULHashTable<ULString, uluint32> featureNameToIDMap;
    ULVector<ULString> featureIDToNameMap;
    ULVector<uluint32> dataSourceIDToRunTimeIDMap;
    void initializeFeatureMaps();
    uluint32 getRunTimeFeatureID(uluint32 dataSourceFeatureID);

    // Utility functions.
    void removeSeparators(ULString& s);
    void loadVerbModels();
    void loadInflectionRules();
    void loadStopWords();

    ULError getRussianVerbFormTypes(const ULDerivation& verb, ULList<ULPartOfSpeech>& verbFormTypes);
    void findFirstMatchingVerb(const ULString& prefix, uluint32& offset, ULDerivation& verb);
    uluint32 getVerbAtOffset(uluint32 offset, ULDerivation& verb);

    ULError doGetFeatureNameList(ULList<ULString>& featureNameList);
    void findFirstMatchingNoun(const ULString& prefix, uluint32& offset, ULDerivation& noun);
    uluint32 getNounAtOffset(uluint32 offset, ULDerivation& noun);
    
    void getFullWordRulesForEmptyInflection(const ULDerivation& derivation, const ULPartOfSpeech& targetPartOfSpeech, ULList<ULInflectionRule>& ruleList);
    void getOrdinaryRulesForGenericEmptyInflection(const ULDerivation& derivation, const ULPartOfSpeech& targetPartOfSpeech, ULList<ULInflectionRule>& ruleList);
    void getOrdinaryRulesForNongenericEmptyInflection(const ULDerivation& derivation, const ULPartOfSpeech& targetPartOfSpeech, ULList<ULInflectionRule>& ruleList);
    void getOrdinaryRulesForSearchKey(const ULDerivation& derivation, const ULPartOfSpeech& targetPartOfSpeech, const uluint32 *searchKey, ULList<ULInflectionRule>& ruleList);
    
    void getMatchingFullWordRules(const ULString& word, bool matchSourceAffix, ULList<ULInflectionRule>& ruleList);
    uluint32 getFullWordIndexEntryAtOffset(uluint32 offset, uluint32 segmentIndex, ULString& text, uluint32& ruleOffset);

    void getOrdinaryRuleIndexEntryAtOffset(uluint32 offset, bool& isStartOfRange, uluint32& ruleOffset, uluint32 *searchKey);
    uluint32 findFirstMatchingInflectionRule(const uluint32 *searchKey);
    uluint32 getInflectionRuleAtOffset(uluint32 offset, ULInflectionRule& rule);
    uluint32 getClosedClassWordAtOffset(uluint32 offset, uluint32 segmentIndex, uluint32 segmentSize, ULDerivation& closedClassWord);
    
    static int frequencyCmp(ULFrequency& a, ULFrequency& b, void *context);
    void findFirstMatchingFrequency(const ULString& word, uluint32& offset, ULFrequency& frequency);
    uluint32 getFrequencyAtOffset(uluint32 offset, ULFrequency& frequency);
};

#endif

