/**
 * @file uldfiledictionarydatasource.h
 * @brief The interface for the ULDFileDictionaryDataSource class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULDFILEDICTIONARYDATASOURCE_H
#define ULDFILEDICTIONARYDATASOURCE_H

#include "uldictionary.h"
#include "uldictionarydatasource.h"
#include "ulpartofspeechcategory.h"
#include "ulsegmentedfile.h"

/**
 * @class ULDFileDictionaryDataSource
 * @brief ULDFileDictionaryDataSource implements the ULDictionaryDataSource
 * protocol (and thus both the ULDictionary and ULDataSource protocols)
 * by using ULAPI 8 .uld files.
 */
class ULDFileDictionaryDataSource : public ULDictionaryDataSource
{
    UL_TEST_FRIEND;
    friend class ULDFileDictionaryIterator;

public:
    ULDFileDictionaryDataSource();
    ULDFileDictionaryDataSource(const ULDFileDictionaryDataSource& other);
    virtual ~ULDFileDictionaryDataSource();

    ULDFileDictionaryDataSource& operator=(const ULDFileDictionaryDataSource& other);
    virtual bool operator==(const ULDictionaryDataSource& dataSource) const;
    virtual bool operator==(const ULDFileDictionaryDataSource& dataSource) const;

    // ULWorker operations.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& serviceList);

    // ULDataSource operations.
    virtual ULError attach(const ULString& dataSourceIdentifier);
    virtual ULError detach();
    virtual ULError load();
    virtual ULError close();
    virtual ULString getDataSourceIdentifier();
    virtual ULDataSourceVersion getVersion();

    // ULDictionary operations.
    virtual ULError begin(const ULLanguage& searchLanguage,
                          uluint32 indexID,
                          ULDictionaryIterator& iterator);

    virtual ULError end(const ULLanguage& searchLanguage,
                        uluint32 indexID,
                        ULDictionaryIterator& iterator);

    virtual ULError find(const ULString& s,
                         const ULLanguage& searchLanguage,
                         uluint32 indexID,
                         ULDictionaryIterator& iterator);

    // ULDictionaryDataSource operations.
    virtual ULDictionaryDescriptor getDictionaryDescriptor();
    virtual const ULLanguage& getFirstLanguage();
    virtual const ULLanguage& getSecondLanguage();
    virtual const ULLanguage& getOtherLanguage(const ULLanguage& language);
    virtual ULForestType getForestType();
    virtual ULForester *getForester();
    virtual void setForester(ULForester *newForester);
    virtual ULError getFeatureNameList(ULList<ULString>& featureNameList);
    virtual ULError getLocalizationRuleList(const ULLanguage& language, ULList<ULLocalizationRule>& localizationRulesList);
    virtual bool getULDAttribute(const ULString& key, ULString& value);

protected:
    enum {ULD8MagicNumber = 0xD1C7DA7A};

    ULString fileName;
    uluint32 magicNumber;
    ULDataSourceVersion version;
    ULLanguage firstLanguage;
    ULLanguage secondLanguage;
    ULHashTable<ULString, ULString> uldAttributes; // (name, value) pairs from the .uld file header

    ULSegmentedFile segmentedFile;
    ULForester *forester;

    // File management.
    void clear();
    ULError open();
    bool isOpen() const;

    // Search.
    ULError doFind(const ULString& s,
                   const ULLanguage& searchLanguage,
                   uluint32 indexID,
                   ULDictionaryIterator& iterator);

    // Utilities.
    uluint32 getSegmentIndexForIndex(uluint32 indexID, const ULLanguage& language);
};

#endif

