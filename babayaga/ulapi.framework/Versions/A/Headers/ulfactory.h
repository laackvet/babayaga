/**
 * @file ulfactory.h
 * @brief The interface for the ULFactory class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULFACTORY_H
#define ULFACTORY_H

#include "ulstring.h"
#include "ulerror.h"
#include "ullock.h"
#include "ulcollator.h"
#include "ulstemmer.h"
#include "uldefiner.h"
#include "ulpartofspeechtagger.h"
#include "ulpluralizer.h"
#include "ulsingularizer.h"
#include "ulconjugator.h"
#include "uldictionary.h"
#include "ulconfiguration.h"
#include "ulnumbertranslator.h"

typedef ULLanguageDataSource * ULLanguageDataSourcePtr;
typedef ULDictionaryDataSource * ULDictionaryDataSourcePtr;

/**
 * @class ULFactory
 * @brief ULFactory is the abstract parent for the classes that provide
 * instantiation and initialization services for ULAPI objects.  For example,
 * the Mac desktop version of Ultralingua would instantiate
 * ULUltralinguaMacFactory and use it to instantiate locks, data sources,
 * workers, etc.
 *
 * Most applications of ULAPI will instantiate a single ULFactory object via
 * ULFactory::createFactory, inform the factory of the various data sources
 * available to it via addLanguageDataSource and addDictionaryDataSource,
 * and then use the factory to instantiate and initialize workers as needed.
 *
 * One of the main benefits of using ULFactory is memory management. A ULFactory
 * object keeps track of the workers, locks, data sources, etc. that it has
 * instantiated, and deletes them when it is destroyed. Thus, if you use
 * ULFactory to instantiate these objects, you only need delete the factory
 * itself. Furthermore, you can reduce the memory footprint of ULAPI at any
 * time by calling freeInessentialMemory, which will close data sources,
 * flush caches, etc. and only reopen them when new service requests are made.
 */
class ULFactory : public ULLockable
{
public:
    static ULFactory *createFactory(const ULString& id);
    
    ULFactory();
    virtual ~ULFactory();

    /**
     * Restores this factory to its default state, with no associated data
     * sources, workers, etc. The factory ID and configuration are not
     * altered, however.
     */
    virtual void clear();

    void setFactoryID(const ULString& id);
    const ULString& getFactoryID();

    /**
     * Closes all the dictionary and language data sources controlled
     * by this factory, thus freeing up all data source data loaded into
     * memory.  Later requests for service from tools obtained via
     * this factory will re-open the data sources as needed.
     */
    virtual void freeInessentialMemory();

    /**
     * Instantiates and initializes a language data source attached to the
     * specified file or database.
     * @return one of these error codes: ULError::NoError,
     * ULError::MemoryAllocationFailed, or ULError::DataSourceAttachFailed (if
     * the specified file can't be opened).
     * @param[in] fileIdentifier A string (such as a path) identifying
     * the desired file or database.
     * @param[in] loadImmediately determines whether this method should open the data
     * source immediately, performing initial loading operations now rather than at
     * the time the data source is first queried.
     * @param[out] dataSource A pointer to the pointer to the resulting data source
     * if one was created successfully
     */
    virtual ULError addLanguageDataSource(const ULString& fileIdentifier, bool loadImmediately=false, ULLanguageDataSource **dataSource=NULL) = 0;

    /**
     * Instantiates and initializes a dictionary data source attached to the
     * specified file or database.
     * @return one of these error codes: ULError::NoError,
     * ULError::MemoryAllocationFailed, ULError::DataSourceAttachFailed (if
     * the specified file can't be opened), or ULError::InvalidLanguageFile
     * (if the specified file is not a valid language file).
     * @param[in] fileIdentifier A string (such as a path) identifying
     * the desired file or database.
     * @param[in] loadImmediately determines whether this method should open the data
     * source immediately, performing initial loading operations now rather than at
     * the time the data source is first queried.
     * @param[out] dataSource A pointer to the pointer to the resulting data source
     * if one was created successfully
     */
    virtual ULError addDictionaryDataSource(const ULString& fileIdentifier, bool loadImmediately=false, ULDictionaryDataSource **dataSource=NULL) = 0;

    /**
     * @return a pointer to an instance of some child of ULLock,
     * as appropriate for this factory and platform. Each call to createLock
     * creates a new ULLock instance. The caller of createLock is responsible for
     * deleting the lock when it is no longer needed.
     */
    virtual ULLock *createLock() = 0;

    /**
     * Retrieves the list of languages for which this factory has a 
     * ULLanguageDataSource (added at some time previously by addLanguageDataSource).
     * This list of languages is a reliable guide to determining the languages
     * for which this factory can generate a ULConjugator, ULNumberTranslator,
     * or any other worker that does not require a ULDictionaryDataSource to
     * operate.
     *
     * @param[out] languageList The list of available languages.
     * @return ULError::NoMatch if there are none, and ULError::NoError otherwise.
     */
    virtual ULError getAvailableLanguages(ULList<ULLanguage>& languageList);
    
    /**
     * Retrieves the list of languages for which this factory has a 
     * ULLanguageDataSource (added at some time previously by addLanguageDataSource).
     * This list of languages is a reliable guide to determining the languages
     * for which this factory can generate a ULConjugator, ULNumberTranslator,
     * or any other worker that does not require a ULDictionaryDataSource to
     * operate.
     *
     * @param[out] languageList The list of available languages.
     * @return ULError::NoMatch if there are none, and ULError::NoError otherwise.
     */
    virtual ULError getAvailableLanguages(ULList<ULLanguageDataSource *>& ldsList);
    
    /**
     * Retrieves the list of dictionaries for which this factory has a 
     * ULDictionaryDataSource (added at some time previously by addDictionaryDataSource).
     * This list of languages is a reliable guide to determining the dictionaries
     * for which this factory can generate a ULDefiner or any other worker that
     * requires only a dictionary data source.
     *
     * @param[out] dictionaryList The list of available dictionaries.
     * @return ULError::NoMatch if there are none, and ULError::NoError otherwise.
     */
    virtual ULError getAvailableDictionaries(ULList<ULDictionaryDescriptor>& dictionaryList);
    
    /**
     * Retrieves the list of dictionaries for which this factory has a 
     * ULDictionaryDataSource (added at some time previously by addDictionaryDataSource).
     * This list of languages is a reliable guide to determining the dictionaries
     * for which this factory can generate a ULDefiner or any other worker that
     * requires only a dictionary data source.
     *
     * @param[out] dictionaryList The list of available dictionaries.
     * @return ULError::NoMatch if there are none, and ULError::NoError otherwise.
     */
    virtual ULError getAvailableDictionaries(ULList<ULDictionaryDataSource *>& ddsList);
    
    /**
     * Retrieves the list of services that this factory can support.
     * This list of languages is a reliable guide to determining the dictionaries
     * for which this factory can generate a ULDefiner or any other worker that
     * requires only a dictionary data source.
     *
     * @param[out] serviceList The list of available services.
     * @return ULError::NoMatch if there are none, and ULError::NoError otherwise.
     */
    virtual ULError getAvailableServices(ULList<ULServiceDescriptor>& serviceList) = 0;

    /**
     * Gets the ULLanguageDataSources objects associated with the
     * specified language.
     * @param[in] language The language of the desired data sources.
     * @param[out] ldsList The list of matching language data sources.
     * @return ULError::NoMatch if no matching data sources were found. Otherwise, ULError::NoError.
     */
    virtual ULError getLanguageDataSources(const ULLanguage& language, ULList<ULLanguageDataSource *>& ldsList);

    /**
     * Gets the ULDictionaryDataSource objects associated with the
     * specified language pair.
     * @param[in] lang1, lang2 The language pair for the desired data sources.
     * @param[out] ddsList The list of matching dictionary data sources.
     * @return ULError::NoMatch if no matching data sources were found. Otherwise, ULError::NoError.
     */
    virtual ULError getDictionaryDataSources(const ULLanguage& lang1, const ULLanguage& lang2, ULList<ULDictionaryDataSource *>& ddsList);
    
    /**
     * Gets the ULDictionaryDataSource objects that has the given language
     * as at least one of its two languages.
     * @param[in] lang The language to be one of the two languages in the desired data sources.
     * @param[out] ddsList The list of matching dictionary data sources.
     * @return ULError::NoMatch if no matching data sources were found. Otherwise, ULError::NoError.
     */
    virtual ULError getDictionaryDataSources(const ULLanguage& lang, ULList<ULDictionaryDataSource *>& ddsList);
    
    /**
     * Gets the ULDictionaryDataSource objects associated with the
     * specified dictionary descriptor.
     * @param[in] descriptor A descriptor for the desired dictionary.
     * @param[out] ddsList The list of matching dictionary data sources.
     * @return ULError::NoMatch if no matching data sources were found. Otherwise, ULError::NoError.
     */
    virtual ULError getDictionaryDataSources(const ULDictionaryDescriptor& descriptor, ULList<ULDictionaryDataSource *>& ddsList);
    
    /**
     * @return a pointer to a ULCollator object suitable for
     * use with the specified language, or 0 if no such object can be
     * instantiated and initialized. The most likely reason for a 0 return
     * value is the absence of a suitable language data source (.ulc
     * or .ull file or equivalent).
     */
    virtual ULCollator *getCollator(const ULLanguage& language) = 0;

    /**
     * @return a pointer to a ULStemmer object based on the specified data sources,
     * or 0 if no such object can be instantiated and initialized.
     *
     * A ULStemmer requires a language data source to provide inflection information,
     * and dictionary data source to look up candidate stems in the dictionary. This method gives
     * you the greatest control over exactly which data sources to use to generate
     * stems. However, for many applications of ULAPI, you will only have one
     * dictionary data source and one or two language data sources. In such cases, it is
     * typically more convenient to use ULFactory::getStemmer(ULLanguage, ULLanguage).
     */
    virtual ULStemmer *getStemmer(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource) = 0;
    
    /**
     * @return a pointer to a ULStemmer object suitable for use with the specified
     * stemming language, or 0 if no such object can be instantiated and initialized.
     * The most likely reason for a 0 return value is the absence of a suitable language
     * data source (.ulc file) or dictionary data source (.uld file).
     *
     * If you have more than one language data source (.ulc file) for stemmingLanguage,
     * or more than one dictionary data source (.uld file) for stemmingLanguage and
     * translationLanguage, then this method will just choose one of each for you. If you
     * need to specify exactly which data sources your ULStemmer uses, use
     * ULFactory::getStemmer(ULLanguageDataSource*, ULDictionaryDataSource*).
     *
     * If translationLanguage == ULLanguage::NoLanguage, the first dictionary data source
     * compatible with stemmingLanguage that the factory finds will be used.
     */
    virtual ULStemmer *getStemmer(const ULLanguage& stemmingLanguage, const ULLanguage& translationLanguage) = 0;

    /**
     * @return a pointer to a ULPartOfSpeechTagger object based on the specified data sources,
     * or 0 if no such object can be instantiated and initialized.
     *
     * A ULPartOfSpeechTagger requires a language data source to provide inflection
     * information, and dictionary data source to support stemming. This method gives
     * you the greatest control over exactly which data sources to use to generate
     * part of speech tags. However, for many applications of ULAPI, you will only have one
     * dictionary data source and one or two language data sources. In such cases, it is
     * typically more convenient to use ULFactory::getPartOfSpeechTagger(ULLanguage, ULLanguage).
     */
    virtual ULPartOfSpeechTagger *getPartOfSpeechTagger(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource) = 0;

    /**
     * @return a pointer to a ULPartOfSpeechTagger object suitable for use with the specified
     * tagging language, or 0 if no such object can be instantiated and initialized.
     * The most likely reason for a 0 return value is the absence of a suitable language
     * data source (.ulc file) or dictionary data source (.uld file).
     *
     * If you have more than one language data source (.ulc file) for taggingLanguage,
     * or more than one dictionary data source (.uld file) for taggingLanguage and
     * translationLanguage, then this method will just choose one of each for you. If you
     * need to specify exactly which data sources your ULPartOfSpeechTagger uses, use
     * ULFactory::getPartOfSpeechTagger(ULLanguageDataSource*, ULDictionaryDataSource*).
     *
     * If translationLanguage == ULLanguage::NoLanguage, the first dictionary data source
     * compatible with taggingLanguage that the factory finds will be used.
     */
    virtual ULPartOfSpeechTagger *getPartOfSpeechTagger(const ULLanguage& taggingLanguage, const ULLanguage& translationLanguage) = 0;

    /**
     * @return a pointer to a ULPluralizer object based on the specified data sources,
     * or 0 if no such object can be instantiated and initialized.
     *
     * A ULPluralizer requires a language data source to provide plural construction
     * information, and dictionary data source to support stemming so you can pluralize
     * a noun without knowing whether you already have the singular or the plural in hand
     * (e.g. both "dog" and "dogs" would yield "dogs" as a plural). This method gives
     * you the greatest control over exactly which data sources to use to generate
     * plural forms. However, for many applications of ULAPI, you will only have one
     * dictionary data source and one or two language data sources. In such cases, it is
     * typically more convenient to use ULFactory::getPluralizer(ULLanguage, ULLanguage).
     */
    virtual ULPluralizer *getPluralizer(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource) = 0;

    /**
     * @return a pointer to a ULPluralizer object suitable for use with the specified
     * pluralizer language, or 0 if no such object can be instantiated and initialized.
     * The most likely reason for a 0 return value is the absence of a suitable language
     * data source (.ulc file) or dictionary data source (.uld file).
     *
     * If you have more than one language data source (.ulc file) for pluralizerLanguage,
     * or more than one dictionary data source (.uld file) for pluralizerLanguage and
     * translationLanguage, then this method will just choose one of each for you. If you
     * need to specify exactly which data sources your ULPluralizer uses, use
     * ULFactory::getPluralizer(ULLanguageDataSource*, ULDictionaryDataSource*).
     *
     * If translationLanguage == ULLanguage::NoLanguage, the first dictionary data source
     * compatible with pluralizerLanguage that the factory finds will be used.
     */
    virtual ULPluralizer *getPluralizer(const ULLanguage& pluralizerLanguage, const ULLanguage& translationLanguage) = 0;

    /**
     * @return a pointer to a ULSingularizer object based on the specified data sources,
     * or 0 if no such object can be instantiated and initialized.
     *
     * A ULSingularizer requires a language data source to provide plural construction
     * information, dictionary data source to look up candidate singular nouns in
     * the dictionary. This getSingularizer interface gives
     * you the greatest control over exactly which data sources to use to generate
     * singular forms. However, for many applications of ULAPI, you will only have one
     * dictionary data source and one or two language data sources. In such cases, it is
     * typically more convenient to use ULFactory::getSingularizer(ULLanguage, ULLanguage).
     */
    virtual ULSingularizer *getSingularizer(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource) = 0;

    /**
     * @return a pointer to a ULSingularizer object suitable for use with the specified
     * singularizer language, or 0 if no such object can be instantiated and initialized.
     * The most likely reason for a 0 return value is the absence of a suitable language
     * data source (.ulc file) or dictionary data source (.uld file).
     *
     * If you have more than one language data source (.ulc file) for singularizerLanguage,
     * or more than one dictionary data source (.uld file) for singularizerLanguage and
     * translationLanguage, then this method will just choose one of each for you. If you
     * need to specify exactly which data sources your ULSingularizer uses, use
     * ULFactory::getSingularizer(ULLanguageDataSource*, ULDictionaryDataSource*).
     *
     * If translationLanguage == ULLanguage::NoLanguage, the first dictionary data source
     * compatible with singularizerLanguage that the factory finds will be used.
     */
    virtual ULSingularizer *getSingularizer(const ULLanguage& singularizerLanguage, const ULLanguage& translationLanguage) = 0;

    /**
     * @return a pointer to a ULConjugator object based on the specified data sources,
     * or 0 if no such object can be instantiated and initialized.
     *
     * A ULConjugator requires a language data source to generate conjugations, and a
     * dictionary data source to perform stemming (so you can ask for the conjugation
     * of "aller" by specifying "allez" or "vais" or whatever form of the verb is
     * convenient, not just via the infinitive). This getConjugator interface gives
     * you the greatest control over exactly which data sources to use to generate
     * conjugations. However, for many applications of ULAPI, you will only have one
     * dictionary data source and one or two language data sources. In such cases, it is
     * typically more convenient to use ULFactory::getConjugator(ULLanguage, ULLanguage).
     */
    virtual ULConjugator *getConjugator(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource) = 0;

    /**
     * @return a pointer to a ULConjugator object suitable for use with the specified
     * conjugation language, or 0 if no such object can be instantiated and initialized.
     * The most likely reason for a 0 return value is the absence of a suitable language
     * data source (.ulc file) or dictionary data source (.uld file).
     *
     * If you have more than one language data source (.ulc file) for conjugationLanguage,
     * or more than one dictionary data source (.uld file) for conjugationLanguage and
     * translationLanguage, then this method will just choose one of each for you. If you
     * need to specify exactly which data sources your ULConjugator uses, use
     * ULFactory::getConjugator(ULLanguageDataSource*, ULDictionaryDataSource*).
     *
     * If translationLanguage == ULLanguage::NoLanguage, the first dictionary data source
     * compatible with conjugationLanguage that the factory finds will be used.
     */
    virtual ULConjugator *getConjugator(const ULLanguage& conjugationLanguage, const ULLanguage& translationLanguage) = 0;

    /**
     * @return a pointer to a ULDefiner object based on the specified data sources,
     * or 0 if no such object can be instantiated and initialized.
     *
     * A ULDefiner requires a dictionary data source to provide word lookup, and one or two
     * language data sources to support stemmed search in the language(s) supported by
     * the dictionary data source. This method gives you the greatest control over exactly
     * which data sources to use to perform dictionary searches. However, for many
     * applications of ULAPI, you will only have one dictionary data source and one
     * or two language data sources. In such cases, it is typically more convenient to
     * use ULFactory::getDefiner(ULLanguage, ULLanguage).
     */
    virtual ULDefiner *getDefiner(ULDictionaryDataSource *dictionaryDataSource, ULLanguageDataSource *firstLanguageDataSource, ULLanguageDataSource *secondLanguageDataSource) = 0;
    
    /**
     * @return a pointer to a ULDefiner object suitable for use with the specified
     * definition language, or 0 if no such object can be instantiated and initialized.
     * The most likely reason for a 0 return value is the absence of a suitable language
     * data source (.ulc file) or dictionary data source (.uld file).
     *
     * If you have more than one language data source (.ulc file) or more than one
     * dictionary data source (.uld file) for the languages in question, then this method
     * will just choose data sources for you. If you need to specify exactly which data
     * sources your ULDefiner uses, use
     * ULFactory::getDefiner(ULDictionaryDataSource*, ULLanguageDataSource*, ULLanguageDataSource*).
     */
    virtual ULDefiner *getDefiner(const ULLanguage& language1, const ULLanguage& language2) = 0;

    /**
     * @return a pointer to a ULInflector object suitable for
     * use with the specified data sources, or 0 if no such object can be
     * instantiated and initialized. The most likely reason for a 0 return
     * value is the absence of a suitable data source files (.ulc or .uld files)
     *
     * A ULInflector requires a both a language data source and a dictionary data
     * source to perform inflections. This getInflector interface gives
     * you the greatest control over exactly which data sources to use to generate
     * inflections. However, for many applications of ULAPI, you will only have one
     * dictionary data source and one or two language data sources. In such cases, it is
     * typically more convenient to use ULFactory::getInflector(ULLanguage, ULLanguage).
     */
    virtual ULInflector *getInflector(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource) = 0;
    
    /**
     * @return a pointer to a ULInflector object suitable for use with the specified
     * inflection language, or 0 if no such object can be instantiated and initialized.
     * The most likely reason for a 0 return value is the absence of a suitable language
     * data source (.ulc file) or dictionary data source (.uld file).
     *
     * If you have more than one language data source (.ulc file) for inflectionLanguage,
     * or more than one dictionary data source (.uld file) for inflectionLanguage and
     * translationLanguage, then this method will just choose one of each for you. If you
     * need to specify exactly which data sources your ULInflector uses, use
     * ULFactory::getInflector(ULLanguageDataSource*, ULDictionaryDataSource*).
     *
     * If translationLanguage == ULLanguage::NoLanguage, the first dictionary data source
     * compatible with inflectionLanguage that the factory finds will be used.
     */
    virtual ULInflector *getInflector(const ULLanguage& inflectionLanguage, const ULLanguage& translationLanguage) = 0;

    /**
     * @return a pointer to a ULDissector object suitable for
     * use with the specified data sources, or 0 if no such object can be
     * instantiated and initialized. The most likely reason for a 0 return
     * value is the absence of a suitable data source files (.ulc or .uld files)
     *
     * A ULDissector requires a both a language data source and a dictionary data
     * source to perform dissections. This getDissector interface gives
     * you the greatest control over exactly which data sources to use to generate
     * dissections. However, for many applications of ULAPI, you will only have one
     * dictionary data source and one or two language data sources. In such cases, it is
     * typically more convenient to use ULFactory::getInflector(ULLanguage, ULLanguage).
     */
    virtual ULDissector *getDissector(ULLanguageDataSource *languageDataSource, ULDictionaryDataSource *dictionaryDataSource) = 0;

    /**
     * @return a pointer to a ULDissector object suitable for use with the specified
     * dissection language, or 0 if no such object can be instantiated and initialized.
     * The most likely reason for a 0 return value is the absence of a suitable language
     * data source (.ulc file) or dictionary data source (.uld file).
     *
     * If you have more than one language data source (.ulc file) for dissectionLanguage,
     * or more than one dictionary data source (.uld file) for dissectionLanguage and
     * translationLanguage, then this method will just choose one of each for you. If you
     * need to specify exactly which data sources your ULDissector uses, use
     * ULFactory::getDissector(ULLanguageDataSource*, ULDictionaryDataSource*).
     *
     * If translationLanguage == ULLanguage::NoLanguage, the first dictionary data source
     * compatible with dissectionLanguage that the factory finds will be used.
     */
    virtual ULDissector *getDissector(const ULLanguage& dissectionLanguage, const ULLanguage& translationLanguage) = 0;

    /**
     * @return a pointer to a ULNumberTranslator object, or
     * 0 if no such object can be instantiated and initialized.
     */
    virtual ULNumberTranslator *getNumberTranslator(const ULLanguage& language) = 0;

protected:
    ULString factoryID;
    ULConfiguration configuration;
    ULList<ULWorker *> workers;
    ULList<ULLanguageDataSource *> languageDataSources;
    ULList<ULDictionaryDataSource *> dictionaryDataSources;
    ULList<ULCollator *> collators;

    ULWorker *findWorker(ULServiceDescriptor descriptor);
    ULError findLanguageDataSources(const ULLanguage& language, ULList<ULLanguageDataSource *>& ldsList);
    ULError findDictionaryDataSources(const ULDictionaryDescriptor& descriptor, ULList<ULDictionaryDataSource *>& ddsList);
    ULError findDictionaryDataSources(const ULLanguage& lang1, const ULLanguage& lang2, ULList<ULDictionaryDataSource *>& ddsList);
    ULError findDictionaryDataSources(const ULLanguage& lang, ULList<ULDictionaryDataSource *>& ddsList);

    ULLanguageDataSource *findLanguageDataSource(const ULString& fileIdentifier);
    ULDictionaryDataSource *findDictionaryDataSource(const ULString& fileIdentifier);
};

#endif

