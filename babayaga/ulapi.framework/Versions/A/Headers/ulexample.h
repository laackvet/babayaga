/**
 * @file ulexample.h
 * @brief The interface for the ULExample class.
 * @author Copyright (C) 2012 Ultralingua, Inc.
 */

#ifndef ULEXAMPLE_H
#define ULEXAMPLE_H

//#include "ulerror.h"
//#include "ulstring.h"
#include "ulcontainers.h"
#include "ulfeature.h"
#include "ulfeaturetype.h"

/**
 * @class ULExample
 * @brief An object of type ULExample represents a single example
 */
class ULExample
{
public:
    ULExample();
    ULExample(const ULExample& other);
    virtual ~ULExample();
    
    void clear();
    ULExample& operator=(const ULExample& other);
    bool operator==(const ULExample& other) const;
    bool operator!=(const ULExample& other) const;
    
    // Accessors
    ULString getText() const;
    ULString getTranslation() const;

    void addFeature(const ULFeatureType& featureType, ULString value);
    void addFeature(const ULFeature& feature);
    void setFeature(const ULFeatureType& featureType, ULString value);
    bool hasFeature(const ULFeatureType& featureType) const;
    bool getFeature(const ULFeatureType& featureType, ULFeature& feature) const;
    
    ULString toString() const;
    
    const ULList<ULFeature>& getFeatureList() const;
    
protected:
    // The feature list is kept sorted for efficient comparison.
    ULList<ULFeature> featureList;
    bool findFeature(const ULFeatureType& featureType, ULString& featureValue) const;
};

#endif

