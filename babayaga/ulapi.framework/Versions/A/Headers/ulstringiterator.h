/**
 * @file ulstringiterator.h
 * @brief The interfaces for the ULStringIterator and ULStringReverseIterator classes.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULSTRINGITERATOR_H
#define ULSTRINGITERATOR_H

#include "ulstring.h"

/**
 * @class ULStringIterator
 * @brief An iterator that moves forward (via operator++) through a ULString object.
 */
class ULStringIterator
{
    friend class ULString;
    friend class ULStringReverseIterator;
    UL_TEST_FRIEND;

public:
    ULStringIterator();
    ULStringIterator(const ULStringIterator& iterator);
    ~ULStringIterator();

    ULStringIterator& operator=(const ULStringIterator& iterator);
    operator ULStringReverseIterator() const;

    bool operator==(const ULStringIterator& iterator) const;
    bool operator!=(const ULStringIterator& iterator) const;

    int getCharIndex() const;
    const ULString *getString() const;

    bool isAtEnd() const;
    bool isAtBeginning() const;
    operator bool() const;

    void operator++();
    void operator++(int);
    void operator--();
    void operator--(int);
    void operator+=(int nChars);
    void operator-=(int nChars);
    ulchar operator*() const;

private:
    int charIndex;
    const ULString *string;
};


/**
 * @class ULStringReverseIterator
 * @brief An iterator that moves backwards (via operator++) through a ULString object.
 */
class ULStringReverseIterator
{
    friend class ULString;
    friend class ULStringIterator;

public:
    ULStringReverseIterator();
    ULStringReverseIterator(const ULStringReverseIterator& iterator);
    ~ULStringReverseIterator();

    ULStringReverseIterator& operator=(const ULStringReverseIterator& iterator);
    operator ULStringIterator() const;

    bool operator==(const ULStringReverseIterator& s) const;
    bool operator!=(const ULStringReverseIterator& s) const;

    int getCharIndex() const;
    const ULString *getString() const;

    bool isAtEnd() const;
    bool isAtBeginning() const;
    operator bool() const;

    void operator++();
    void operator++(int);
    void operator--();
    void operator--(int);
    void operator+=(int nChars);
    void operator-=(int nChars);
    ulchar  operator*() const;

private:
    int charIndex;
    const ULString *string;
};

#endif
