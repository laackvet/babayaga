/**
 * @file ulpartofspeechsubcategory.h
 * @brief The interface for the ULPartOfSpeechSubcategory class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULPARTOFSPEECHSUBCATEGORY_H
#define ULPARTOFSPEECHSUBCATEGORY_H

#include "ulenum.h"

/**
 * @class ULPartOfSpeechSubcategory
 * @brief An enhanced enumerated type used to represent the "subcategory" attribute associated with many
 * part of speech categories. For example, noun subcategories include proper, diminutive, and
 * abbreviation, while pronoun types include demonstrative, personal, possessive, etc.
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULPartOfSpeechSubcategory : public ULEnum
{
public:
    static const ULPartOfSpeechSubcategory& None;
    static const ULPartOfSpeechSubcategory& Any;
    static const ULPartOfSpeechSubcategory& Proper;
    static const ULPartOfSpeechSubcategory& Diminutive;
    static const ULPartOfSpeechSubcategory& Abbreviation;
    static const ULPartOfSpeechSubcategory& Acronym;
    static const ULPartOfSpeechSubcategory& Doer;
    static const ULPartOfSpeechSubcategory& Personal;
    static const ULPartOfSpeechSubcategory& Demonstrative;
    static const ULPartOfSpeechSubcategory& Interrogative;
    static const ULPartOfSpeechSubcategory& Relative;
    static const ULPartOfSpeechSubcategory& Reflexive;
    static const ULPartOfSpeechSubcategory& Reciprocal;
    static const ULPartOfSpeechSubcategory& Indefinite;
    static const ULPartOfSpeechSubcategory& Definite;
    static const ULPartOfSpeechSubcategory& Subjective;
    static const ULPartOfSpeechSubcategory& Objective;
    static const ULPartOfSpeechSubcategory& DirectObject;
    static const ULPartOfSpeechSubcategory& IndirectObject;
    static const ULPartOfSpeechSubcategory& PrepositionalObject;
    static const ULPartOfSpeechSubcategory& ReflexivePrepositionalObject;
    static const ULPartOfSpeechSubcategory& Comitative;
    static const ULPartOfSpeechSubcategory& Possessive;
    static const ULPartOfSpeechSubcategory& PossessiveAdjective;
    static const ULPartOfSpeechSubcategory& DemonstrativeAdjective;
    static const ULPartOfSpeechSubcategory& Disjunctive;
    static const ULPartOfSpeechSubcategory& Pronominal;
    static const ULPartOfSpeechSubcategory& PronominalPossible;
    static const ULPartOfSpeechSubcategory& Existential;
    static const ULPartOfSpeechSubcategory& Proximal;
    static const ULPartOfSpeechSubcategory& Distal;
    static const ULPartOfSpeechSubcategory& Comparative;
    static const ULPartOfSpeechSubcategory& Superlative;
    static const ULPartOfSpeechSubcategory& Proverb;
    static const ULPartOfSpeechSubcategory& Exclamation;
    static const ULPartOfSpeechSubcategory& Phrase;
    static const ULPartOfSpeechSubcategory& Question;
    static const ULPartOfSpeechSubcategory& Cardinal;
    static const ULPartOfSpeechSubcategory& Ordinal;
    static const ULPartOfSpeechSubcategory& Coordinating;
    static const ULPartOfSpeechSubcategory& Subordinating;
    static const ULPartOfSpeechSubcategory& Prefix;
    static const ULPartOfSpeechSubcategory& Suffix;
    static const ULPartOfSpeechSubcategory& Infix;
    static const ULPartOfSpeechSubcategory& Circumfix;
    static const ULPartOfSpeechSubcategory& Interfix;
    static const ULPartOfSpeechSubcategory& Partitive;
    static const ULPartOfSpeechSubcategory& Negative;
    static const ULPartOfSpeechSubcategory& Modal;
    static const ULPartOfSpeechSubcategory& Auxiliary;
    static const ULPartOfSpeechSubcategory& Irregular;
    static const ULPartOfSpeechSubcategory& Defective;
    static const ULPartOfSpeechSubcategory& Impersonal;
    static const ULPartOfSpeechSubcategory& Deponent;
    static const ULPartOfSpeechSubcategory& Contraction;
    static const ULPartOfSpeechSubcategory& Strong;
    static const ULPartOfSpeechSubcategory& Weak;
    static const ULPartOfSpeechSubcategory& Mixed;
    static const ULPartOfSpeechSubcategory& Animate;
    static const ULPartOfSpeechSubcategory& Inanimate;


    ULPartOfSpeechSubcategory();
    ULPartOfSpeechSubcategory(const ULPartOfSpeechSubcategory& other);
    ULPartOfSpeechSubcategory(int id);
    ULPartOfSpeechSubcategory(const char *otherStringID);
    virtual ~ULPartOfSpeechSubcategory();

    virtual void clear();
    ULPartOfSpeechSubcategory& operator=(const ULPartOfSpeechSubcategory& other);
    ULPartOfSpeechSubcategory& operator=(int otherID);
    ULPartOfSpeechSubcategory& operator=(const char *otherStringID);

    bool operator==(const ULPartOfSpeechSubcategory& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULPartOfSpeechSubcategory& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULPartOfSpeechSubcategory& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULPartOfSpeechSubcategory& getInstance(int id);



private:
    static int nEnumeratedValues;
    static ULVector<const ULPartOfSpeechSubcategory *> enumeratedValueVector;
    ULPartOfSpeechSubcategory(const char *stringID, const char *displayString);
};


#endif

