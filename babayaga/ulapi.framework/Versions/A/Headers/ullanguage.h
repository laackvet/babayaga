/**
 * @file ullanguage.h
 * @brief The interface for the ULLanguage class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULLANGUAGE_H
#define ULLANGUAGE_H

#include "ulenum.h"

/**
 * @class ULLanguage
 * @brief An enhanced enumerated type used to represent languages.
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULLanguage : public ULEnum
{
public:
    static const ULLanguage& NoLanguage;
    static const ULLanguage& Any;
    static const ULLanguage& English;
    static const ULLanguage& French;
    static const ULLanguage& Spanish;
    static const ULLanguage& German;
    static const ULLanguage& Italian;
    static const ULLanguage& Portuguese;
    static const ULLanguage& Norwegian;
    static const ULLanguage& Latin;
    static const ULLanguage& Esperanto;
    static const ULLanguage& Russian;
    static const ULLanguage& Dutch;
    static const ULLanguage& Polish;
    static const ULLanguage& Chinese;
    static const ULLanguage& Mandarin;
    static const ULLanguage& Cantonese;
    static const ULLanguage& Klingon;
    static const ULLanguage& Turkish;
    static const ULLanguage& Haitian;
    static const ULLanguage& Swedish;
    static const ULLanguage& Danish;
    static const ULLanguage& Dakota;
    static const ULLanguage& Japanese;
    static const ULLanguage& Hindi;
    static const ULLanguage& Korean;
    static const ULLanguage& Icelandic;
    static const ULLanguage& SwissGerman;
    static const ULLanguage& Romanian;
    static const ULLanguage& Lithuanian;
    static const ULLanguage& Bulgarian;
    static const ULLanguage& Latvian;
    static const ULLanguage& Arabic;
    static const ULLanguage& Hebrew;
    static const ULLanguage& Swahili;
    static const ULLanguage& Urdu;
    static const ULLanguage& Sanskrit;
    static const ULLanguage& Persian;
    static const ULLanguage& Thai;
    static const ULLanguage& Vietnamese;
    static const ULLanguage& Synonyms;


    ULLanguage();
    ULLanguage(const ULLanguage& other);
    ULLanguage(int id);
    ULLanguage(const char *otherStringID);
    virtual ~ULLanguage();

    virtual void clear();
    ULLanguage& operator=(const ULLanguage& other);
    ULLanguage& operator=(int otherID);
    ULLanguage& operator=(const char *otherStringID);

    bool operator==(const ULLanguage& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULLanguage& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULLanguage& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULLanguage& getInstance(int id);
    const char *getTwoLetterISOCode() const;
    const char *getThreeLetterISOCode() const;



private:
    static int nEnumeratedValues;
    static ULVector<const ULLanguage *> enumeratedValueVector;
    ULLanguage(const char *stringID, const char *displayString);
    const char *twoLetterISOCode;
    const char *threeLetterISOCode;
    ULLanguage(const char *twoLetterISOCode,
               const char *threeLetterISOCode,
               const char *displayString);
};


#endif

