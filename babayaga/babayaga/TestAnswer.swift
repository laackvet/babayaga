//
//  TestAnswer.swift
//  babayaga
//
//  Created by Thor Laack-Veeder, Sarah LeBlanc, Gustav Danielsson on 5/15/15.
//  Copyright (c) 2015 mobileApp. All rights reserved.
//

import UIKit

class TestAnswer: UIViewController {
    
    /*

        TestAnswer checks if user input matches the correct russian conjugation. 
    
    After the user sees if they are correct, they are given the choice to test themselves on another word by pressing Next, change preferences by choosing settings, or learning how to use the app more effectively by choosing info

    */
    
    @IBOutlet weak var correctAnswer: UILabel!
    @IBOutlet weak var messageToUser: UILabel!
    @IBOutlet weak var englishForm: UILabel!
    @IBOutlet weak var userAnswer: UILabel!

    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var correctCount: Int = 0
    var totalCount: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.correctAnswer.text =  "прáвильный отвéт: " + appDelegate.correctVerb
        self.userAnswer.text = "ваш отвéт: " + appDelegate.userInputVerb
        self.englishForm.text = appDelegate.englishVerbPhrase
        checkIfUserIsCorrect()
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //Compare strings using Jeff's ulapi compareString method, if they are the same, print correct in russian, otherwise print incorrect in russian. 
    //Note: Compare string disregards accent marks
    func checkIfUserIsCorrect(){
        println(appDelegate.correctVerb)
        let ulapi : ULAPI = ULAPI.sharedULAPI()
        if(ulapi.compareString(appDelegate.userInputVerb, withString: appDelegate.correctVerb, language: "ru") == 0){
            messageToUser.text = "Пра́вильно"
            messageToUser.textColor = UIColor.greenColor()
        }
        else{
            messageToUser.text = "Непра́вильно"
            messageToUser.textColor = UIColor.redColor()
        }
    }
    
    //Handle next button takes user back to testMainScreen, which will generate a new random conjugation
    @IBAction func handleNextButton() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
        let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("TestMode") as! UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    


    
}
