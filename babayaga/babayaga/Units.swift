//
//  Units.swift
//  babayaga
//
//  Created by Sarah LeBlanc, Thor Laack-Veeder, Gustav Danielsson on 5/13/15.
//  Copyright (c) 2015 mobileApp. All rights reserved.
//

import UIKit

/*

UnitsController is the class to create the Units page in Settings.  Units fall between Chapters and Words.
    Users have the option to turn on all units from a certain chapter, or to click individual units,
    where they will go to the words in that section in WordsController.  Every section is kept in a sectionDictionary
    with a value of 0, 1 or 2.  0 means off, 1 means on, 2 means on and off (some words in the section are used
    while other words in the section are now).

*/


class UnitsController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //Global variables for class
    @IBOutlet weak var sectionTable: UITableView!
    @IBOutlet weak var allswitch: UISwitch!
    @IBOutlet weak var alllabel: UILabel!
    let cellIdentifier = "ChapterList"
    var options : NSMutableArray = []
    var saveString:String = ""
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sectionTable?.delegate = self
        self.saveString = appDelegate.userSelectionInChapterSettingsView
        self.sectionTable?.dataSource = self
        generateOptionsArray(appDelegate.columnDictionary[self.saveString + " Sections"]!)
        //self.sectionListName.someString = ""
    }
    
    
    // Determine the value of the switch for that chapter's sections
    // Update the table to have correct on off labels for each section
    override func viewWillAppear(animated: Bool) {
        
        // Pull the chapter number from string being passed
        var indexOfChapNumber = advance(self.saveString.startIndex, 6)
        var chapter = self.saveString.substringFromIndex(indexOfChapNumber)
        
        var sectionCount = 0
        var sectionOnCount = 0
        
        // Determine how many total sections are in the chapter vs sections in the chapter that are on
        for section in appDelegate.sectionDictionary.keys {
            
            // Determine chapter of the current section
            var indexOfChap = advance(section.endIndex, -2)
            var chapter2 = section.substringToIndex(indexOfChap)
            
            // If section is in desired chapter, determine if it is on, update accumulator variables
            if chapter2 == chapter {
                sectionCount += 1
                if appDelegate.sectionDictionary[section] == 1 {
                    sectionOnCount += 1
                }
            }
        }

        // If the number of sections for chapter is same as sections on for chapter, 
        // set the switch to on and turn that chapter on, 
        // if any of the sections are off, turn the switch off and the chapter off
        if sectionCount == sectionOnCount {
            allswitch.setOn(true, animated:true)
            appDelegate.chapterDictionary[appDelegate.chapter + " " + chapter] = 1
        } else {
            allswitch.setOn(false, animated:true)
            appDelegate.chapterDictionary[appDelegate.chapter + " " + chapter] = 0
        }
        
        // Update the table to reload the on off labels
        self.sectionTable?.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // Switch handler
    @IBAction func handleSwitch() {
        
        // Pull the chapter number from the passed string
        var indexOfChapNumber = advance(self.saveString.startIndex, 6)
        var chapter = self.saveString.substringFromIndex(indexOfChapNumber)
        
        // If the switch is turned on
        if allswitch.on {
            
            //Change all words of chapter to frequency 1
            for word in appDelegate.wordDictionary.keys {
                if appDelegate.wordDictionary[word]!.0 == chapter {
                    appDelegate.wordDictionary[word]!.6 = 1
                }
            }
            
            //Change all sections of chapter to 1
            for section in appDelegate.sectionDictionary.keys {
                var indexOfChap = advance(section.endIndex, -2)
                var secChapter = section.substringToIndex(indexOfChap)
                if secChapter == chapter {
                    appDelegate.sectionDictionary[section] = 1
                }
            }
            
            //Change chapter value to 1
            appDelegate.chapterDictionary[self.saveString] = 1
            
            // Update the switch and label
            allswitch.setOn(true, animated:true)
            alllabel.text = "All on"
        }
        
            
        // If switch is turned off
        else {
            
            // Change all words of chapter to frequency 0
            for word in appDelegate.wordDictionary.keys {
                if appDelegate.wordDictionary[word]!.0 == chapter {
                    appDelegate.wordDictionary[word]!.6 = 0
                }
            }
            
            // Change all sections of chapter to frequency 0
            for section in appDelegate.sectionDictionary.keys {
                var indexOfChap = advance(section.endIndex, -2)
                var chapter2 = section.substringToIndex(indexOfChap)
                if chapter2 == chapter {
                    appDelegate.sectionDictionary[section] = 0
                }
            }
            
            // Change chapter value to 0
            appDelegate.chapterDictionary[self.saveString] = 0
            allswitch.setOn(false, animated:true)
            alllabel.text = "All off"
        }
        
        // Reload the table to update the on off labels
        self.sectionTable?.reloadData()
    }
    
    
    func generateOptionsArray(ColumnNames: [String]) {
        for index in 0...ColumnNames.count-1{
            if ColumnNames[index] != ""{
                self.options.addObject(ColumnNames[index])
            }
        }
    }
    
    
    // Given a section, determine what label the table row should have
    // based on how many words from the section are on or off
    func checkOn(section: String) -> String {
        var wordsInSection = 0
        var wordsInSectionOn = 0
        
        // Tally the total number of words in the section
        // and the number of words in the section turned on
        for word in appDelegate.wordDictionary.keys {
            if appDelegate.wordDictionary[word]!.7 == section {
                wordsInSection += 1
                if appDelegate.wordDictionary[word]!.6 == 1 {
                    wordsInSectionOn += 1
                }
            }
        }
        
        // If all words in the section are on, we want label "On"
        if wordsInSection == wordsInSectionOn {
            return "On"
        }
        
        // If none of the words in the section are on, we want label "Off"
        else if wordsInSectionOn == 0 {
            return "Off"
        }
        
        // If some of the words are on, we want label "On/Off"
        else {
            return "On/Off"
        }
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    
    
    // Populate the table with the sections from the csv file
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: cellIdentifier)
            cell!.selectionStyle = .None
            cell!.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }
        cell!.textLabel?.text = self.options[indexPath.row] as? String
        
        // Determine what on/off label the section needs
        cell!.detailTextLabel?.text = self.checkOn((self.options[indexPath.row] as? String)!)
        return cell!
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return indexPath
    }
    
    
    // When a row is selected, go to the WordsController page and pass through the section number
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let index = indexPath.row
        let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
        let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("WordsController") as! UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
        appDelegate.userSelectionInUnitsSettingsView = String(self.options[index] as! NSString)
    }
}