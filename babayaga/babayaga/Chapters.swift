//
//  Chapters.swift
//  babayaga
//
//  Created by Sarah LeBlanc, Thor Laack-Veeder, Gustav Danielsson on 5/10/15.
//  Copyright (c) 2015 mobileApp. All rights reserved.
//

import UIKit

/*

ChaptersController takes control for the initial settings menu listing the different chapters.
    Users should be able to either turn off and on the entire list of chapters through a switch,
    or click on a chapter and go to that chapter's page listing the sections.  Each chapter number
    is kept in a dictionary with two different values - 0 (off), 1 (on).

*/

class ChaptersController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //Global variables for class
    @IBOutlet weak var chapterTable: UITableView?
    @IBOutlet weak var allswitch: UISwitch!
    @IBOutlet weak var alllabel: UILabel!
    let cellIdentifier = "ChapterList"
    var options : NSMutableArray = []
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generateOptionsArray(appDelegate.columnDictionary["Chapters"]!)
        self.chapterTable?.delegate = self
        self.chapterTable?.dataSource = self
    }
    
    
    // Set the switch to on or off depending on if all chapters are on or not
    // Reset the table to update the On Off labels
    override func viewWillAppear(animated: Bool) {
        var chapterCount = 0
        var chapterOnCount = 0
        
        // Count the number of total chapters vs. number of chapter on
        for chap in appDelegate.chapterDictionary.keys {
            chapterCount += 1
            if appDelegate.chapterDictionary[chap] == 1 {
                chapterOnCount += 1
            }
        }
        
        // If all chapters are on, set switch on, otherwise, switch to off
        if chapterCount == chapterOnCount {
            allswitch.setOn(true, animated:true)
            alllabel.text = "All on"
        } else {
            allswitch.setOn(false, animated:true)
            alllabel.text = "All off"
        }
        
        // Update on, off labels
        self.chapterTable?.reloadData()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func generateOptionsArray(ColumnNames: [String]){
        for index in 0...ColumnNames.count-1{
            if ColumnNames[index] != ""{
                self.options.addObject(ColumnNames[index])
            }
        }
    }
    
    // Switch handler
    @IBAction func handleSwitch() {
        
        // Turning the switch on
        if allswitch.on {
            
            // Turn every word in the dictionary to frequency 1
            for word in appDelegate.wordDictionary.keys {
                appDelegate.wordDictionary[word]!.6 = 1
            }
            
            // Turn every section in the dictionary to frequency 1
            for section in appDelegate.sectionDictionary.keys {
                appDelegate.sectionDictionary[section] = 1
            }
            
            // Turn every chapter in the dictionary to frequency 1
            for chapter in appDelegate.chapterDictionary.keys {
                appDelegate.chapterDictionary[chapter] = 1
            }
            
            // Update the switch and label
            allswitch.setOn(true, animated:true)
            alllabel.text = "All on"
        }
        
        // Turning the switch off
        else {
            
            // Turn every word in the dictionary to frequency 0
            for word in appDelegate.wordDictionary.keys {
                appDelegate.wordDictionary[word]!.6 = 0
            }
            
            // Turn every section in the dictionary to frequency 0
            for section in appDelegate.sectionDictionary.keys {
                appDelegate.sectionDictionary[section] = 0
            }
            
            // Turn every chapter in the dictionary to frequency 0
            for chapter in appDelegate.chapterDictionary.keys {
                appDelegate.chapterDictionary[chapter] = 0
            }
            
            // Update the switch and label
            allswitch.setOn(false, animated:true)
            alllabel.text = "All off"
        }
        
        // Update the on, off labels in the table
        self.chapterTable?.reloadData()
    }
    
    
    // Given a chapter, determine if the chapter is on, off, or some of both
    // Used to determine the on/off label for each individual chapter
    func checkOn(chapter: String) -> String {
        
        // If the chapter has frequency 1, we want the label "On"
        if appDelegate.chapterDictionary[chapter] == 1 {
            return "On"
        }
        
        // Pull chapter number from string
        var indexOfChapNumber = advance(chapter.startIndex, 6)
        var chapterN = chapter.substringFromIndex(indexOfChapNumber)
        
        // Check each section from the sectionDictionary from the current chapter
        for section in appDelegate.sectionDictionary.keys {
            var indexOfChap = advance(section.endIndex, -2)
            var chapter2 = section.substringToIndex(indexOfChap)
            
            // If any of the sections are on or partially on, we want the label "On/Off"
            if chapter2 == chapterN {
                if appDelegate.sectionDictionary[section] > 0 {
                    return "On/Off"
                }
            }
        }
        
        // If the chapter is not 1 and none of the sections are 1 or partial, we want label "Off"
        return "Off"
        
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    
    
    // Fill in the table with the Chapters from the csv file
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }
        
        cell.textLabel?.text = self.options[indexPath.row] as? String
        
        // Determine the (on, off, on/off) label
        cell!.detailTextLabel?.text = self.checkOn((self.options[indexPath.row] as? String)!)
        return cell
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return indexPath
    }
    
    
    // Once a row is selected, go to the Units Controller and pass the chapter number through shareData
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let index = indexPath.row
        let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
        let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("UnitsController") as! UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
        let ch = self.options[index] as! NSString
        appDelegate.userSelectionInChapterSettingsView = String(self.options[index] as! NSString)
    }
}