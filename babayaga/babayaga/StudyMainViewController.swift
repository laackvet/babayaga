//
//  FirstViewController.swift
//  babayaga
//
//  Created by Sarah LeBlanc on 5/10/15.
//  Copyright (c) 2015 mobileApp. All rights reserved.
//

import UIKit

class StudyMainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var Chapter_AllWords_Seg_Control: UISegmentedControl!
    @IBOutlet weak var chapterTable: UITableView?
    let cellIdentifier = "ChapterList"
    var options:NSMutableArray = []
    let shareData = ShareData.sharedInstance
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    

    override func viewDidLoad() {
        super.viewDidLoad()
        refresh()
        self.shareData.someString = ""
        self.chapterTable?.delegate = self
        self.chapterTable?.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func chapAllWordsSegCtrlHandler(sender: AnyObject) {
        refresh()
    }
    func generateOptionsArray(ColumnNames: [String]){
        self.options.removeAllObjects()
        for index in 0...ColumnNames.count-1{
            if ColumnNames[index] != ""{
                self.options.addObject(ColumnNames[index])
            }
        }
    }
    
    func refresh(){
        if (Chapter_AllWords_Seg_Control.selectedSegmentIndex == 0){
            generateOptionsArray(appDelegate.columnDictionary["Chapters"]!)
            }
                
        else{
            generateOptionsArray(appDelegate.columnDictionary["инфинити́в"]!)}
        
        self.chapterTable?.reloadData()
    
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }
        
        cell.textLabel?.text = self.options[indexPath.row] as? String
        
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return indexPath
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let index = indexPath.row
        if (Chapter_AllWords_Seg_Control.selectedSegmentIndex == 0){
            println("clicked!" + String(self.options[index] as! NSString))
            let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
            let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("sarahView") as! UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
            self.shareData.someString = String(self.options[index] as! NSString)
        }
        else{
            let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
            let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("gustavView") as! UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
            self.shareData.someString = String(self.options[index] as! NSString)
            
        }
        
        
        
        
    }



}

