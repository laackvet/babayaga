//
//  TestController.swift
//  babayaga
//
//  Created by Thor Laack-Veeder, Sarah LeBlanc, Gustav Danielsson on 5/15/15.
//  Copyright (c) 2015 mobileApp. All rights reserved.
//

import UIKit

class TestController: UIViewController {
    
    /*
    
    TestController generates a random russian verb from our csvFile dictionary, and if that has conjugations AND an english translation for a conjugation, it adds both the conjugation and english translation to a dictionary. 
    
    TestController checks to see if a verb has present tense, and if it does, it will store the present tense conjugations. If it does not have present tense conjugations, it will store future tense conjugations. 
    
    TestController then chooses a random key in the dictionary, which has the english conjugation translation of the verb as a key and the russian form of the verb conjugation as the value. 
    
    The key is displayed to the user, and the value is stored to compare in TestAnswer.swift.
    
    */

    
    //Globals
    @IBOutlet weak var userInputField: UITextField!
    @IBOutlet weak var englishForm: UITextField!
    @IBOutlet weak var pronounTextField: UITextField!
    @IBOutlet weak var aboutButton: UIButton!
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var randomWord: String = ""
    var hasPresentTense: Bool = false
    var randomWordIndex: Int = 0
    var russianStem: String = ""
    var conjugationList:NSArray = []
    var conjugationDict: [String: String] = [:]
    var hasTranslationsForConjugations: Bool = false
    var isSingularAndFirstOrSecondPerson: Bool = false
    var isPluralAndThirdPerson: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userInputField.autocorrectionType = .No
        userInputField.addTarget(self, action: "textFieldDidReturn:", forControlEvents: .EditingDidEndOnExit)
    }
    
    // Each time the view appears, we want to make sure that there are words with frequency
    // 1 that we can pull from the dictionary.
    override func viewWillAppear(animated: Bool) {
        
        // If there are no words on, send an alert and take the user to the settings page
        // so that they can turn words on
        if !self.checkForWords() {
            let alert = UIAlertView()
            alert.title = "Settings"
            alert.message = "Please choose some words to practice"
            alert.addButtonWithTitle("Go")
            alert.show()
            self.handleSettingButton()
        }
        
        // If there are words on, generate the conjugations and choose the pronoun and english form
        else {
            self.navigationItem.setHidesBackButton(true, animated: true)
            self.conjugationDict = [:]
            generateConjugations()
            choosePronoun()
            chooseEnglishForm()
        }
    }
    
    // We want to make sure at least one word is on.  A word will be on if a section has a value 1 (all on)
    // or 2 (some words on).  Go through the sectionDictionary and check for any section without a value 0
    func checkForWords() -> Bool {
        for section in appDelegate.sectionDictionary.keys {
            if appDelegate.sectionDictionary[section] != 0 {
                return true
            }
        }
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //english verb to be displayed to end-user
    func chooseEnglishForm(){
        englishForm.text? = appDelegate.englishVerbPhrase
    }
    
    //Russian pronoun to be displayed to end-user
    func choosePronoun(){
        pronounTextField.text? = self.russianStem.lowercaseString
    }
    
    /*
        generateConjugations chooses a word from our dictionary at random, based on the parameters in settings mode.
        It then checks to see is a verb has conjugations
        if it has conjugations, it checks to see if it has present tense conjugations, if it does, it chooses one at random and stores it and its english translation
        If it does not have a present tense, we grab future tense conjugations and store them in a list
        We then choose from the dictionary at random, and store these values, which are later displayed to the user
    */
    func generateConjugations(){
        var hasConjugationList = false
        self.hasTranslationsForConjugations = false
        let ulapi : ULAPI = ULAPI.sharedULAPI()
        var language = "ru"
        
        //ensure that chosen word has conjugations, has english translations for its conjugations, and that conjugationDictionary that stores the conjguations is not empty when we run test mode
        
        while hasConjugationList != true || hasTranslationsForConjugations != true || self.conjugationDict.count == 0 {
            chooseWord()
            self.conjugationList = ulapi.conjugations(self.randomWord, language: language)
            if self.conjugationList.count > 0 {
                hasConjugationList = true
            }
        
            for item in conjugationList {
                if let conjugation = item as? ULAPIDerivation {
                    checkIfRightTenseAndPerson(conjugation)
                    if(self.isSingularAndFirstOrSecondPerson == true || self.isPluralAndThirdPerson == true){
                        let tense = conjugation.derivedFormPartOfSpeech.tense
                        if checkIfVerbHasPresentTense(self.randomWord) && tense == "present"{
                            generatePresentTenseConjugationWord(conjugation)
                        }
                            
                        else{
                            if !checkIfVerbHasPresentTense(self.randomWord) && tense == "future"{
                                generateFutureTenseConjugationWord(conjugation)
                            }
                        }
                    }
                }
            }
        }
        
        //choose a random conjugation from conjugationDictionary and set it to display text field globals
        
        let russianVerbKey = chooseRandomKey(self.conjugationDict)
        appDelegate.englishVerbPhrase = russianVerbKey.0
        self.russianStem = russianVerbKey.1.componentsSeparatedByString(" ")[0]
        appDelegate.correctVerb = russianVerbKey.1.componentsSeparatedByString(" ")[1]
    }
    
    
    
    //if present tense, translate into english and store in conjugationDictionary
    //does not guarantee that a translation for conjugation exists, but this error is handled in the while loop of generateConjugations()
    func generatePresentTenseConjugationWord(conjugation: ULAPIDerivation){
        let ulapi : ULAPI = ULAPI.sharedULAPI()
        var language = "ru"
        var translatedConjugation = ulapi.translationForConjugation(conjugation, targetLanguage: "en")
        if translatedConjugation != nil {
            self.hasTranslationsForConjugations = true
            self.conjugationDict[ulapi.displayTextForConjugation(translatedConjugation)] = ulapi.displayTextForConjugation(conjugation)
        }
    }
    
    //if verb does not have present tense, we find first future tense conjugation, and translate into english and store in conjugationDictionary
    //does not guarantee that a translation for conjugation exists, but this error is handled in the while loop of generateConjugations()
    func generateFutureTenseConjugationWord(conjugation: ULAPIDerivation){
        let ulapi : ULAPI = ULAPI.sharedULAPI()
        var language = "ru"
        var futureTranslatedConjugation = ulapi.translationForConjugation(conjugation, targetLanguage: "en")
        if futureTranslatedConjugation != nil {
            self.hasTranslationsForConjugations = true
            self.conjugationDict[ulapi.displayTextForConjugation(futureTranslatedConjugation)] = ulapi.displayTextForConjugation(conjugation)
        }
        
    }
    
    //Laura only wants fist person singular, second person singular, and third person plural to be tested in test mode
    //checkIfRightTenseAndPerson updates two globals, isSingularAndFirstOrSecondPerson and isPluralAndThirdPerson based on Laura's needs
    func checkIfRightTenseAndPerson(conjugation: ULAPIDerivation){
        let ulapi : ULAPI = ULAPI.sharedULAPI()
        var language = "ru"
        if(conjugation.derivedFormPartOfSpeech.person == "first" || conjugation.derivedFormPartOfSpeech.person == "second"){
            if(conjugation.derivedFormPartOfSpeech.number == "singular"){
                self.isSingularAndFirstOrSecondPerson = true
                self.isPluralAndThirdPerson = false
            }
        }
        else if(conjugation.derivedFormPartOfSpeech.person == "third" && conjugation.derivedFormPartOfSpeech.number == "plural"){
            self.isPluralAndThirdPerson = true
            self.isSingularAndFirstOrSecondPerson = false
        }
        else{
            self.isSingularAndFirstOrSecondPerson = false
            self.isPluralAndThirdPerson = false
        }
        
    }
    
    //find a random Key in dictionary of Strings and store as a tuple
    func chooseRandomKey(dictionary: [String:String]) -> (String, String){
        let randomIndex = generateRandomNumber(0, upper: dictionary.count-1)
        let value = Array(dictionary.values)[randomIndex]
        let key = Array(dictionary.keys)[randomIndex]
        let val = dictionary[key]
        return (key, value)
        
    }

    
    //if verb has present tense, we want to grab a present tense conjugation
    func checkIfVerbHasPresentTense(verb: String) -> Bool{
        var hasPresentTense = false
        let ulapi : ULAPI = ULAPI.sharedULAPI()
        var language = "ru"
        //need to figure out what to do with verbs that don't have any present tenses
        var conjugationList = ulapi.conjugations(verb, language: language)
        for item in conjugationList {
            if let conjugation  = item as? ULAPIDerivation {
                let tense = conjugation.derivedFormPartOfSpeech.tense
                if tense == "present"{
                    hasPresentTense = true
                    return hasPresentTense
                    }
                }
            }
        return hasPresentTense
        }
    

    
    /* chooseWord() updates globals randomWordIndex and randomWord with a  random number and a randomly chosen word from columnDictionary, which reads straight from the csv file
    */
    func chooseWord(){
        var foundWord = false
        while foundWord != true {
            self.randomWordIndex = generateRandomNumber(0, upper: appDelegate.columnDictionary["инфинити́в"]!.count-1)
            self.randomWord = appDelegate.columnDictionary["инфинити́в"]![self.randomWordIndex]
            if appDelegate.wordDictionary[self.randomWord]!.6 == 1 {
                foundWord = true
            }
        }
    }
    
    
    /* function to generate a random number
    ** obtained at 
    ** http://stackoverflow.com/questions/24058195/what-is-the-easiest-way-to-generate-random-integers-within-a-range-in-swift
    */
    func generateRandomNumber(lower: Int , upper: Int) -> Int {
        return lower + Int(arc4random_uniform(UInt32(upper - lower + 1)))
    }
    
    // User enters answer by clicking return on the keyboard
    func textFieldDidReturn(textField: UITextField!){
        textField.resignFirstResponder()
        let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
        let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("testAnswer") as! UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
        appDelegate.userInputVerb = userInputField.text
        
    }
    
    //If user presses settings wheel, go to settings screen
    @IBAction func handleSettingButton() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())        
        let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("chapterController") as! UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func handleAboutButton() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
        let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("testAbout") as! UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
