//
//  ULAPI.mm
//  Reader
//
//  Created by Jeffrey Ondich on 12/6/14.
//  Copyright (c) 2014 Ultralingua, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ULAPI.h"
#import "ULAPI+CPP.h"

@interface ULAPI() {
    ULFactory *factory;
    NSMutableDictionary *stemmers;
    NSMutableDictionary *conjugators;
    NSMutableDictionary *definers;
}
- (BOOL)pospMatches:(const ULPartOfSpeech&)posp1 pops2:(const ULPartOfSpeech&)posp2;
@end

@implementation ULAPI

+ (ULAPI *)sharedULAPI {
    static dispatch_once_t once;
    static ULAPI *sharedInstance;
    dispatch_once(&once, ^{ sharedInstance = [[ULAPI alloc] init]; });
    return sharedInstance;
}

- (id)init {
    if (self) {
        self->factory = ULFactory::createFactory("default");
        self->stemmers = [[NSMutableDictionary alloc] init];
        self->conjugators = [[NSMutableDictionary alloc] init];
        self->definers = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (BOOL)addDictionaryDataSource:(NSString *)dataSourceFilePath {
    ULError error = self->factory->addDictionaryDataSource([dataSourceFilePath ULString]);
    return (error == ULError::NoError ? YES : NO);
}

- (BOOL)addLanguageDataSource:(NSString *)dataSourceFilePath {
    ULError error = self->factory->addLanguageDataSource([dataSourceFilePath ULString]);
    return (error == ULError::NoError ? YES : NO);
}

- (NSString *)translateNumber:(NSString *)digits language:(NSString *)language {
    ULString result;
    ULLanguage ulLanguage([language ULString]);
    ULNumberTranslator *translator = self->factory->getNumberTranslator(ulLanguage);
    translator->translateNumber([digits ULString], result);
    return [NSString stringWithULString:result];
}

- (ULStemmer *)stemmerForLanguage:(NSString *)language {
    // Get the cached stemmer if it's there.
    ULAPIStemmer *wrappedStemmer = [self->stemmers objectForKey:language];
    if (wrappedStemmer) {
        return wrappedStemmer.ulStemmer;
    }
    
    // Get the stemmer from the factory and cache it.
    ULError error;
    ULLanguage targetLanguage([language ULString]);
    
    ULList<ULDictionaryDataSource *> dictionaryList;
    ULList<ULLanguageDataSource *> ldsList;
    ULDictionaryDataSource *dictionaryDataSource = 0;
    ULLanguageDataSource *languageDataSource = 0;
    error = self->factory->getDictionaryDataSources(targetLanguage, ULLanguage::English, dictionaryList);
    if (error == ULError::NoError && dictionaryList.length() > 0) {
        dictionaryDataSource = dictionaryList[0];
        error = self->factory->getLanguageDataSources(targetLanguage, ldsList);
        if (error == ULError::NoError && ldsList.length() > 0) {
            languageDataSource = ldsList[0];
        }
    }
    
    ULStemmer *stemmer = 0;
    if (dictionaryDataSource != 0 && languageDataSource != 0) {
        stemmer = self->factory->getStemmer(languageDataSource, dictionaryDataSource);
    }
    
    if (stemmer != 0) {
        wrappedStemmer = [[ULAPIStemmer alloc] initWithULStemmer:stemmer];
        [self->stemmers setObject:wrappedStemmer forKey:language];
    }
    
    return stemmer;
}

- (ULConjugator *)conjugatorForLanguage:(NSString *)language {
    // Get the cached conjugator if it's there.
    ULAPIConjugator *wrappedConjugator = [self->conjugators objectForKey:language];
    if (wrappedConjugator) {
        return wrappedConjugator.ulConjugator;
    }
    
    // Get the conjugator from the factory and cache it.
    ULError error;
    ULLanguage targetLanguage([language ULString]);
    
    ULList<ULDictionaryDataSource *> dictionaryList;
    ULList<ULLanguageDataSource *> ldsList;
    ULDictionaryDataSource *dictionaryDataSource = 0;
    ULLanguageDataSource *languageDataSource = 0;
    error = self->factory->getDictionaryDataSources(targetLanguage, ULLanguage::English, dictionaryList);
    if (error == ULError::NoError && dictionaryList.length() > 0) {
        dictionaryDataSource = dictionaryList[0];
        error = self->factory->getLanguageDataSources(targetLanguage, ldsList);
        if (error == ULError::NoError && ldsList.length() > 0) {
            languageDataSource = ldsList[0];
        }
    }
    
    ULConjugator *conjugator = 0;
    if (dictionaryDataSource != 0 && languageDataSource != 0) {
        conjugator = self->factory->getConjugator(languageDataSource, dictionaryDataSource);
    }
    
    if (conjugator != 0) {
        wrappedConjugator = [[ULAPIConjugator alloc] initWithULConjugator:conjugator];
        [self->conjugators setObject:wrappedConjugator forKey:language];
    }
    
    return conjugator;
}

- (ULDefiner *)definerFor:(NSString *)language1 andLanguage:(NSString *)language2  {
    // Get the cached definer if it's there.
    NSString *key;
    if ([language1 compare:language2] == NSOrderedAscending) {
        key = [NSString stringWithFormat:@"%@#%@", language1, language2];
    } else {
        key = [NSString stringWithFormat:@"%@#%@", language2, language1];
    }
    
    ULAPIDefiner *wrappedDefiner = [self->definers objectForKey:key];
    if (wrappedDefiner) {
        return wrappedDefiner.ulDefiner;
    }
    
    // Get the definer from the factory and cache it.
    ULError error;
    ULLanguage lang1([language1 ULString]);
    ULLanguage lang2([language2 ULString]);
    
    ULList<ULDictionaryDataSource *> dictionaryList;
    ULList<ULLanguageDataSource *> ldsList;
    ULDictionaryDataSource *dictionaryDataSource = 0;
    ULLanguageDataSource *languageDataSource1 = 0;
    ULLanguageDataSource *languageDataSource2 = 0;
    error = self->factory->getDictionaryDataSources(lang1, lang2, dictionaryList);
    if (error == ULError::NoError && dictionaryList.length() > 0) {
        dictionaryDataSource = dictionaryList[0];
        error = self->factory->getLanguageDataSources(lang1, ldsList);
        if (error == ULError::NoError && ldsList.length() > 0) {
            languageDataSource1 = ldsList[0];
        }

        error = self->factory->getLanguageDataSources(lang2, ldsList);
        if (error == ULError::NoError && ldsList.length() > 0) {
            languageDataSource2 = ldsList[0];
        }
    }
    
    ULDefiner *definer = 0;
    if (dictionaryDataSource != 0 && languageDataSource1 != 0 && languageDataSource2 != 0) {
        definer = self->factory->getDefiner(dictionaryDataSource, languageDataSource1, languageDataSource2);
    }
    
    if (definer != 0) {
        wrappedDefiner = [[ULAPIDefiner alloc] initWithULDefiner:definer];
        [self->definers setObject:wrappedDefiner forKey:key];
    }
    
    return definer;
}

- (ULCollator *)collatorForLanguage:(NSString *)language {
    ULLanguage collatorLanguage([language ULString]);
    return self->factory->getCollator(collatorLanguage);
}

- (NSArray *)stems:(NSString *)word language:(NSString *)language {
    NSMutableArray *stems = [[NSMutableArray alloc] init];
    
    ULStemmer *stemmer = [self stemmerForLanguage:language];
    if (stemmer != 0) {
        ULList<ULDerivation> stemList;
        ULListIterator<ULDerivation> stemIterator;
        stemmer->getStems([word ULString], stemList);
        for (stemIterator = stemList.begin(); stemIterator; ++stemIterator) {
            [stems addObject:[[ULAPIDerivation alloc] initWithULDerivation:*stemIterator]];
        }
    }
    
    return stems;
}

// Returns an array of ULAPIDerivation objects.
- (NSArray *)conjugations:(NSString *)word language:(NSString *)language {
    NSMutableArray *conjugations = [[NSMutableArray alloc] init];
    
    ULConjugator *conjugator = [self conjugatorForLanguage:language];
    if (conjugator != 0) {
        ULList<ULDerivation> verbList;
        ULListIterator<ULDerivation> verbIterator;
        ULList<ULDerivation> conjugationList;
        ULListIterator<ULDerivation> conjugationIterator;
        
        // For each verb matching the given verb form...
        conjugator->getVerbsMatchingVerbForm([word ULString], verbList);
        for (verbIterator = verbList.begin(); verbIterator; ++verbIterator) {
            conjugator->getAllConjugations(*verbIterator, conjugationList);
            for (conjugationIterator = conjugationList.begin(); conjugationIterator; ++conjugationIterator) {
                [conjugations addObject:[[ULAPIDerivation alloc] initWithULDerivation:*conjugationIterator]];
            }
        }
    }
    
    return conjugations;
}

- (NSString *)displayTextForConjugation:(ULAPIDerivation *)conjugation {
    NSString *language = [NSString stringWithUTF8String:conjugation.ulDerivation.getLanguage().getTwoLetterISOCode()];
    ULConjugator *conjugator = [self conjugatorForLanguage:language];
    if (conjugator == 0) {
        return @"";
    }
    
    return [NSString stringWithULString:conjugator->getConjugationDisplayText(conjugation.ulDerivation)];
}

- (NSString *)tenseName:(NSString *)tense language:(NSString *)language {
    NSString *tenseName = @"";
    ULConjugator *conjugator = [self conjugatorForLanguage:language];
    ULTense ulTense = ULTense([tense ULString]);
    if (conjugator != 0) {
        ULInflector *inflector = conjugator->getInflector();
        if (inflector != 0) {
            ULDictionaryDataSource *dataSource = inflector->getDictionaryDataSource();
            if (dataSource != 0) {
                ULForester *forester = dataSource->getForester();
                if (forester != 0) {
                    ULString localization;
                    ULError error = forester->getLocalizationForFeatureValue(ulTense.getStringID(), conjugator->getLanguage(), localization);
                    if (error == ULError::NoError) {
                        tenseName = [NSString stringWithULString:localization];
                    }
                }
            }
        }
    }
    
    return tenseName;
}

- (int)compareString:(NSString *)a withString:(NSString *)b language:(NSString *)language {
    int comparison = 0;
    ULCollator *collator = [self collatorForLanguage:language];
    if (collator != 0) {
        comparison = collator->compare([a ULString], [b ULString]);
    }
    return comparison;
}

- (ULAPIDerivation *)translationForConjugation:(ULAPIDerivation *)conjugation targetLanguage:(NSString *)language {
    if (conjugation == nil || language == nil) {
        return nil;
    }

    ULDefiner *definer = [self definerFor:language andLanguage:@"en"];
    if (definer == 0) {
        return nil;
    }

    ULConjugator *conjugator = [self conjugatorForLanguage:language];
    if (conjugator == 0) {
        return nil;
    }

    ULInflector *inflector = conjugator->getInflector();
    if (inflector == 0) {
        return nil;
    }

    ULDictionaryDataSource *dataSource = inflector->getDictionaryDataSource();
    if (dataSource == 0) {
        return nil;
    }

    ULForester *forester = dataSource->getForester();
    if (forester == 0) {
        return nil;
    }

    ULList<ULString> translations;
    ULDictionaryIterator it = conjugation.ulDerivation.getRootDictionaryIterator();
    forester->getTranslationsForPartOfSpeechCategory(it, ULPartOfSpeechCategory::Verb, translations);
    if (translations.length() == 0) {
        return nil;
    }

    ULList<ULDerivation> verbList;
    ULString translatedInfinitive = translations[0];
    translatedInfinitive.getBuffer();
    conjugator->getVerbsMatchingInfinitive(translatedInfinitive, verbList);
    if (verbList.length() == 0) {
        return nil;
    }

    ULList<ULDerivation> conjugationList;
    ULListIterator<ULDerivation> conjugationIterator;
    conjugator->getAllConjugations(verbList[0], conjugationList);
    ULConjugator::removeNegativeConjugations(conjugationList);
    const ULPartOfSpeech& sourcePOSP = conjugation.ulDerivation.getDerivedFormPartOfSpeech();
    for (conjugationIterator = conjugationList.begin(); conjugationIterator; ++conjugationIterator) {
        const ULPartOfSpeech& targetPOSP = (*conjugationIterator).getDerivedFormPartOfSpeech();
        if ([self pospMatches:sourcePOSP pops2:targetPOSP]) {
            return [[ULAPIDerivation alloc] initWithULDerivation:(*conjugationIterator)];
        }
    }

    return nil;
}

- (BOOL)pospMatches:(const ULPartOfSpeech&)posp1 pops2:(const ULPartOfSpeech&)posp2 {
    if (posp1.getTense() != posp2.getTense()) {
        return NO;
    }

    if (posp1.getPerson() != posp2.getPerson()) {
        return NO;
    }

    if (posp1.getNumber() != posp2.getNumber()) {
        return NO;
    }

    ULFormality formality1 = posp1.getFormality();
    ULFormality formality2 = posp2.getFormality();
    if (formality1 != formality2 && formality1 != ULFormality::None && formality2 != ULFormality::None) {
        return NO;
    }
    
    ULGender gender1 = posp1.getGender();
    ULGender gender2 = posp2.getGender();
    if (gender1 != gender2 && gender1 != ULGender::None && gender2 != ULGender::None) {
        return NO;
    }
    
    return YES;
}

@end

#pragma mark - ULAPIPartOfSpeech
@interface ULAPIPartOfSpeech() {
    ULPartOfSpeech _ulPartOfSpeech;
}
@end

@implementation ULAPIPartOfSpeech

- (NSString *)partOfSpeechCategory {
    return [NSString stringWithUTF8String:_ulPartOfSpeech.getPartOfSpeechCategory().getStringID()];
}

- (NSDictionary *)features {
    NSDictionary *features = [[NSMutableDictionary alloc] init];
    ULListConstIterator<ULFeature> featureIterator;
    for (featureIterator = _ulPartOfSpeech.getFeatureList().cbegin(); featureIterator; ++featureIterator) {
        NSString *featureType = [NSString stringWithUTF8String:(*featureIterator).getFeatureType().getStringID()];
        NSString *featureValue = [NSString stringWithULString:(*featureIterator).getDisplayValue()];
        [features setValue:featureValue forKey:featureType];
    }
    return features;
}

- (NSString *)tense {
    return [NSString stringWithUTF8String:_ulPartOfSpeech.getTense().getStringID()];
}

- (NSString *)person {
    return [NSString stringWithUTF8String:_ulPartOfSpeech.getPerson().getStringID()];
}

- (NSString *)number {
    return [NSString stringWithUTF8String:_ulPartOfSpeech.getNumber().getStringID()];
}

- (NSString *)aspect {
    return [NSString stringWithUTF8String:_ulPartOfSpeech.getAspect().getStringID()];
}

@end

@implementation ULAPIPartOfSpeech(CPP)

- (id)initWithULPartOfSpeech:(const ULPartOfSpeech&)partOfSpeech {
    if (self) {
        _ulPartOfSpeech = partOfSpeech;
    }
    
    return self;
}

- (const ULPartOfSpeech&)ulPartOfSpeech {
    return _ulPartOfSpeech;
}

@end

#pragma mark - ULAPIDerivation
@interface ULAPIDerivation() {
    ULDerivation _ulDerivation;
}
@end

@implementation ULAPIDerivation
- (NSString *)root {
    return [NSString stringWithULString:_ulDerivation.getRoot()];
}

- (ULAPIPartOfSpeech *)rootPartOfSpeech {
    return [[ULAPIPartOfSpeech alloc] initWithULPartOfSpeech:_ulDerivation.getRootPartOfSpeech()];
}

- (NSDictionary *)rootFeatures {
    NSDictionary *features = [[NSMutableDictionary alloc] init];
    ULListConstIterator<ULFeature> featureIterator;
    for (featureIterator = _ulDerivation.getRootFeatureList().cbegin(); featureIterator; ++featureIterator) {
        NSString *featureType = [NSString stringWithUTF8String:(*featureIterator).getFeatureType().getStringID()];
        NSString *featureValue = [NSString stringWithULString:(*featureIterator).getDisplayValue()];
        [features setValue:featureValue forKey:featureType];
    }
    return features;
}

- (NSString *)derivedForm {
    return [NSString stringWithULString:_ulDerivation.getDerivedForm()];
}

- (ULAPIPartOfSpeech *)derivedFormPartOfSpeech {
    return [[ULAPIPartOfSpeech alloc] initWithULPartOfSpeech:_ulDerivation.getDerivedFormPartOfSpeech()];
}

@end

@implementation ULAPIDerivation(CPP)
- (id)initWithULDerivation:(const ULDerivation&)derivation {
    if (self) {
        _ulDerivation = derivation;
    }
    return self;
}

- (const ULDerivation&)ulDerivation {
    return _ulDerivation;
}

@end

#pragma mark - ULAPIStemmer

@implementation ULAPIStemmer
- (id)initWithULStemmer:(ULStemmer *)stemmer {
    if (self) {
        _ulStemmer = stemmer;
    }
    return self;
}
@end


#pragma mark - ULAPIConjugator

@implementation ULAPIConjugator
- (id)initWithULConjugator:(ULConjugator *)conjugator {
    if (self) {
        _ulConjugator = conjugator;
    }
    return self;
}

@end

#pragma mark - ULAPIDefiner

@implementation ULAPIDefiner
- (id)initWithULDefiner:(ULDefiner *)definer {
    if (self) {
        _ulDefiner = definer;
    }
    return self;
}
@end


