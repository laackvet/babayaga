//
//  conjugationsViewController.swift
//  babayaga
//
//  Created by Thor Laack-Veeder, Sarah LeBlanc, Gustav Danielsson on 5/15/15.
//  Copyright (c) 2015 mobileApp. All rights reserved.
//

import UIKit

class conjugationsViewController: UIViewController {
    
    // Many UITextFields to be populated with text 
    @IBOutlet weak var infinitiveForm: UITextField!
    @IBOutlet weak var firstPersonSingularForm: UITextField!
    @IBOutlet weak var secondPersonSingularForm: UITextField!
    @IBOutlet weak var thirdPersonSingularForm: UITextField!
    @IBOutlet weak var aspectType: UITextField!
    @IBOutlet weak var translationField: UITextField!
    @IBOutlet weak var firstPersonPluralForm: UITextField!
    @IBOutlet weak var secondPersonPluralForm: UITextField!
    @IBOutlet weak var thirdPersonPluralForm: UITextField!
    @IBOutlet weak var aspectPartner: UITextField!
    @IBOutlet weak var unitTextField: UITextField!
    @IBOutlet weak var imperativeConjugations: UITextField!
    @IBOutlet weak var pastConjugations: UITextField!
    @IBOutlet weak var tenseField: UITextField!

    // appDelegate object for word array
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let ulapi : ULAPI = ULAPI.sharedULAPI()
    let language = "ru"
    var verbForm = ""
    var verbToConjugate = ""
    var verbParticle = ""
    
    var hasPresentTense: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set the verbForm since new verb each time screen loads
        verbForm = appDelegate.userSelectionInStudyModeWordsInSectionView
        if verbForm.componentsSeparatedByString(" ").count > 1 {
            verbParticle = " " + verbForm.componentsSeparatedByString(" ")[1]
        }
        verbToConjugate = verbForm.componentsSeparatedByString(" ")[0]
        populateConjugationTextFields();
        populateAdditionalVerbDataTextFields();
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    // Function to populate the fields that require ULAPI
    func populateConjugationTextFields() {
        // Initialize past and imperative text fields
        var pastTextField = ""
        var imperativeTextField = ""
        
        // Get the conjugations from ULAPI
        var conjugationList = ulapi.conjugations(verbToConjugate, language: language)
        
        // To see if future or present tense should populate the conjugation fields
        checkIfVerbHasPresentTense(verbToConjugate)

        // Big loop to unwrap the object from ULAPI
        for item in conjugationList {
            if let conjugation = item as? ULAPIDerivation {
                let tense = conjugation.derivedFormPartOfSpeech.tense
                
                // Present tense conjugations for imperfective verbs
                if hasPresentTense && tense == "present" {
                    
                    // Singular forms
                    if conjugation.derivedFormPartOfSpeech.number == "singular"{
                        if conjugation.derivedFormPartOfSpeech.person == "first"{
                            firstPersonSingularForm.text? = ulapi.displayTextForConjugation(conjugation).lowercaseString + verbParticle
                        
                        }
                        else if conjugation.derivedFormPartOfSpeech.person == "second"{
                            secondPersonSingularForm.text? = ulapi.displayTextForConjugation(conjugation).lowercaseString + verbParticle
                        }
                        // Only displaying he/she to save space
                        else{
                            thirdPersonSingularForm.text? = "он/она́ " + ulapi.displayTextForConjugation(conjugation).componentsSeparatedByString(" ")[1] + verbParticle
                        }
                        
                    }
                        
                    // Plural forms
                    else if conjugation.derivedFormPartOfSpeech.number == "plural"{
                        if conjugation.derivedFormPartOfSpeech.person == "first"{
                            firstPersonPluralForm.text? = ulapi.displayTextForConjugation(conjugation).lowercaseString + verbParticle
                        }
                        else if conjugation.derivedFormPartOfSpeech.person == "second"{
                              secondPersonPluralForm.text? = ulapi.displayTextForConjugation(conjugation).lowercaseString + verbParticle
                        }
                        // Concatetanting plural form to get proper stress on pronoun
                        else{
                              thirdPersonPluralForm.text? = "они́ " + ulapi.displayTextForConjugation(conjugation).componentsSeparatedByString(" ")[1] + verbParticle
                        }
                    }
                }
                    
                // Future tense conjugations for perfective verbs
                else if !hasPresentTense && tense == "future"{
                    
                    // Singular forms
                    if conjugation.derivedFormPartOfSpeech.number == "singular"{
                        if conjugation.derivedFormPartOfSpeech.person == "first"{
                            firstPersonSingularForm.text? = ulapi.displayTextForConjugation(conjugation).lowercaseString + verbParticle
                            
                        }
                        else if conjugation.derivedFormPartOfSpeech.person == "second"{
                            secondPersonSingularForm.text? = ulapi.displayTextForConjugation(conjugation).lowercaseString + verbParticle
                        }
                        else{
                            thirdPersonSingularForm.text? = "он/она́ " + ulapi.displayTextForConjugation(conjugation).componentsSeparatedByString(" ")[1] + verbParticle
                        }
                        
                    }
                        
                    // Plural forms
                    else if conjugation.derivedFormPartOfSpeech.number == "plural"{
                        if conjugation.derivedFormPartOfSpeech.person == "first"{
                            firstPersonPluralForm.text? = ulapi.displayTextForConjugation(conjugation).lowercaseString + verbParticle
                        }
                        else if conjugation.derivedFormPartOfSpeech.person == "second"{
                            secondPersonPluralForm.text? = ulapi.displayTextForConjugation(conjugation).lowercaseString + verbParticle
                        }
                        else{
                            thirdPersonPluralForm.text? = "они́ " + ulapi.displayTextForConjugation(conjugation).componentsSeparatedByString(" ")[1] + verbParticle
                        }
                    }
        
                }
                    
                // Past tense only has four forms for each gender and plural...
                else if tense == "past"{
                    
                    // So get each of the three third person singular forms
                    if conjugation.derivedFormPartOfSpeech.number == "singular"{
                        if conjugation.derivedFormPartOfSpeech.person == "third"{
                            // Do not get pronouns
                            pastTextField += ulapi.displayTextForConjugation(conjugation).componentsSeparatedByString(" ")[1] + verbParticle + ", "
                            
                        }
                        
                        
                    }
                        
                    // Get one of the plural forms since they are all the same
                    else if conjugation.derivedFormPartOfSpeech.number == "plural"{
                        if conjugation.derivedFormPartOfSpeech.person == "first"{
                            pastTextField += ulapi.displayTextForConjugation(conjugation).componentsSeparatedByString(" ")[1] + verbParticle
                        }
                    }
                }
                    
                // Get imperative forms from both persons
                else if tense == "imperative"{
                    if conjugation.derivedFormPartOfSpeech.number == "singular"{
                        if conjugation.derivedFormPartOfSpeech.person == "second"{
                            imperativeTextField += ulapi.displayTextForConjugation(conjugation).componentsSeparatedByString(" ")[1] + verbParticle + ", "
                            
                        }
                        
                        
                    }
                    else if conjugation.derivedFormPartOfSpeech.number == "plural"{
                        if conjugation.derivedFormPartOfSpeech.person == "second"{
                            imperativeTextField += ulapi.displayTextForConjugation(conjugation).componentsSeparatedByString(" ")[1] + verbParticle
                        }
                    }
                }
            }
        }
        
        // Populate the actual past and imperative text fields
        pastConjugations.text? = "Past: " + pastTextField
        imperativeConjugations.text? = "Imperative: " + imperativeTextField

    }
    
    // Function that checks to see if the verb has a present tense entry in the ULD
    func checkIfVerbHasPresentTense(verb: String) {
        var conjugationList = ulapi.conjugations(verb, language: language)
        for item in conjugationList {
            if let conjugation = item as? ULAPIDerivation {
                let tense = conjugation.derivedFormPartOfSpeech.tense
                if tense == "present"{
                    self.hasPresentTense = true
                }
            }
        }
    }
    
    // Populate other text fields that don't require ULAPI
    func populateAdditionalVerbDataTextFields() {
        // Populate aspect text field with the verb's aspect type and the stem
        aspectType.text? = appDelegate.wordDictionary[verbForm]!.4 + " (" + appDelegate.wordDictionary[verbForm]!.8 + ")"
        // Populate the translation field with the translation
        translationField.text? = appDelegate.wordDictionary[verbForm]!.5
        // Populate the chapter unit for the verb by concatenating the chapter with the unit
        unitTextField.text? = appDelegate.wordDictionary[verbForm]!.0 + "." + appDelegate.wordDictionary[verbForm]!.1
        // If the verb is imperfective, then set the tense to present
        if appDelegate.wordDictionary[verbForm]!.4 == "НСВ" {
            // If there is an aspect partner, populate the the aspect partner text field
            if appDelegate.wordDictionary[verbForm]!.2 != ""{
                aspectPartner.text? = "(СВ: " + appDelegate.wordDictionary[verbForm]!.2 + ")"
            }
            tenseField.text? = "Present"
        }
        // else if the verb is perfective, set the tense to future
        else if appDelegate.wordDictionary[verbForm]!.4 == "СВ" {
            // If there is an aspect partner, populate the the aspect partner text field
            if appDelegate.wordDictionary[verbForm]!.2 != ""{
                aspectPartner.text? = "(НСВ: " + appDelegate.wordDictionary[verbForm]!.3 + ")"
            }
            tenseField.text? = "Future"
        }
        // The shared data verbform is infinitive
        infinitiveForm.text? = verbForm
        
    }
}
