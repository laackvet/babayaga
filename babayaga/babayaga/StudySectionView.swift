//
//  StudySectionView.swift
//  babayaga
//
//  Created by Thor Laack-Veeder, Sarah LeBlanc, Gustav Danielsson on 5/10/15.
//  Copyright (c) 2015 mobileApp. All rights reserved.
//

import UIKit

class StudySectionView: UIViewController, UITableViewDataSource, UITableViewDelegate{

    /*
    
    StudySectionView generates a table populated with sectionTitles or all words available throughout the selected Chapter in Laura's introductory Russian course. 
    
    The user can toggle between a list of sectionTitles, or a list of all the words encountered throughout the Chapter. 
    
    The userClick is saved as sectionListName.someString, which allows us to access it in either conjugationsViewController(if user clicked a word from list of all word) or in StudyWordsInSectionView(if user clicked a chapter from the list of chatpers). 
    
    ChapterTitles and words are generated from reading through Verbs.csv, which occurs in our appDelegate.swift file. 
    
    When the user clicks on a word, it will take them to conjugationViewController, which displays the conjugations for that verb in Russian. If a user chooses a sectionTitle, they will be taken to StudyWordsInSectionView.swift.
    
    */
    
    //Globals to be used throughout the function
    @IBOutlet weak var Section_Seg_Ctrl: UISegmentedControl!
    @IBOutlet weak var sectionTable: UITableView!
    let cellIdentifier = "ChapterList"
    //let shareData = ShareData.sharedInstance
    //var sectionListName = ShareData.sharedInstance
    var saveString:String = ""
    var options : NSMutableArray = []
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.saveString = appDelegate.userSelectionInStudyModeMainView
        refresh()
        //self.sectionListName.someString = ""
        self.sectionTable?.delegate = self
        self.sectionTable?.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func Section_Seg_Ctrl_Handler(sender: AnyObject) {
        refresh()
    }
    
      //refresh is called when the page loads, or when user toggles between segmented control. Depending on which is selected (0 means sections/lessons, and 1 means all words), we generate either sectionTitles or words in the tableview
    func refresh(){
        if (Section_Seg_Ctrl.selectedSegmentIndex == 0){
            generateOptionsArray(appDelegate.columnDictionary[self.saveString + " Sections"]!)
        }
        else{
            generateOptionsArray(appDelegate.columnDictionary[self.saveString]!)
        }
        self.sectionTable?.reloadData()
        
    }
    
    //generate an array that stores all items to be displayed in tableView
    func generateOptionsArray(ColumnNames: [String]){
        self.options.removeAllObjects()
        for index in 0...ColumnNames.count-1{
            if ColumnNames[index] != ""{
                println(ColumnNames[index])
                self.options.addObject(ColumnNames[index])
            }
        }
    }
    
    //Generic Setup Function given by Jeff Ondich
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //Generic Setup Function given by Jeff Ondich
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    
    //Generic Setup Function given by Jeff Ondich
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }
        
        cell.textLabel?.text = self.options[indexPath.row] as? String
        
        return cell
    }
    
    //Generic Setup Function given by Jeff Ondich
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    //Generic Setup Function given by Jeff Ondich
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return indexPath
    }
    
    //Generic Setup Function given by Jeff Ondich, with a variation. Save either chapter title or word name, depending on which seg_control index was selected, which is used in either StudyWordsInSectionView or conjugationViewController respectively. Accessed via user click.
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let index = indexPath.row
        if(Section_Seg_Ctrl.selectedSegmentIndex == 0){
            let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
            let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("listOfWords") as! UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
            appDelegate.userSelectionInStudyModeSectionView = String(self.options[index] as! NSString)
        }
        else{
            let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
            let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("conjugationsView") as! UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
            appDelegate.userSelectionInStudyModeWordsInSectionView = String(self.options[index] as! NSString)
        }
    }
}

