//
//  StudyMainView.swift
//  babayaga
//
//  Created by Thor Laack-Veeder, Sarah LeBlanc, Gustav Danielsson on 5/10/15.
//  Copyright (c) 2015 mobileApp. All rights reserved.
//

import UIKit

class StudyMainView: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    /*
    
    Purpose for StudyMainView is to generate a table that populates with chapterTitles or all words available throughout the term in Laura's introductory Russian course. 
    
    The user can toggle between a list of chapterTitles, or a list of all the words encountered throughout the term. 
    
    The userClick is saved as shareData.someString, which allows us to access it in either conjugationsViewController(if user clicked a word from list of all word) or in StudySectionView(if user clicked a chapter from the list of chapters).
    
    ChapterTitles and words are generated from reading through Verbs.csv, which occurs in our appDelegate.swift file. When the user clicks on a word, it will take them to conjugationViewController, which displays the conjugations for that verb in Russian. 
    
    If a user chooses a chapterTitle, they will be taken to StudySectionView.swift.
    
    */
    
    //Globals to be used in function
    @IBOutlet weak var Chapter_AllWords_Seg_Control: UISegmentedControl!
    @IBOutlet weak var chapterTable: UITableView?
    let cellIdentifier = "ChapterList"
    var options:NSMutableArray = []
    //let shareData = ShareData.sharedInstance
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    

    override func viewDidLoad() {
        super.viewDidLoad()
        refresh()
        //self.shareData.someString = ""
        self.chapterTable?.delegate = self
        self.chapterTable?.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Handles when user toggles between segmented controls(all words, chapters)
    @IBAction func chapAllWordsSegCtrlHandler(sender: AnyObject) {
        refresh()
    }
    
    //generate an array that stores all items to be displayed in tableView
    func generateOptionsArray(ColumnNames: [String]){
        self.options.removeAllObjects()
        for index in 0...ColumnNames.count-1{
            if ColumnNames[index] != ""{
                self.options.addObject(ColumnNames[index])
            }
        }
    }
    
    //refresh is called when the page loads, or when user toggles between segmented control. Depending on which is selected (0 means chapters, and 1 means all words), we generate the either chapterTitles or words in the tableview
    func refresh(){
        if (Chapter_AllWords_Seg_Control.selectedSegmentIndex == 0){
            generateOptionsArray(appDelegate.columnDictionary["Chapters"]!)
        }
        else {
            generateOptionsArray(appDelegate.columnDictionary["инфинити́в"]!)
        }
        self.chapterTable?.reloadData()
    }
    
    //Generic Setup Function given by Jeff Ondich
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //Generic Setup Function given by Jeff Ondich
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    
    //Generic Setup Function given by Jeff Ondich
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }
        
        cell.textLabel?.text = self.options[indexPath.row] as? String
        
        return cell
    }
    
    //Generic Setup Function given by Jeff Ondich
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    //Generic Setup Function given by Jeff Ondich
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return indexPath
    }
    
    
    //Generic Setup Function given by Jeff Ondich, with a variation. Save either chapter title or word name, depending on which seg_control index was selected, which is used in either StudySectionView or conjugationViewController respectively. Accessed via user click.
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let index = indexPath.row
        if (Chapter_AllWords_Seg_Control.selectedSegmentIndex == 0){
            let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
            let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("sarahView") as! UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
            appDelegate.userSelectionInStudyModeMainView = String(self.options[index] as! NSString)
        }
        else{
            let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
            let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("conjugationsView") as! UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
            appDelegate.userSelectionInStudyModeWordsInSectionView = String(self.options[index] as! NSString)
        }
    }
}

