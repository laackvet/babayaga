//
//  Words.swift
//  babayaga
//
//  Created by Sarah LeBlanc, Thor Laack-Veeder, Gustav Danielsson on 5/10/15.
//  Copyright (c) 2015 mobileApp. All rights reserved.
//

import UIKit

/*

WordsController is in charge of showing the words for a certain section
    Users can either turn a switch to turn all words from section on or off or can
    select individual words to turn on or off.  All words are kept in a wordDictionary
    and the value for each word is 0 (off) or 1 (on).

*/


class WordsController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // Global variables for this class
    @IBOutlet weak var listName: UILabel!
    @IBOutlet weak var wordTable: UITableView!
    @IBOutlet weak var alllabel: UILabel!
    @IBOutlet weak var allswitch: UISwitch!
    let options : NSMutableArray = []
    let cellIdentifier = "ChapterList"
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    

    // Determine if the switch for the section should be on or off
    // at the beginning of the page
    override func viewDidLoad() {
        super.viewDidLoad()
        generateOptionsArray(appDelegate.columnDictionary[appDelegate.userSelectionInUnitsSettingsView]!)
        self.wordTable?.delegate = self
        self.wordTable?.dataSource = self
        //self.listName.text = "Section " + sectionListName.someString
        if (appDelegate.sectionDictionary[appDelegate.userSelectionInUnitsSettingsView]! == 1) {
            self.allswitch.setOn(true, animated: true)
            self.alllabel.text = "All on"
        } else {
            self.allswitch.setOn(false, animated: true)
            self.alllabel.text = "All off"
        }
    }
    
    
    // Change the value of the section to a certain value
    func turnSection(on: Int) {
        appDelegate.sectionDictionary[appDelegate.userSelectionInUnitsSettingsView] = on
    }
    
    
    // Switch handler
    @IBAction func handleSwitch() {
        
        // Turning the section on
        if allswitch.on {
            
            //Turn all words of the section on
            for word in 0...self.options.count-1 {
                let strWord = self.options[word] as! String
                if (strWord != "") {
                    appDelegate.wordDictionary[strWord]!.6 = 1
                }
            }
            
            // Update the switch and label
            alllabel.text = "All on"
            allswitch.setOn(true, animated: true)
            
            // Turn the section on in the dictionary
            appDelegate.sectionDictionary[appDelegate.userSelectionInUnitsSettingsView]! = 1
            
            // Refresh the page to update the check marks for the on words
            self.refresh()
        }
        
        // Turning the section off
        else {
            
            // Turn all words of the section off
            for word in 0...self.options.count-1 {
                let strWord = self.options[word] as! String
                if (strWord != "") {
                    appDelegate.wordDictionary[strWord]!.6 = 0
                }
            }
            
            // Update the switch and label
            alllabel.text = "All off"
            allswitch.setOn(false, animated: true)
            
            // Turn the section off in the dictionary
            appDelegate.sectionDictionary[appDelegate.userSelectionInUnitsSettingsView]! = 0
            
            // Refresh the page to update the check marks for the on words
            self.refresh()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func generateOptionsArray(ColumnNames: [String]) {
        for index in 0...ColumnNames.count-1{
            if ColumnNames[index] != "" {
                self.options.addObject(ColumnNames[index])
            }
        }
    }
    
    // Update the table
    func refresh() {
        self.wordTable?.reloadData()
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    // Count the number of words in section
    func totalWordsOnPage() -> Int {
        var count = 0
        for word in 0...self.options.count-1 {
            let strWord = self.options[word] as! String
            if (strWord != "") {
                count += 1
            }
        }
        return count
    }
    
    
    // Count the number of words On from the section
    func wordsOn() -> Int {
        var on = 0
        for word in 0...self.options.count-1 {
            let strWord = self.options[word] as! String
            if (strWord != "") {
                if appDelegate.wordDictionary[strWord]!.6 == 1 {
                    on += 1
                }
            }
        }
        return on
    }
    
    
    // Count the number of words Off from the section
    func wordsOff() -> Int {
        var off = 0
        for word in 0...self.options.count-1 {
            let strWord = self.options[word] as! String
            if (strWord != "") {
                if appDelegate.wordDictionary[strWord]!.6 == 0 {
                    off += 1
                }
            }
        }
        return off
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    
    
    // Populate the table with the words from the current section
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
        }
    
        cell.textLabel?.text = self.options[indexPath.row] as? String
        if self.options[indexPath.row] as? String != "" {
            
            // Determine if the word should have a check mark or not (a checkmark means a frequency of 1)
            let wordVal = appDelegate.wordDictionary[(self.options[indexPath.row] as? String)!]!.6
            if wordVal == 1 {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            } else {
                cell.accessoryType = UITableViewCellAccessoryType.None
            }
        }
        return cell
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        return indexPath
    }
    
    
    // If a row is selected, the word needs to change value in the dictionary and the checkmark needs
    // to be toggled
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell: UITableViewCell! = tableView.cellForRowAtIndexPath(indexPath)

        let index = indexPath.row
        let word = self.options[index] as! String
        
        let freq = appDelegate.wordDictionary[word]!.6
        
        // If the frequency is 0, turn the word to frequency 1 in the dictionary and add
        // a checkmark to the row
        if freq == 0 {
            appDelegate.wordDictionary[word]!.6 = 1
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            
            // If all words are now on, set the section to value 1 and turn the switch on
            if self.totalWordsOnPage() == self.wordsOn() {
                allswitch.setOn(true, animated: true)
                alllabel.text = "All on"
                self.turnSection(1)
            }
            
            // If all words aren't on, turn the section to value 2 (on and off)
            else {
                self.turnSection(2)
            }
  
        }
        
        // If the frequency is 1, turn the word to frequency 0 in the dictionary and take 
        // away the checkmark from the row
        else {
            appDelegate.wordDictionary[word]!.6 = 0
            cell.accessoryType = UITableViewCellAccessoryType.None
            
            // If all words are now off, turn the switch off and turn the section to 0
            if self.totalWordsOnPage() == self.wordsOff() {
                allswitch.setOn(false, animated: true)
                alllabel.text = "All off"
                self.turnSection(0)
            }
            
            // If all words aren't off, turn the section to 2 (on and off) and turn the switch off
            else {
                allswitch.setOn(false, animated: true)
                alllabel.text = "All off"
                self.turnSection(2)
            }
        }
    }
}