//
//  FlashCardAbout.swift
//  babayaga
//
//  Created by Sarah LeBlanc on 6/6/15.
//  Copyright (c) 2015 mobileApp. All rights reserved.
//

import UIKit

class FlashCardAbout: UIViewController {
    @IBOutlet weak var ButtonLabel: UILabel!
    @IBOutlet weak var SettingsLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ButtonLabel.text = "Next : \t Go to the next randomly generated \n\t\t vocabulary word \n\n" +
                                
                                "See Russian Conjugation : \t See the conjugation \n\n" +
                                "Back to English : \t View the English translation"
        
        self.SettingsLabel.text = "In settings you can determine which words you would like \n" +
                                "to see appear as flashcards.  You can turn entire \n" +
                                "chapters on at a time, entire lessons, or individual \n" +
                                "words.  Words that you turn off will not appear as \n" +
                                "flashcards.  These are the same settings as the \n" +
                                "settings for the test."
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}

