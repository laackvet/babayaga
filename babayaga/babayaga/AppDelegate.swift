//
//  AppDelegate.swift
//  babayaga
//  ulapi_swift_example
//
//  Created by Jeff Ondich, Thor Laack-Veeder, Sarah LeBlanc, Gustav Danielsson on 5/15/15.
//  Copyright (c) 2015 Jeff Ondich, mobileapp. All rights reserved.
//

import UIKit

@UIApplicationMain

/*

AppDelegate is where we set up our global variables for the app, it is called only when the
    app is booted up for the first time.  We set up ULAPI and gather the information from
    the csv file Laura gave us of the words and their information.

*/


class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    // Create empty dictionaries for the words, chapters and sections
    var wordDictionary: [String: (String,String,String,String,String,String,Int,String,String)] = [:]
    var chapterDictionary: [String: Int] = [:]
    var sectionDictionary: [String: Int] = [:]
    
    var columnDictionary: [String: [String]] = [:]
    var correctCount: Int = 0
    var totalCount: Int = 0
    var userInputVerb: String = ""
    var correctVerb: String = ""
    var userSelectionInStudyModeMainView: String = ""
    var userSelectionInStudyModeSectionView: String = ""
    var userSelectionInStudyModeWordsInSectionView: String = ""
    var userSelectionInChapterSettingsView: String = ""
    var userSelectionInUnitsSettingsView: String = ""
    
    
    
    // Set global variables to keep track of the headers (which are in Russian)
    var chapter = "Глава́"
    var sectionTitle = "уро́к"
    var infiniteVerbs = "инфинити́в"
    var aspectPartner = "вид"
    var aspectTypePresent = "НСВ"
    var aspectTypeFuture = "СВ"
    var englishTranslation = "значе́ние"
    var englishVerbPhrase = ""

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Set up ULAPI factory
        self.initializeULAPI()
        self.makeColumnDictionary()
        self.makeWordDictionary()
        self.makeChapterDictionary()
        self.makeSectionDictionary()
        return true
    }
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    // Take the chapters from the csv file and put them in the chapterDictionary with an initial value of 0
    func makeChapterDictionary() {
        let path = NSBundle.mainBundle().pathForResource("verbsAlphabatized", ofType: "csv")
        if let url = NSURL(fileURLWithPath: path!) {
            var error: NSErrorPointer = nil
            if let csv = CSV(contentsOfURL: url, error: error) {
                let rows = csv.rows
                for index in 0...rows.count-1 {
                    var chapterN = csv.rows[index]["ch"]!
                    self.chapterDictionary[self.chapter + " " + chapterN] = 0
                }
            }
        }
    }
    
    
    // Take the sections from the csv file and put them in the sectionDictionary with an initial value of 0
    func makeSectionDictionary() {
        let path = NSBundle.mainBundle().pathForResource("verbsAlphabatized", ofType: "csv")
        if let url = NSURL(fileURLWithPath: path!) {
            var error: NSErrorPointer = nil
            if let csv = CSV(contentsOfURL: url, error: error) {
                let rows = csv.rows
                for index in 0...rows.count-1 {
                    var section = csv.rows[index][sectionTitle]!
                    self.sectionDictionary[section] = 0
                }
            }
        }
    }
    

    
    // For each row of the csv file, make an entry for the word in the wordDictionary.  Each word needs to know
    // which chapter and section it came from, it's aspect, verb partner (CB or HCB), its translation, stem,
    // and if it is on or off.  Each word starts with a value 0 (is turned off).
    func makeWordDictionary() {
        let path = NSBundle.mainBundle().pathForResource("verbsAlphabatized", ofType: "csv")
        if let url = NSURL(fileURLWithPath: path!) {
            var error: NSErrorPointer = nil
            if let csv = CSV(contentsOfURL: url, error: error) {
                let rows = csv.rows
                for index in 0...rows.count-1{
                    var dictKey = csv.rows[index][infiniteVerbs]!
                    var chap = csv.rows[index]["ch"]!
                    var sec = csv.rows[index]["sec"]!           // Just the section number
                    var aspect = csv.rows[index][aspectPartner]!
                    var active = 0
                    var verbPartnerCB = csv.rows[index][aspectTypeFuture]!
                    var verbPartnerHCB = csv.rows[index][aspectTypePresent]!
                    var translation = csv.rows[index][englishTranslation]!
                    var section = csv.rows[index][sectionTitle]!        // Chapter.Section
                    var stem = csv.rows[index]["stem"]!
                    var value = (chap, sec, verbPartnerCB, verbPartnerHCB, aspect, translation, active, section, stem)
                    self.wordDictionary[dictKey] = value
                }
            }
        }
    }
    
    
    // Creates a dictionary where the keys are the column header, used to generate lists of chapters, sections, words
    func makeColumnDictionary() {
        let path = NSBundle.mainBundle().pathForResource("verbsAlphabatized", ofType: "csv")
        if let url = NSURL(fileURLWithPath: path!) {
            var error: NSErrorPointer = nil
            if let csv = CSV(contentsOfURL: url, error: error) {
                self.columnDictionary = csv.columns
            }
        }
    }
    
    // MARK: - ULAPI
    func initializeULAPI() {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0] as! String // A place to look
        let mainBundlePath = NSBundle.mainBundle().resourcePath
        self.addULAPIFilesFromDirectory(mainBundlePath!)
    }
    
    
    // Add ULAPI files
    func addULAPIFilesFromDirectory(directoryPath: String) {
        let fileManager = NSFileManager.defaultManager()
        let enumerator = fileManager.enumeratorAtPath(directoryPath)
        if enumerator != nil {
            let ulapi : ULAPI = ULAPI.sharedULAPI()
            
            while let element = enumerator!.nextObject() as? String {
                if element.hasSuffix("uld") {
                    if !ulapi.addDictionaryDataSource("\(directoryPath)/\(element)") {
                        // TODO: do whatever we need to do to indicate this is not available
                    } else {
                        println("Added \(element)")
                    }
                } else if element.hasSuffix("ulc") {
                    if !ulapi.addLanguageDataSource("\(directoryPath)/\(element)") {
                        // TODO: do whatever we need to do to indicate this is not available
                    } else {
                        println("Added \(element)")
                    }
                }
            }
        }
    }
}