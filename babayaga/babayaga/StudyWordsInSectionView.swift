//
//  StudyWordsInSectionView.swift
//  babayaga
//
//  Created by Thor Laack-Veeder, Sarah LeBlanc, Gustav Danielsson on 5/10/15.
//  Copyright (c) 2015 mobileApp. All rights reserved.
//

import UIKit

class StudyWordsInSectionView: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    /*
    
    StudyWordsInSectionView generates a table that populates with words from a particular section. 
    
    The section number is saved as sectionListName, which was accessed from a user click in StudySectionView.swift. 
    
    The words are generated from reading through Verbs.csv. 
    
    When the user clicks on a word, it will take them to conjugationViewController, which displays the conjugations for that verb in Russian.
    
    */
    
    //Globals to be used throughout the function
    @IBOutlet weak var listName: UILabel!
    @IBOutlet weak var wordTable: UITableView!

    //let sectionListName = ShareData.sharedInstance
    //let wordListName = ShareData.sharedInstance
    let options : NSMutableArray = []
    let cellIdentifier = "ChapterList"
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //Populate table of words given sectionListName, columnDictionary is set in the appDelegate.swift file, and is created after we read Verbs.csv.
    override func viewDidLoad() {
        super.viewDidLoad()
        generateOptionsArray(appDelegate.columnDictionary[appDelegate.userSelectionInStudyModeSectionView]!)
        self.wordTable?.delegate = self
        self.wordTable?.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Function that takes in a column stored as a String array, and adds the strings as objects to our options Array, which is used to populate the table
    func generateOptionsArray(ColumnNames: [String]!){
        self.options.removeAllObjects()
        for index in 0...ColumnNames.count-1{
            if ColumnNames[index] != ""{
                self.options.addObject(ColumnNames[index])
            }
        }
    }
    
    //Generic Setup Function given by Jeff Ondich
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //Generic Setup Function given by Jeff Ondich
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    
    //Generic Setup Function given by Jeff Ondich
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }
        
        cell.textLabel?.text = self.options[indexPath.row] as? String
        
        return cell
    }
    
    //Generic Setup Function given by Jeff Ondich
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    //Generic Setup Function given by Jeff Ondich
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return indexPath
    }
    
    //Generic Setup Function given by Jeff Ondich, with a variation. Save wordName, which is used in conjugationViewController. Accessed via user click.
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let index = indexPath.row
        let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
        let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("conjugationsView") as! UIViewController

        self.navigationController?.pushViewController(vc, animated: true)
        appDelegate.userSelectionInStudyModeWordsInSectionView = String(self.options[index] as! NSString)
        
    }

}
