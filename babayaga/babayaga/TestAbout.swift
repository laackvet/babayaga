//
//  TestAbout.swift
//  babayaga
//
//  Created by Sarah LeBlanc on 6/6/15.
//  Copyright (c) 2015 mobileApp. All rights reserved.
//

import UIKit

class TestAbout: UIViewController {
    
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var keyboardInstructionsLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.keyboardInstructionsLabel.text = "1. Go to Settings > General > Keyboard > \n \t Keyboards \n" + "2. Click Add New Keyboard \n" + "3. Select Russian \n" + "4. From the standard keyboard, \n\t click on the 'globe' icon \n 5. Select the Russian keyboard"
        
       self.settingsLabel.text = "In settings you can determine which words you would like " +
        "to see be tested on.  You can turn entire " +
        "chapters on at a time, entire lessons, or individual " +
        "words.  You will not be tested on words you turn " +
        "off.  These are the same settings as the flash " +
        "card settings."
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}
