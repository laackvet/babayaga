/**
 * @file ultense.h
 * @brief The interface for the ULTense class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULTENSE_H
#define ULTENSE_H

#include "ulenum.h"

/**
 * @class ULTense
 * @brief An enhanced enumerated type used to represent tenses.
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULTense : public ULEnum
{
public:
    static const ULTense& None;
    static const ULTense& Any;
    static const ULTense& Infinitive;
    static const ULTense& ToInfinitive;
    static const ULTense& PastParticiple;
    static const ULTense& PastParticipleMasculineSingular;
    static const ULTense& PastParticipleMasculinePlural;
    static const ULTense& PastParticipleFeminineSingular;
    static const ULTense& PastParticipleFemininePlural;
    static const ULTense& PresentParticiple;
    static const ULTense& Gerund;
    static const ULTense& Present;
    static const ULTense& PresentProgressive;
    static const ULTense& Future;
    static const ULTense& FutureProgressive;
    static const ULTense& FuturePerfect;
    static const ULTense& Past;
    static const ULTense& PresentPerfect;
    static const ULTense& PastPerfectCompound;
    static const ULTense& Imperfect;
    static const ULTense& Conditional;
    static const ULTense& PastConditional;
    static const ULTense& ConditionalProgressive;
    static const ULTense& PastConditionalProgressive;
    static const ULTense& PresentPerfectProgressive;
    static const ULTense& PastPerfectProgressive;
    static const ULTense& FuturePerfectProgressive;
    static const ULTense& Imperative;
    static const ULTense& PastSubjunctive;
    static const ULTense& PastSubjunctive2;
    static const ULTense& PastAnterior;
    static const ULTense& PresentSubjunctive;
    static const ULTense& PresentSubjunctive2;
    static const ULTense& ImperfectSubjunctive;
    static const ULTense& FutureSubjunctive;
    static const ULTense& FutureSubjunctive2;
    static const ULTense& PastPerfectSubjunctive;
    static const ULTense& FuturePerfectSubjunctive;
    static const ULTense& FuturePerfectSubjunctive2;
    static const ULTense& PastPerfect;
    static const ULTense& PresentPerfectSubjunctive;
    static const ULTense& PreteritStem;
    static const ULTense& Subjunctive2Stem;
    static const ULTense& PastAnteriorSubjunctive;
    static const ULTense& PresentPersonalInfinitive;
    static const ULTense& PersonalInfinitivePerfect;
    static const ULTense& PerfectParticiple;
    static const ULTense& SubjunctivePastParticiple;
    static const ULTense& PastParticipleStem;
    static const ULTense& Gerundive;
    static const ULTense& PresentPassive;
    static const ULTense& PresentSubjunctivePassive;
    static const ULTense& ImperfectPassive;
    static const ULTense& ImperfectSubjunctivePassive;
    static const ULTense& FuturePassive;
    static const ULTense& PresentPerfectPassive;
    static const ULTense& PresentPerfectSubjunctivePassive;
    static const ULTense& PastPerfectPassive;
    static const ULTense& PastPerfectSubjunctivePassive;
    static const ULTense& FuturePerfectPassive;
    static const ULTense& Imperative2;
    static const ULTense& ImperativePassive;
    static const ULTense& ImperativeFuture;
    static const ULTense& PresentDeponent;
    static const ULTense& PresentSubjunctiveDeponent;
    static const ULTense& ImperfectDeponent;
    static const ULTense& ImperfectSubjunctiveDeponent;
    static const ULTense& FutureDeponent;
    static const ULTense& PresentPerfectDeponent;
    static const ULTense& PresentPerfectSubjunctiveDeponent;
    static const ULTense& PastPerfectDeponent;
    static const ULTense& PastPerfectSubjunctiveDeponent;
    static const ULTense& FuturePerfectDeponent;
    static const ULTense& NegativeImperative;


    ULTense();
    ULTense(const ULTense& other);
    ULTense(int id);
    ULTense(const char *otherStringID);
    virtual ~ULTense();

    virtual void clear();
    ULTense& operator=(const ULTense& other);
    ULTense& operator=(int otherID);
    ULTense& operator=(const char *otherStringID);

    bool operator==(const ULTense& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULTense& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULTense& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULTense& getInstance(int id);
    ULLanguage getLanguage() const;
    bool isParticiple() const;
    bool isCompound() const;
    bool isListed() const;
    static void getAllTenses(const ULLanguage& language, ULList<ULTense>& tenses);
    static void getAllTenses(ULList<ULTense>& tenses);



private:
    static int nEnumeratedValues;
    static ULVector<const ULTense *> enumeratedValueVector;
    ULTense(const char *stringID, const char *displayString);
    const char *languageISOCode;
    bool isAParticiple;
    bool isACompoundTense;
    bool isAListedTense;

    ULTense(const char *stringID,
            const char *languageISOCode,
            const char *displayString,
            bool isParticiple,
            bool isCompound,
            bool isListed);
};


#endif

