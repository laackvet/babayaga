/**
 * @file ulnumbertranslatormandarin.h
 * @brief The interface for the ULNumberTranslatorMandarin class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORMANDARIN_H
#define ULNUMBERTRANSLATORMANDARIN_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorMandarin
 * @brief ULNumberTranslatorMandarin does number translation in Mandarin.
 */
class ULNumberTranslatorMandarin : public ULNumberTranslator
{
public:
    ULNumberTranslatorMandarin();
    ULNumberTranslatorMandarin(const ULNumberTranslatorMandarin& other);
    virtual ~ULNumberTranslatorMandarin();
    
    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);
    
    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

