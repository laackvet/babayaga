/**
 * @file ulnumbertranslatorklingon.h
 * @brief The interface for the ULNumberTranslatorKlingon class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORKLINGON_H
#define ULNUMBERTRANSLATORKLINGON_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorKlingon
 * @brief ULNumberTranslatorKlingon does number translation in Klingon.
 */
class ULNumberTranslatorKlingon : public ULNumberTranslator
{
public:
    ULNumberTranslatorKlingon();
    ULNumberTranslatorKlingon(const ULNumberTranslatorKlingon& other);
    virtual ~ULNumberTranslatorKlingon();
    
    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);
    
    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

