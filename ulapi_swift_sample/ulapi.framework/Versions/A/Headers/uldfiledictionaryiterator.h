/**
 * @file uldfiledictionaryiterator.h
 * @brief The interface for the ULDFileDictionaryIterator class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULDFILEDICTIONARYITERATOR_H
#define ULDFILEDICTIONARYITERATOR_H

#ifndef __STDC_LIMIT_MACROS
#define __STDC_LIMIT_MACROS
#endif
#include <stdint.h>

#include "uldictionarydatasourceiterator.h"
#include "ulsegmentedfile.h"
#include "ulstandardforester.h"

/**
 * @class ULDFileDictionaryIterator
 * @brief ULDFileDictionaryIterator is the dictionary data source iterator for
 * use with ULDFileDictionaryDataSource (i.e. dictionary files using the ULAPI 8
 * .uld file format).
 */
class ULDFileDictionaryIterator : public ULDictionaryDataSourceIterator
{
    UL_TEST_FRIEND;
    friend class ULDFileDictionaryDataSource;
    friend class ULStandardForester;

public:
    ULDFileDictionaryIterator();
    ULDFileDictionaryIterator(const ULDFileDictionaryIterator& other);
    virtual ~ULDFileDictionaryIterator();

    virtual ULDFileDictionaryIterator& operator=(const ULDFileDictionaryIterator& other);
    virtual void clear();

    virtual ULDictionaryDataSourceIterator *clone() const;

    virtual const ULDictionaryNode& operator*();
    virtual void operator++();
    virtual void operator--();
    virtual bool operator==(const ULDictionaryDataSourceIterator& iterator) const;
    virtual bool operator==(const ULDFileDictionaryIterator& iterator) const;
    virtual uluint32 hash(uluint32 tableSize) const;

    virtual void toNextTopLevelNode();
    virtual void toCurrentTopLevelNode();
    virtual void toPreviousTopLevelNode();
    virtual bool hasParent();
    virtual void toParent();
    virtual bool hasChildren();
    virtual void toFirstChild();
    virtual bool hasNextSibling();
    virtual void toNextSibling();
    virtual bool hasPreviousSibling();
    virtual void toPreviousSibling();
    virtual bool hasNextCousin();
    virtual void toNextCousin();
    virtual bool hasPreviousCousin();
    virtual void toPreviousCousin();
    virtual int getLevel();
    virtual uluint32 getType();
    virtual bool isAtEnd() const;
    virtual bool isAtBeginning() const;
    virtual bool isInFirstTree() const;
    virtual bool isInLastTree() const;
    virtual ULString getIndexKey();
    virtual uluint32 getIndexRank();
    virtual const ULLanguage& getLanguage() const;
    virtual void setLanguage(const ULLanguage& language);
    virtual const ULLanguage& getSearchLanguage() const;
    virtual const ULLanguage& getTranslationLanguage() const;

protected:
    // Parameters for the ULAPI 8 .uld file format.
    enum {
        BytesInNodeOffset = 4,
        NodeTerminator = 0,
        IndexEntryTerminator = 0, // Really can't be anything but 0
        IndexTextTerminator = 1 // Must be different from IndexEntryTerminator
    };

    // The data source's segmented file object. This iterator is *not* responsible for
    // deallocating this segmented file.
    ULSegmentedFile *segmentedFile;

    // The language of the portion of the dictionary data source to which this iterator points.
    ULLanguage language;

    // indexSegmentIndex is the index (within the segmented file) of the
    // segment containing the index that this iterator is using to refer to nodes
    // in the dictionary data source. It might refer to the headword/sortkey index,
    // or to an index referring to ontology tags, or to some other index.
    //
    // nodeSegmentIndex is the segment index for the segment containing the nodes
    // (i.e. the actual dictionary data).
    uluint32 indexSegmentIndex;
    uluint32 nodeSegmentIndex;

    // A ULDFileDictionaryIterator points first to an "index entry" within the index segment.
    // For example, it might point to the "rake" entry in the headword/sortkey index.
    // The "rake" entry will consist of:
    //
    //    r a k e \x01 [node offset for "rake noun"] [node offset for "rake verb"] 
    //
    // where the node offsets are 4-byte base-255 integers specifying offsets into
    // the node segment that contains the actual dictionary data. Note that the nodes
    // for "rake noun" and "rake verb" are known as "top-level nodes". They in turn
    // have child nodes specifying definitions/translations, example phrases, etc.
    // 
    // Once we know which sortkey index entry (e.g. "rake") and which top-level node
    // (e.g. "rake verb") this iterator points to, we also need to know more precisely
    // which descendant node of the top-level node it points to (e.g. the node containing
    // the French translation "ratisser", or the translation node for "râteler", or
    // even the top-level "rake verb" node itself).
    //
    // To store all this information, we use "position" variables to indicate byte offsets
    // within the index segment, and "nodeOffset" variables to indicate byte offsets within
    // the node segment.
    //
    // minPosition and maxPosition mark off this iterator's boundaries (e.g. they might
    // be set to constrain iteration only to the Q's), while position is the byte offset
    // of the current index entry (e.g. the offset of the "rake\x01" in the example above).
    // topLevelNodeOffsetPosition is the position within the index segment at which we find the
    // node offset for the current top-level node. In the example above ("rake verb"),
    // this->topLevelNodeOffsetPosition == this->position + 9, to account for the 9 bytes between
    // the "r" and the beginning of the "rake verb" node offset. this->topLevelNodeOffset
    // is the "rake verb" integer in the sortkey index entry shown above, and
    // this->nodeOffset points to "rake verb"'s descendant node, which is the actual
    // node we're pointing to.
    uluint32 minPosition;
    uluint32 maxPosition;
    uluint32 position;
    uluint32 topLevelNodeOffsetPosition;
    uluint32 topLevelNodeOffset;
    uluint32 nodeOffset; 
    ULDictionaryNode currentNode;
    bool currentNodeLoaded;

    // Utility functions for getting the job done.
    void loadCurrentNode();
    void increment(uluint32& pos, uluint32& offsetPos, uluint32& topOffset, uluint32& offset);
    void decrement(uluint32& pos, uluint32& offsetPos, uluint32& topOffset, uluint32& offset, bool toTopLevel=false);
    void finishInitialization();
    void lockDataSource() const;
    void unlockDataSource() const;
};

#endif

