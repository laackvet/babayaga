/**
 * @file ulsegment.h
 * @brief The interface for the ULSegment class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULSEGMENT_H
#define ULSEGMENT_H

#include "ulfile.h"
#include "ulcompressor.h"

/**
 * @class ULSegmentCacheEntry
 * @brief Used to cache data for ULSegment.
 */
struct ULSegmentCacheEntry
{
    uluint32 blockNumber;
    uluint32 nBytes;
    uluint8 *uncompressedData;

    ULSegmentCacheEntry();
    ULSegmentCacheEntry(uluint32 blockSize);
    ~ULSegmentCacheEntry();

    void clear();
};

/**
 * @class ULSegment
 * @brief Each ULSegment object represents a single segment from
 * a ULSegmentedFile. Segments can be loaded fully into memory or
 * into a cache.  If a cache is used, the cache size is configurable.
 */
class ULSegment
{
    friend class ULSegmentedFile;

public:
    enum { DoNotOpenSegmentType=1, LoadedSegmentType, CachedSegmentType };
    enum { DefaultCacheSize = 8, DefaultBlockSize = 0x400, DefaultSegmentSize = 128 };

    ULSegment();
    ULSegment(ULFile *file,
              ULCompressor *compressor,
              uluint32 start,
              uluint32 uncompressedSize, 
              uluint32 compressedSize,
              uluint32 blockSize,
              uluint32 type=ULSegment::LoadedSegmentType,
              uluint32 cacheSize=ULSegment::DefaultCacheSize);

    ~ULSegment();

    void init(ULFile *file,
              ULCompressor *compressor,
              uluint32 start,
              uluint32 uncompressedSize, 
              uluint32 compressedSize,
              uluint32 blockSize,
              uluint32 type=ULSegment::LoadedSegmentType,
              uluint32 cacheSize=ULSegment::DefaultCacheSize);

    uluint32 getStart();
    uluint32 getUncompressedSize();
    uluint32 getCompressedSize();
    uluint32 getBlockSize();
    bool isCompressed();

    uluint8 getByte(uluint32 offset);

    void putByte(uluint8 byte, uluint32 offset);
    void flush();

private:
    ULFile *file;
    ULCompressor *compressor;
    uluint32 segmentType;
    uluint32 start;
    uluint32 uncompressedSize;
    uluint32 compressedSize;
    uluint32 blockSize;
    bool compressed;
    uluint32 nBlocks;

    // Used by all memory management schemes.
    uluint8 *tmpBuffer;

    // For segmentType == ULSegment::LoadedSegmentType.
    uluint8 *blockValid;
    uluint8 *segmentBuffer;

    // For segmentType == ULSegment::CachedSegmentType.
    ULSegmentCacheEntry **cache;
    uluint32 cacheSize;

    // For writing.
    uluint32 capacity;

    void clear();

    void initMemory();
    bool isBlockReady(uluint32 blockNumber);
    bool loadBlock(uluint32 blockNumber);
    uluint8 getByteFromBlock(uluint32 blockNumber, uluint32 blockOffset);
};

#endif

