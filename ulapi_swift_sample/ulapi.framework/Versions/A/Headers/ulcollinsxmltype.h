/**
 * @file ulcollinsxmltype.h
 * @brief The interface for the ULCollinsXMLType class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULCOLLINSXMLTYPE_H
#define ULCOLLINSXMLTYPE_H

#include "ulenum.h"

/**
 * @class ULCollinsXMLType
 * @brief An enhanced enumerated type used to represent the enumerable values of various
 * tag types in CXML data.
 * 
 * Certain tags were enumerated to help speed up rendering.
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULCollinsXMLType : public ULEnum
{
public:
    static const ULCollinsXMLType& None;
    static const ULCollinsXMLType& Any;
    static const ULCollinsXMLType& CompleteForm;
    static const ULCollinsXMLType& Full;
    static const ULCollinsXMLType& Substitute;
    static const ULCollinsXMLType& AddOn;
    static const ULCollinsXMLType& Ending;
    static const ULCollinsXMLType& MinimalUnit;
    static const ULCollinsXMLType& TranslationException;
    static const ULCollinsXMLType& Long;
    static const ULCollinsXMLType& Idiom;
    static const ULCollinsXMLType& PospRegional;
    static const ULCollinsXMLType& SeeAlso;
    static const ULCollinsXMLType& CollocateSubject;
    static const ULCollinsXMLType& FalseFriends;
    static const ULCollinsXMLType& PospModifier;
    static const ULCollinsXMLType& Ragbag;
    static const ULCollinsXMLType& Equivalent;
    static const ULCollinsXMLType& AlternativeFormWithEtc;
    static const ULCollinsXMLType& Weblink;
    static const ULCollinsXMLType& Expansion;
    static const ULCollinsXMLType& AdditionalExamples;
    static const ULCollinsXMLType& AlternativeForm;
    static const ULCollinsXMLType& Compound;
    static const ULCollinsXMLType& Proverb;
    static const ULCollinsXMLType& Translation;
    static const ULCollinsXMLType& CollocateAdjective;
    static const ULCollinsXMLType& LexicalisedForm;
    static const ULCollinsXMLType& Gloss;
    static const ULCollinsXMLType& Synonym;
    static const ULCollinsXMLType& Language;
    static const ULCollinsXMLType& CollocateNounOfAdjective;
    static const ULCollinsXMLType& Vanilla;
    static const ULCollinsXMLType& CrossReference;
    static const ULCollinsXMLType& Table;
    static const ULCollinsXMLType& AdditionalGrammar;
    static const ULCollinsXMLType& PhrasalVerb;
    static const ULCollinsXMLType& Example;
    static const ULCollinsXMLType& WithEtc;
    static const ULCollinsXMLType& LabelGrammar;
    static const ULCollinsXMLType& CollocateVerb;
    static const ULCollinsXMLType& CollocateNounOfNoun;
    static const ULCollinsXMLType& LexicalSetStructure;
    static const ULCollinsXMLType& Sign;
    static const ULCollinsXMLType& Abbreviation;
    static const ULCollinsXMLType& See;
    static const ULCollinsXMLType& ReflexiveVerb;
    static const ULCollinsXMLType& XRefOf;
    static const ULCollinsXMLType& CollocateObject;
    static const ULCollinsXMLType& PartOfSpeechCXML;
    static const ULCollinsXMLType& SeeCulturalBox;
    static const ULCollinsXMLType& Equiv;
    static const ULCollinsXMLType& RelatedWord;
    static const ULCollinsXMLType& PotentialRunOn;
    static const ULCollinsXMLType& Mathematics;
    static const ULCollinsXMLType& Symbol;
    static const ULCollinsXMLType& AccessionDate;
    static const ULCollinsXMLType& RefUseLabelText;
    static const ULCollinsXMLType& Definition;
    static const ULCollinsXMLType& Stress;
    static const ULCollinsXMLType& TechnicalTerm;
    static const ULCollinsXMLType& ConfusableWords;
    static const ULCollinsXMLType& Gram;
    static const ULCollinsXMLType& RunOn;
    static const ULCollinsXMLType& Partial;
    static const ULCollinsXMLType& Relationship;
    static const ULCollinsXMLType& ShortFor;
    static const ULCollinsXMLType& Alternative;
    static const ULCollinsXMLType& PospRegister;
    static const ULCollinsXMLType& PronunciationType;
    static const ULCollinsXMLType& PospSubjectfield;
    static const ULCollinsXMLType& Collocate;
    static const ULCollinsXMLType& Auxiliary;
    static const ULCollinsXMLType& SeeSet;
    static const ULCollinsXMLType& Cultural;
    static const ULCollinsXMLType& Word;
    static const ULCollinsXMLType& Keyword;
    static const ULCollinsXMLType& Phrase;


    ULCollinsXMLType();
    ULCollinsXMLType(const ULCollinsXMLType& other);
    ULCollinsXMLType(int id);
    ULCollinsXMLType(const char *otherStringID);
    virtual ~ULCollinsXMLType();

    virtual void clear();
    ULCollinsXMLType& operator=(const ULCollinsXMLType& other);
    ULCollinsXMLType& operator=(int otherID);
    ULCollinsXMLType& operator=(const char *otherStringID);

    bool operator==(const ULCollinsXMLType& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULCollinsXMLType& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULCollinsXMLType& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULCollinsXMLType& getInstance(int id);



private:
    static int nEnumeratedValues;
    static ULVector<const ULCollinsXMLType *> enumeratedValueVector;
    ULCollinsXMLType(const char *stringID, const char *displayString);
};


#endif

