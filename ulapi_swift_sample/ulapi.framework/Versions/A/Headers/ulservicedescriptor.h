/**
 * @file ulservicedescriptor.h
 * @brief The interface for the ULServiceDescriptor class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULSERVICEDESCRIPTOR_H
#define ULSERVICEDESCRIPTOR_H

#include "ullanguage.h"

class ULDictionaryDataSource;
class ULLanguageDataSource;
class ULLanguage;

/**
 * @enum The various types of service that can be requested from a
 * ULAPI worker object.
 */
enum ULServiceType
{
    ULServiceTypeNone,
    ULServiceTypeDictionarySearch,
    ULServiceTypeInflection,
    ULServiceTypeDissection,
    ULServiceTypeNumberTranslation,
    ULServiceTypePartOfSpeechTagging,
    ULServiceTypeStemming,
    ULServiceTypeConjugation,
    ULServiceTypePluralization,
    ULServiceTypeSingularization,

    ULNServiceTypes
};


/**
 * @class ULServiceDescriptor
 * @brief An object of type ULServiceDescriptor describes a
 * service required by the invoker of the service, along with information
 * about the data source from which the service will be supplied.
 */
struct ULServiceDescriptor
{
    ULServiceDescriptor()
    : type(ULServiceTypeNone), language(ULLanguage::NoLanguage), languageDataSource(0), secondLanguageDataSource(0), dictionaryDataSource(0)
    { }

    ULServiceType type;
    ULLanguage language;
    ULLanguageDataSource *languageDataSource;
    ULLanguageDataSource *secondLanguageDataSource;
    ULDictionaryDataSource *dictionaryDataSource;
    
    bool operator==(const ULServiceDescriptor& other) const;
    bool operator<(const ULServiceDescriptor& other) const;
};

#endif

