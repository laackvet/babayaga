/**
 * @file ulnumbertranslatorrussian.h
 * @brief The interface for the ULNumberTranslatorRussian class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORRUSSIAN_H
#define ULNUMBERTRANSLATORRUSSIAN_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorRussian
 * @brief ULNumberTranslatorRussian does number translation in Russian.
 */
class ULNumberTranslatorRussian : public ULNumberTranslator
{
public:
    ULNumberTranslatorRussian();
    ULNumberTranslatorRussian(const ULNumberTranslatorRussian& other);
    virtual ~ULNumberTranslatorRussian();

    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

