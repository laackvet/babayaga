/**
 * @file ullock-pthread.h
 * @brief The interface for the ULLockPThread class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULLOCKPTHREAD_H
#define ULLOCKPTHREAD_H

#include "ullock.h"
#ifndef UL_NO_PTHREADS
#include "pthread.h"
#endif

/**
 * @class ULLockPThread
 * @brief A ULLockPThread object represents a mutual exclusion
 * primitive for use in a context in which POSIX threads are
 * being used.  See ULLock, ULLockable, and ULScopeLocker for
 * more details.
 */
class ULLockPThread : public ULLock
{
public:
    ULLockPThread();
    ULLockPThread(const ULLockPThread& other);
    virtual ~ULLockPThread();

    const ULLockPThread& operator=(const ULLockPThread& other);
    void clear();

    virtual void claim();
    virtual void release();

private:
#ifndef UL_NO_PTHREADS
    pthread_mutex_t mutex;
#endif
};

#endif

