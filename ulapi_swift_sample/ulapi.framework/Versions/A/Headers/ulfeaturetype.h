/**
 * @file ulfeaturetype.h
 * @brief The interface for the ULFeatureType class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULFEATURETYPE_H
#define ULFEATURETYPE_H

#include "ulenum.h"

/**
 * @class ULFeatureType
 * @brief An enhanced enumerated type used to represent the various features (e.g. gender)
 * stored with individual words in ULAPI data sources.
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULFeatureType : public ULEnum
{
public:
    static const ULFeatureType& None;
    static const ULFeatureType& Any;
    static const ULFeatureType& Other;
    static const ULFeatureType& Type;
    static const ULFeatureType& Headword;
    static const ULFeatureType& Definition;
    static const ULFeatureType& Text;
    static const ULFeatureType& PartOfSpeech;
    static const ULFeatureType& PartOfSpeechCategory;
    static const ULFeatureType& PartOfSpeechSubcategory;
    static const ULFeatureType& Tense;
    static const ULFeatureType& Person;
    static const ULFeatureType& Number;
    static const ULFeatureType& Gender;
    static const ULFeatureType& Formality;
    static const ULFeatureType& Case;
    static const ULFeatureType& Aspect;
    static const ULFeatureType& Mood;
    static const ULFeatureType& Transitivity;
    static const ULFeatureType& Reflexivity;
    static const ULFeatureType& Pronunciation;
    static const ULFeatureType& Ontology;
    static const ULFeatureType& Etymology;
    static const ULFeatureType& Usage;
    static const ULFeatureType& Tone;
    static const ULFeatureType& Dialect;
    static const ULFeatureType& Clarification;
    static const ULFeatureType& Homonym;
    static const ULFeatureType& Synonym;
    static const ULFeatureType& Antonym;
    static const ULFeatureType& Derivation;
    static const ULFeatureType& SeeAlso;
    static const ULFeatureType& AlternateSpelling;
    static const ULFeatureType& GrammarUsage;
    static const ULFeatureType& Example;
    static const ULFeatureType& Semantic;
    static const ULFeatureType& Source;
    static const ULFeatureType& Target;
    static const ULFeatureType& Socio;
    static const ULFeatureType& Term;
    static const ULFeatureType& Translation;
    static const ULFeatureType& PrepositionForIndirectObject;
    static const ULFeatureType& Aspirated;
    static const ULFeatureType& AuxiliaryVerb;
    static const ULFeatureType& Disambiguation;
    static const ULFeatureType& FirstPrincipalPart;
    static const ULFeatureType& PresentSubjunctiveAffix;
    static const ULFeatureType& Strong;
    static const ULFeatureType& Separated;
    static const ULFeatureType& Inseparable;
    static const ULFeatureType& PreteritStem;
    static const ULFeatureType& Prefix2;
    static const ULFeatureType& Prefix3;
    static const ULFeatureType& Prefix4;
    static const ULFeatureType& PossessedNumber;
    static const ULFeatureType& PossessedGender;
    static const ULFeatureType& ObjectNumber;
    static const ULFeatureType& ObjectGender;
    static const ULFeatureType& PastParticipleStem;
    static const ULFeatureType& Class;
    static const ULFeatureType& Contains;
    static const ULFeatureType& PresentType;
    static const ULFeatureType& PresentParticipleType;
    static const ULFeatureType& PastParticipleType;
    static const ULFeatureType& StartsWithVowel;
    static const ULFeatureType& AllUppercase;
    static const ULFeatureType& HasGrammarUsage;
    static const ULFeatureType& NotPartOfSpeechSubcategory;
    static const ULFeatureType& Syllables;
    static const ULFeatureType& SPAFinalVowelStressed;
    static const ULFeatureType& SPAFinalVowelUnstressed;
    static const ULFeatureType& ReflexiveInfinitive;
    static const ULFeatureType& NonreflexiveInfinitive;
    static const ULFeatureType& NonAspirated;
    static const ULFeatureType& BOXB;
    static const ULFeatureType& BOXC;
    static const ULFeatureType& BOXF;
    static const ULFeatureType& BOXL;
    static const ULFeatureType& BOXP;
    static const ULFeatureType& BOXR;
    static const ULFeatureType& BOXS;
    static const ULFeatureType& BOXT;
    static const ULFeatureType& BXRF;
    static const ULFeatureType& CAT1;
    static const ULFeatureType& CAT2;
    static const ULFeatureType& CCAF;
    static const ULFeatureType& CCCF;
    static const ULFeatureType& CCPD;
    static const ULFeatureType& CCXT;
    static const ULFeatureType& EXPN;
    static const ULFeatureType& GRAM;
    static const ULFeatureType& HCME;
    static const ULFeatureType& HDAD;
    static const ULFeatureType& HDCF;
    static const ULFeatureType& HDGR;
    static const ULFeatureType& HDHN;
    static const ULFeatureType& HDIF;
    static const ULFeatureType& HDSB;
    static const ULFeatureType& HWAD;
    static const ULFeatureType& HWAE;
    static const ULFeatureType& HWAF;
    static const ULFeatureType& HWCF;
    static const ULFeatureType& HWCP;
    static const ULFeatureType& HWEX;
    static const ULFeatureType& HWFV;
    static const ULFeatureType& HWGN;
    static const ULFeatureType& HWGR;
    static const ULFeatureType& HWHN;
    static const ULFeatureType& HWIF;
    static const ULFeatureType& HWKE;
    static const ULFeatureType& HWLE;
    static const ULFeatureType& HWME;
    static const ULFeatureType& HWRF;
    static const ULFeatureType& HWRV;
    static const ULFeatureType& HWSB;
    static const ULFeatureType& HWSV;
    static const ULFeatureType& HWXP;
    static const ULFeatureType& HWXT;
    static const ULFeatureType& HWYU;
    static const ULFeatureType& IFGR;
    static const ULFeatureType& IFHN;
    static const ULFeatureType& LBCA;
    static const ULFeatureType& LBCC;
    static const ULFeatureType& LBCI;
    static const ULFeatureType& LBCN;
    static const ULFeatureType& LBCO;
    static const ULFeatureType& LBCS;
    static const ULFeatureType& LBCV;
    static const ULFeatureType& LBFF;
    static const ULFeatureType& LBGR;
    static const ULFeatureType& LBIN;
    static const ULFeatureType& LBLF;
    static const ULFeatureType& LBNC;
    static const ULFeatureType& LBPO;
    static const ULFeatureType& LBPS;
    static const ULFeatureType& LBRN;
    static const ULFeatureType& LBRR;
    static const ULFeatureType& LBSF;
    static const ULFeatureType& LBSN;
    static const ULFeatureType& LBST;
    static const ULFeatureType& LBTM;
    static const ULFeatureType& LBXX;
    static const ULFeatureType& LEST;
    static const ULFeatureType& LLEX;
    static const ULFeatureType& LLXT;
    static const ULFeatureType& LXIN;
    static const ULFeatureType& LXRN;
    static const ULFeatureType& LXRR;
    static const ULFeatureType& LXSF;
    static const ULFeatureType& OPPR;
    static const ULFeatureType& PINT;
    static const ULFeatureType& PINY;
    static const ULFeatureType& PHEG;
    static const ULFeatureType& PHEX;
    static const ULFeatureType& PHID;
    static const ULFeatureType& PHGR;
    static const ULFeatureType& PHPB;
    static const ULFeatureType& PHMU;
    static const ULFeatureType& PHPS;
    static const ULFeatureType& PHRS;
    static const ULFeatureType& PHXT;
    static const ULFeatureType& POCH;
    static const ULFeatureType& POSP;
    static const ULFeatureType& PRLB;
    static const ULFeatureType& PRPS;
    static const ULFeatureType& PRRN;
    static const ULFeatureType& PRON;
    static const ULFeatureType& RFGR;
    static const ULFeatureType& RFVB;
    static const ULFeatureType& RFXT;
    static const ULFeatureType& SUBE;
    static const ULFeatureType& TGGR;
    static const ULFeatureType& TLGR;
    static const ULFeatureType& TLIN;
    static const ULFeatureType& TLLF;
    static const ULFeatureType& TLRN;
    static const ULFeatureType& TLRR;
    static const ULFeatureType& TLSF;
    static const ULFeatureType& TLTM;
    static const ULFeatureType& TRAD;
    static const ULFeatureType& TRAN;
    static const ULFeatureType& TRCF;
    static const ULFeatureType& TRCP;
    static const ULFeatureType& TREG;
    static const ULFeatureType& TREQ;
    static const ULFeatureType& TREX;
    static const ULFeatureType& TRIF;
    static const ULFeatureType& TRGL;
    static const ULFeatureType& TRPB;
    static const ULFeatureType& TRPR;
    static const ULFeatureType& TRSB;
    static const ULFeatureType& TRXP;
    static const ULFeatureType& URL1;
    static const ULFeatureType& VERB;
    static const ULFeatureType& WOEG;
    static const ULFeatureType& XASE;
    static const ULFeatureType& XRBC;
    static const ULFeatureType& XRBL;
    static const ULFeatureType& XRCA;
    static const ULFeatureType& XRCP;
    static const ULFeatureType& XREQ;
    static const ULFeatureType& XRHN;
    static const ULFeatureType& XRLK;
    static const ULFeatureType& XROF;
    static const ULFeatureType& XRSA;
    static const ULFeatureType& XRSB;
    static const ULFeatureType& XRSE;
    static const ULFeatureType& XUSA;
    static const ULFeatureType& XUSE;
    static const ULFeatureType& YUEG;
    static const ULFeatureType& ULTerm;
    static const ULFeatureType& ULText;
    static const ULFeatureType& CXMLAttrDisplay;
    static const ULFeatureType& CXMLAttrEnding;
    static const ULFeatureType& CXMLAttrForm;
    static const ULFeatureType& CXMLAttrHomonym;
    static const ULFeatureType& CXMLAttrMain;
    static const ULFeatureType& CXMLAttrSense;
    static const ULFeatureType& CXMLAttrSortkey;
    static const ULFeatureType& CXMLAttrText;
    static const ULFeatureType& CXMLAttrType;
    static const ULFeatureType& CXMLBox;
    static const ULFeatureType& CXMLCatAltForm;
    static const ULFeatureType& CXMLCatBoxes;
    static const ULFeatureType& CXMLCatCompound;
    static const ULFeatureType& CXMLCatCompoundSem;
    static const ULFeatureType& CXMLCatCrossRef;
    static const ULFeatureType& CXMLCatEntry;
    static const ULFeatureType& CXMLCatEquiv;
    static const ULFeatureType& CXMLCatExceptions;
    static const ULFeatureType& CXMLCatGenericPhrase;
    static const ULFeatureType& CXMLCatGenPhraseSem;
    static const ULFeatureType& CXMLCatGrammar;
    static const ULFeatureType& CXMLCatIdiom;
    static const ULFeatureType& CXMLCatLexicalPlural;
    static const ULFeatureType& CXMLCatLexSemantic;
    static const ULFeatureType& CXMLCatLexSubSemantic;
    static const ULFeatureType& CXMLCatOf;
    static const ULFeatureType& CXMLCatOptPrep;
    static const ULFeatureType& CXMLCatPhrase;
    static const ULFeatureType& CXMLCatPhraseSem;
    static const ULFeatureType& CXMLCatSemantic;
    static const ULFeatureType& CXMLCatSubEntry;
    static const ULFeatureType& CXMLCatSubSemantic;
    static const ULFeatureType& CXMLCatSubSubSemantic;
    static const ULFeatureType& CXMLEntry;
    static const ULFeatureType& CXMLEquivSrc;
    static const ULFeatureType& CXMLEquivTrg;
    static const ULFeatureType& CXMLGrammar;
    static const ULFeatureType& CXMLInfl;
    static const ULFeatureType& CXMLLabel;
    static const ULFeatureType& CXMLLabelLitFig;
    static const ULFeatureType& CXMLLabelModifier;
    static const ULFeatureType& CXMLLabelOpen;
    static const ULFeatureType& CXMLLabelRegional;
    static const ULFeatureType& CXMLLabelRegister;
    static const ULFeatureType& CXMLLabelSubjectField;
    static const ULFeatureType& CXMLLabelTrademark;
    static const ULFeatureType& CXMLPattern;
    static const ULFeatureType& CXMLPosp;
    static const ULFeatureType& CXMLPron;
    static const ULFeatureType& CXMLSentence;
    static const ULFeatureType& CXMLSpelling;
    static const ULFeatureType& CXMLText;
    static const ULFeatureType& CXMLAttrCompoundHead;
    static const ULFeatureType& CXMLAttrHyph;
    static const ULFeatureType& CXMLAttrLevel;
    static const ULFeatureType& CXMLAttrMainHomonym;
    static const ULFeatureType& CXMLAttrSenses;
    static const ULFeatureType& CXMLCatBoxInternal;
    static const ULFeatureType& CXMLCatEtyms;
    static const ULFeatureType& CXMLCatExamples;
    static const ULFeatureType& CXMLCatRelated;
    static const ULFeatureType& CXMLCrossReference;
    static const ULFeatureType& CXMLEtym;
    static const ULFeatureType& CXMLLabelGrammar;
    static const ULFeatureType& CXMLLabelLanguage;
    static const ULFeatureType& CXMLCatPhraseBlock;
    static const ULFeatureType& CXMLCatPhraseSubSem;
    static const ULFeatureType& CXMLLabelHierarchy;
    static const ULFeatureType& CXMLLabelFreqBand;


    ULFeatureType();
    ULFeatureType(const ULFeatureType& other);
    ULFeatureType(int id);
    ULFeatureType(const char *otherStringID);
    virtual ~ULFeatureType();

    virtual void clear();
    ULFeatureType& operator=(const ULFeatureType& other);
    ULFeatureType& operator=(int otherID);
    ULFeatureType& operator=(const char *otherStringID);

    bool operator==(const ULFeatureType& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULFeatureType& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULFeatureType& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULFeatureType& getInstance(int id);



private:
    static int nEnumeratedValues;
    static ULVector<const ULFeatureType *> enumeratedValueVector;
    ULFeatureType(const char *stringID, const char *displayString);
};


#endif

