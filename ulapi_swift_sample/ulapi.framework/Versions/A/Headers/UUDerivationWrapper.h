//
//  UUDerivationWrapper.h
//  ulapi
//
//  Created by SHAUN REYNOLDS on 5/21/12.
//  Copyright (c) 2013 Ultralingua, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UUDissectionWrapper.h"
#import "ulderivation.h"

@interface UUDerivationWrapper : NSObject<NSCoding>

// Using dot notation with C++ types seems to lead to memory errors.
// So we'll just not delcare this as a property and do it ourselves.
//@property (nonatomic, assign) ULPartOfSpeech partOfSpeech;
- (const ULDerivation&)derivation;

- (id)initWithDissectionWrapper:(UUDissectionWrapper *)dissectionWrapper;
- (id)initWithDerivation:(const ULDerivation&)aDerivation;
- (id)initWithDerivationWrapper:(UUDerivationWrapper*)other;

- (void)adoptDataSource:(ULDictionaryDataSource*)dataSource;
- (void)unload;

@property(nonatomic, strong) UUDictionaryIteratorWrapper *iteratorWrapper;

@end
