/**
 * @file ulnumbertranslatorfrench.h
 * @brief The interface for the ULNumberTranslatorFrench class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORFRENCH_H
#define ULNUMBERTRANSLATORFRENCH_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorFrench
 * @brief ULNumberTranslatorFrench does number translation in French.
 */
class ULNumberTranslatorFrench : public ULNumberTranslator
{
public:
    ULNumberTranslatorFrench();
    ULNumberTranslatorFrench(const ULNumberTranslatorFrench& other);
    virtual ~ULNumberTranslatorFrench();

    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

