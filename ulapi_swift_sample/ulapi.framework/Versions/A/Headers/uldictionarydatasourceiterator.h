/**
 * @file uldictionarydatasourceiterator.h
 * @brief The interface for the ULDictionaryDataSourceIterator class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULDICTIONARYDATASOURCEITERATOR_H
#define ULDICTIONARYDATASOURCEITERATOR_H

#include "ullockable.h"
#include "ulcontainers.h"
#include "uldictionarynode.h"

class ULDictionaryDataSource;
class ULDictionaryIterator;

/**
 * @class ULDictionaryDataSourceIterator
 * @brief ULDictionaryDataSourceIterator is the abstract parent for the classes that
 * iterate over various ULDictionaryDataSource subclasses.
 *
 * Any ULDictionaryDataSourceIterator object iterates through a forest of trees
 * of ULDictionaryNode objects. The top-level nodes (i.e. the roots of the
 * trees in the forest) typically correspond to the "entries" in the dictionary
 * (like the major bold-faced words found in a paper dictionary--"aardvark", then
 * "abacus", etc.). The nodes in each tree will depend on the particular source
 * of the data and its idiosyncracies (e.g. Collins data will have different trees
 * than Ultralingua data).
 */
class ULDictionaryDataSourceIterator : public ULLockable
{
    friend class ULDictionaryIterator;

public:
    ULDictionaryDataSourceIterator();
    ULDictionaryDataSourceIterator(const ULDictionaryDataSourceIterator&);
    virtual ~ULDictionaryDataSourceIterator();

    virtual ULDictionaryDataSourceIterator& operator=(const ULDictionaryDataSourceIterator& other);
    virtual void clear();

    virtual ULDictionaryDataSource *getDataSource() const;
    virtual void setDataSource(ULDictionaryDataSource *source);

    // These are implemented in terms of the abstract methods below.
    virtual void operator++(int);
    virtual void operator--(int);
    virtual operator bool() const;


    // These are the methods that subclasses must implement.
    
    virtual uluint32 hash(uluint32 tableSize) const = 0;
    virtual bool operator==(const ULDictionaryDataSourceIterator& iterator) const = 0;
    
    /**
     * Dereferencing operator.
     * @return a const reference to the ULDictionaryNode object
     * to which this iterator points.
     * @pre This iterator is not pointing to the end of its range.
     * This condition holds if this->isAtEnd() is false or equivalently,
     * bool(*this) is true.
     */
    virtual const ULDictionaryNode& operator*() = 0;

    /**
     * Pre-incrementation operator. Moves this iterator to point to the
     * next node in the forest of nodes that constitute its range of
     * motion. If this iterator is already at the end of its range, it
     * stays there after operator++. Note that because neither returns a
     * value, this operator and the post-incremenation operator are identical.
     */
    virtual void operator++() = 0;

    /**
     * Pre-decrementation operator. Moves this iterator to point to the
     * previous node in the forest of nodes that constitute its range of
     * motion. If this iterator is already at the beginning of its range, it
     * stays there after operator--. Note that because neither returns a
     * value, this operator and the post-decremenation operator are identical.
     */
    virtual void operator--() = 0;

    /**
     * Moves this iterator to point to the nearest top-level node that
     * comes after the node this iterator currently points to. If no
     * such node exists, this iterator is moved to the end of its range.
     */
    virtual void toNextTopLevelNode() = 0;

    /**
     * Moves this iterator to point to the top-level node that is an
     * ancestor of the node to which this iterator currently points.
     * If this iterator currently points to a top-level node, it continues
     * to point to that node.
     */
    virtual void toCurrentTopLevelNode() = 0;

    /**
     * Moves this iterator to point to the top-level node of the tree
     * immediately to the left of the tree containing the node to
     * which this iterator currently points. If no such tree exists, this
     * iterator is moved to point to the current top-level node.
     */
    virtual void toPreviousTopLevelNode() = 0;

    /**
     * @return true if the node to which this iterator points has a parent node.
     */
    virtual bool hasParent() = 0;

    /**
     * Moves this iterator to the parent of the node to which this iterator
     * currently points. If no such node exists, this iterator is not changed.
     */
    virtual void toParent() = 0;

    /**
     * @return true if the node to which this iterator points has at least
     * one child.
     */
    virtual bool hasChildren() = 0;

    /**
     * Moves this iterator to the first child of the node to which this iterator
     * currently points. If no such node exists, this iterator is not changed.
     */
    virtual void toFirstChild() = 0;

    /**
     * @return true if the node to which this iterator points has a
     * right-hand sibling.
     */
    virtual bool hasNextSibling() = 0;

    /**
     * Moves this iterator to the right-hand sibling of the node to which this
     * iterator currently points. If no such node exists (i.e. this node is the
     * rightmost child of its parent node), this iterator is not changed.
     */
    virtual void toNextSibling() = 0;

    /**
     * @return true if the node to which this iterator points has a
     * left-hand sibling.
     */
    virtual bool hasPreviousSibling() = 0;

    /**
     * Moves this iterator to the left-hand sibling of the node to which this
     * iterator currently points. If no such node exists (i.e. this node is the
     * rightmost child of its parent node), this iterator is not changed.
     */
    virtual void toPreviousSibling() = 0;

    /**
     * @return true if the node to which this iterator points has a
     * a node to its right that is at the same level of the forest that makes
     * up the range of this iterator. Such nodes (which include siblings)
     * are known as "cousins".
     */
    virtual bool hasNextCousin() = 0;

    /**
     * Moves this iterator to the right-hand cousin of the node to which this
     * iterator currently points. If no such node exists (i.e. this node is the
     * rightmost node at its level in the forest that makes up the range of this
     * iterator), the iterator is moved to the end of its range.
     */
    virtual void toNextCousin() = 0;

    /**
     * @return true if the node to which this iterator points has a
     * a node to its left that is at the same level of the forest that makes
     * up the range of this iterator. Such nodes (which include siblings)
     * are known as "cousins".
     */
    virtual bool hasPreviousCousin() = 0;

    /**
     * Moves this iterator to the left-hand cousin of the node to which this
     * iterator currently points. If no such node exists (i.e. this node is the
     * leftmost node at its level in the forest that makes up the range of this
     * iterator), the iterator is not changed.
     */
    virtual void toPreviousCousin() = 0;

    /**
     * @return the 0-based level of the node to which this iterator
     * points. For example, the root node of one of the trees in the forest
     * that makes up the range of this iterator will have level 0, while its
     * children will have level 1, their children will have level 2, etc.
     * If this iterator is at the end of its range or is otherwise not pointing
     * at a node, getLevel returns -1
     */
    virtual int getLevel() = 0;

    /**
     * @return an integer indicating the type of the node to which this iterator points.
     * This can be implemented by dereferencing the iterator and calling getType on
     * the resulting node, but in some important cases, loading the complete node into
     * memory can be avoided.
     */
    virtual uluint32 getType() = 0;

    /**
     * @return true if this iterator is at the end of its range,
     * and thus does not point to a ULDictionaryNode.
     */
    virtual bool isAtEnd() const = 0;

    /**
     * @return true if this iterator is pointing to the earliest
     * ULDictionaryNode in this iterator's range of motion.
     */
    virtual bool isAtBeginning() const = 0;
    
    /**
     * @return true if this iterator is pointing to a node in the first
     * tree in this iterator's range of motion.
     */
    virtual bool isInFirstTree() const = 0;
    
    /**
     * @return true if this iterator is pointing to a node in the final
     * tree in this iterator's range of motion.
     */
    virtual bool isInLastTree() const = 0;
    
    /**
     * @return a pointer to a clone of this iterator. The clone has its
     * own dynamically allocated sub-structures copied from this iterator,
     * so the two iterators may be used completely independently.
     */
    virtual ULDictionaryDataSourceIterator *clone() const = 0;

    /**
     * Each dictionary iterator points first to an index entry, which includes
     * a key word/term plus a list of pointers to top-level nodes. The iterator
     * includes a pointer to the index entry, a pointer to one of the index entry's
     * top-level nodes, and finally a pointer to one of the descendant nodes of
     * the top-level nodes. For example, this iterator might point to the index
     * entry for "rake", and then the top-level node associated with the verb "rake"
     * (as opposed to the noun "rake"), and finally to the descendant node that
     * contains the Spanish translation "rastrear" (as opposed to the translation
     * "rastrillar"). In this example, the "index key" is "rake".
     *
     * @return the key contained in the index entry to which this iterator points.
     */
    virtual ULString getIndexKey() = 0;

    /**
     * As described in the description of getIndexKey, this iterator points first to
     * an index entry, which consists of an index key and a list of pointers to
     * top-level nodes. The rank returned by this method indicates the position within
     * the index entry of the top-level tree into which this iterator points.
     *
     * For example, suppose there are 3 top-level trees for the index key "fly"
     * (corresponding to the noun, verb, and adjective meanings of this word), and
     * our iterator points into the third tree. Then getIndexRank would return 2.
     *
     * @return the 0-based rank within its index entry of this iterator's top-level tree.
     */
    virtual uluint32 getIndexRank() = 0;

    /**
     * @return the language of the portion of data source into which
     * this iterator points. For example, if this iterator is pointing
     * to a node in the entry for the English word "goat" in an English-French
     * dictionary, then getLanguage() will return ULLanguage::English.
     */
    virtual const ULLanguage& getLanguage() const = 0;

    /**
     * @return the language of the portion of data source into which
     * this iterator points. For example, if this iterator is pointing
     * to a node in the entry for the English word "goat" in an English-French
     * dictionary, then getSearchLanguage() will return ULLanguage::English.
     */
    virtual const ULLanguage& getSearchLanguage() const = 0;

    /**
     * @return the language of the translations or definitions found in
     * the portion of data source into which this iterator points. For
     * example, if this iterator is pointing to a node in the entry
     * for the English word "goat" in an English-French dictionary,
     * then getTranslationLanguage() will return ULLanguage::French.
     */
    virtual const ULLanguage& getTranslationLanguage() const = 0;

protected:
    ULDictionaryDataSource *dataSource;
};

#endif

