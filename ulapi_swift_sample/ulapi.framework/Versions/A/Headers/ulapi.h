//
//  ulapi.h
//  ulapi
//
//  Created by Jeff Ondich on 12/3/13.
//  Copyright (c) 2013 Ultralingua, Inc. All rights reserved.
//

#ifndef ULAPI_H
#define ULAPI_H

#ifndef UL_USING_LEGACY_STRINGS
#define UL_USING_LEGACY_STRINGS
#endif

#ifndef UL_ANSI
#define UL_ANSI
#endif

#ifndef UL_HAS_IOSTREAMS
#define UL_HAS_IOSTREAMS
#endif

#ifdef DEBUG
#ifndef UL_DEBUG
#define UL_DEBUG
#endif
#endif


#include "ulapi-cpp.h"

#ifdef __OBJC__
#include "ulapi/NSString+ULAPI.h"
#include "ulapi/ULAPIWW+CPP.h"
#include "ulapi/ULAPIWW.h"
#include "ulapi/UUDerivationWrapper.h"
#include "ulapi/UUDictionaryIteratorWrapper.h"
#include "ulapi/UUDissectionWrapper.h"
#include "ulapi/UUPartOfSpeechWrapper.h"
#include "ulapi/UURendererWrapper.h"
#include "ulapi/UUTenseWrapper.h"
#endif

#endif
