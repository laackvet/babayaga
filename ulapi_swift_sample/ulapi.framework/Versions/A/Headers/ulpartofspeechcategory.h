/**
 * @file ulpartofspeechcategory.h
 * @brief The interface for the ULPartOfSpeechCategory class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULPARTOFSPEECHCATEGORY_H
#define ULPARTOFSPEECHCATEGORY_H

#include "ulenum.h"

/**
 * @class ULPartOfSpeechCategory
 * @brief An enhanced enumerated type used to represent the various
 * broad part of speech categories (e.g. verb, noun, adjective, etc.)
 * as opposed to the more specific parts of speech (e.g. transitive verb).
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULPartOfSpeechCategory : public ULEnum
{
public:
    static const ULPartOfSpeechCategory& None;
    static const ULPartOfSpeechCategory& Any;
    static const ULPartOfSpeechCategory& Nonverb;
    static const ULPartOfSpeechCategory& Unknown;
    static const ULPartOfSpeechCategory& AnyNominal;
    static const ULPartOfSpeechCategory& Article;
    static const ULPartOfSpeechCategory& Pronoun;
    static const ULPartOfSpeechCategory& Noun;
    static const ULPartOfSpeechCategory& Verb;
    static const ULPartOfSpeechCategory& Adjective;
    static const ULPartOfSpeechCategory& Adverb;
    static const ULPartOfSpeechCategory& Conjunction;
    static const ULPartOfSpeechCategory& Preposition;
    static const ULPartOfSpeechCategory& Numeral;
    static const ULPartOfSpeechCategory& Affix;
    static const ULPartOfSpeechCategory& Expression;
    static const ULPartOfSpeechCategory& Interjection;
    static const ULPartOfSpeechCategory& Modifier;


    ULPartOfSpeechCategory();
    ULPartOfSpeechCategory(const ULPartOfSpeechCategory& other);
    ULPartOfSpeechCategory(int id);
    ULPartOfSpeechCategory(const char *otherStringID);
    virtual ~ULPartOfSpeechCategory();

    virtual void clear();
    ULPartOfSpeechCategory& operator=(const ULPartOfSpeechCategory& other);
    ULPartOfSpeechCategory& operator=(int otherID);
    ULPartOfSpeechCategory& operator=(const char *otherStringID);

    bool operator==(const ULPartOfSpeechCategory& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULPartOfSpeechCategory& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULPartOfSpeechCategory& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULPartOfSpeechCategory& getInstance(int id);
    bool isKindOf(const ULPartOfSpeechCategory& other) const;
    static void getAllPartOfSpeechCategories(ULList<ULPartOfSpeechCategory>& categories);



private:
    static int nEnumeratedValues;
    static ULVector<const ULPartOfSpeechCategory *> enumeratedValueVector;
    ULPartOfSpeechCategory(const char *stringID, const char *displayString);
};


#endif

