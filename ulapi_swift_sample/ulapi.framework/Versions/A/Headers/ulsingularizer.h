/**
 * @file ulsingularizer.h
 * @brief The interface for the ULSingularizer class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULSINGULARIZER_H
#define ULSINGULARIZER_H

#include "ulworker.h"
#include "uldissector.h"

/**
 * @class ULSingularizer
 * @brief A simplified interface to the noun singularization services provided by ULStemmer.
 */
class ULSingularizer : public ULWorker
{
public:
    ULSingularizer();
    ULSingularizer(const ULSingularizer& other);
    virtual ~ULSingularizer();

    ULSingularizer& operator=(const ULSingularizer& other);
    void clear();

    // Accessors
    ULDissector *getDissector();
    void setDissector(ULDissector *newDissector);
    const ULLanguage& getLanguage() const;

    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The core services provided by the singularizer.
    ULError getSingular(const ULString& word, ULString& singular); 
    ULError getAllSingulars(const ULString& word, ULList<ULString>& singularList); 

private:
    ULDissector *dissector;
};

#endif
