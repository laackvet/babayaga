/**
 * @file ulinflectionrule.h
 * @brief The interface for the ULInflectionRule class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULINFLECTIONRULE_H
#define ULINFLECTIONRULE_H

#include "ulstring.h"
#include "ulfeature.h"
#include "ulpartofspeech.h"
#include "ulinflectionruletransform.h"

class ULDerivation;

/**
 * @class ULInflectionRule
 * @brief A ULInflectionRule object stores one of the rules stored in a
 * ULLanguageDataSource and used by ULDissector and ULInflector to perform
 * morphological analyses and syntheses.
 */
class ULInflectionRule
{
    friend class ULCFileLanguageDataSource;

public:
    ULInflectionRule();
    ULInflectionRule(const ULInflectionRule& other);
    ~ULInflectionRule();

    ULInflectionRule& operator=(const ULInflectionRule& other);
    bool operator==(const ULInflectionRule& other) const;
    bool operator!=(const ULInflectionRule& other) const;
    void clear();

    uluint32 getGroup() const;
    void setGroup(uluint32 newGroup);
    uluint32 getPriority() const;
    void setPriority(uluint32 newPriority);
    bool isLookupRequired() const;
    void setLookupRequired(bool required);
    bool isInitial() const;
    void setInitial(bool newIsInitial);
    bool isTerminal() const;
    void setTerminal(bool newIsTerminal);

    void addConstraint(const ULFeatureType& featureType, int value);
    void addConstraint(const ULFeature& constraint);
    void addStringConstraint(const ULFeatureType& featureType, const ULString& stringValue);
    bool hasConstraint(const ULFeatureType& featureType) const;
    bool getFeature(const ULFeatureType& featureType, ULFeature& constraint) const;
    const ULList<ULFeature>& getConstraintList() const;

    const ULString& getSourceAffix() const;
    void setSourceAffix(const ULString& newAffix);
    const ULString& getTargetAffix() const;
    void setTargetAffix(const ULString& newAffix);
    const ULPartOfSpeech& getSourcePartOfSpeech() const;
    void setSourcePartOfSpeech(const ULPartOfSpeech& newPartOfSpeech);
    const ULPartOfSpeech& getTargetPartOfSpeech() const;
    void setTargetPartOfSpeech(const ULPartOfSpeech& newPartOfSpeech);
    
    const ULList<uluint32>& getSuccessorOffsets() const;
    const ULList<uluint32>& getPredecessorOffsets() const;

    bool isFullWordRule() const;
    bool constraintsMatchForInflection(const ULDerivation& derivation) const;
    bool affixesMatchForInflection(const ULDerivation& derivation) const;
    bool affixesMatchForDissection(const ULDerivation& derivation) const;
    bool partsOfSpeechMatchForDissection(const ULDerivation& derivation) const;
    bool partsOfSpeechMatchForInflection(const ULDerivation& derivation) const;

    bool applyForInflection(ULDerivation& derivation) const;
    bool applyForDissection(ULDerivation& derivation) const;
    
    ULString toString() const;
    
    static bool satisfies(const ULPartOfSpeech& posp, const ULPartOfSpeech& desiredPOSP);
    static bool affixMatches(const ULString& affix, const ULString& s, const ULLanguage& language, bool ignoreCase=false, bool ignoreAccents=false);
    static void removeAffix(const ULString& affix, ULString& s);
    static bool applyAffix(const ULString& affix, const ULLanguage& language, ULString& s);
    
private:
    uluint32 group;
    uluint32 priority;
    bool lookupRequired;
    bool initial;
    bool terminal;
    ULList<ULFeature> constraints;
    ULList<ULInflectionRuleTransform> transforms;
    ULString sourceAffix;
    ULPartOfSpeech sourcePartOfSpeech;
    ULString targetAffix;
    ULPartOfSpeech targetPartOfSpeech;
    ULList<uluint32> successorOffsets;
    ULList<uluint32> predecessorOffsets;

    static bool charMatchesEscape(ulchar ch, ulchar escape, const ULLanguage& language);
    static uluint32 getSyllableCount(const ULString& s, const ULLanguage& language);
    bool applyTransformForInflection(const ULInflectionRuleTransform& transform, ULString& root, ULString& affix) const;
    bool applyTransformForDissection(const ULInflectionRuleTransform& transform, ULString& root, ULString& affix) const;
    
    enum {
        VerbClassPrefix = 253,
        VerbClassReflexive = 252,
        VerbClassToBe = 1,
        
        CyrillicSmallBe = 0x0431,
        CyrillicSmallVe = 0x0432,
        CyrillicSmallDe = 0x0434,
        CyrillicSmallIe = 0x0435,
        CyrillicSmallZhe = 0x0436,
        CyrillicSmallZe = 0x0437,
        CyrillicSmallI = 0x0438,
        CyrillicSmallShortI = 0x0439,
        CyrillicSmallEl = 0x043B,
        CyrillicSmallEm = 0x043C,
        CyrillicSmallPe = 0x043F,
        CyrillicSmallEr = 0x0440,
        CyrillicSmallEs = 0x0441,
        CyrillicSmallTe = 0x0442,
        CyrillicSmallU = 0x0443,
        CyrillicSmallChe = 0x0447,
        CyrillicSmallSha = 0x0448,
        CyrillicSmallShcha = 0x0449,
        CyrillicSmallSoftSign = 0x044C,
        CyrillicSmallYu = 0x044E,
        CyrillicSmallIo = 0x0451,
    };

};

#endif

