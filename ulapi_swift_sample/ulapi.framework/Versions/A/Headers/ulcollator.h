/**
 * @file ulcollator.h
 * @brief The interface for the ULCollator class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULCOLLATOR_H
#define ULCOLLATOR_H

#include "ulstring.h"
#include "ulcontainers.h"
#include "ullanguage.h"

#ifdef UL_USING_ICU
#include "unicode/coll.h"
#endif

/**
 * @class ULCollator
 * @brief A ULCollator object does string comparison appropriate to
 * a particular locale or other context.  ULCollator also maintains
 * a static pool of ULCollator objects for fast access.
 *
 * Problem: Every string comparison requires a collator (typically
 * an ICU Collator object, but different if ULString isn't based
 * on ICU), but string comparison is frequent, and instantiation
 * of collators is time-consuming.  Furthermore, string comparison
 * operations tend to be buried pretty deep in the call stack, so
 * it's hard to make the right collator available where it's needed
 * via parameter passing.
 *
 * Solution: Make the collators globally available via something
 * like ULCollator and its static collator pool.  The ICU implementation
 * of collators (which we use on all platforms for which it is
 * available) takes care of thread safety with respect to the collators.
 */
class ULCollator
{
public:
    ULCollator();
    ULCollator(const ULCollator& other);
    virtual ~ULCollator();

    const ULCollator& operator=(const ULCollator& other);

    /**
     * This method is used to delete collators in the python wrapper.
     */
    void dispose();
    
    /**
     * Comparison function that uses this collator to compare the
     * specified strings.
     *
     * @return -1 if a is strictly less than b according to
     * this collator, 0 if a and b are equal, and 1 if a is strictly
     * greater than b.
     * @param[in] a A string.
     * @param[in] b Another string.
     */
    virtual ulint32 compare(const ULString& a, const ULString& b);

    /**
     * Comparison function that uses this collator to compare the
     * specified strings, using appropriate collator strength to
     * account for case and accent sensitivity.  If you ignore
     * accents, case is also ignored.
     *
     * @return -1 if a is strictly less than b according to
     * this collator, 0 if a and b are equal, and 1 if a is strictly
     * greater than b.
     *
     * @param[in] a A string.
     * @param[in] b Another string.
     * @param[in] ignoreCase If true, the comparison is done case-insensitively.
     * @param[in] ignoreAccents If true, the comparison is done both
     * case- and accent-insensitively.
     */
    virtual ulint32 compare(const ULString& a, const ULString& b, bool ignoreCase, bool ignoreAccents);
    
    /**
     * @return the language with which this ULCollator is associated.
     */
    const ULLanguage& getLanguage() const;

    /**
     * Static method that retrieves a ULCollator object suitable
     * for use with the specified language.
     *
     * @return a pointer to the desired ULCollator, or NULL
     * if the system is unable to create the collator.
     * @param[in] language The language for which a collator is needed.
     */
    static ULCollator *getCollator(const ULLanguage& language);

    /**
     * Static method that retrieves a ULCollator object suitable
     * for use with the specified locale information.
     *
     * @return a pointer to a ULCollator suitable for collating
     * with respect to the specified language, country, and variant.
     * @param[in] language The lowercase two-letter or three-letter ISO 639
     * code for the desired language.
     * @param[in] country The (optional) uppercase two-letter ISO 3166 code
     * for the desired country.
     * @param[in] variant An (optional) uppercase vendor or browser-specific code.
     */
    static ULCollator *getCollator(const char *language,
                                   const char *country=0,
                                   const char *variant=0);

protected:
    ULLanguage language;
#ifdef UL_USING_ICU
    Collator *icuCollator;
#elif defined(UL_USING_COCOA_STRINGS)
    NSLocale *locale;
#elif defined(UL_USING_CFSTRING)
    CFLocaleRef locale;
#elif defined(UL_USING_LEGACY_STRINGS)
    // Legacy strings are the default.
#endif
};

#endif

