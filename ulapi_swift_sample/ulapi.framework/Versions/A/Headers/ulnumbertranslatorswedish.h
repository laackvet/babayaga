/**
 * @file ulnumbertranslatorswedish.h
 * @brief The interface for the ULNumberTranslatorSwedish class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULNUMBERTRANSLATORSWEDISH_H
#define ULNUMBERTRANSLATORSWEDISH_H

#include "ulnumbertranslator.h"

/**
 * @class ULNumberTranslatorSwedish
 * @brief ULNumberTranslatorSwedish does number translation in Swedish.
 */
class ULNumberTranslatorSwedish : public ULNumberTranslator
{
public:
    ULNumberTranslatorSwedish();
    ULNumberTranslatorSwedish(const ULNumberTranslatorSwedish& other);
    virtual ~ULNumberTranslatorSwedish();

    virtual ULLanguage getLanguage() const;
    
    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The number translator's core services.
    virtual ULError translateNumber(const ULString& digits, ULString& result);
};

#endif

