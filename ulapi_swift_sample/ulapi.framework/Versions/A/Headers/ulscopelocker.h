/**
 * @file ulscopelocker.h
 * @brief The interface for the ULScopeLocker class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULSCOPELOCKER_H
#define ULSCOPELOCKER_H

#include "ullock.h"

/**
 * @class ULScopeLocker
 * @brief ULScopeLocker objects are used to make operations atomic.
 * Just declare a ULScopeLocker object (with a suitable <code>ULLock *</code>
 * parameter) at the top of the function or other block you want made atomic,
 * and the constructor ULScopeLocker object will claim the lock, and its
 * destructor will release the lock whenever control exits the block.
 * See (http://www.cs.wustl.edu/~schmidt/PDF/ScopedLocking.pdf) for details
 * on the <em>Scoped Locking idiom</em> and related concepts..
 *
 * The UL_LOCKSCOPE macro provides a way of simplifying scope locking
 * via the ULLockable parent class.  See ULLockable for details.
 */
class ULScopeLocker
{
public:
    ULScopeLocker(ULLock *theLock);
    ~ULScopeLocker();

private:
    ULLock *lock;
};

#define UL_LOCKSCOPE ULScopeLocker _locker(this->getLock())

#endif

