/**
 * @file uljsondictionarydatasource.h
 * @brief The interface for the ULJSONDictionaryDataSource class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULJSONDICTIONARYDATASOURCE_H
#define ULJSONDICTIONARYDATASOURCE_H

#ifdef UL_USING_JSON
  
#include "uldictionary.h"
#include "uldictionarydatasource.h"
#include "ulpartofspeechcategory.h"
#include "ulsegmentedfile.h"
#include "json.h"

/**
 * @class ULJSONDictionaryDataSource
 * @brief ULJSONDictionaryDataSource implements the ULDictionaryDataSource
 * protocol (and thus both the ULDictionary and ULDataSource protocols)
 * by using ULAPI 8 .uld files.
 */
class ULJSONDictionaryDataSource : public ULDictionaryDataSource
{
    UL_TEST_FRIEND;
    friend class ULJSONDictionaryIterator;

public:
    ULJSONDictionaryDataSource();
    ULJSONDictionaryDataSource(const ULJSONDictionaryDataSource& other);
    virtual ~ULJSONDictionaryDataSource();

    ULJSONDictionaryDataSource& operator=(const ULJSONDictionaryDataSource& other);

    virtual bool operator==(const ULDictionaryDataSource& dataSource) const;
    virtual bool operator==(const ULJSONDictionaryDataSource& dataSource) const;

    // ULWorker operations.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& serviceList);

    // ULDataSource operations.
    virtual ULError attach(const ULString& dataSourceIdentifier);
    virtual ULError detach();
    virtual ULError load();
    virtual ULError close();
    virtual ULString getDataSourceIdentifier();
    virtual ULDataSourceVersion getVersion();

    // ULDictionary operations.
    virtual ULError begin(const ULLanguage& searchLanguage,
                          uluint32 indexID,
                          ULDictionaryIterator& iterator);

    virtual ULError end(const ULLanguage& searchLanguage,
                        uluint32 indexID,
                        ULDictionaryIterator& iterator);

    virtual ULError find(const ULString& s,
                         const ULLanguage& searchLanguage,
                         uluint32 indexID,
                         ULDictionaryIterator& iterator);

    // ULDictionaryDataSource operations.
    virtual ULDictionaryDescriptor getDictionaryDescriptor();
    virtual const ULLanguage& getFirstLanguage();
    virtual const ULLanguage& getSecondLanguage();
    virtual const ULLanguage& getOtherLanguage(const ULLanguage& language);
    virtual ULForestType getForestType();
    virtual ULForester *getForester();
    virtual void setForester(ULForester *newForester);
    virtual ULError getFeatureNameList(ULList<ULString>& featureNameList);
    virtual ULError getLocalizationRuleList(const ULLanguage& language, ULList<ULLocalizationRule>& localizationRulesList);
    virtual bool getULDAttribute(const ULString& key, ULString& value);

protected:
    ULString jsonData;
    Json::Value jsonRoot;
    uluint32 nNodes;
    ULLanguage firstLanguage;
    ULLanguage secondLanguage;
    ULForester *forester;
    ULString *featureNames;

    bool isOpen() const;
    void clear();
};

#endif // UL_USING_JSON

#endif

