//
//  ULAPI+CPP.h
//  ulapi
//
//  Created by Jeff Ondich on 11/7/12.
//
//

#import "ULAPIWW.h"
#import "ulfactory.h"

@interface ULAPIWW (CPP)

+ (ULFactory *)sharedFactory;
+ (ULLanguage)languageWithName:(NSString *)languageName;
+ (NSString *)languageNameWithLanguage:(ULLanguage)language;

@end
