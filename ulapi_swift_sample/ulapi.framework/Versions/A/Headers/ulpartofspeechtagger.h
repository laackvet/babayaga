/**
 * @file ulpartofspeechtagger.h
 * @brief The interface for the ULPartOfSpeechTagger class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULPARTOFSPEECHTAGGER_H
#define ULPARTOFSPEECHTAGGER_H

#include "ulworker.h"
#include "uldissector.h"

/**
 * @class ULTaggedWord
 * @brief Each ULTaggedWord object represents the list of possible interpretations
 * of a word from a sentence, ordered by decreasing likelihood. For example,
 * in the sentence "Alice drinks coffee", the most likely interpretation of the
 * word "drinks" would be as the third-person singular form of the verb "to drink".
 * The word "drinks" also has a less likely (in this sentence) interpretation as
 * the plural of the noun "drink". Thus, the list of tags corresponding to "drinks"
 * in the context of the sentence "Alice drinks coffee" would have two elements,
 * first the verb and second the noun.
 * 
 * ULTaggedWord objects are used by one form of ULPartOfSpeechTagger::getTags to
 * return detailed tagging results.
 */
class ULTaggedWord
{
public:
    friend class ULPartOfSpeechTagger;

    ULTaggedWord();
    ULTaggedWord(const ULTaggedWord& other);
    virtual ~ULTaggedWord();

    void operator=(const ULTaggedWord& other);
    bool operator==(const ULTaggedWord& other) const;
    bool operator<(const ULTaggedWord& other) const;

    const ULString& getSurfaceForm() const;
    const ULList<ULDerivation>& getTagList() const;

private:
    ULString surfaceForm;
    ULList<ULDerivation> tags;
    bool ordered;
};

/**
 * @class ULPartOfSpeechTagger
 * @brief A ULPartOfSpeechTagger marks the words in a sentence with
 * parts of speech. The tagger uses a combination of dictionary look-up
 * and rule-based ambiguity resolution to choose parts of speech for each
 * word in the sentence.
 */
class ULPartOfSpeechTagger : public ULWorker
{
public:
    ULPartOfSpeechTagger();
    ULPartOfSpeechTagger(const ULPartOfSpeechTagger& other);
    virtual ~ULPartOfSpeechTagger();

    ULPartOfSpeechTagger& operator=(const ULPartOfSpeechTagger& other);
    void clear();

    // Accessors.
    const ULLanguage& getLanguage() const;
    void setDissector(ULDissector *newDissector);
    ULDissector *getDissector();

    // ULWorker interfaces.
    virtual bool isServiceAvailable(const ULServiceDescriptor& service);
    virtual void getAvailableServices(ULList<ULServiceDescriptor>& services);

    // The tagger's core services.
    ULError getTags(const ULString& sentence, ULList<ULTaggedWord>& taggedWords);
    ULError getTags(const ULString& sentence, ULList<ULDerivation>& words);

private:
    ULDissector *dissector;

    void tokenize(const ULString& s, ULList<ULString>& tokenList);
    bool resolveAmbiguityWithRule(const ULTaggingRule& rule,
                                  ULList<ULTaggedWord>& taggedWords,
                                  uluint32& nAmbiguities);
    bool dissectionListHasMatchingCategory(const ULList<ULDerivation>& wordList,
                                           const ULPartOfSpeechCategory& category);
};

#endif

