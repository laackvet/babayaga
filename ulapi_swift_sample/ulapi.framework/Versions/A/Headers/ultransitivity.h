/**
 * @file ultransitivity.h
 * @brief The interface for the ULTransitivity class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULTRANSITIVITY_H
#define ULTRANSITIVITY_H

#include "ulenum.h"

/**
 * @class ULTransitivity
 * @brief An enhanced enumerated type used to represent the transitivity of verbs.
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULTransitivity : public ULEnum
{
public:
    static const ULTransitivity& None;
    static const ULTransitivity& Any;
    static const ULTransitivity& Transitive;
    static const ULTransitivity& Intransitive;
    static const ULTransitivity& Ditransitive;
    static const ULTransitivity& Ambitransitive;


    ULTransitivity();
    ULTransitivity(const ULTransitivity& other);
    ULTransitivity(int id);
    ULTransitivity(const char *otherStringID);
    virtual ~ULTransitivity();

    virtual void clear();
    ULTransitivity& operator=(const ULTransitivity& other);
    ULTransitivity& operator=(int otherID);
    ULTransitivity& operator=(const char *otherStringID);

    bool operator==(const ULTransitivity& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULTransitivity& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULTransitivity& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULTransitivity& getInstance(int id);



private:
    static int nEnumeratedValues;
    static ULVector<const ULTransitivity *> enumeratedValueVector;
    ULTransitivity(const char *stringID, const char *displayString);
};


#endif

