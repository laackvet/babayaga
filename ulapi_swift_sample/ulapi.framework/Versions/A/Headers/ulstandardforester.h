/**
 * @file ulstandardforester.h
 * @brief The interface for the ULStandardForester class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULSTANDARDFORESTER_H
#define ULSTANDARDFORESTER_H

#include "ulforester.h"

/**
 * @class ULStandardForester
 * @brief ULStandardForester is the ULForester child responsible for knowing the internal structure
 * of the node forests used in Ultralingua-owned data.
 */
class ULStandardForester : public ULForester
{
public:
    ULStandardForester();
    ULStandardForester(const ULStandardForester& other);
    virtual ~ULStandardForester();
    virtual ULStandardForester& operator=(const ULStandardForester& other);
    virtual void clear();


    // Services common to all ULForester children.
    virtual ULError getMatchingTerms(const ULString& searchString,
                                     const ULLanguage& searchLanguage,
                                     bool ignoreCase,
                                     bool ignoreAccents,
                                     ULList<ULDictionaryIterator>& iteratorList);

    virtual ULError getMatchingTerms(const ULString& searchString,
                                     const ULString& sortkeyString,
                                     const ULLanguage& searchLanguage,
                                     bool ignoreCase,
                                     bool ignoreAccents,
                                     ULList<ULDictionaryIterator>& iteratorList);

    virtual ULError getApproximateMatchingTerms(const ULString& searchString,
                                                const ULLanguage& searchLanguage,
                                                uluint32 requiredPrefixLength,
                                                ULList<ULDictionaryIterator>& approximateMatches);
    
    virtual ULError getApproximateMatchingTerms(const ULString& searchString,
                                                const ULLanguage& searchLanguage,
                                                ULList<ULString>& approximateMatches);
    
    virtual ULError toCurrentRenderingRoot(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual ULError toNextRenderingRoot(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual ULError toPreviousRenderingRoot(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual bool isAtFirstRenderingRoot(const ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual bool isAtLastRenderingRoot(const ULDictionaryDataSourceIterator& dictionaryIterator);

    virtual ULError toCurrentTerm(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual ULError toNextTerm(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual ULError toPreviousTerm(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual bool isAtTerm(const ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual bool isAtFirstTerm(const ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual bool isAtLastTerm(const ULDictionaryDataSourceIterator& dictionaryIterator);

    virtual ULError toCurrentDefinition(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual ULError toNextDefinition(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual ULError toPreviousDefinition(ULDictionaryDataSourceIterator& dictionaryIterator);

    virtual ULString getText(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual ULError getPartOfSpeech(ULDictionaryDataSourceIterator& dictionaryIterator, ULPartOfSpeech& partOfSpeech);
    virtual ULError getExample(ULDictionaryDataSourceIterator& dictionaryIterator, ULExample& example);
    virtual ULError getExamples(ULDictionaryDataSourceIterator& dictionaryIterator, ULList< ULExample > &exampleList);    
    virtual void getWord(ULDictionaryDataSourceIterator& dictionaryIterator, ULDerivation& word);
    
    virtual bool hasPartOfSpeechCategory(ULDictionaryDataSourceIterator& dictionaryIterator, const ULPartOfSpeechCategory& category);
    virtual bool getWordWithPartOfSpeechCategory(ULDictionaryDataSourceIterator& dictionaryIterator, const ULPartOfSpeechCategory& category, ULDerivation& word);
    virtual void getTranslationsForPartOfSpeechCategory(ULDictionaryDataSourceIterator& dictionaryIterator,
                                                        const ULPartOfSpeechCategory& category,
                                                        ULList<ULString>& translations);
    
    virtual ULError getPronunciation(ULDictionaryDataSourceIterator& dictionaryIterator, ULString &pronunciation);
    virtual ULError getSemanticClarification(ULDictionaryDataSourceIterator& dictionaryIterator, const ULLanguage& language, ULString& clarification);
    virtual ULError getSemanticHomonym(ULDictionaryDataSourceIterator& dictionaryIterator, const ULLanguage& language, ULString& homonym);
    virtual ULError getSemanticHomonyms(ULDictionaryDataSourceIterator& dictionaryIterator, const ULLanguage& language, ULList< ULString > &homonymList);
    virtual ULError getSemanticSynonym(ULDictionaryDataSourceIterator& dictionaryIterator, const ULLanguage& language, ULString& synonym);
    virtual ULError getSemanticSynonyms(ULDictionaryDataSourceIterator& dictionaryIterator, const ULLanguage& language, ULList< ULString > &synonymList);
    virtual ULError getSemanticAntonym(ULDictionaryDataSourceIterator& dictionaryIterator, const ULLanguage& language, ULString& antonym);
    virtual ULError getSemanticAntonyms(ULDictionaryDataSourceIterator& dictionaryIterator, const ULLanguage& language, ULList< ULString > &antonymList);
    virtual ULError getSocioTone(ULDictionaryDataSourceIterator& dictionaryIterator, const ULLanguage& language, ULString& tone);
    virtual ULError getSocioDialect(ULDictionaryDataSourceIterator& dictionaryIterator, const ULLanguage& language, ULString& dialect);
    virtual ULError getOntology(ULDictionaryDataSourceIterator& dictionaryIterator, const ULLanguage& language, ULString& ontology);
    virtual ULError getUsage(ULDictionaryDataSourceIterator& dictionaryIterator, ULString& usage);
    virtual ULError getAlternateSpelling(ULDictionaryDataSourceIterator& dictionaryIterator, ULString& altSpelling);
    virtual ULError getAlternateSpellings(ULDictionaryDataSourceIterator& dictionaryIterator, ULList< ULString > &altSpellingList);
    virtual ULError getGrammarUsage(ULDictionaryDataSourceIterator& dictionaryIterator, ULString& grammarUsage);
    virtual ULError getEtymology(ULDictionaryDataSourceIterator& dictionaryIterator, ULString& etymology);
    virtual ULError getDerivation(ULDictionaryDataSourceIterator& dictionaryIterator, ULString& derivation);
    virtual ULError getSeeAlso(ULDictionaryDataSourceIterator& dictionaryIterator, ULString& seeAlso);
    virtual ULError getSeeAlsos(ULDictionaryDataSourceIterator& dictionaryIterator, ULList< ULString > &seeAlsoList);
    virtual ULError getTranslation(ULDictionaryDataSourceIterator& dictionaryIterator, ULString& translation);
    virtual ULError getFirstPrincipalPart(ULDictionaryDataSourceIterator& dictionaryIterator, ULString& firstPrincipalPart);

    // Part of Speech localization methods
    virtual ULError getPartOfSpeechLocalization(const ULPartOfSpeech& posp, const ULLanguage& language, ULString& localization);
    virtual ULError getPartOfSpeechLocalizedAbbreviation(const ULPartOfSpeech& posp, const ULLanguage& language, ULString& abbreviation);
    virtual ULError getLocalizationForFeatureValue(const ULString& featureValue, const ULLanguage& language, ULString& localization);
    virtual ULError getLocalizedAbbreviationForFeatureValue(const ULString& featureValue, const ULLanguage& language, ULString& abbreviation);

    virtual void freeInessentialMemory();
    
private:
    uluint32 definitionDataSourceID;

    // Support for getApproximateMatchingTerms.
    uluint32 getEditDistance(ULString& s, ULString& t);
    enum { MaxHeadwordLength = 16, InsertionCost = 1, DeletionCost = 1, SubstitutionCost = 2, MaxAcceptableEditDistance = 2 };
    uluint32 distanceTable[MaxHeadwordLength*MaxHeadwordLength];
    void addAppoximationIfFound(const ULString& candidate, const ULLanguage& searchLanguage, ULCollator *collator, ULList<ULString>& matches);

    // Private methods and variables to support localization
    // The outside ULHashTable maps from three letter ISO codes and a localization rule list
    // while the inside one maps feature values to specific localization rules.
    ULHashTable<ULString, ULHashTable<ULString, ULLocalizationRule> > languageToLocalizationsMap;
    void initializeLocalizationMaps();
    ULHashTable<ULLanguage, ULHashTable<ULPartOfSpeech, ULString> > partOfSpeechLocalizationCache;
    ULHashTable<ULLanguage, ULHashTable<ULPartOfSpeech, ULString> > partOfSpeechLocalizedAbbreviationCache;
    ULHashTable<ULLanguage, ULHashTable<ULString, ULString> > featureValueLocalizationCache;
    ULHashTable<ULLanguage, ULHashTable<ULString, ULString> > featureValueLocalizedAbbreviationCache;
    void initializeLocalizationCaches();
};

#endif

