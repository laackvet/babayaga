/**
 * @file ulcollinstaggedforester.h
 * @brief The interface for the ULCollinsTaggedForester class.
 * @author Copyright (C) 2013 Ultralingua, Inc.
 */

#ifndef ULCOLLINSTAGGEDFORESTER_H
#define ULCOLLINSTAGGEDFORESTER_H

#include "ulforester.h"

/**
 * @class ULCollinsTaggedForester
 * @brief ULCollinsTaggedForester is the ULForester child responsible for knowing the internal structure
 * of the node forests used in Collins tagged datasets.
 */
class ULCollinsTaggedForester : public ULForester
{
public:
    ULCollinsTaggedForester();
    ULCollinsTaggedForester(const ULCollinsTaggedForester& other);
    virtual ~ULCollinsTaggedForester();
    virtual ULCollinsTaggedForester& operator=(const ULCollinsTaggedForester& other);
    virtual void clear();


    // Services common to all ULForester children.
    virtual ULError getMatchingTerms(const ULString& searchString,
                                     const ULLanguage& searchLanguage,
                                     bool ignoreCase,
                                     bool ignoreAccents,
                                     ULList<ULDictionaryIterator>& iteratorList);

    virtual ULError getMatchingTerms(const ULString& searchString,
                                     const ULString& sortkeyString,
                                     const ULLanguage& searchLanguage,
                                     bool ignoreCase,
                                     bool ignoreAccents,
                                     ULList<ULDictionaryIterator>& iteratorList);

    virtual ULError getApproximateMatchingTerms(const ULString& searchString,
                                                const ULLanguage& searchLanguage,
                                                uluint32 requiredPrefixLength,
                                                ULList<ULDictionaryIterator>& approximateMatches);
    
    virtual ULError getApproximateMatchingTerms(const ULString& searchString,
                                                const ULLanguage& searchLanguage,
                                                ULList<ULString>& approximateMatches);
    
    virtual ULError toCurrentRenderingRoot(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual ULError toNextRenderingRoot(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual ULError toPreviousRenderingRoot(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual bool isAtFirstRenderingRoot(const ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual bool isAtLastRenderingRoot(const ULDictionaryDataSourceIterator& dictionaryIterator);

    virtual ULError toCurrentTerm(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual ULError toNextTerm(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual ULError toPreviousTerm(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual bool isAtTerm(const ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual bool isAtFirstTerm(const ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual bool isAtLastTerm(const ULDictionaryDataSourceIterator& dictionaryIterator);

    virtual ULError toCurrentDefinition(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual ULError toNextDefinition(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual ULError toPreviousDefinition(ULDictionaryDataSourceIterator& dictionaryIterator);

    virtual ULString getText(ULDictionaryDataSourceIterator& dictionaryIterator);
    virtual ULError getPartOfSpeech(ULDictionaryDataSourceIterator& dictionaryIterator, ULPartOfSpeech& partOfSpeech);
    virtual void getWord(ULDictionaryDataSourceIterator& dictionaryIterator, ULDerivation& word);
    virtual bool hasPartOfSpeechCategory(ULDictionaryDataSourceIterator& dictionaryIterator, const ULPartOfSpeechCategory& category);
    virtual bool getWordWithPartOfSpeechCategory(ULDictionaryDataSourceIterator& dictionaryIterator, const ULPartOfSpeechCategory& category, ULDerivation& word);
    virtual void getTranslationsForPartOfSpeechCategory(ULDictionaryDataSourceIterator& dictionaryIterator,
                                                        const ULPartOfSpeechCategory& category,
                                                        ULList<ULString>& translations);
 
    // Part of Speech localization methods
    virtual ULError getPartOfSpeechLocalization(const ULPartOfSpeech& posp, const ULLanguage& language, ULString& localization);
    virtual ULError getPartOfSpeechLocalizedAbbreviation(const ULPartOfSpeech& posp, const ULLanguage& language, ULString& abbreviation);
    virtual ULError getLocalizationForFeatureValue(const ULString& featureValue, const ULLanguage& language, ULString& localization);
    virtual ULError getLocalizedAbbreviationForFeatureValue(const ULString& featureValue, const ULLanguage& language, ULString& abbreviation);

    virtual void freeInessentialMemory();
    
private:
    uluint32 definitionDataSourceID;

    // Support for getApproximateMatchingTerms.
    uluint32 getEditDistance(ULString& s, ULString& t);
    enum { MaxHeadwordLength = 16, InsertionCost = 1, DeletionCost = 1, SubstitutionCost = 2, MaxAcceptableEditDistance = 2 };
    uluint32 distanceTable[MaxHeadwordLength*MaxHeadwordLength];
    void addAppoximationIfFound(const ULString& candidate, const ULLanguage& searchLanguage, ULCollator *collator, ULList<ULString>& matches);

    // Private methods and variables to support localization
    // The outside ULHashTable maps from three letter ISO codes and a localization rule list
    // while the inside one maps feature values to specific localization rules.
    ULHashTable<ULString, ULHashTable<ULString, ULLocalizationRule> > languageToLocalizationsMap;
    void initializeLocalizationMaps();
    ULHashTable<ULLanguage, ULHashTable<ULPartOfSpeech, ULString> > partOfSpeechLocalizationCache;
    ULHashTable<ULLanguage, ULHashTable<ULPartOfSpeech, ULString> > partOfSpeechLocalizedAbbreviationCache;
    ULHashTable<ULLanguage, ULHashTable<ULString, ULString> > featureValueLocalizationCache;
    ULHashTable<ULLanguage, ULHashTable<ULString, ULString> > featureValueLocalizedAbbreviationCache;
    void initializeLocalizationCaches();
};

#endif

