/**
 * @file ulrenderer.h
 * @brief The interface for the ULRenderer class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULRENDERER_H
#define ULRENDERER_H

#include "ulerror.h"
#include "ulstring.h"
#include "uldictionaryiterator.h"

/**
 * @class ULRenderer
 * @brief ULRenderer is the abstract parent for classes that render dictionary
 * data into string form.
 */
class ULRenderer
{
public:
    ULRenderer();
    ULRenderer(const ULRenderer& renderer);
    virtual ~ULRenderer();

    virtual ULRenderer& operator=(const ULRenderer& renderer);
    virtual void clear();

    // Required subclass interfaces.

    /**
     * Generates a string representation of the ULDictionaryNode or the
     * tree of ULDictionaryNodes to which the specified iterator points.
     * Each subclass of ULRenderer implements its own render method
     * suitable for whatever context the subclass was written for.
     *
     * @return an error code
     * @param[in] iterator An iterator pointing to the data whose
     * rendering is desired.
     * @param[out] result The resulting rendered string.
     */
    virtual ULError render(const ULDictionaryIterator& iterator, ULString& result) const = 0;

    // Static methods.
    static ULError makeQuotationMarksCurly(ULString& text);
    static ULError makeQuotationMarksStraight(ULString& text);
    static ULError removeParenthesizedText(ULString& text);
    static ULError makeNonBreaking(ULString& string);
};

#endif

