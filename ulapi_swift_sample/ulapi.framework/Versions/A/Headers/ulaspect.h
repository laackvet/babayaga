/**
 * @file ulaspect.h
 * @brief The interface for the ULAspect class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 *
 * THIS FILE IS AUTO-GENERATED. DON'T EDIT IT DIRECTLY. EDIT IT VIA
 * THE TEMPLATES IN source/ulenumtemplates.
 */

#ifndef ULASPECT_H
#define ULASPECT_H

#include "ulenum.h"

/**
 * @class ULAspect
 * @brief An enhanced enumerated type used to represent the aspect of verbs.
 * 
 * See ULEnum for a discussion of enhanced enumerated types in ULAPI.
 */
class ULAspect : public ULEnum
{
public:
    static const ULAspect& None;
    static const ULAspect& Any;
    static const ULAspect& Simple;
    static const ULAspect& Perfect;
    static const ULAspect& Progressive;
    static const ULAspect& Perfective;
    static const ULAspect& Imperfective;
    static const ULAspect& Inchoative;


    ULAspect();
    ULAspect(const ULAspect& other);
    ULAspect(int id);
    ULAspect(const char *otherStringID);
    virtual ~ULAspect();

    virtual void clear();
    ULAspect& operator=(const ULAspect& other);
    ULAspect& operator=(int otherID);
    ULAspect& operator=(const char *otherStringID);

    bool operator==(const ULAspect& other) const;
    bool operator==(const char *otherStringID) const;
    bool operator!=(const ULAspect& other) const;
    bool operator!=(const char *otherStringID) const;
    bool operator<(const ULAspect& other) const;
    bool operator<(const char *otherStringID) const;

    static int getEnumeratedValueCount();
    static const ULAspect& getInstance(int id);



private:
    static int nEnumeratedValues;
    static ULVector<const ULAspect *> enumeratedValueVector;
    ULAspect(const char *stringID, const char *displayString);
};


#endif

