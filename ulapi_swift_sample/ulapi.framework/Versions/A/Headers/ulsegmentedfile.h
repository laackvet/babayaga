/**
 * @file ulsegmentedfile.h
 * @brief The interface for the ULSegmentedFile class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULSEGMENTEDFILE_H
#define ULSEGMENTEDFILE_H

#include "ulfile.h"
#include "ulcompressor.h"
#include "ulsegment.h"

/**
 * @class ULSegmentProperties
 * @brief A utility class used by ULSegmentedFile.
 */
struct ULSegmentProperties
{
    // Which segment?
    int index;
    const char *name;

    // Properties.
    uluint32 type;
    uluint32 cacheSize;
    uluint32 blockSize;
    bool usingCompression;
};

/**
 * @class ULSegmentDescriptor
 * @brief A utility class used by ULSegmentedFile.
 */
struct ULSegmentDescriptor
{
    uluint32 mStart;
    uluint32 mUncompressedSize;
    uluint32 mCompressedSize;
    uluint32 mBlockSize;
    bool mUsingCompression;
};

class ULSegment;

/**
 * @class ULSegmentedFile
 * @brief This class describes a file that is broken into
 * segments.
 * 
 * A segmented file begins with a segment header, followed
 * by the segments themselves.  The segment header looks
 * like this (sixteen bytes per row, all integers stored
 * in big-endian byte order):
 *
 * <blockquote>
 * 0x05E9F17E | [# segments] | 0x00000000 | 0x00000000<br />
 * [starting offset] | [uncompressed size] | [compressed size] | [block size]<br />
 * (one row per segment)
 * </blockquote>
 *
 * The "starting offset" is the byte offset within the file at which
 * the segment begins (note that this offset includes the segmentation
 * header, so the first segment's starting offset will be 32 for a one-segment
 * file, 48 for a two-segment file, etc.).  Compressed segments are
 * compressed one block at a time.  The left-most bit of the "block size"
 * field determines whether the segment uses compression (1 = compressed,
 * 0 = not compressed).  The rest of the block size field
 * (i.e. (block size) & 0x7FFFFFFF) is the uncompressed size of the blocks.
 *
 * An uncompressed segment is just data, starting at the "starting offset"
 * within the file, and consisting of the "uncompressed size" number of
 * bytes.
 *
 * A compressed segment, on the other hand, consists of a "block map"
 * followed by a sequence of compressed blocks.  Consider a compressed
 * segment before compression.  It has "uncompressed size" bytes, which
 * we divide into blocks of a fixed size ((block size) & 0x7FFFFFFF).
 * We compress these blocks, yielding a sequence of compressed blocks
 * of various sizes.  If we want to be able to extract these blocks,
 * we'll need to know how big they are.  Thus, each compressed segment
 * begins with a block map, which is a sequence of 4-byte big endian
 * integers pointing to the offset within the segment at which each
 * successive block begins.  Suppose, for example, that the first three
 * blocks have compressed sizes of 850, 650, and 750.  Then the block
 * map for this segment will begin with the integers 0, 850, 1500, and
 * 2250 for the first four blocks.
 */
class ULSegmentedFile
{
public:
    ULSegmentedFile();
    ULSegmentedFile(ULCompressor *compressor);
    ~ULSegmentedFile();

    enum { MagicNumber = 0x05E9F17E };

    void setCompressor(ULCompressor *compressor);
    ULCompressor *getCompressor();

    bool open(const ULString& fileID,
              uluint32 openMode,
              ULSegmentProperties *segmentProperties,
              uluint32 defaultSegmentType,
              uluint32 defaultCacheSize);

    void close();
    bool isOpen() const;
    ULString getHash();

    uluint8 getByte(uluint32 segmentIndex, uluint32 offset);
    bool getULChar(uluint32 segmentIndex, uluint32& offset, uluint32 limit, ulchar& ch);
    uluint16 getInt16(uluint32 segmentIndex, uluint32 offset);
    uluint32 getInt24(uluint32 segmentIndex, uluint32 offset);
    uluint32 getInt32(uluint32 segmentIndex, uluint32 offset);
    uluint32 getBase255Int8(uluint32 segmentIndex, uluint32 offset);
    uluint32 getBase255Int16(uluint32 segmentIndex, uluint32 offset);
    uluint32 getBase255Int24(uluint32 segmentIndex, uluint32 offset);
    uluint32 getBase255Int32(uluint32 segmentIndex, uluint32 offset);
    void getString(ULString& s,
                   uluint32 segmentIndex,
                   uluint32& offset,
                   ulchar delimiter=0);

    uluint32 getNSegments() const;
    bool hasNamedSegments() const;
    const char *getSegmentName(uluint32 segmentIndex) const;
    uluint32 getSegmentIndexForName(const char *name) const;
    uluint32 getSegmentSize(uluint32 segmentIndex);
    uluint32 getCompressedSegmentSize(uluint32 segmentIndex);
    uluint32 getSegmentBlockSize(uluint32 segmentIndex);
    bool isCompressed(uluint32 segmentIndex);

    void putByte(uluint32 segmentIndex, uluint32& offset, uluint8 byte);
    void putInt16(uluint32 segmentIndex, uluint32& offset, uluint16 n);
    void putInt24(uluint32 segmentIndex, uluint32& offset, uluint32 n);
    void putInt32(uluint32 segmentIndex, uluint32& offset, uluint32 n);
    void putString(uluint32 segmentIndex, uluint32& offset, const ULString& s);
    void putInt32(uluint32 n);
    static void putInt32(ULFile *out, uluint32 n);
    void setCompression(uluint32 segmentIndex, bool useCompression);
    uluint32 seek(ulint32 offset, int origin=ULFile::Beginning);
    void flush();

    bool writeUncompressed(const ULString& fileID);

private:
    ULFile *file;
    ULCompressor *compressor;
    ULSegment **segments;
    uluint8 **segmentNames;
    uluint32 nSegments;

    void flush(uluint32 segmentIndex, const uluint8 *name);
};

#endif

