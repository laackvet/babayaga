/**
 * @file ulconsolerenderer.h
 * @brief The interface for the ULConsoleRenderer class.
 * @author Copyright (C) 2011 Ultralingua, Inc.
 */

#ifndef ULCONSOLERENDERER_H
#define ULCONSOLERENDERER_H

#include "ulrenderer.h"

/**
 * @class ULConsoleRenderer
 * @brief The ULRenderer child that renders dictionary data for display in
 * a text-only output environment like a terminal window.
 */
class ULConsoleRenderer : public ULRenderer
{
public:
    ULConsoleRenderer();
    ULConsoleRenderer(const ULConsoleRenderer& renderer);
    virtual ~ULConsoleRenderer();

    virtual ULConsoleRenderer& operator=(const ULConsoleRenderer& renderer);
    virtual void clear();

    // Required ULRenderer interfaces.
    virtual ULError render(const ULDictionaryIterator& iterator, ULString& result) const;
};

#endif

