//
//  ULAPI.h
//  Reader
//
//  Created by Jeffrey Ondich on 12/6/14.
//  Copyright (c) 2014 Ultralingua, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

// ULPartOfSpeech wrapper
@interface ULAPIPartOfSpeech : NSObject
@property (readonly) NSString *partOfSpeechCategory;
@property (readonly) NSDictionary *features;
@property (readonly) NSString *tense;
@property (readonly) NSString *person;
@property (readonly) NSString *number;
@property (readonly) NSString *aspect;
@end

// ULDerivation wrapper
@interface ULAPIDerivation : NSObject
@property (readonly) NSString *root;
@property (readonly) ULAPIPartOfSpeech *rootPartOfSpeech;
@property (readonly) NSDictionary *rootFeatures;
@property (readonly) NSString *derivedForm;
@property (readonly) ULAPIPartOfSpeech *derivedFormPartOfSpeech;
@end

// ULAPI
@interface ULAPI : NSObject

+ (ULAPI *)sharedULAPI;
- (id)init;
- (BOOL)addDictionaryDataSource:(NSString *)dataSourceFilePath;
- (BOOL)addLanguageDataSource:(NSString *)dataSourceFilePath;
- (NSString *)translateNumber:(NSString *)digits language:(NSString *)language;
- (NSArray *)stems:(NSString *)word language:(NSString *)language;
- (NSArray *)conjugations:(NSString *)word language:(NSString *)language; // Returns an array of ULAPIDerivation objects
- (NSString *)displayTextForConjugation:(ULAPIDerivation *)conjugation;
- (NSString *)tenseName:(NSString *)tense language:(NSString *)language;

@end


