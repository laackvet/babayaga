//
//  ViewController.swift
//  ulapi_swift_sample
//
//  Created by Jeffrey Ondich on 4/30/15.
//  Copyright (c) 2015 Jeff Ondich. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textField: UITextField?
    @IBOutlet weak var goButton: UIButton?
    @IBOutlet weak var conjugationTextView: UITextView?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func goButtonHandler() {
        let ulapi : ULAPI = ULAPI.sharedULAPI()

        // 
        var language = "ru"
        var verbForm = "перезвони́ть"
        var conjugationList = ulapi.conjugations(verbForm, language: language)
        var currentTense = ""
        var fullConjugationText = ""
        for item in conjugationList {
            if let conjugation = item as? ULAPIDerivation {
                // If the current conjugation is in a new tense, print the tense
                // name as a heading.
                let tense = conjugation.derivedFormPartOfSpeech.tense
                if tense == "present"{println("HASPRESENTESNDIGNFDNG")}
                if tense != currentTense {
                    currentTense = tense
                    let tenseName = ulapi.tenseName(tense, language: language)
                    if count(fullConjugationText) > 0 {
                        fullConjugationText += "\n"
                    }
                    fullConjugationText += "====== \(tenseName) ======\n"
                }
                fullConjugationText += ulapi.displayTextForConjugation(conjugation)
                fullConjugationText += "\n"

                // Want to get at other pieces of the derivation? This code is not necessary for
                // this demo application, but you can do stuff like this.
                let person = conjugation.derivedFormPartOfSpeech.person
                let number = conjugation.derivedFormPartOfSpeech.number
                println("\(conjugation.derivedForm) (\(person)/\(number)/\(tense))")
            }
        }
        
        self.conjugationTextView?.text = fullConjugationText
        println(fullConjugationText)

    }
}

