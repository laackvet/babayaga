//
//  ULAPI.mm
//  Reader
//
//  Created by Jeffrey Ondich on 12/6/14.
//  Copyright (c) 2014 Ultralingua, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ULAPI.h"
#import "ULAPI+CPP.h"

@interface ULAPI() {
    ULFactory *factory;
    NSMutableDictionary *stemmers;
    NSMutableDictionary *conjugators;
}
@end

@implementation ULAPI

+ (ULAPI *)sharedULAPI {
    static dispatch_once_t once;
    static ULAPI *sharedInstance;
    dispatch_once(&once, ^{ sharedInstance = [[ULAPI alloc] init]; });
    return sharedInstance;
}

- (id)init {
    if (self) {
        self->factory = ULFactory::createFactory("default");
        self->stemmers = [[NSMutableDictionary alloc] init];
        self->conjugators = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (BOOL)addDictionaryDataSource:(NSString *)dataSourceFilePath {
    ULError error = self->factory->addDictionaryDataSource([dataSourceFilePath ULString]);
    return (error == ULError::NoError ? YES : NO);
}

- (BOOL)addLanguageDataSource:(NSString *)dataSourceFilePath {
    ULError error = self->factory->addLanguageDataSource([dataSourceFilePath ULString]);
    return (error == ULError::NoError ? YES : NO);
}

- (NSString *)translateNumber:(NSString *)digits language:(NSString *)language {
    ULString result;
    ULLanguage ulLanguage([language ULString]);
    ULNumberTranslator *translator = self->factory->getNumberTranslator(ulLanguage);
    translator->translateNumber([digits ULString], result);
    return [NSString stringWithULString:result];
}

- (ULStemmer *)stemmerForLanguage:(NSString *)language {
    // Get the cached stemmer if it's there.
    ULAPIStemmer *wrappedStemmer = [self->stemmers objectForKey:language];
    if (wrappedStemmer) {
        return wrappedStemmer.ulStemmer;
    }
    
    // Get the stemmer from the factory and cache it.
    ULError error;
    ULLanguage targetLanguage([language ULString]);
    
    ULList<ULDictionaryDataSource *> dictionaryList;
    ULList<ULLanguageDataSource *> ldsList;
    ULDictionaryDataSource *dictionaryDataSource = 0;
    ULLanguageDataSource *languageDataSource = 0;
    error = self->factory->getDictionaryDataSources(targetLanguage, ULLanguage::English, dictionaryList);
    if (error == ULError::NoError && dictionaryList.length() > 0) {
        dictionaryDataSource = dictionaryList[0];
        error = self->factory->getLanguageDataSources(targetLanguage, ldsList);
        if (error == ULError::NoError && ldsList.length() > 0) {
            languageDataSource = ldsList[0];
        }
    }
    
    ULStemmer *stemmer = 0;
    if (dictionaryDataSource != 0 && languageDataSource != 0) {
        stemmer = self->factory->getStemmer(languageDataSource, dictionaryDataSource);
    }
    
    if (stemmer != 0) {
        wrappedStemmer = [[ULAPIStemmer alloc] initWithULStemmer:stemmer];
        [self->stemmers setObject:wrappedStemmer forKey:language];
    }
    
    return stemmer;
}

- (ULConjugator *)conjugatorForLanguage:(NSString *)language {
    // Get the cached stemmer if it's there.
    ULAPIConjugator *wrappedConjugator = [self->conjugators objectForKey:language];
    if (wrappedConjugator) {
        return wrappedConjugator.ulConjugator;
    }
    
    // Get the stemmer from the factory and cache it.
    ULError error;
    ULLanguage targetLanguage([language ULString]);
    
    ULList<ULDictionaryDataSource *> dictionaryList;
    ULList<ULLanguageDataSource *> ldsList;
    ULDictionaryDataSource *dictionaryDataSource = 0;
    ULLanguageDataSource *languageDataSource = 0;
    error = self->factory->getDictionaryDataSources(targetLanguage, ULLanguage::English, dictionaryList);
    if (error == ULError::NoError && dictionaryList.length() > 0) {
        dictionaryDataSource = dictionaryList[0];
        error = self->factory->getLanguageDataSources(targetLanguage, ldsList);
        if (error == ULError::NoError && ldsList.length() > 0) {
            languageDataSource = ldsList[0];
        }
    }
    
    ULConjugator *conjugator = 0;
    if (dictionaryDataSource != 0 && languageDataSource != 0) {
        conjugator = self->factory->getConjugator(languageDataSource, dictionaryDataSource);
    }
    
    if (conjugator != 0) {
        wrappedConjugator = [[ULAPIConjugator alloc] initWithULConjugator:conjugator];
        [self->conjugators setObject:wrappedConjugator forKey:language];
    }
    
    return conjugator;
}

- (NSArray *)stems:(NSString *)word language:(NSString *)language {
    NSMutableArray *stems = [[NSMutableArray alloc] init];
    
    ULStemmer *stemmer = [self stemmerForLanguage:language];
    if (stemmer != 0) {
        ULList<ULDerivation> stemList;
        ULListIterator<ULDerivation> stemIterator;
        stemmer->getStems([word ULString], stemList);
        for (stemIterator = stemList.begin(); stemIterator; ++stemIterator) {
            [stems addObject:[[ULAPIDerivation alloc] initWithULDerivation:*stemIterator]];
        }
    }
    
    return stems;
}

- (NSArray *)conjugations:(NSString *)word language:(NSString *)language {
    NSMutableArray *conjugations = [[NSMutableArray alloc] init];
    
    ULConjugator *conjugator = [self conjugatorForLanguage:language];
    if (conjugator != 0) {
        ULList<ULDerivation> verbList;
        ULListIterator<ULDerivation> verbIterator;
        ULList<ULDerivation> conjugationList;
        ULListIterator<ULDerivation> conjugationIterator;
        
        // For each verb matching the given verb form...
        conjugator->getVerbsMatchingVerbForm([word ULString], verbList);
        for (verbIterator = verbList.begin(); verbIterator; ++verbIterator) {
            conjugator->getAllConjugations(*verbIterator, conjugationList);
            for (conjugationIterator = conjugationList.begin(); conjugationIterator; ++conjugationIterator) {
                [conjugations addObject:[[ULAPIDerivation alloc] initWithULDerivation:*conjugationIterator]];
            }
        }
    }
    
    return conjugations;
}

- (NSString *)displayTextForConjugation:(ULAPIDerivation *)conjugation {
    NSString *language = [NSString stringWithUTF8String:conjugation.ulDerivation.getLanguage().getTwoLetterISOCode()];
    ULConjugator *conjugator = [self conjugatorForLanguage:language];
    if (conjugator == 0) {
        return @"";
    }
    
    return [NSString stringWithULString:conjugator->getConjugationDisplayText(conjugation.ulDerivation)];
}

- (NSString *)tenseName:(NSString *)tense language:(NSString *)language {
    NSString *tenseName = @"";
    ULConjugator *conjugator = [self conjugatorForLanguage:language];
    ULTense ulTense = ULTense([tense ULString]);
    if (conjugator != 0) {
        ULInflector *inflector = conjugator->getInflector();
        if (inflector != 0) {
            ULDictionaryDataSource *dataSource = inflector->getDictionaryDataSource();
            if (dataSource != 0) {
                ULForester *forester = dataSource->getForester();
                if (forester != 0) {
                    ULString localization;
                    ULError error = forester->getLocalizationForFeatureValue(ulTense.getStringID(), conjugator->getLanguage(), localization);
                    if (error == ULError::NoError) {
                        tenseName = [NSString stringWithULString:localization];
                    }
                }
            }
        }
    }
    
    return tenseName;
}

@end

#pragma mark - ULAPIPartOfSpeech
@interface ULAPIPartOfSpeech() {
    ULPartOfSpeech _ulPartOfSpeech;
}
@end

@implementation ULAPIPartOfSpeech

- (NSString *)partOfSpeechCategory {
    return [NSString stringWithUTF8String:_ulPartOfSpeech.getPartOfSpeechCategory().getStringID()];
}

- (NSDictionary *)features {
    NSDictionary *features = [[NSMutableDictionary alloc] init];
    ULListConstIterator<ULFeature> featureIterator;
    for (featureIterator = _ulPartOfSpeech.getFeatureList().cbegin(); featureIterator; ++featureIterator) {
        NSString *featureType = [NSString stringWithUTF8String:(*featureIterator).getFeatureType().getStringID()];
        NSString *featureValue = [NSString stringWithULString:(*featureIterator).getDisplayValue()];
        [features setValue:featureValue forKey:featureType];
    }
    return features;
}

- (NSString *)tense {
    return [NSString stringWithUTF8String:_ulPartOfSpeech.getTense().getStringID()];
}

- (NSString *)person {
    return [NSString stringWithUTF8String:_ulPartOfSpeech.getPerson().getStringID()];
}

- (NSString *)number {
    return [NSString stringWithUTF8String:_ulPartOfSpeech.getNumber().getStringID()];
}

- (NSString *)aspect {
    return [NSString stringWithUTF8String:_ulPartOfSpeech.getAspect().getStringID()];
}

@end

@implementation ULAPIPartOfSpeech(CPP)

- (id)initWithULPartOfSpeech:(const ULPartOfSpeech&)partOfSpeech {
    if (self) {
        _ulPartOfSpeech = partOfSpeech;
    }
    
    return self;
}

- (const ULPartOfSpeech&)ulPartOfSpeech {
    return _ulPartOfSpeech;
}

@end

#pragma mark - ULAPIDerivation
@interface ULAPIDerivation() {
    ULDerivation _ulDerivation;
}
@end

@implementation ULAPIDerivation
- (NSString *)root {
    return [NSString stringWithULString:_ulDerivation.getRoot()];
}

- (ULAPIPartOfSpeech *)rootPartOfSpeech {
    return [[ULAPIPartOfSpeech alloc] initWithULPartOfSpeech:_ulDerivation.getRootPartOfSpeech()];
}

- (NSDictionary *)rootFeatures {
    NSDictionary *features = [[NSMutableDictionary alloc] init];
    ULListConstIterator<ULFeature> featureIterator;
    for (featureIterator = _ulDerivation.getRootFeatureList().cbegin(); featureIterator; ++featureIterator) {
        NSString *featureType = [NSString stringWithUTF8String:(*featureIterator).getFeatureType().getStringID()];
        NSString *featureValue = [NSString stringWithULString:(*featureIterator).getDisplayValue()];
        [features setValue:featureValue forKey:featureType];
    }
    return features;
}

- (NSString *)derivedForm {
    return [NSString stringWithULString:_ulDerivation.getDerivedForm()];
}

- (ULAPIPartOfSpeech *)derivedFormPartOfSpeech {
    return [[ULAPIPartOfSpeech alloc] initWithULPartOfSpeech:_ulDerivation.getDerivedFormPartOfSpeech()];
}

@end

@implementation ULAPIDerivation(CPP)
- (id)initWithULDerivation:(const ULDerivation&)derivation {
    if (self) {
        _ulDerivation = derivation;
    }
    return self;
}

- (const ULDerivation&)ulDerivation {
    return _ulDerivation;
}

@end

#pragma mark - ULAPIStemmer

@implementation ULAPIStemmer
- (id)initWithULStemmer:(ULStemmer *)stemmer {
    if (self) {
        _ulStemmer = stemmer;
    }
    return self;
}
@end


#pragma mark - ULAPIConjugator

@implementation ULAPIConjugator
- (id)initWithULConjugator:(ULConjugator *)conjugator {
    if (self) {
        _ulConjugator = conjugator;
    }
    return self;
}

@end


