//
//  ULAPI+CPP.h
//  Reader
//
//  Created by Jeffrey Ondich on 12/7/14.
//  Copyright (c) 2014 Ultralingua, Inc. All rights reserved.
//

#define UL_USING_LEGACY_STRINGS
#define UL_ANSI

#import "ulapi/ulapi.h"

@interface ULAPIPartOfSpeech (CPP)
- (id)initWithULPartOfSpeech:(const ULPartOfSpeech&)partOfSpeech;
@property (readonly) const ULPartOfSpeech& ulPartOfSpeech;
@end

@interface ULAPIDerivation (CPP)
- (id)initWithULDerivation:(const ULDerivation&)derivation;
@property (readonly) const ULDerivation& ulDerivation;
@end

@interface ULAPIStemmer : NSObject
- (id)initWithULStemmer:(ULStemmer *)stemmer;
@property (readonly) ULStemmer *ulStemmer;
@end

@interface ULAPIConjugator : NSObject
- (id)initWithULConjugator:(ULConjugator *)conjugator;
@property (readonly) ULConjugator *ulConjugator;
@end
