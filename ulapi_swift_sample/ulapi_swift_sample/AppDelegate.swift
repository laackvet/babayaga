//
//  AppDelegate.swift
//  ulapi_swift_sample
//
//  Created by Jeffrey Ondich on 4/30/15.
//  Copyright (c) 2015 Jeff Ondich. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Set up ULAPI factory
        self.initializeULAPI()
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: - ULAPI
    func initializeULAPI() {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0] as! String // A place to look
        let mainBundlePath = NSBundle.mainBundle().resourcePath
        self.addULAPIFilesFromDirectory(mainBundlePath!)
    }
    
    func addULAPIFilesFromDirectory(directoryPath: String) {
        let fileManager = NSFileManager.defaultManager()
        let enumerator = fileManager.enumeratorAtPath(directoryPath)
        if enumerator != nil {
            let ulapi : ULAPI = ULAPI.sharedULAPI()
            while let element = enumerator!.nextObject() as? String {
                if element.hasSuffix("uld") {
                    if !ulapi.addDictionaryDataSource("\(directoryPath)/\(element)") {
                        // TODO: do whatever we need to do to indicate this is not available
                    } else {
                        println("Added \(element)")
                    }
                } else if element.hasSuffix("ulc") {
                    if !ulapi.addLanguageDataSource("\(directoryPath)/\(element)") {
                        // TODO: do whatever we need to do to indicate this is not available
                    } else {
                        println("Added \(element)")
                    }
                }
            }
        }
    }
    
}

