Swift sample for ULAPI
Copyright (c) 2015 Ultralingua, Inc.

To use this sample:

-- Copy ulapi.framework into ulapi_swift_sample/ (or make a ulapi.framework link there pointing to your copy of ulapi.framework).
-- Open ulapi_swift_sample/ulapi_swift_sample.xcodeproj in Xcode
-- Add whichever .ulc and .uld files you wish to use with ulapi_swift_sample to the "data" folder in the project navigator.

Note that this early version of the sample has hard-coded "es", "en", and "fr" in some of its method calls. For those calls to produce results, you'll want to have the corresponding .ulc and .uld files included in the project's data folder.

