readme.txt

Babayaga: Conjugation of Russian Verbs

Client: Laura Goering

Team: Gustav Danielsson, Thor Laack-Veeder, Sarah LeBlanc

Project Description:  This project was created to help students in Introductory Russian learn to conjugate Russian verbs.  There are three modes to help the students study.  In Study mode the student can find an individual word and see information about the verb.  For each verb, the translation, aspect, stem, present, past and imperative forms are displayed.  Users may choose words through a list of the vocabulary in alphabetical order, or go through the chapters and sections to find a vocabulary word.  There is also a Practice mode, which is just flashcards.  In the flash cards, the English form of the verb is shown and the user can either go to the next vocabulary word or choose to see the Russian conjugation of the verb.  Once the Russian form is shown, the user can choose to go to the next English form or see the English form of the current vocabulary word.  The final part of the app is the Test mode.  When in Test, the user is shown a verb in English similar to the flashcard.  However, in this mode, the user is presented with the correct pronoun and then must type in the rest of the verb.  After pressing enter, the user is taken to a screen that tells the user if they are correct or not, shows the translation, as well as displaying their answer vs the correct answer.  The user can then choose to move to the next word.  For both the practice and the test modes, the user also has access to a Settings menu, where they can determine which words to be tested on.  The user has the ability to turn entire chapters, sections, or individual words on and off.

Instructions for future developers:  

Changing the csv file: If in the future a new csv file needs to be read in, add the csv file to the most internal babayaga folder, and then in AppDelegate.swift, change "verbsAlphabatized" to the name of your csv file in the methods: makeChapterDictionary, makeSectionDictionary, makeWordDictionary and makeColumnDictionary.  The main dictionary is created in makeWordDictionary, and the csv reader uses the column headers.  They are hard coded into the top of the AppDelegate as global variables.  They are also hardcoded into makeChapterDictionary as "ch" as the column header for the column with the chapter.  

Implementing weighted randomness:  One feature we were hoping to get to was having weighted randomness in choosing practice and test cards - as the user got them correct, the vocabulary word would show up less often.  Each word has a frequency value in the dictionary (in place 6 in the tuple), so as a user would answer the test correctly, this frequency should be decreased, and choosing a random word would need to take this frequency into consideration.

Implementing sound:  We were unable to receive the sound files from the Russian department, but ULAPI has sound generation, so adding a play of the Russian pronunciation every time the flashcard is turned over would help the user hear the verbs over and over.  Turning this feature on or off could also be taken care of in the Settings menu.

Implementing scoring function for test:  Allowing users to see their improvement would be a useful studying method.  One way to go about doing this would be to implement a score for a test session.  In the Settings menu, the student could specify how many words they want to be tested on.  Then, the test mode could choose ten words and keep track of how many words the user got correct.  After the user tries all ten words, the app could show them their score and offer the user to go again.  This could help the user to see how they are improving.

Allowing users to add words:  Adding words actually wouldn't be too much of a hassle.  The reason we did not make it a priority was because we were given all the words the introductory students would need based on their text book.  Having an option to allow users to input their own word would only need a screen with text fields for all the value entries needed for the values of the wordDictionary.  After gathering the data from all the text fields, the backend would put the information together in a tuple like the ones made in AppDelegate in makeWordDictionary and add the key value pair to the wordDictionary.


